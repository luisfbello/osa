<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicioAlClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitud Liquidaciones Pendientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-al-cliente-index">

    <div class="card">
            <div class="card-header bg-danger text-white">
                <center><h3><?= Html::encode($this->title) ?></h3></center>
            </div>
            <div class="card-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pager' => [
                      
                      
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'servicio_al_cliente_id',
                        'servicio_al_cliente_nombre_usuario',
                        'servicio_al_cliente_apellidos_usuario',
                        //'servicio_al_cliente_tipoIdentificacion',
                        'servicio_al_cliente_numero_documento',
                        // 'servicio_al_cliente_razonsocial',
                        // 'servicio_al_cliente_municipio',
                        // 'servicio_al_cliente_departamento',
                        // 'servicio_al_cliente_zona',
                        // 'servicio_al_cliente_direccion',
                        // 'servicio_al_cliente_nit',
                        // 'servicio_al_cliente_telefono1',
                        // 'servicio_al_cliente_telefono2',
                        // 'servicio_al_cliente_email:email',
                        // 'servicio_al_cliente_observacion:ntext',
                        // 'servicio_al_cliente_tipo_solicitud',
                        // 'servicio_al_cliente_fecha_solicitud',
                        // 'servicio_al_cliente_estado_solicitud',
                        // 'servicio_al_cliente_operario_gestion',
                        // 'servicio_al_cliente_fecha_gestion',
                        // 'servicio_al_cliente_observacion_gestion:ntext',

                        
                        [
                          'class' => 'yii\grid\ActionColumn',
                          'template' => '{view}{update}{check}{close}',
                          'buttons' => [
                              // ====== Button ver comprobante
                                  'view' => function ($url, $model) {
                                      
                                      return  '<center><a href="#ModalVer" data-toggle="modal" class="fa fa-eye ver" id="'.$model->servicio_al_cliente_id.'" value="'.$model->servicio_al_cliente_id.'" title="Ver Solicitud"></a></center>' ;
                                  },
                                  // ====== Button Editar comprobante
                                  'update' => function ($url, $model) {
                                    
                                      return  '<center><a href="#ModalGestion" data-toggle="modal" class="fa fa-align-justify editar" id="'.$model->servicio_al_cliente_id.'" value="'.$model->servicio_al_cliente_id.'" title="Agregar Observacion"></a></center>' ;
                                  },
                                      // ====== Button check comprobante
                                     'check' => function ($url, $model) {
                                       if (Yii::$app->user->identity->id==2) {
                                        if($model->servicio_al_cliente_check==0){
                                          return  '<center><a href="#ModalAprobargestion" data-toggle="modal" class="fa fa-check aprobar" id="'.$model->servicio_al_cliente_id.'" value="'.$model->servicio_al_cliente_id.'" title="Aprobar Para Gestion"></a></center>' ;
                                         }else{
                                          return  '<center><a data-toggle="modal" class="fa fa-clock" title="Aprobado Para Gestion en Zona" disabled></a></center>' ;
                                         }
                                       }
                                  },
                                   // ====== Button cerrar comprobante
                                   'close' => function ($url, $model) {
                                            
                                    return  '<center><a href="#ModalCerrargestion" data-toggle="modal" class="fa fa-thumbs-up cerrar" id="'.$model->servicio_al_cliente_id.'" value="'.$model->servicio_al_cliente_id.'" title="Cerrar Solicitud"></a></center>' ;
                                },    
                              
                              ],
                        ],
                    ],
                    
                ]); ?>

            </div>
            <?php echo html::a('Volver',['servicio-al-cliente/solicitudesliquidacionindex','idsolicitud' => $idsolicitud],['class' => 'btn btn-block btn-info','style'=>'margin-top: 30px']) ?>

    </div>

</div>

<!-- MODAL DE LA VISTA SOLICITUD -->
<div class="modal fade" id="ModalVer">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
    
        <div class="modal-header text-white bg-danger">
        <h4 class="modal-title" id="numticket"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        
        <div class="modal-body">
                <div class="contenido">

                </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>


</div>

<!-- MODAL DE LA GESTION SOLICITUD -->
<div class="modal fade" id="ModalGestion">
    <div class="modal-dialog modal-mg">
      <div class="modal-content">
      
    
        <div class="modal-header text-white bg-success">
          <h4 class="modal-title">Gestion de Liquidaciones Pendientes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        
        <div class="modal-body">
            <div class="container">
                <form action="">
                    <div class="form-group">
                        <label>Observacion Gestion</label>
                        
                        <textarea class="form-control" id="observaciongestion" cols="20" rows="5" require></textarea>
                        
                    </div>
                </form>
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            <input type="hidden" class="ideditar" >
            <button type="button" class="btn btn-success" id="guardargestion">Guardar Gestion</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>


</div>

<!-- MODAL DE LA CERRAR SOLICITUD -->
<div class="modal fade" id="ModalCerrargestion">
    <div class="modal-dialog modal-mg">
      <div class="modal-content">
      
    
        <div class="modal-header text-white bg-success">
          <h4 class="modal-title">Gestion de Liquidaciones Pendientes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        
        <div class="modal-body">
            <div class="container">
                <form action="">
                    <div class="form-group">
                        <label>Cerrar Gestion</label>
                        
                        <h5>Esta Seguro de Cerrar la gestion</h5>
                        <textarea class="form-control" id="observacioncerrargestion" cols="20" rows="5" require placeholder="Ingrese la observacion"></textarea>
                        
                    </div>
                </form>
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        <input type="hidden" class="idcerrar" >
            <button type="button" class="btn btn-success" id="cerrargestion">Cerrar Gestion</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>


</div>

<!-- MODAL DE LA APROBAR SOLICITUD -->
<div class="modal fade" id="ModalAprobargestion">
    <div class="modal-dialog modal-mg">
      <div class="modal-content">
      
    
        <div class="modal-header text-white bg-success">
          <h4 class="modal-title">Gestion de Liquidaciones Pendientes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        
        <div class="modal-body">
            <div class="container">
                <form action="">
                    <div class="form-group">
                        <label>Aprobar Gestion</label>            
                        <h5>Esta Seguro de Aprobar la gestion para ser Gestionada en la Zona</h5>
                        <textarea class="form-control" id="observacionaprobargestion" cols="20" rows="5" require placeholder="Ingrese la observacion"></textarea>
                        
                    </div>
                </form>
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        <input type="hidden" class="idaprobar" >
            <button type="button" class="btn btn-success" id="Aprobargestion">Aprobar Gestion</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>


</div>

<!-- MODAL DE LA RESPUESTA SOLICITUD -->
<div class="modal fade" id="ModalRespuesta">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
    
        <div class="modal-header text-white bg-success">
          <h4 class="modal-title">Gestion de Liquidaciones Pendientes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        
        <div class="modal-body">
                <div class="respuesta">

                </div>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-danger salir" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>


</div>

 

<script>
// Ver la Gestion
$('.ver').click(function(){
  var id=$(this).attr('value');
  $('#numticket').text("Nº TICKET: "+id);
    $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=servicio-al-cliente/view',
                type: 'get',
                data: {
                    id : id
                },
                success: function (data) {
                    $('.contenido').html(data);
                }
            }); // fin ajax
    
});

// Guardar Gestion
$('.editar').click(function(){
  var ideditar=$(this).attr('id');
  $('.ideditar').attr('value',ideditar);
  
    
});

$('#guardargestion').click(function(){
  var idt=$('.ideditar').attr('value');
  if ($('#observaciongestion').val() != "") {
    $.ajax({
              url: '<?php echo Yii::$app->request->baseUrl ?>/?r=servicio-al-cliente/update',
              type: 'get',
              data: {
                  id : idt,
                  observacion: $('#observaciongestion').val(),
              },
              success: function (data) {
                  $('#ModalGestion').modal('hide');
                  $('.respuesta').html(data);
                  $('#ModalRespuesta').modal('show');
                  
              }
          }); // fin ajax
  }else{
    alert("El Campor de Observacion no debe estar vacio");
  }
  
});

// Cerrar Gestion
$('.cerrar').click(function(){
  var idcerrar=$(this).attr('id');
  $('.idcerrar').attr('value',idcerrar);
  
    
});

$('#cerrargestion').click(function(){
  var idc=$('.idcerrar').attr('value');    
  if ($('#observacioncerrargestion').val() != "") {
    $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=servicio-al-cliente/cerrar',
                type: 'get',
                data: {
                    id : idc,
                    observacion: $('#observacioncerrargestion').val(),
                },
                success: function (data) {
                    $('#ModalCerrargestion').modal('hide');
                    $('.respuesta').html(data);
                    $('#ModalRespuesta').modal('show');
                    
                }
            }); // fin ajax
  }else{
    alert("El Campor de Observacion no debe estar vacio");
  }
    
    
});

// Aprobar Gestion
$('.aprobar').click(function(){
  var idaprobar=$(this).attr('id');
  $('.idaprobar').attr('value',idaprobar);

});

$('#Aprobargestion').click(function(){
    
  var idap=$('.idaprobar').attr('value');
  if($('#observacionaprobargestion').val() != ""){
    $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=servicio-al-cliente/aprobar',
                type: 'get',
                data: {
                    id : idap,
                    observacion: $('#observacionaprobargestion').val(),
                },
                success: function (data) {
                    $('#ModalAprobargestion').modal('hide');
                    $('.respuesta').html(data);
                    $('#ModalRespuesta').modal('show');
                    
                }
            }); // fin ajax
  }else{
    alert("El Campor de Observacion no debe estar vacio");
  }  
    
    
});


$('.salir').click(function(){
    location.reload();
});
</script>