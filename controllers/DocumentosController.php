<?php

namespace app\controllers;

use Yii;
use app\models\Documentos;
use app\models\DocumentosSearch;
use app\models\Carpetas;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\db\Query;


/**
 * DocumentosController implements the CRUD actions for Documentos model.
 */
class DocumentosController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Documentos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionVer()
    {

        // echo "<pre>";
        // print_r($_REQUEST);die;
        $query = new Query();
        $connection = Yii::$app->db;

        $model = new Documentos();

        $query = $connection->createCommand('SELECT * from procesos where proceso_id = ' . $_REQUEST['proceso'] . ' ');
        $tituloProceso = $query->queryAll();

        $query = $connection->createCommand('SELECT * from carpetas where carpeta_id = ' . $_REQUEST['carpeta'] . ' and carpeta_depende_id is null');
        $tituloCarpeta = $query->queryAll();

        $query = $connection->createCommand('SELECT * from carpetas where carpeta_depende_id = ' . $_REQUEST['carpeta'] . ' and carpeta_depende_id is not null ');
        $depende = $query->queryAll();

        // echo "<pre>";
        // print_r($depende);die;


        $query = $connection->createCommand('SELECT * from documentos where documento_carpeta_id = ' . $_REQUEST['carpeta'] . ' and documento_proceso_id = ' . $_REQUEST['proceso'] . ' ');
        $result = $query->queryAll();

        return $this->render('ver', [
            'archivos' => $result,
            'carpeta' => $_REQUEST['carpeta'],
            'proceso' => $_REQUEST['proceso'],
            'model' => $model,
            'tituloProceso' => $tituloProceso,
            'tituloCarpeta' => $tituloCarpeta,
            'depende' => $depende,
        ]);
    }

    public function actionVerdepende()
    {

        // echo "<pre>";
        // print_r($_REQUEST);
        // die;

        $query = new Query();
        $query1 = new Query();
        $query2 = new Query();
        $query3 = new Query();
        $query4 = new Query();
        $connection = Yii::$app->db;
        $model = new Documentos();

        $query = $connection->createCommand('SELECT *
                                                  from documentos 
                                                 where documento_carpeta_id = ' . $_REQUEST['carpeta'] . '  ');
        $documentosDepende = $query->queryAll();

        $query1 = $connection->createCommand('select * from procesos where proceso_id = ' . $_REQUEST['proceso'] . ' ');
        $procesonombre = $query1->queryAll();

        $query2 = $connection->createCommand('select * from carpetas where carpeta_id = ' . $_REQUEST['depende'] . '');
        $dependenombre = $query2->queryAll();

        $query4 = $connection->createCommand('select * from carpetas where carpeta_depende_id=' . $_REQUEST['carpeta']);
        $carpetadepende=$query4->queryAll();

        $query3 = $connection->createCommand('select * from carpetas where carpeta_id = ' . $_REQUEST['carpeta'] . '');
        $final = $query3->queryAll();

        // echo "<pre>";
        // print_r($carpetadepende);
        // print_r($dependenombre);
        // print_r($procesonombre);
        // print_r($final);
        // die();

        return $this->render('verdepende', [
            'documentos' => $documentosDepende,
            'carpetadepende'=>$carpetadepende,
            'proceso' => $procesonombre,
            'depende' => $dependenombre,
            'final' => $final,
            'model' => $model,
        ]);
    }



    public function actionVersubdepende()
    {



        $query = new Query();
        $connection = Yii::$app->db;
        $model = new Documentos();

        $query = $connection->createCommand('SELECT *
                                                  from documentos 
                                                 where documento_carpeta_id = ' . $_REQUEST['carpeta'] . '  ');
        $documentosDepende = $query->queryAll();

        $query = $connection->createCommand('select * from procesos where proceso_id = ' . $_REQUEST['proceso'] . ' ');
        $procesonombre = $query->queryAll();

        $query = $connection->createCommand('select * from carpetas where carpeta_id = ' . $_REQUEST['depende'] . '');
        $dependenombre = $query->queryAll();

        $query = $connection->createCommand('select * from carpetas where carpeta_id = ' . $_REQUEST['carpeta'] . '');
        $final = $query->queryAll();

        // echo "<pre>";
        // print_r($dependenombre);
        // print_r($procesonombre);
        // die();
        return $this->render('versubdepende', [
            'documentos' => $documentosDepende,
            'proceso' => $procesonombre,
            'depende' => $dependenombre,
            'final' => $final,
            'model' => $model,
        ]);
    }

    public function actionAuditoria()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id and carpeta_depende_id is null)
                                            where   proceso_id = 1
                                            order by  carpeta_id');
        $result = $query->queryAll();

        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 1
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();

        // echo "<pre>";
        // print_r($result);die;

        return $this->render('indexauditoria', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,
        ]);
    }
    public function actionContabilidad()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id)
                                            where   proceso_id = 2
                                            order by  carpeta_id');
        $result = $query->queryAll();

        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 2
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();

        return $this->render('indexcontabilidad', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,
        ]);
    }
    public function actionDireccionejecutiva()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id)
                                            where   proceso_id = 3
                                            order by  carpeta_id');
        $result = $query->queryAll();
        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 3
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();

        return $this->render('indexdireccionejecutiva', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,

        ]);
    }
    public function actionGestionhumana()
    {

        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id and carpeta_depende_id is null)
                                            where   proceso_id = 4
                                            order by  carpeta_id');
        $result = $query->queryAll();
        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 4
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();

        return $this->render('indexgestionhumana', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,

        ]);
    }
    public function actionJuridica()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id)
                                            where   proceso_id = 5
                                            order by  carpeta_id');
        $result = $query->queryAll();

        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 5
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();

        return $this->render('indexjuridica', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,

        ]);
    }
    public function actionRecaudo()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id and carpeta_depende_id is null)                                                    
                                            where   proceso_id = 6
                                            order by  carpeta_id');
        $result = $query->queryAll();

        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 6
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();

        return $this->render('indexrecaudo', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,

        ]);
    }
    public function actionTecnologia()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id)
                                            where   proceso_id = 7
                                            order by  carpeta_id');
        $result = $query->queryAll();

        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 7
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();

        return $this->render('indextecnologia', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,

        ]);
    }
    public function actionTesoreria()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id)
                                            where   proceso_id = 8
                                            order by  carpeta_id');
        $result = $query->queryAll();
        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 8
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();

        return $this->render('indextesoreria', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,

        ]);
    }
    public function actionCalidad()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id)
                                            where   proceso_id = 9
                                            order by  carpeta_id');
        $result = $query->queryAll();

        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 9
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();

        return $this->render('indexcalidad', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,

        ]);
    }

    public function actionSgsst()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id and carpeta_depende_id is null)                                                    
                                            where   proceso_id = 11
                                            order by  carpeta_id');
        $result = $query->queryAll();

        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 11
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();


        // echo "<pre>";
        // print_r($result);
        // die();
        return $this->render('indexsgsst', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,
        ]);
    }

    public function actionInfosgc()
    {
        $model = new Documentos();
        $searchModel = new DocumentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *, 
                                                    (select count(*) from documentos where documento_carpeta_id = carpeta_id and documento_proceso_id = proceso_id) as documentos
                                            from    procesos
                                                    join carpetas on (proceso_id = carpeta_proceso_id and carpeta_depende_id is null)                                                    
                                            where   proceso_id = 10
                                            order by  carpeta_id');
        $result = $query->queryAll();

        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_proceso_id = 10
                                                    and documento_carpeta_id is null
                                            order by 1 asc ');
        $sinCarpeta = $query->queryAll();


        // echo "<pre>";
        // print_r($result);
        // die();
        return $this->render('indexinfosgc', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'result' => $result,
            'model' => $model,
            'archivos' => $sinCarpeta,
        ]);
    }
    

    
    public function actionAddfolder($idproceso, $nombrecarpeta, $iddepende)
    {

           
        $idproceso = $idproceso;
        $nombrecarpeta = $nombrecarpeta;
        $newCarpeta = new Carpetas();
        $newCarpeta->carpeta_proceso_id = $idproceso;
        $newCarpeta->carpeta_nombre = $nombrecarpeta;
        if ($iddepende != 0) {
            $newCarpeta->carpeta_depende_id = $iddepende;
        }


        if ($newCarpeta->save()) {
            echo "Se Genero la Carpeta ";
        } else {
            print_r($newCarpeta->getErrors());
        }
    }

    public function actionCarga()
    {

        // echo "<pre>";
        // print_r($_REQUEST);die;
       
        $query = new Query();
        $connection = Yii::$app->db;

        if (isset($_REQUEST['carpeta']) != null) {
            $carpeta = $_REQUEST['carpeta'];
        } else {
            $carpeta = null;
        }

        $model = new Documentos();
        $model->documento_nombre = UploadedFile::getInstances($model, 'documento_nombre');

        // echo "<pre>";
        // print_r($model);
        // die();


        ############################################
        # CREACION DE LAS IMAGENES DEL DOCUMENTO
        ############################################
        if ($model->documento_nombre) {
            $contador = 0;
            foreach ($model->documento_nombre as $file) {

                $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
                $modificadas = 'AAAAAAACEEEEIIIIDNOOOOOOUUUUYbsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

                $nombreGuardar = utf8_decode($file->name);
                $nombreGuardar = strtr($nombreGuardar, utf8_decode($originales), $modificadas);
                // $nombreGuardar=strtolower($nombreGuardar);
                $sql = $connection->createCommand()->insert(
                    'documentos',
                    [
                        'documento_nombre' => $file->name,
                        'documento_proceso_id' => $_REQUEST['proceso'],
                        'documento_carpeta_id' => $carpeta,
                        // 'documento_ruta' => 'gestioncalidad/' . $nombreGuardar,
                        
                    ]
                )->execute();

                $insert_id = Yii::$app->db->getLastInsertID();
                $extencion=pathinfo($file->name,PATHINFO_EXTENSION);

                $archivo_ruta=$insert_id.".".$extencion;

                $update=$connection->createCommand()
                ->update('documentos',['documento_ruta'=>'gestioncalidad/'.$archivo_ruta],"documento_id =". $insert_id."")
                ->execute();
                // die();

                $file->saveAs('gestioncalidad/' . $archivo_ruta);
                
                $contador++;
            }
        }

        if (isset($_REQUEST['carpeta']) != null) {

            if (isset($_REQUEST['esdepende']) != null) {
                return $this->redirect(['verdepende', 'proceso' => $_REQUEST['proceso'], 'carpeta' => $_REQUEST['carpeta'], 'depende' => $_REQUEST['esdepende']]);
            } else {

                return $this->redirect(['ver', 'proceso' => $_REQUEST['proceso'], 'carpeta' => $carpeta]);
            }
            return $this->redirect(['ver', 'proceso' => $_REQUEST['proceso'], 'carpeta' => $carpeta]);
        } else {

            switch ($_REQUEST['proceso']) {
                case 1:
                    return $this->redirect(['auditoria']);
                    break;
                case 2:
                    return $this->redirect(['contabilidad']);
                    break;
                case 3:
                    return $this->redirect(['direccionejecutiva']);
                    break;
                case 4:
                    return $this->redirect(['gestionhumana']);
                    break;
                case 5:
                    return $this->redirect(['juridica']);
                    break;
                case 6:
                    return $this->redirect(['recaudo']);
                    break;
                case 7:
                    return $this->redirect(['tecnologia']);
                    break;
                case 8:
                    return $this->redirect(['tesoreria']);
                    break;
                case 9:
                    return $this->redirect(['calidad']);
                    break;
            }
        }
    }

    # aqui funcion para eliminar documento con el id en la tabla documento. 
    public function actionEliminardocumento($id)
    {

        // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_id =' . $id);
        $archivo = $query->queryAll();


        foreach ($archivo as $key => $value) {

            unlink($value['documento_ruta']);
        }

        $query = $connection->createCommand('DELETE
                                                from    documentos
                                                where   1=1
                                                        and documento_id = ' . $id);
        $result = $query->execute();
        echo $id;
    }

    public function actionEliminarfolder($id)
    {

        // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;

        $mensaje = '';

        $query = $connection->createCommand('SELECT *
                                            from carpetas
                                            where carpeta_depende_id=' . $id);
        $carpeta = $query->queryAll();

        // echo "<pre>";
        // print_r($query);


        $query = $connection->createCommand('SELECT *
                                            from    documentos
                                            where   documento_carpeta_id =' . $id);
        $archivo = $query->queryAll();

        if ($carpeta || $archivo) {
            $mensaje = "La carpeta tiene Contenido, No se puede eliminar";
        } else {
            $query = $connection->createCommand('DELETE
                                                from    carpetas
                                                where   1=1
                                                        and carpeta_id = ' . $id);
            $result = $query->execute();

            $mensaje = "Se elimino correctamente";
        }

        echo $mensaje;
    }
    /**
     * Displays a single Documentos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Documentos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Documentos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->documento_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Documentos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->documento_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Documentos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Documentos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Documentos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documentos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
