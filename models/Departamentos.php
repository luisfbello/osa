<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "departamentos".
 *
 * @property integer $idDepartamento
 * @property string $nombre
 * @property integer $codigo
 * @property string $fechaCreacion
 * @property integer $concepto
 * @property integer $contabilidad
 *
 * @property Municipios[] $municipios
 */
class Departamentos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'departamentos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDepartamento', 'nombre', 'codigo'], 'required'],
            [['idDepartamento', 'codigo', 'concepto', 'contabilidad'], 'integer'],
            [['fechaCreacion'], 'safe'],
            [['nombre'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDepartamento' => 'Id Departamento',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'fechaCreacion' => 'Fecha Creacion',
            'concepto' => 'Concepto',
            'contabilidad' => 'Contabilidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipios()
    {
        return $this->hasMany(Municipios::className(), ['departamento_iddepartamento' => 'idDepartamento']);
    }
}
