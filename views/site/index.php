<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'OsaIn';
?>
<div class="site-index">
    <?php $rolesPerfil = ''; ?>
    <?php $init = true; ?>
    <?php $idRolesPerfil = explode(',', $userIdentity->role); ?>
    <?php foreach ($roles as $r) { ?>
      <?php if (in_array($r->id, $idRolesPerfil)) { ?>
        <?php if ($init) { ?>
          <?php $rolesPerfil = $r->nombre; ?>
          <?php $init = false; ?>
        <?php }else{ ?>
          <?php $rolesPerfil = $rolesPerfil . '/' . $r->nombre; ?>
        <?php } ?>
      <?php } ?>
    <?php } ?>
    <div class="jumbotron">
        <h1><?php echo yii::$app->name ?> 1.0</h1>

        <p class="lead"><strong><?php echo $rolesPerfil; ?></strong></p>
        <img width="150" src="<?php echo Url::base(); ?>/images/fotos/<?php echo yii::$app->user->identity->foto; ?>" alt="..." class="img-circle">
        <p class="lead"><?php echo yii::$app->user->identity->nombres.' '.yii::$app->user->identity->apellidos; ?></p>
    </div>

    <div class="body-content">
        <div class="col-lg-12">
            <div class="col-lg-8 noPadding form-group col-lg-offset-2">
                <?php $accesosArray = array(); ?>
                <?php foreach ($accesosDirectos as $ac) { ?>
                    <?php if (!in_array($ac->etiqueta,$accesosArray)) { ?>
                        <div class="col-lg-4 form-group">
                            <?= Html::a( $ac->etiqueta, [$ac->action, 'id' => 12], ['class' => 'btn btn-'.$ac->color.' btn-block btn-lg']) ?>
                            <?php array_push($accesosArray, $ac->etiqueta); ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

