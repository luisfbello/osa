<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Carpetas */

$this->title = 'Create Carpetas';
$this->params['breadcrumbs'][] = ['label' => 'Carpetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carpetas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
