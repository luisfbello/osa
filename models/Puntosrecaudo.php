<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "puntosrecaudo".
 *
 * @property integer $idPuntoRecaudo
 * @property string $nombre
 * @property integer $sucursales_idsucursales
 * @property integer $localidades_idlocalidades
 * @property string $fechaCreacion
 *
 * @property Actas[] $actas
 * @property Comprobantes[] $comprobantes
 * @property Documentos[] $documentos
 * @property Listaconsecutivos[] $listaconsecutivos
 * @property Pagos[] $pagos
 * @property Localidades $localidadesIdlocalidades
 * @property Sucursales $sucursalesIdsucursales
 * @property SolicitudesVisita[] $solicitudesVisitas
 * @property Usuarios[] $usuarios
 */
class Puntosrecaudo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'puntosrecaudo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sucursales_idsucursales', 'localidades_idlocalidades'], 'required'],
            [['sucursales_idsucursales', 'localidades_idlocalidades'], 'integer'],
            [['fechaCreacion'], 'safe'],
            [['nombre'], 'string', 'max' => 45],
            [['localidades_idlocalidades'], 'exist', 'skipOnError' => true, 'targetClass' => Localidades::className(), 'targetAttribute' => ['localidades_idlocalidades' => 'idLocalidad']],
            [['sucursales_idsucursales'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursales::className(), 'targetAttribute' => ['sucursales_idsucursales' => 'idSucursal']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPuntoRecaudo' => 'Id Punto Recaudo',
            'nombre' => 'Nombre',
            'sucursales_idsucursales' => 'Sucursales Idsucursales',
            'localidades_idlocalidades' => 'Localidades Idlocalidades',
            'fechaCreacion' => 'Fecha Creacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActas()
    {
        return $this->hasMany(Actas::className(), ['puntosrecaudo_idpuntosrecaudo' => 'idPuntoRecaudo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComprobantes()
    {
        return $this->hasMany(Comprobantes::className(), ['puntosrecaudo_idpuntosrecaudo' => 'idPuntoRecaudo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documentos::className(), ['puntosrecaudo_idpuntosrecaudo' => 'idPuntoRecaudo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListaconsecutivos()
    {
        return $this->hasMany(Listaconsecutivos::className(), ['puntosrecaudo_idpuntosrecaudo' => 'idPuntoRecaudo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagos()
    {
        return $this->hasMany(Pagos::className(), ['puntosrecaudo_idpuntosrecaudo' => 'idPuntoRecaudo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidadesIdlocalidades()
    {
        return $this->hasOne(Localidades::className(), ['idLocalidad' => 'localidades_idlocalidades']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursalesIdsucursales()
    {
        return $this->hasOne(Sucursales::className(), ['idSucursal' => 'sucursales_idsucursales']);
    }

    public function getCentroscosto()
    {
        return $this->hasOne(CentrosCosto::className(), ['idcentrosCosto' => 'centroscosto_idcentroscosto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudesVisitas()
    {
        return $this->hasMany(SolicitudesVisita::className(), ['puntosrecaudo_idpuntosrecaudo' => 'idPuntoRecaudo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['puntosrecaudo_idpuntosrecaudo' => 'idPuntoRecaudo']);
    }

    public function getCentrocosto(){

        return $this->hasOne(CentrosCosto::className(),['idcentrosCosto' => 'centroscosto_idcentroscosto']);
    }

}
