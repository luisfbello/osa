<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ItemHasRol */

$this->title = 'Update Item Has Rol: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'menu', 'url' => ['opcionhasrol/index']];
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['itemmenu/index']];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="item-has-rol-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'rol' => $rol,
        'opcion' => $opcion,
    ]) ?>

</div>
