<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Carpetas */

$this->title = 'Update Carpetas: ' . ' ' . $model->carpeta_id;
$this->params['breadcrumbs'][] = ['label' => 'Carpetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->carpeta_id, 'url' => ['view', 'id' => $model->carpeta_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="carpetas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
