<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consultas_y_reclamos".
 *
 * @property integer $consulta_reclamo_id
 * @property string $consulta_reclamo_fecha
 * @property string $consulta_reclamo_nombres
 * @property string $consulta_reclamo_apellidos
 * @property integer $consulta_reclamo_identificacion
 * @property string $consulta_reclamo_nombre_establecimiento
 * @property string $consulta_reclamo_departamento
 * @property string $consulta_reclamo_ciudad
 * @property string $consulta_reclamo_direccion
 * @property integer $consulta_reclamo_nit
 * @property string $consulta_reclamo_telefono1
 * @property string $consulta_reclamo_telefono2
 * @property string $consulta_reclamo_email
 * @property string $consulta_reclamo_observacion
 */
class ConsultasYReclamos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consultas_y_reclamos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['consulta_reclamo_fecha'], 'safe'],
            [['consulta_reclamo_identificacion', 'consulta_reclamo_nit'], 'integer'],
            [['consulta_reclamo_observacion'], 'string'],
            [['consulta_reclamo_nombres', 'consulta_reclamo_apellidos', 'consulta_reclamo_nombre_establecimiento', 'consulta_reclamo_direccion', 'consulta_reclamo_email'], 'string', 'max' => 255],
            [['consulta_reclamo_departamento', 'consulta_reclamo_ciudad', 'consulta_reclamo_telefono1', 'consulta_reclamo_telefono2'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'consulta_reclamo_id' => 'Consulta Reclamo ID',
            'consulta_reclamo_fecha' => 'Consulta Reclamo Fecha',
            'consulta_reclamo_nombres' => 'Consulta Reclamo Nombres',
            'consulta_reclamo_apellidos' => 'Consulta Reclamo Apellidos',
            'consulta_reclamo_identificacion' => 'Consulta Reclamo Identificacion',
            'consulta_reclamo_nombre_establecimiento' => 'Consulta Reclamo Nombre Establecimiento',
            'consulta_reclamo_departamento' => 'Consulta Reclamo Departamento',
            'consulta_reclamo_ciudad' => 'Consulta Reclamo Ciudad',
            'consulta_reclamo_direccion' => 'Consulta Reclamo Direccion',
            'consulta_reclamo_nit' => 'Consulta Reclamo Nit',
            'consulta_reclamo_telefono1' => 'Consulta Reclamo Telefono1',
            'consulta_reclamo_telefono2' => 'Consulta Reclamo Telefono2',
            'consulta_reclamo_email' => 'Consulta Reclamo Email',
            'consulta_reclamo_observacion' => 'Consulta Reclamo Observacion',
        ];
    }
}
