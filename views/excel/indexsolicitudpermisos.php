<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;

$objPHPExcel = new PHPExcel(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");
	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$objPHPExcel->getActiveSheet()->getStyle("A1:AB1")->getFont()->setBold(true);
   	foreach(range('A','Z') as $columnID) {
   	    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	        ->setAutoSize(true);
   	}
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'FUNCIONARIO')
				->setCellValue('B1', 'JEFE INMEDIATO')
				->setCellValue('C1', 'MOTIVO')                	            
	            ->setCellValue('D1', 'APRUEBA JEFE')
	            ->setCellValue('E1', 'REVISADO NOMINA')
	            ->setCellValue('F1', 'FECHA INICIO')
				->setCellValue('G1', 'FECHA FIN')
				->setCellValue('H1', 'TOTAL DIAS')
				->setCellValue('I1', 'HORA INICIO')
				->setCellValue('J1', 'HORA FIN')
				->setCellValue('K1', 'TOTAL HORAS')
				->setCellValue('L1', 'FECHA CREACION')				
				->setCellValue('M1', 'OBSERVACION');				
                							
	$count = 2;
	foreach ($general as $value) {

		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $value['funcionario'])
		            ->setCellValue('B'.$count, $value['jefe_inmediato'])
		            ->setCellValue('C'.$count, $value['motivo'])
		            ->setCellValue('D'.$count, $value['aprueba_jefe'])
		            ->setCellValue('E'.$count, $value['Revisa_nomina'])
		            ->setCellValue('F'.$count, $value['fechaInicio'])
		            ->setCellValue('G'.$count, $value['fechaFin'])
					->setCellValue('H'.$count, $value['Total_dias'])
					->setCellValue('I'.$count, $value['horaInicio'])
					->setCellValue('J'.$count, $value['horaFin'])
					->setCellValue('K'.$count, $value['Total_horas'])
					->setCellValue('L'.$count, $value['Fcreacion'])
					->setCellValue('M'.$count, $value['observaciones']);                    
                    		         					
		$count++;
	}
	$objPHPExcel->getActiveSheet()->setTitle('REPORTE_SOLICITUDES_APROBADAS');
	$objPHPExcel->setActiveSheetIndex(0);
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="REPORTE_SOLICITUDES_APROBADAS'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

 ?>