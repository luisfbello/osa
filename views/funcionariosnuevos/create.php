<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Funcionariosnuevos */

$this->title = 'Crear Funcionarios Nuevos';
$this->params['breadcrumbs'][] = ['label' => 'Funcionarios nuevos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funcionariosnuevos-create">

        <div class="card">
            <div class="cards-header alert alert-primary">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="card-body">
                <?= $this->render('_form', [
                'model' => $model,
                ]) ?>
            </div>
        </div>

    

</div>
