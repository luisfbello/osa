<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Acciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="acciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombreAccion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear Accion' : 'Actualizar Accion', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
