<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicioAlClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Servicio al Cliente ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-al-cliente-index">


 <div class="card">
    <div class="card-header">
        <center>SOLICITUDES SERVICIO AL CLIENTE</center> 
    </div>
    <div class="card-body">
        <div class="row">
        <div class="col-lg-6">
            <div style="border: solid #ffbf4b; border-left: 10px solid #ffbf4b;border-radius:15px;">
                <center><h1 class="fa fa-exclamation-circle text-warning"></h1></center>
                <div class="container">
                    <center><h2>P.Q.R</h2></center>
                    <br>
                    <center><span class="badge"># <?php echo $pqr ?></span></center>
                    <br>
                    <?php echo html::a('Ver',['servicio-al-cliente/pqrindex', 'idsolicitud' => 139],['class' => 'btn btn-block btn-warning','style'=>'margin: 5px']) ?>
                    
                </div>
            </div>
        </div>
        <!-- <div class="col-lg-6">
        <div style="border: solid #337ab7; border-left: 10px solid #337ab7;border-radius:15px;">
                <center><h1 class="fa fa-file-pdf text-primary"></h1></center>
                <div class="container">
                    <center><h2>Solicitud Liquidaciones</h2></center>
                    <br>
                    <center><span class="badge"># <?php echo $liquidaciones ?></span></center>
                    <br>
                    <?php echo html::a('Ver',['servicio-al-cliente/solicitudesliquidacionindex', 'idsolicitud' => 138],['class' => 'btn btn-block btn-primary','style'=>'margin: 5px']) ?>
                </div>
            </div>
        </div>

        </div> -->
    </div>

 </div>   

    
    

    

</div>
