<div class="container-fluid">
<?php

use yii\widgets\DetailView;
use app\models\Users;
use app\models\Cargos;
use app\models\Puntosrecaudo;
use app\models\Tiposdetalles;
use app\models\Jefeinmediato;
use yii\helpers\Url; 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Municipios;
use app\models\Departamentos;
use yii\helpers\ArrayHelper;



/* @var $this yii\web\View */
/* @var $model app\models\solicitudpermisos */
//$this->title = $model->idsolicitudpermisos;
$this->title="Datos de solicitud";
$this->params['breadcrumbs'][] = ['label' => 'Solicitudpermisos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
// \yii\web\YiiAsset::register($this);

// echo '<pre>';
// print_r($model);
// die();



///////////Total dias/////////////////////////////////
$fecha_inicio = strtotime($model->fechaInicio); 
$fecha_fin = strtotime($model->fechaFin); 
$diff = $fecha_fin - $fecha_inicio; 
$total=round(($diff / 86400)+1). " dias";
$imagen=$model->incapacidad;

$hora_inicio = new Datetime ($model->horaInicio); 
$hora_fin = new Datetime ($model->horaFin); 
$interval = $hora_inicio->diff($hora_fin);
$interval->format('%H:%i:%s Horas');




?>
<div class="solicitudpermisos-view">  
    <h3><div class= "alert alert-info" role="alert"><?= Html::encode($this->title) ?></div></h3>
          
            <?php if ($model->estadoId==122 && $model->usuarioIdusuario==(Yii::$app->user->identity->id))
            {
            echo Html::a('Modificar', ['update', 'id' => $model->idsolicitudpermisos], ['class' => 'btn btn-primary']);
            }
            ?>

        <!-- ///////////////////////BOTON APROBAR//////////////////////////////////// -->
        
                           
         <!-- ///////////////////////BOTON RECHAZAR//////////////////////////////////// -->    
          
             <!-- ///////////////////////BOTON APROBAR NOMINA//////////////////////////////////// -->
        
            <?php if((Yii::$app->user->identity->id)==2 && $model->estadoId==123) {?>

            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#aprobar">
              aprobar
            </button>

            <div class="modal fade" id="aprobar" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">aprobar solicitud</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body containeraprobar" >
                          !Esta seguro de aprobar la solicitud¡
                        </div>
                        <div class="modal-footer">
                          <div class="form-group">
                              <div class="solicitudpermisos-form">
                              <?php $form = ActiveForm::begin(['action' => ['aprobarnom'], 'method' => 'get' ]); ?>                                
                              <input type="hidden" name="id" value="<?php echo $model->idsolicitudpermisos; ?>">                               
                              <div class="form-group">
                                <br>
                                  <button type="button" class="btn btn-warning" data-dismiss="modal">cerrar</button>
                                  <button type="submit" class="btn btn-success">aprobar</button>                           
                              </div>
                              <?php ActiveForm::end(); ?>
                          </div>
                        </div>                
                      </div>
                    </div>
                  </div>
                </div>                               
            <?php } ?>             

          <!-- //////////////////////////////////////IMPRIMIR PDF////////////////////////////////////////////////////// -->
          <!-- <?php if($model->usuarioIdusuario==(Yii::$app->user->identity->id) && $model->estadoId==13) {?>              
              <?= Html::a('imprimir', ['/pdf/pdf'], ['class'=>'btn btn-success']) ?>              
             
          <?php } ?>  -->    
  <!-- //////////////////////////////////////PANEL DATOS EMPLEADO////////////////////////////////////////////////////// -->                                                
    <p>    
        <div class="card border-primary mb-3">
            <div class="card-header text-white" style="background-color: #337AB7;">Datos del empleado</div>
                <div class="card-body">                                         
                    <div class="col-lg-12"> 
                            <div class="table-responsive">                                                    
                            <table class="table"> 
                                  <thead><tr>                                          
                                          <th scope="col">Nombres</th>
                                          <th scope="col">Apellidos</th>
                                          <th scope="col">Cedula</th>                                           
                                  </thead></tr>                                                        
                                  <tbody><tr>                                          
                                          <td> <?php echo $model->users->nombres; ?></td>                                                            
                                          <td> <?php echo $model->users->apellidos; ?></td>                                                                                   
                                          <td> <?php echo $model->users->numeroIdentificacion; ?></td>                                                                                                                                          
                                  </tbody></tr>
                                    <thead><tr>                                            
                                            <th scope="col">Telefono</th>
                                            <th scope="col">Cargo</th>
                                            <th scope="col">Sucursal</th>                                             
                                    </thead></tr>                                                               
                                    <tbody><tr>                                            
                                            <td> <?php echo $model->users->telefono; ?></td>                                                                                       
                                            <td> <?php echo $model->users->cargo->nombre; ?></td>
                                            <td> <?php echo $model->users->puntorecaudo->nombre; ?></td>
                                            </tr>                                                                                                                                                                                                                                  
                                    </tbody>
                            </table> 
                            </div> 
                    </div>
                </div>                                               
            </div>
               

<!-- //////////////////////////////////////INFORMACIÓN SOLICITUD////////////////////////////////////////////////////// -->          
      <div class="card border-primary mb-3">
            <div class="card-header text-white" style="background-color: #337AB7;">Información de solicitud</div>
                <div class="card-body">                                         
                    <div class="col-lg-12">                                                     
                            <div class="table-responsive">
                            <table class="table">
                                    <thead><tr>                                            
                                            <th scope="col">Motivo</th>
                                            <th scope="col">Jefe inmediato</th>
                                            <th scope="col">Fecha inicio</th>                                            
                                    </thead></tr>   
                                    <tbody><tr>                                            
                                            <td> <?php echo $model->tiposdetalles->nombre; ?></td>                                                           
                                            <td> <?php echo $model->jefeinmediato->nombres.''.$model->jefeinmediato->apellidos; ?></td>                                                                                   
                                            <td> <?php echo $model->fechaInicio; ?></td>                                                                                                                                            
                                    </tbody></tr>                                          
                                     <thead><tr>                                            
                                            <th scope="col">Total dias</th>
                                            <th scope="col">Cargo</th>
                                            <th scope="col">Fecha fin</th>                                             
                                    </thead></tr> 
                                    <tbody><tr>                                          
                                          <td> <?php echo $total; ?></td>
                                          <td> <?php echo $model->users->cargo->nombre; ?></td>                                                                                          
                                          <td> <?php echo $model->fechaFin; ?></td>                                                                                                                                                                                    
                                    </tbody></tr> 
                                    <thead><tr>  
                                          <?php if ($interval->format('%H:%i:%s Horas')>0) { ?>                                        
                                            <th scope="col">Hora inicio</th> 
                                            <th scope="col">Hora fin</th> 
                                            <th scope="col">Total horas</th> 
                                          <?php } ?>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                                    </thead></tr>
                                    <tbody><tr>
                                          <?php if ($interval->format('%H:%i:%s Horas')>0) { ?>                                        
                                            <td><?php echo $model->horaInicio?></td>
                                            <td><?php echo $model->horaFin?></td>
                                            <td><?php echo $interval->format('%H:%i:%s Horas') ?></td>
                                            <td></td>
                                          <?php } ?>                                                                                                                               
                                    </tbody></tr> 

                                    <thead><tr>                                                                                 
                                            <th scope="col">Observaciones</th>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                    </thead></tr>
                                    <tbody><tr>                                                                                     
                                          <td><?php echo $model->observaciones; ?></td>                                          
                                    </tbody></tr>                                    
                             </table>
                             </div>
                   </div>    
                </div>
                <?php if($model->motivoId==127){?>
                <div class="card-body">                                         
                    <div class="col-lg-12">                                                     
                            <div class="table-responsive">
                            <table class="table">
                                    <tbody><th scope="col">Incapacidad</th>                                   
                                    <tbody><tr>
                                           <td>                                            
                                           <a download="<?php echo $model->incapacidad?>" href="http:/35.175.13.101/OsaIntranet/web/images/<?php echo $model->incapacidad?>"> <img src="images/<?php echo $model->incapacidad?>"style="width: 700px; height: 600px;"> </a>
                                           </td>                                                                                                     
                                    </tr><tbody>
                             </table>
                             </div>
                   </div>    
                </div>
                <?php } ?> 
      </div>    
    </p>                       

<!--////////////////////////////////////////BOTON INICIO////////////////////////////////-->

<?= Html::a(' Inicio ', ['/solicitudpermisos/index'], ['class'=>'btn btn-primary']) ?> 
