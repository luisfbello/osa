<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AccesosDirectosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accesos-directos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idaccesos_directos') ?>

    <?= $form->field($model, 'etiqueta') ?>

    <?= $form->field($model, 'action') ?>

    <?= $form->field($model, 'color') ?>

    <?= $form->field($model, 'idRol') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
