<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Solicitudliquidacion */
/* @var $form ActiveForm */
?>
<div class="solicitud">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'solicitud_liquidacion_fecha') ?>
        <?= $form->field($model, 'solicitud_liquidacion_identificacion') ?>
        <?= $form->field($model, 'solicitud_liquidacion_nit') ?>
        <?= $form->field($model, 'solicitud_liquidacion_nombres') ?>
        <?= $form->field($model, 'solicitud_liquidacion_apellidos') ?>
        <?= $form->field($model, 'solicitud_liquidacion_nombre_establecimiento') ?>
        <?= $form->field($model, 'solicitud_liquidacion_direccion') ?>
        <?= $form->field($model, 'solicitud_liquidacion_email') ?>
        <?= $form->field($model, 'solicitud_liquidacion_departamento') ?>
        <?= $form->field($model, 'solicitud_liquidacion_telefono1') ?>
        <?= $form->field($model, 'solicitud_liquidacion_telefono2') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- solicitud -->
