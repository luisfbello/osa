<?php

use yii\helpers\Html;

$this->title = 'Boletines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<section>
    
        <div class="card-header alert alert-primary"><h1><?= Html::encode($this->title) ?></h1></div>
        <center>  
            <!-- begin logo -->
            <section>
                <div class="container">
                    <div id="osaimg" class="col-lg-12">
                        <?=Html::img('images/Sede.jpg',['width'=> '600px']) ?>
                    </div>
                </div>
            </section>
        </center>
        
</section>
<section>

    <div class="card-body">
        <div class="container">
            <div class="card-header alert alert-primary"><h2>Hsitorial de Boletines</h2></div>

            <div class="card">
                <div class="card-header alert alert-info">
                    <h4><a class="card-link" href="#boletinexterno" class data-toggle="collapse">Boletines Externos</a></h4>
                </div>
                <div id=boletinexterno class="collapse">
                    <center>
                    <?=Html::img('images/BoletinEXT-Mayo-2018-001.jpg',['width'=>'600px']) ?>
                    <?=Html::img('images/BoletinEXT-Mayo-2018-002.jpg',['width'=>'600px']) ?>
                    <?=Html::img('images/BoletinEXT-Mayo-2018-003.jpg',['width'=>'600px']) ?>
                    </center>
                </div>
            </div>
            <div class="card">
                <div class="card-header alert alert-info">
                    <h4><a class="card-link" href="#boletininterno" class data-toggle="collapse">Boletines Internos</a></h4>
                </div>
                <div id=boletininterno class="collapse">
                    <center>
                    <?=Html::img('images/images-interno-001.jpg',['width'=>'600px']) ?>
                    <?=Html::img('images/images-interno-002.jpg',['width'=>'600px']) ?>
                    </center>
                </div>
            </div>
            
            
            

        </div> 
    </div>

</section>
    

</div>
