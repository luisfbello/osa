<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ConsultasYReclamos */
/* @var $form ActiveForm */
?>
<div class="pqr">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'consulta_reclamo_fecha') ?>
        <?= $form->field($model, 'consulta_reclamo_identificacion') ?>
        <?= $form->field($model, 'consulta_reclamo_nit') ?>
        <?= $form->field($model, 'consulta_reclamo_observacion') ?>
        <?= $form->field($model, 'consulta_reclamo_nombres') ?>
        <?= $form->field($model, 'consulta_reclamo_apellidos') ?>
        <?= $form->field($model, 'consulta_reclamo_nombre_establecimiento') ?>
        <?= $form->field($model, 'consulta_reclamo_direccion') ?>
        <?= $form->field($model, 'consulta_reclamo_email') ?>
        <?= $form->field($model, 'consulta_reclamo_departamento') ?>
        <?= $form->field($model, 'consulta_reclamo_ciudad') ?>
        <?= $form->field($model, 'consulta_reclamo_telefono1') ?>
        <?= $form->field($model, 'consulta_reclamo_telefono2') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- pqr -->
