<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiposdetalles".
 *
 * @property integer $idTipoDetalle
 * @property string $nombre
 * @property integer $tipo_idtipo
 * @property string $fechaCreacion
 * @property integer $activo
 *
 * @property Documentosgestioncalidad[] $documentosgestioncalidads
 * @property Tipos $tipoIdtipo
 */
class Tiposdetalles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiposdetalles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo_idtipo'], 'required'],
            [['tipo_idtipo', 'activo'], 'integer'],
            [['fechaCreacion'], 'safe'],
            [['nombre'], 'string', 'max' => 45],
            [['tipo_idtipo'], 'exist', 'skipOnError' => true, 'targetClass' => Tipos::className(), 'targetAttribute' => ['tipo_idtipo' => 'idtipos']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTipoDetalle' => 'Id Tipo Detalle',
            'nombre' => 'Nombre',
            'tipo_idtipo' => 'Tipo Idtipo',
            'fechaCreacion' => 'Fecha Creacion',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentosgestioncalidads()
    {
        return $this->hasMany(Documentosgestioncalidad::className(), ['tipoDocumento' => 'idTipoDetalle']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoIdtipo()
    {
        return $this->hasOne(Tipos::className(), ['idtipos' => 'tipo_idtipo']);
    }
}
