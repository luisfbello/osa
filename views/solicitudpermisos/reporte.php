
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\db\Connection;
use yii\helpers\Url;
use app\models\Users;
use app\models\Puntosrecaudo;
use app\models\Tiposdetalles;
use app\models\Solicitudpermisos;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\solicitudpermisosSearch *
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Descargar Solicitudes';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $form = ActiveForm::begin(['action' => ['excel/solicitudpermisos'], 'method' => 'get', 'id'=>'formu' ]); ?>   
<!-- /////////////////////panel reporte excel /////////////////////////  -->
    <div class="card-body">
         <div class="card border-primary mb-3">
              <div class="card-header text-white" style="background-color: #337AB7;">Generar reporte</div>
              <div= class="card-body">
              <p>
                  <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <h4><div class= "alert alert-info" role="alert"><button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"><h5>Filtros para la solicitud</h5></button><div></h4>									
                                </h2>
                            </div>
                         <!-- <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample"> -->
                                <div class="card-body">
                                <div class="col-lg-12">
                                <table class="table">
                                   <tbody><tr>
                                    <td>
                                        <!-- contenedor filtro sucursal -->
                                        <?= Html::label('sucursal', 'select-sucursal', ['class'=>'control-label']) ?>
                                            <?= Html::dropDownList(
                                                'select-sucursal',
                                                '',
                                                ArrayHelper::map(puntosrecaudo::find()->all(),'idPuntoRecaudo', 'nombre'),
                                                [
                                                    'class'=>'form-control ',
                                                    'prompt'=>'[-- Seleccione sucursal --]',
                                                    'value'=> '',
                                                    'id' => 'select-sucursal',                                                  
                                                //     'onchange' => '$.get("index.php?r=excel/funcionarios&id='.'"+$(this).val(), function(data){
                                                //      $("#select-funcionario").html(data);
                                                //      reload.location();
                                                //   })',
                                                //   'onchange'=>'
                                                //               $.get( "'.Url::toRoute('excel/funcionarios').'", { id: $(this).val() } )
                                                //                   .done(function( data ) {
                                                //                       $( "#select-funcionario" ).html( data );
                                                //                   }
                                                //               );'
                                                ]) 
                                              ?>
                                              <div class="help-block"></div>                                          
                                    </td>
                                    <td>
                                        <!-- contenedor filtro funcionario -->                                      
                                          <?= Html::label('funcionario', 'idPuntoRecaudo', ['class'=>'control-label']) ?>
                                          <?= Html::dropDownList(
                                              'select-funcionario',
                                              '',
                                              // ArrayHelper::map(users::find()->all(),'id', 'nombres'),
                                              [],
                                              [
                                                  'class'=>'form-control ',
                                                  'prompt'=>'[-- Seleccione funcionario --]',
                                                  'value'=> '',
                                                  'id' => 'select-funcionario',                                                  
                                              ]) 
                                          ?>
                                          <div class="help-block"></div>                                      
                                    </td>
                                    <td>
                                         <!-- contenedor filtro motivo -->                                      
                                          <?= Html::label('motivo', 'select-motivo', ['class'=>'control-label']) ?>
                                          <?= Html::dropDownList(
                                              'select-motivo',
                                              '',
                                              ArrayHelper::map(tiposdetalles::find()
                                                                            ->where (['tipo_idtipo'=>(15)])
                                                                            ->orderBy(['nombre'=>'ASC'])->all(),'idTipoDetalle', 'nombre'),
                                            // [],
                                              [
                                                  'class'=>'form-control ',
                                                  'prompt'=>'[-- Seleccione motivo --]',
                                                  'value'=> '',
                                                  'id' => 'select-motivo',                                                                                                   
                                              ]) 
                                          ?>
                                          <div class="help-block"></div>                                      
                                    </td> 
                                    <td>
                                         <!-- contenedor filtro Aprobadas -->                                      
                                          <?= Html::label('Estado', 'select-aprobadas', ['class'=>'control-label']) ?>
                                          <?= Html::dropDownList(
                                              'select-aprobadas',
                                              '',
                                              ArrayHelper::map(tiposdetalles::find()
                                                                              ->where (['tipo_idtipo'=>(14)])
                                                                              ->all(), 'idTipoDetalle', 'nombre'),
                                              // [],
                                              [
                                                  'class'=>'form-control ',
                                                  'prompt'=>'[-- Seleccione aprobadas --]',
                                                  'value'=> '',
                                                  'id'=>'select-aprobadas',                                                 
                                              ]) 
                                          ?>
                                          <div class="help-block"></div>                                      
                                     </td>                                   
                                   </tr></tbody>
                                   <tbody><tr>                                                                     
                                     <div class="col-lg-6">
                                     <td>
                                         <!-- contenedor filtro Revisadas -->                                      
                                          <?= Html::label('Revisadas', 'select-revisadas', ['class'=>'control-label']) ?>
                                          <?= Html::dropDownList(
                                              'select-revisadas',
                                              '',
                                              ArrayHelper::map(tiposdetalles::find()
                                                                              ->where(['tipo_idtipo'=>(16)])
                                                                              ->all(), 'idTipoDetalle', 'nombre'), 
                                              // [],
                                              [
                                                  'class'=>'form-control',
                                                  'prompt'=>'[-- Seleccione revisadas --]',
                                                  'value'=> '',
                                                  'id'=>'select-revisadas',                                                 
                                              ])
                                          ?>
                                          <div class="help-block"></div>                                      
                                     </td>
                                     </div> 
                                     <div class="col-lg-4">
                                      <td>       
                                            <label for="start">Fecha desde</label>             
                                            <input type="date" class="form-control" id="finicio" name="fcreador" value="date('Y-m-d')" >
                                     </td>
                                     </div>
                                     <div class="col-lg-4">
                                      <td>       
                                            <label for="start">Fecha hasta</label>             
                                            <input type="date" class="form-control" id="ffin" name="fechaCreacion" value="date('Y-m-d')" >
                                     </td>                                                                                  
                                      </div>                                    
                                   </tr></tbody>                                                                                                                                                                                                                            
                                </div>
                                </table>
                                </div>                                
                            </div>                            
                        <!-- </div>        -->
                  </div> 
                </p>               
                <?= Html::submitButton("Buscar",["class"=>"btn btn-primary", 'id' => "buscar-carteras"])?>                                                            
              </div>            
         </div>
    </div>
    <?php ActiveForm::end(); ?>           

<script type="text/javascript">
          $(document).ready(function(){                     
            $( "#select-sucursal" ).change(function() {
                // alert( "si envia" );
                $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=excel/funcionarios',
                type : 'get',
                data: {
                    id: $(this).val()
                },
                success : function(data){
                 $("#select-funcionario").html(data);
                }
             });
            });
            var form= $('#formu');
                form.submit(function(event){                
                $.ajax({
                        
                    }).done(function(data){                        
                       $('#formu').trigger("reset");
                });
            });                                                           
           });                                        
</script>
