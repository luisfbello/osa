<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
use app\models\Municipios;
use app\models\Departamentos;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Registronegocios */
/* @var $form ActiveForm */
?>
<div class="registro">
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="panel panel-primary">
        <div class="panel-heading">Registro de Establecimiento Nuevo</div>
        <div class="panel-body">
            <label class="control-label" >Marca Temporal</label>
                
                <div class="col-lg-12">
                <?php // usage without model
                    echo DatePicker::widget([
                        'name' => 'fechaEntrega',
                        //'value' => date('d-M-Y', strtotime('+2 days')),
                        'options' => [
                            'placeholder' => 'Seleccione la Fecha' , 
                            'class' => 'form-control a', 
                            'required' => 'required'
                        ],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true
                        ]
                ]);?>
                <?= $form->field($model, 'registro_negocio_identificacion')->textInput(['class'=>'form-control a']) ?>
                <?= $form->field($model, 'registro_negocio_nit_establecimiento')->textInput(['class'=>'form-control a']) ?>
                <?= $form->field($model, 'registro_negocio_nombre')->textInput(['class'=>'form-control a']) ?>
                <?= $form->field($model, 'registro_negocio_apellido')->textInput(['class'=>'form-control a']) ?>
                <?= $form->field($model, 'registro_negocio_email')->textInput(['class'=>'form-control a']) ?>
                <?= $form->field($model, 'registro_negocio_nombre_establecimiento')->textInput(['class'=>'form-control a']) ?>
                </div>
                
                <div class="col-lg-6">
                    <?= $form->field($model, 'registro_negocio_telefono1')->textInput(['class'=>'form-control a']) ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'registro_negocio_telefono2')->textInput(['class'=>'form-control a']) ?>                   
                </div>
                <div class="col-lg-12">
                
                <div class="form-group">
                <?= Html::label('Departamentos', 'select-departamentos', ['class'=>'control-label']) ?>
                <?= Html::dropDownList(
                    'select-departamentos',
                    '',
                    ArrayHelper::map(Departamentos::find()->orderBy(['nombre'=>'asc'])->all(),'idDepartamento', 'nombre'),
                   [
                        'class'=>'form-control a',
                        'prompt'=>'[-- Seleccione Departamento --]',
                        'value'=> 0,
                        'id' => 'select-departamentos',
                        'onchange' => '$.post("index.php?r=formularios/lists&id='.'"+$(this).val(), function(data){
                                $("#select-municipios").val("");
                                $("#select-municipios").html(data);
                                $("#select-municipios").removeAttr("disabled");
                        })',
                        'required' => 'required'
                    ]) 
                ?>
                <div class="help-block"></div>
                
            </div>
        <!-- FIN DE DROP DOWN LIST DE DEPARTAMENTO-->       
        <!-- DROP DOWN LIST PARA SELECCION DEL MUNICIPIO SEGUN EL DEPARTAMENTO -->
            <div class="form-group">
                <?= Html::label('Municipios', 'select-municipios', ['class'=>'control-label']) ?>
                <?= Html::dropDownList(
                    'select-municipios',
                    '',
                    ArrayHelper::map(Municipios::find()->all(), 'idMunicipio', 'nombre'),
                    [
                        'class'=>'form-control a',
                        'for'=>'disabledTextInput',
                        'prompt'=>'[-- Seleccione Municipios --]',
                        'value'=> null,
                        'id'=>'select-municipios',
                        'onchange' => '',
                        'required' => 'required'
                    ]) 
                ?>
                <div class="help-block"></div>
                <div style="color:#F40808;display:none" id="alertaMunicipio"><p>El campo Municipio es obligatorio</p></div>
            </div>     


                <!-- <?= $form->field($model, 'registro_negocio_departamento')->textInput(['class'=>'form-control a']) ?>
                <?= $form->field($model, 'registro_negocio_ciudad')->textInput(['class'=>'form-control a']) ?> -->
                <?= $form->field($model, "registro_negocio_file1[]")->fileInput(['multiple' => false, 'class'=> 'a' ])->label('Adjuntar (Formulario F17-RDC Autodeclaración)')  ?> 
                <?= $form->field($model, "registro_negocio_file1[]")->fileInput(['multiple' => false, 'class'=> 'a' ])->label('Adjuntar (Cámara de Comercio)')  ?> 
                <?= $form->field($model, "registro_negocio_file1[]")->fileInput(['multiple' => false, 'class'=> 'a' ])->label('Adjuntar (Cédula de Ciudadanía)')  ?> 
                </div>
        </div>
    </div>
        <div class="form-group">
            <?= Html::submitButton('Enviar Solicitud', ['class' => 'btn btn-primary enviar' ]) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div><!-- registro -->
<script type="text/javascript">
	
	$(".enviar").click(function(){

        var validate = false;
        
        $('.a').each(function(){
            if ($(this).val() == "" || $(this).val() == null ) {
                $(this).css("border", "1px solid red");
                validate = false;

            }else{
                $(this).css("border", "1px solid #ccc");
                validate = true;
            }
        });

        if(validate) {

            return true;
            $("#w0").submit();

        }else{
            
            confirm('ERROR: se debe seleccionar Todos los campos para continuar con la asignación');
            return false;
        }
    });

</script>