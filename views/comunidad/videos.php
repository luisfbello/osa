<?php

use yii\helpers\Html;

$this->title = 'Videos Internos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <section>
        <div class="card-header alert alert-primary"><h1><?= Html::encode($this->title) ?></h1></div>
    </section>

    <div class="card-body">
        <section>
            <div class="card">
            
            <button href="#codigopolicia" class=" btn alert-primary" data-toggle="modal"><h3>Derechos de Autor y Nuevo Código de Policía (Directores y Delegados)</h3></button>
                    <!-- <div id="demo" class="carousel slide" data-ride="carousel">
                        
                        <ul class="carousel-indicators">
                            <li data-target="#demo" data-slide-to="0" class="active"></li>
                            <li data-target="#demo" data-slide-to="1"></li>
                            <li data-target="#demo" data-slide-to="2"></li>
                        </ul>

                        
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="la.jpg" alt="Los Angeles" width="1100" height="500">
                            </div>
                            <div class="carousel-item">
                                <img src="chicago.jpg" alt="Chicago" width="1100" height="500">
                            </div>
                            <div class="carousel-item">
                                <img src="ny.jpg" alt="New York" width="1100" height="500">
                            </div>
                        </div>

                        
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>-->
            </div>
            
        </section>
        <section>
            <div class="card">
                <button href="#diligenciamientoPostal" class=" btn alert-primary" data-toggle="modal"><h3>Diligenciamiento Guía postal 472</h3></button>
                <div class="modal fade" id="diligenciamientoPostal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header alert-primary">
                                <h4 class="modal-title">Instructivo</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <video width="450" height="240" controls>
                                    <source src="videos/InstructivoSIPOST.mp4" type="video/mp4">
                                </video>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="modalsalir" class="btn btn-primary" data-dismiss="modal">Salir</button>
                            </div> 
                        </div>
                    </div>
                </div>
            </div> 
        </section>
        
            
        
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        $('#modalsalir').click(function(){
            $('video').each(function(){this.pause()})
        });
    });
</script>

