<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Solicitudes;

/**
 * SolicitudesSearch represents the model behind the search form about `app\models\Solicitudes`.
 */
class SolicitudesSearch extends Solicitudes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['solicitud_id', 'solicitud_importancia'], 'integer'],
            [['solicitud_observacion', 'solicitud_usuario', 'solicitud_fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Solicitudes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'solicitud_id' => $this->solicitud_id,
            'solicitud_importancia' => $this->solicitud_importancia,
            'solicitud_fecha' => $this->solicitud_fecha,
        ]);

        $query->andFilterWhere(['like', 'solicitud_observacion', $this->solicitud_observacion])
            ->andFilterWhere(['like', 'solicitud_usuario', $this->solicitud_usuario]);

        return $dataProvider;
    }
}
