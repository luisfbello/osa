<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sucursales".
 *
 * @property integer $idSucursal
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono
 * @property string $telefono2
 * @property string $fax
 * @property string $email
 * @property integer $zona
 * @property string $fechaCreacion
 */
class Sucursales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sucursales';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'direccion', 'telefono'], 'required'],
            [['zona'], 'integer'],
            [['fechaCreacion'], 'safe'],
            [['nombre'], 'string', 'max' => 100],
            [['direccion'], 'string', 'max' => 255],
            [['telefono', 'telefono2', 'fax'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 155],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idSucursal' => 'Id Sucursal',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'telefono2' => 'Telefono2',
            'fax' => 'Fax',
            'email' => 'Email',
            'zona' => 'Zona',
            'fechaCreacion' => 'Fecha Creacion',
        ];
    }
}
