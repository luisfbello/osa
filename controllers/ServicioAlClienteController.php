<?php

namespace app\controllers;

use Yii;
use app\models\ServicioAlCliente;
use app\models\ServicioAlClienteSearch;
use app\models\Municipios;
use app\models\ObservacionesServicioAlCliente;
use app\models\Sucursales;
use app\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * ServicioAlClienteController implements the CRUD actions for ServicioAlCliente model.
 */
class ServicioAlClienteController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServicioAlCliente models.
     * @return mixed
     */
    public function actionIndex()
    {
        // // $searchModel = new ServicioAlClienteSearch();
        // // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// VISTA SOLICITUDES  /////////////////////////////////////////////////////////////////////////////

    public function actionServicioclienteindex()
    {   
        $zona=0;
        $pqr=0;
        $liquidaciones=0;
        $query = new Query();
        $queryzona=new Query();

        $connection = Yii::$app->db;
        if (Yii::$app->user->identity->id != 2) {
            $users=Users::findOne(Yii::$app->user->identity->id);
            
            $queryzona=$connection->createCommand("SELECT zona 
                                                from puntosrecaudo 
                                                    join sucursales on (idSucursal=sucursales_idsucursales)
                                                where idPuntoRecaudo=".$users->puntosrecaudo_idpuntosrecaudo."");
            $respuesta=$queryzona->queryAll();
            $zona=$respuesta[0]['zona'];
        }

        
        

        if ($zona != 0) {
            $query = $connection->createCommand("SELECT count(servicio_al_cliente_id) as nsolicitud,servicio_al_cliente_tipo_solicitud 
                                            FROM servicio_al_cliente
                                            where 1=1
                                            and servicio_al_cliente_zona=".$zona." 
                                            and servicio_al_cliente_check=1
                                            group by servicio_al_cliente_tipo_solicitud
                                            order by servicio_al_cliente_id desc;");
            // echo '<pre>';
            // print_r($query);
            // die();

            $solicitudes=$query->queryAll();
        }else{
            $query = $connection->createCommand("SELECT count(servicio_al_cliente_id) as nsolicitud,servicio_al_cliente_tipo_solicitud 
                                                    FROM servicio_al_cliente
                                                    where 1=1
                                                     
                                                    group by servicio_al_cliente_tipo_solicitud
                                                    order by servicio_al_cliente_id desc;");

            $solicitudes=$query->queryAll();  
        }
        

        foreach($solicitudes as $solicitud){
            if($solicitud['servicio_al_cliente_tipo_solicitud']==139){
                $pqr=$solicitud['nsolicitud'];
            }else if($solicitud['servicio_al_cliente_tipo_solicitud']==138){
                $liquidaciones=$solicitud['nsolicitud'];
            }
        }

        // echo "<pre>";
        // echo $pqr."<br>";
        // echo $liquidaciones;
        // die();

        return $this->render('servicioclienteindex',[
            'pqr'=>$pqr,
            'liquidaciones'=>$liquidaciones,
        ]);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// VISTA SOLICITUDES LIQUIDACION /////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// INDEX
    public function actionSolicitudesliquidacionindex($idsolicitud)
    {
        $zona=0;
        $pendientes=0;
        $gestionadas=0;
        $query = new Query();
        $queryzona=new Query();
        $connection = Yii::$app->db;

        if (Yii::$app->user->identity->id != 2) {
            $users=Users::findOne(Yii::$app->user->identity->id);
            $queryzona=$connection->createCommand("SELECT zona 
                                                from puntosrecaudo 
                                                    join sucursales on (idSucursal=sucursales_idsucursales)
                                                where idPuntoRecaudo=".$users->puntosrecaudo_idpuntosrecaudo."");
            $respuesta=$queryzona->queryAll();
            $zona=$respuesta[0]['zona'];
        }


        if ($zona != 0){
            $query = $connection->createCommand("SELECT count(servicio_al_cliente_id) as nsolicitud,
                                                    servicio_al_cliente_tipo_solicitud,
                                                    servicio_al_cliente_estado_solicitud 
                                                FROM servicio_al_cliente
                                                WHERE servicio_al_cliente_tipo_solicitud=".$idsolicitud."
                                                and servicio_al_cliente_zona=".$zona."
                                                and servicio_al_cliente_check=1  
                                                group by servicio_al_cliente_estado_solicitud;");

        $solicitudes=$query->queryAll();

        }else{
            $query = $connection->createCommand("SELECT count(servicio_al_cliente_id) as nsolicitud,
                                                servicio_al_cliente_tipo_solicitud,
                                                servicio_al_cliente_estado_solicitud 
                                            FROM servicio_al_cliente
                                            WHERE servicio_al_cliente_tipo_solicitud=".$idsolicitud." 
                                            
                                            group by servicio_al_cliente_estado_solicitud;");

        $solicitudes=$query->queryAll();

        }
        

        foreach($solicitudes as $solicitud){
            if($solicitud['servicio_al_cliente_estado_solicitud']==0){
                $pendientes=$solicitud['nsolicitud'];
            }else if($solicitud['servicio_al_cliente_estado_solicitud']==1){
                $gestionadas=$solicitud['nsolicitud'];
            }
        }

        return $this->render('solicitudesliquidacionindex',[
            'pendientes'=>$pendientes,
            'gestionadas'=>$gestionadas,
            'idsolicitud'=>$idsolicitud,
        ]);
        
    }

//////////////////////////////////// GESTIONADAS
    public function actionGestionadaslq($idsolicitud,$estado)
    {   
        // echo $idsolicitud;
        // die();
        $zona=0;
        $check=0;
        $queryzona=new Query();
        $connection = Yii::$app->db;
        if (Yii::$app->user->identity->id != 2) {
            $users=Users::findOne(Yii::$app->user->identity->id);
            $queryzona=$connection->createCommand("SELECT zona 
                                                from puntosrecaudo 
                                                    join sucursales on (idSucursal=sucursales_idsucursales)
                                                where idPuntoRecaudo=".$users->puntosrecaudo_idpuntosrecaudo."");
            $respuesta=$queryzona->queryAll();
            $zona=$respuesta[0]['zona'];
            $check=1;
        }

        $searchModel = new ServicioAlClienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idsolicitud,$estado,$zona,$check);

        return $this->render('gestionadaslq', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'idsolicitud'=>$idsolicitud,
        ]);
    }
//////////////////////////////////// PENDIENTES
    public function actionPendienteslq($idsolicitud,$estado)
    {   
        $zona=0;
        $check=0;
        $queryzona=new Query();
        $connection = Yii::$app->db;
        if (Yii::$app->user->identity->id != 2) {
            $users=Users::findOne(Yii::$app->user->identity->id);
            $queryzona=$connection->createCommand("SELECT zona 
                                                from puntosrecaudo 
                                                    join sucursales on (idSucursal=sucursales_idsucursales)
                                                where idPuntoRecaudo=".$users->puntosrecaudo_idpuntosrecaudo."");
            $respuesta=$queryzona->queryAll();
            $zona=$respuesta[0]['zona'];
            $check=1;
        }
        $searchModel = new ServicioAlClienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idsolicitud,$estado,$zona,$check);

        return $this->render('pendienteslq', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'idsolicitud'=>$idsolicitud,
        ]);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// VISTA SOLICITUDES PQR /////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// PQR INDEX    
    public function actionPqrindex($idsolicitud)
    {   
        $zona=0;
        $pendientes=0;
        $gestionadas=0;
        $query = new Query();
        $queryzona=new Query();
        $connection = Yii::$app->db;

        if (Yii::$app->user->identity->id != 2) {
            $users=Users::findOne(Yii::$app->user->identity->id);
            $queryzona=$connection->createCommand("SELECT zona 
                                                from puntosrecaudo 
                                                    join sucursales on (idSucursal=sucursales_idsucursales)
                                                where idPuntoRecaudo=".$users->puntosrecaudo_idpuntosrecaudo."");
            $respuesta=$queryzona->queryAll();
            $zona=$respuesta[0]['zona'];
            
        }


        if ($zona != 0){
            $query = $connection->createCommand("SELECT count(servicio_al_cliente_id) as nsolicitud,
                                                servicio_al_cliente_tipo_solicitud,
                                                servicio_al_cliente_estado_solicitud 
                                            FROM servicio_al_cliente
                                            WHERE servicio_al_cliente_tipo_solicitud=".$idsolicitud."
                                            and servicio_al_cliente_zona=".$zona."
                                            and servicio_al_cliente_check=1 
                                            group by servicio_al_cliente_estado_solicitud;");

        $solicitudes=$query->queryAll();

        }else{
            $query = $connection->createCommand("SELECT count(servicio_al_cliente_id) as nsolicitud,
                                                servicio_al_cliente_tipo_solicitud,
                                                servicio_al_cliente_estado_solicitud 
                                            FROM servicio_al_cliente
                                            WHERE servicio_al_cliente_tipo_solicitud=".$idsolicitud." 
                                            
                                            group by servicio_al_cliente_estado_solicitud;");

        $solicitudes=$query->queryAll();

        }

    
    

        foreach($solicitudes as $solicitud){
            if($solicitud['servicio_al_cliente_estado_solicitud']==0){
                $pendientes=$solicitud['nsolicitud'];
            }else if($solicitud['servicio_al_cliente_estado_solicitud']==1){
                $gestionadas=$solicitud['nsolicitud'];
            }
        }

        return $this->render('pqrindex',[
            'pendientes'=>$pendientes,
            'gestionadas'=>$gestionadas,
            'idsolicitud'=>$idsolicitud,
        ]);
        
        
    }
//////////////////////////////////// PQR GESTIONADAS
    public function actionGestionadaspqr($idsolicitud,$estado)
    {   
        $zona=0;
        $check=0;
        $queryzona=new Query();
        $connection = Yii::$app->db;
        if (Yii::$app->user->identity->id != 2) {
            $users=Users::findOne(Yii::$app->user->identity->id);
            $queryzona=$connection->createCommand("SELECT zona 
                                                from puntosrecaudo 
                                                    join sucursales on (idSucursal=sucursales_idsucursales)
                                                where idPuntoRecaudo=".$users->puntosrecaudo_idpuntosrecaudo."");
            $respuesta=$queryzona->queryAll();
            $zona=$respuesta[0]['zona'];
            $check=1;
        }
        $searchModel = new ServicioAlClienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idsolicitud,$estado,$zona,$check);

        return $this->render('gestionadaspqr', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'idsolicitud'=>$idsolicitud,
        ]);
    }
//////////////////////////////////// PQR PENDIENTES
    public function actionPendientespqr($idsolicitud,$estado)
    {   
        $zona=0;
        $check=0;
        $queryzona=new Query();
        $connection = Yii::$app->db;
        if (Yii::$app->user->identity->id != 2) {
            $users=Users::findOne(Yii::$app->user->identity->id);
            $queryzona=$connection->createCommand("SELECT zona 
                                                from puntosrecaudo 
                                                    join sucursales on (idSucursal=sucursales_idsucursales)
                                                where idPuntoRecaudo=".$users->puntosrecaudo_idpuntosrecaudo."");
            $respuesta=$queryzona->queryAll();
            $zona=$respuesta[0]['zona'];
            $check=1;
        }
        $searchModel = new ServicioAlClienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idsolicitud,$estado,$zona,$check);

        return $this->render('pendientespqr', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'idsolicitud'=>$idsolicitud,
        ]);
    }

    /**
     * Displays a single ServicioAlCliente model.
     * @param integer $id
     * @return mixed
     */

////////////////////////////////////////////////////////////// VIEW /////////////////////////////////////////////////
    public function actionView()
    {
        $data="";
        $solicitud=ServicioAlCliente::findOne($_GET['id']);
        $observacionesSolicitud=ObservacionesServicioAlCliente::findAll(['observaciones_servicio_al_cliente_servicio_al_cliente_id'=>$_GET['id']]);
        // echo "<pre>";
        // print_r($observacionesSolicitud);
        // die();
        if($solicitud->servicio_al_cliente_estado_solicitud==0){
            $data="";
            $data.='<table class="table table-bordered table-striped">';
                $data.='<thead>';
                    $data.='<tr>';
                        $data.='<th colspan="1">Zona</th>';
                        $data.='<th>Departamento</th>';
                        $data.='<th>Municipio</th>';
                        $data.='<th>Direccion</th>';
                        $data.='</tr>';
                $data.='</thead>';
                $data.='<tbody>';
                    $data.='<tr>';
                        $data.='<td colspan="1">'.$solicitud->servicio_al_cliente_zona.'</td>';
                        $data.='<td>'.$solicitud->servicioAlClienteDepartamento->nombre.'</td>';
                        $data.='<td>'.$solicitud->servicioAlClienteMunicipio->nombre.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_direccion.'</td>';
                        $data.='</tr>';
                    $data.='</tbody>';
                $data.='<thead>';
                    $data.='<tr>';
                        $data.='<th>Nombre Usuario</th>';
                        $data.='<th>Tipo Identificacion</th>';
                        $data.='<th>Nº Documento</th>';
                        $data.='<th>Razon Social</th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                $data.='<tbody>';
                    $data.='<tr>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_nombre_usuario.' '.$solicitud->servicio_al_cliente_apellidos_usuario.'</td>';
                        $data.='<td>'.$solicitud->servicioAlClienteTipoIdentificacion->nombre.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_numero_documento.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_razonsocial.'</td>';
                    $data.='</tr>';
                $data.='</tbody>';
                $data.='<thead>';
                    $data.='<tr>';
                        $data.='<th>Telefono</th>';
                        $data.='<th>Telefono 2</th>';
                        $data.='<th>Correo Electronico</th>';
                        $data.='<th>Fecha Solicitud</th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                $data.='<tbody>';
                    $data.='<tr>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_telefono1.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_telefono2.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_email.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_fecha_solicitud.'</td>';
                    $data.='</tr>';
                $data.='</tbody>';
                $data.='<thead>';
                    $data.='<tr >';
                        $data.='<th colspan="4"><center>Observacion Cliente</center></th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                $data.='<tbody>';
                    $data.='<tr>';
                        $data.='<td colspan="4">'.$solicitud->servicio_al_cliente_observacion.'</td>';
                    $data.='</tr>';
                $data.='</tbody>';
                $data.='<thead>';
                    $data.='<tr >';
                        $data.='<th colspan="4"><center>Observacion Gestion</center></th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                $data.='<thead>';
                    $data.='<tr >';
                        $data.='<th colspan="1"><center>Fecha de la Gestion</center></th>';
                        $data.='<th colspan="1"><center>Usuario que Gestiona</center></th>';
                        $data.='<th colspan="3"><center>Observacion de la Gestion</center></th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                
                $data.='<tbody>';
                foreach($observacionesSolicitud as $ob){
                    $data.='<tr>';
                        
                            $data.='<td colspan="1">'.$ob->observaciones_servicio_al_cliente_fecha_observacion.'</td>';
                            $data.='<td colspan="1">'.$ob->observacionesServicioAlClienteUsuario->username.'</td>';
                            $data.='<td colspan="3">'.$ob->observaciones_servicio_al_cliente_observacion.'</td>';
                        
                    $data.='</tr>';
                }
                $data.='</tbody>';
                
            $data.='</table>';
        }else{
            $data="";
            $data.='<table class="table table-bordered table-striped">';
                $data.='<thead>';
                    $data.='<tr>';
                        $data.='<th><center>Zona</center></th>';
                        $data.='<th>Departamento</th>';
                        $data.='<th>Municipio</th>';
                        $data.='<th>Direccion</th>';
                        $data.='</tr>';
                $data.='</thead>';
                $data.='<tbody>';
                    $data.='<tr>';
                        $data.='<td colspan="1"><center>'.$solicitud->servicio_al_cliente_zona.'</center></td>';
                        $data.='<td>'.$solicitud->servicioAlClienteDepartamento->nombre.'</td>';
                        $data.='<td>'.$solicitud->servicioAlClienteMunicipio->nombre.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_direccion.'</td>';
                        $data.='</tr>';
                    $data.='</tbody>';
                $data.='<thead>';
                    $data.='<tr>';
                        $data.='<th>Nombre Usuario</th>';
                        $data.='<th>Tipo Identificacion</th>';
                        $data.='<th>Nº Documento</th>';
                        $data.='<th>Razon Social</th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                $data.='<tbody>';
                    $data.='<tr>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_nombre_usuario.' '.$solicitud->servicio_al_cliente_apellidos_usuario.'</td>';
                        $data.='<td>'.$solicitud->servicioAlClienteTipoIdentificacion->nombre.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_numero_documento.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_razonsocial.'</td>';
                    $data.='</tr>';
                $data.='</tbody>';
                $data.='<thead>';
                    $data.='<tr>';
                        $data.='<th>Telefono</th>';
                        $data.='<th>Telefono 2</th>';
                        $data.='<th>Correo Electronico</th>';
                        $data.='<th>Fecha Solicitud</th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                $data.='<tbody>';
                    $data.='<tr>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_telefono1.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_telefono2.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_email.'</td>';
                        $data.='<td>'.$solicitud->servicio_al_cliente_fecha_solicitud.'</td>';
                    $data.='</tr>';
                $data.='</tbody>';
                $data.='<thead>';
                    $data.='<tr >';
                        $data.='<th colspan="4"><center>Observacion Cliente</center></th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                $data.='<tbody>';
                    $data.='<tr>';
                        $data.='<td colspan="4">'.$solicitud->servicio_al_cliente_observacion.'</td>';
                    $data.='</tr>';
                $data.='</tbody>';
                $data.='<thead>';
                    $data.='<tr >';
                        $data.='<th colspan="4"><center>Observacion Gestion</center></th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                $data.='<thead>';
                    $data.='<tr >';
                        $data.='<th colspan="1"><center>Fecha de la Gestion</center></th>';
                        $data.='<th colspan="1"><center>Usuario que Gestiona</center></th>';
                        $data.='<th colspan="3"><center>Observacion de la Gestion</center></th>';
                    $data.='</tr>'; 
                $data.='</thead>';
                $data.='<tbody>';
                foreach($observacionesSolicitud as $ob){
                    $data.='<tr>';
                        
                            $data.='<td colspan="1">'.$ob['observaciones_servicio_al_cliente_fecha_observacion'].'</td>';
                            $data.='<td colspan="1">'.$ob->observacionesServicioAlClienteUsuario->username.'</td>';
                            $data.='<td colspan="3">'.$ob['observaciones_servicio_al_cliente_observacion'].'</td>';
                       
                    $data.='</tr>';
                    }
                $data.='</tbody>';
            $data.='</table>';
        }
        

        // echo "<pre>";
        // print_r($solicitud);
        // die();

        echo $data;
        // die();

        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        // ]);
    }

    /**
     * Creates a new ServicioAlCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// FORMULARIO SOLICITUDES LIQUIDACIONES /////////////////////////////////////////////////////////////////////////////////////

    // public function actionFormulariosolicitudesliquidaciones()
    // {
    //     $this->layout = 'formulario2';
    //     $model = new ServicioAlCliente();



    //     if ($model->load(Yii::$app->request->post())) {
            

    //         // echo "esta despues de la validacion";
    //         // echo "<pre>";
    //         // print_r(Yii::$app->request->post());
    //         // die();

    //         $solicitudeslq=Yii::$app->request->post();
            
    //             $nombre=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_nombre_usuario'];
    //             $apellido=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_apellidos_usuario'];
    //             $tipoidentificacion=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_tipoIdentificacion'];
    //             $numeroidentificacion=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_numero_documento'];
    //             $razonsocial=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_razonsocial'];
    //             $direccion=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_direccion'];
    //             $telefono=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_telefono1'];
    //             $nit=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_nit'];
    //             $email=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_email'];
    //             $telefono2=$solicitudeslq['ServicioAlCliente']['servicio_al_cliente_telefono2'];
    //             $observacion="";
    //             $departamento=$solicitudeslq['select-departamentos'];
    //             $municipio=$solicitudeslq['municipios'];
    //             $fecha=date('Y-m-d');

    //             $queryzona= new Query();
    //             $query=new Query();
    //             $connection= Yii::$app->db;

    //             $queryzona=$connection->createCommand("SELECT zona 
    //                                                     FROM municipios
    //                                                         join sucursales on (idSucursal=sucursales_idsucursal)
    //                                                     WHERE municipios.departamento_iddepartamento=".$departamento."");
    //             $zona=$queryzona->queryAll();

    //             $fechasolicitud= date("Y-m-d",strtotime($fecha));

    //             $query=$connection->createCommand("insert into servicio_al_cliente(
    //                                                 `servicio_al_cliente_nombre_usuario`,
    //                                                 `servicio_al_cliente_apellidos_usuario`,
    //                                                 `servicio_al_cliente_tipoIdentificacion`,
    //                                                 `servicio_al_cliente_numero_documento`,
    //                                                 `servicio_al_cliente_razonsocial`,
    //                                                 `servicio_al_cliente_municipio`,
    //                                                 `servicio_al_cliente_departamento`,
    //                                                 `servicio_al_cliente_zona`,
    //                                                 `servicio_al_cliente_direccion`,
    //                                                 `servicio_al_cliente_nit`,
    //                                                 `servicio_al_cliente_telefono1`,
    //                                                 `servicio_al_cliente_telefono2`,
    //                                                 `servicio_al_cliente_email`,
    //                                                 `servicio_al_cliente_observacion`,
    //                                                 `servicio_al_cliente_tipo_solicitud`,
    //                                                 `servicio_al_cliente_fecha_solicitud`
    //                                                 ) values(
    //                                                     '".$nombre."',
    //                                                     '".$apellido."',
    //                                                     ".$tipoidentificacion.",
    //                                                     ".$numeroidentificacion.",
    //                                                     '".$razonsocial."',
    //                                                     ".$municipio.",
    //                                                     ".$departamento.",
    //                                                     ".$zona[0]['zona'].",
    //                                                     '".$direccion."',
    //                                                     ".$nit.",
    //                                                     ".$telefono.",
    //                                                     ".$telefono2.",
    //                                                     '".$email."',
    //                                                     '".$observacion."',
    //                                                     138,
    //                                                     '".$fechasolicitud."'
    //                                                 );");
            
    //         // echo "<pre>";                                      
    //         // print_r($query);
    //         // die();

    //         $save=$query->execute();
            

    //         if($save){
    //             $mensaje="<center><h5>Hemos Recibido tu Solicitud, te Enviaremos la Liquidacion <br>
    //                         lo mas Pronto Posible</h5></center>";

    //             return $this->render('respuesta',[
    //                 'mensaje'=>$mensaje,
    //             ]);

    //         }else{
    //                 echo '<pre>';
    //                 print_r($model->getErrors() );
    //                 die();
    //         }


            
    //     } else {
    //         return $this->render('formulariosolicitudesliquidacion', [
    //             'model' => $model,
    //         ]);
    //     }
    // }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// FORMULARIO SOLICITUDES PQR /////////////////////////////////////////////////////////////////////////////////////

    public function actionFormulariosolicitudes()
    {
        $this->layout = 'formulario2';
        $model = new ServicioAlCliente();

    

        if ($model->load(Yii::$app->request->post())) {

            
            // echo "esta despues de la validacion";
            // echo "<pre>";
            // print_r(Yii::$app->request->post());
            // die();

            $solicitudespqr=Yii::$app->request->post();
            
                $nombre=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_nombre_usuario'];
                $apellido=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_apellidos_usuario'];
                $tipoidentificacion=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_tipoIdentificacion'];
                $numeroidentificacion=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_numero_documento'];
                $razonsocial=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_razonsocial'];
                $direccion=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_direccion'];
                $telefono=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_telefono1'];
                $nit=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_nit'];
                $email=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_email'];
                $telefono2=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_telefono2'];
                $observacion=$solicitudespqr['ServicioAlCliente']['servicio_al_cliente_observacion'];
                $departamento=$solicitudespqr['select-departamentos'];
                $municipio=$solicitudespqr['municipios'];
                $fecha=date('Y-m-d');
                $idTipo=$solicitudespqr['solicitud'];

                $queryzona= new Query();
                $query=new Query();
                $connection= Yii::$app->db;

                $queryzona=$connection->createCommand("SELECT zona 
                                                        FROM municipios
                                                            join sucursales on (idSucursal=sucursales_idsucursal)
                                                        WHERE municipios.departamento_iddepartamento=".$departamento."");
                $zona=$queryzona->queryAll();

                $fechasolicitud= date("Y-m-d",strtotime($fecha));

                $query=$connection->createCommand("insert into servicio_al_cliente(
                                                    `servicio_al_cliente_nombre_usuario`,
                                                    `servicio_al_cliente_apellidos_usuario`,
                                                    `servicio_al_cliente_tipoIdentificacion`,
                                                    `servicio_al_cliente_numero_documento`,
                                                    `servicio_al_cliente_razonsocial`,
                                                    `servicio_al_cliente_municipio`,
                                                    `servicio_al_cliente_departamento`,
                                                    `servicio_al_cliente_zona`,
                                                    `servicio_al_cliente_direccion`,
                                                    `servicio_al_cliente_nit`,
                                                    `servicio_al_cliente_telefono1`,
                                                    `servicio_al_cliente_telefono2`,
                                                    `servicio_al_cliente_email`,
                                                    `servicio_al_cliente_observacion`,
                                                    `servicio_al_cliente_tipo_solicitud`,
                                                    `servicio_al_cliente_fecha_solicitud`
                                                    ) values(
                                                        '".$nombre."',
                                                        '".$apellido."',
                                                        ".$tipoidentificacion.",
                                                        ".$numeroidentificacion.",
                                                        '".$razonsocial."',
                                                        ".$municipio.",
                                                        ".$departamento.",
                                                        ".$zona[0]['zona'].",
                                                        '".$direccion."',
                                                        ".$nit.",
                                                        ".$telefono.",
                                                        ".$telefono2.",
                                                        '".$email."',
                                                        '".$observacion."',
                                                        ".$idTipo.",
                                                        '".$fechasolicitud."'
                                                    );");
            
            // echo "<pre>";                                      
            // print_r($query);
            // die();

            $save=$query->execute();
            

            if($save){
                $mensaje="<center><h5>Hemos Recibido tu Solicitud, te Responderemos <br>
                            lo mas Pronto Posible</h5></center>";

                return $this->render('respuesta',[
                    'mensaje'=>$mensaje,
                ]);

            }else{
                    echo '<pre>';
                    print_r($model->getErrors() );
                    die();
            }
            
        } else {
            return $this->render('formulariosolicitudes', [
                'model' => $model,
            ]);
        }
    }


    public function actionCreate()
    {
        $model = new ServicioAlCliente();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->servicio_al_cliente_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ServicioAlCliente model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {

        $mensaje="";
        $modelObservacion= new ObservacionesServicioAlCliente();
        
        // $solicitud->servicio_al_cliente_estado_solicitud=1;
        $modelObservacion->observaciones_servicio_al_cliente_usuario=Yii::$app->user->identity->id;
        $modelObservacion->observaciones_servicio_al_cliente_fecha_observacion=date("Y-m-d");
        $modelObservacion->observaciones_servicio_al_cliente_observacion=$_GET['observacion'];
        $modelObservacion->observaciones_servicio_al_cliente_servicio_al_cliente_id=$_GET['id'];

        if($modelObservacion->save()){
             $mensaje.="<div>";
                $mensaje.="<center><h5>La Solicitud se Gestiono Correctamente</h5></center>";
            $mensaje.="</div>";

            echo $mensaje;
        }else{
            echo $mensaje="<h5>Lo Sentimos La Gestion Presenta Un Error: ".print_r($modelObservacion->getErrors())."</h5>";
        }


        
    }

    /**
     * Deletes an existing ServicioAlCliente model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
////////////////////////////////// CERRAR SOLICITUD ///////////////////////////////////////
    public function actionCerrar()
    {
        $solicitud=ServicioAlCliente::findOne($_GET['id']);
        $mensaje="";
        $modelObservacion= new ObservacionesServicioAlCliente();
        
        $solicitud->servicio_al_cliente_estado_solicitud=1;
        $solicitud->servicio_al_cliente_operario_gestion=Yii::$app->user->identity->id;
        $modelObservacion->observaciones_servicio_al_cliente_usuario=Yii::$app->user->identity->id;
        $modelObservacion->observaciones_servicio_al_cliente_fecha_observacion=date("Y-m-d");
        $modelObservacion->observaciones_servicio_al_cliente_observacion=$_GET['observacion'];
        $modelObservacion->observaciones_servicio_al_cliente_servicio_al_cliente_id=$_GET['id'];

        if($modelObservacion->save() && $solicitud->save()){
             $mensaje.="<div>";
                $mensaje.="<center><h5>La Solicitud se Cerro Correctamente</h5></center>";
            $mensaje.="</div>";

            echo $mensaje;
        }else{
            echo $mensaje="<h5>Lo Sentimos La Gestion Presenta Un Error: ".print_r($modelObservacion->getErrors())."</h5>";
        }


        
    }
///////////////////////////////// APROBAR PARRA GESTION //////////////////////////////////
    public function actionAprobar()
    {

        $solicitud=ServicioAlCliente::findOne($_GET['id']);

        // echo "<pre>";
        // print_r($solicitud);
        // die();
        $mensaje="";
        $modelObservacion= new ObservacionesServicioAlCliente();
        
        $solicitud->servicio_al_cliente_check=1;
        $solicitud->servicio_al_cliente_operario_gestion=Yii::$app->user->identity->id;
        $modelObservacion->observaciones_servicio_al_cliente_usuario=Yii::$app->user->identity->id;
        $modelObservacion->observaciones_servicio_al_cliente_fecha_observacion=date("Y-m-d");
        $modelObservacion->observaciones_servicio_al_cliente_observacion=$_GET['observacion'];
        $modelObservacion->observaciones_servicio_al_cliente_servicio_al_cliente_id=$_GET['id'];

    

        if($modelObservacion->save() && $solicitud->save()){
            $mensaje.="<div>";
                $mensaje.="<center><h5>La Solicitud se Aprobo Correctamente</h5></center>";
            $mensaje.="</div>";

            echo $mensaje;
        }else{
            echo $mensaje="<h5>Lo Sentimos La Gestion Presenta Un Error: ".print_r($modelObservacion->getErrors())."</h5>";
        }


        
    }


/**
     * Finds the ServicioAlCliente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServicioAlCliente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServicioAlCliente::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////// ACCION PARA GENERAR EL LSITADO DE MUNICIPIOS ///////

    public function actionLists(){
        
        // print_r($_REQUEST['id']);
        $id=$_REQUEST['id'];
        $countMunicipios = Municipios::find()->where(['departamento_iddepartamento'=>$id])->count();

        $municipios = Municipios::find()->where(['departamento_iddepartamento'=>$id])->all();

        if($countMunicipios > 0){
            echo "<option value=''> [--Seleccione Municipio--]</option>";
            foreach ($municipios as $municipio) {
                echo "<option value='".$municipio->idMunicipio."'>".$municipio->nombre."</option>";
            }
        }else{
            echo "<option value=''>Opción Inválida</option>";
        }        
    }


///////////////////////////////////////////////////////////// REPORTE  /////////////////////////////////////////////////////////////////////////////////////

    public function actionReporte($idsolicitud){

        return $this->render('reporteindex',[
            'idsolicitud'=>$idsolicitud,
        ]);

    }



}
