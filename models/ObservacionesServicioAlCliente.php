<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "observaciones_servicio_al_cliente".
 *
 * @property integer $observaciones_servicio_al_cliente_id
 * @property integer $observaciones_servicio_al_cliente_usuario
 * @property string $observaciones_servicio_al_cliente_observacion
 * @property integer $observaciones_servicio_al_cliente_servicio_al_cliente_id
 *
 * @property Users $observacionesServicioAlClienteUsuario
 * @property ServicioAlCliente $observacionesServicioAlClienteServicioAlCliente
 */
class ObservacionesServicioAlCliente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'observaciones_servicio_al_cliente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['observaciones_servicio_al_cliente_usuario', 'observaciones_servicio_al_cliente_servicio_al_cliente_id','observaciones_servicio_al_cliente_fecha_observacion'], 'required'],
            [['observaciones_servicio_al_cliente_usuario', 'observaciones_servicio_al_cliente_servicio_al_cliente_id'], 'integer'],
            [['observaciones_servicio_al_cliente_observacion'], 'string'],
            [['observaciones_servicio_al_cliente_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['observaciones_servicio_al_cliente_usuario' => 'id']],
            [['observaciones_servicio_al_cliente_servicio_al_cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServicioAlCliente::className(), 'targetAttribute' => ['observaciones_servicio_al_cliente_servicio_al_cliente_id' => 'servicio_al_cliente_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'observaciones_servicio_al_cliente_id' => 'Observaciones Servicio Al Cliente ID',
            'observaciones_servicio_al_cliente_usuario' => 'Observaciones Servicio Al Cliente Usuario',
            'observaciones_servicio_al_cliente_observacion' => 'Observaciones Servicio Al Cliente Observacion',
            'observaciones_servicio_al_cliente_servicio_al_cliente_id' => 'Observaciones Servicio Al Cliente Servicio Al Cliente ID',
            'observaciones_servicio_al_cliente_fecha_observacion'=>'fecha Observacion'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservacionesServicioAlClienteUsuario()
    {
        return $this->hasOne(Users::className(), ['id' => 'observaciones_servicio_al_cliente_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservacionesServicioAlClienteServicioAlCliente()
    {
        return $this->hasOne(ServicioAlCliente::className(), ['servicio_al_cliente_id' => 'observaciones_servicio_al_cliente_servicio_al_cliente_id']);
    }
}
