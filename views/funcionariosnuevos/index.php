<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FuncionariosnuevosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Funcionarios Nuevos Pendientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funcionariosnuevos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Funcionarios nuevos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'funcionarios_nuevos_id',
            'funcionarios_nuevos_nombre',
            'funcionarios_nuevos_apellidos',
            // 'funcionarios_nuevos_tipodocumento',
            'funcionarios_nuevos_documento',
            // 'funcionarios_nuevos_direccion',
            // 'funcionarios_nuevos_email:email',
            // 'funcionarios_nuevos_telefono',
            // 'funcionarios_nuevos_tipocontrato',
            // 'funcionarios_nuevos_fechainiciocontrato',
            // 'funcionarios_nuevos_fechafincontrato',
            // 'funcionarios_nuevos_cargo',
            // 'funcionarios_nuevos_sucursal',
            // 'funcionarios_nuevos_permisosrecaudo',
            // 'funcionarios_nuevos_permisosvisita',
            // 'funcionarios_nuevos_permisosconcertacion',
            // 'funcionarios_nuevos_permisosdocumentos',
            // 'funcionarios_nuevos_permisoscontabilidad',
            // 'funcionarios_nuevos_permisosreportes',
            // 'funcionarios_nuevos_estado',
            // 'funcionarios_nuevos_aplicacion'

            ['class' => 'yii\grid\ActionColumn',
            'template'=> '{view}',
            'buttons'=> [
                'view' => function ($url, $model) {
                    return Html::a('', ['view','id'=>$model->funcionarios_nuevos_id], ['class' => 'fas fa-eye']);
                    },
            ]
            
            ],
        ],
    ]); ?>

</div>
