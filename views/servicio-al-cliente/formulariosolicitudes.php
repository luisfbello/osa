<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Municipios;
use app\models\Departamentos;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioAlCliente */
/* @var $form yii\widgets\ActiveForm */


$this->title = '';


?>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo Url::base(); ?>/css/bootstrap.min.css"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />


<h1><?= Html::encode($this->title) ?></h1>

<div class="card">
    <div class="card-header bg-primary" style="color:white">Formulario Registro de Solicitud</div>
    
    <div class="card-body">
        <div class="servicio-al-cliente-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="card">
            <div class="card-header bg-primary" style="color:white">Tipo Solicitud</div>
            <div class="card-body">
                <div class="col-lg-6">
                    <select name="solicitud" id="TipoSolicitud" class="form-control a">
                        <option value="#">-- Seleccione una Opcion --</option>
                        <!-- <option value="138">Solicitud Liquidacion</option> -->
                        <option value="139">Solicitud PQR</option>
                    </select>
                </div>
            </div>
            </div>
            <div id="formulario">
                    <!-- CARD DATOS PERSONALES -->
                    <div class="card">
                        <div class="card-header bg-primary" style="color: white">Datos Personales</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                <?= $form->field($model, 'servicio_al_cliente_nombre_usuario')->textInput(['class'=>'form-control a','maxlength' => true]) ?>
                                <?= $form->field($model, 'servicio_al_cliente_apellidos_usuario')->textInput(['class'=>'form-control a','maxlength' => true]) ?>
                                </div>
                                
                                <div class="col-lg-6">
                                <?= $form->field($model, 'servicio_al_cliente_tipoIdentificacion')->dropdownList([  12=>'Cedula',
                                                                                                                    13=>'Cedula Extrangeria'],
                                                                                                ['prompt'=>'[-- Seleccione una Opcion --]']) ?>
                                <?= $form->field($model, 'servicio_al_cliente_numero_documento')->textInput(['class'=>'form-control a','maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- CARD DATOS NEGOCIO -->
                    <div class="card">
                        <div class="card-header bg-primary" style="color: white">Datos del Negocio</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                <?= $form->field($model, 'servicio_al_cliente_razonsocial')->textInput(['class'=>'form-control a','maxlength' => true]) ?>
                                <?= $form->field($model, 'servicio_al_cliente_direccion')->textInput(['class'=>'form-control a','maxlength' => true]) ?>
                                <?= $form->field($model, 'servicio_al_cliente_telefono1')->textInput(['class'=>'form-control a']) ?>
                                </div>
                                
                                <div class="col-lg-6">
                                <?= $form->field($model, 'servicio_al_cliente_nit')->textInput(['class'=>'form-control a']) ?>
                                <?= $form->field($model, 'servicio_al_cliente_email')->textInput(['class'=>'form-control a','maxlength' => true]) ?>
                                <?= $form->field($model, 'servicio_al_cliente_telefono2')->textInput(['class'=>'form-control a']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- CARD DATOS UBICACION -->
                    <div class="card">
                        <div class="card-header bg-primary" style="color: white">Ubicacion</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                <?= Html::label('Departamentos','select-departamentos',['class'=>'control-label']) ?>
                                    <?= Html::dropdownList(
                                        'select-departamentos',
                                        '',
                                        ArrayHelper::map(Departamentos::find()->orderBy(['nombre'=>'asc'])->all(),'idDepartamento','nombre'),
                                        [
                                            'class'=>'form-control a',
                                            'prompt'=>'[-- Seleccione el Departamento --]',
                                            'value'=>'',
                                            'id'=> 'select-departamentos',
                                        //     'onchange'=>'$.post("index.php?r=servicio-al-cliente/lists&id='.'"+$(this).val(), function(data){
                                        //         $("#municipios").html(data);
                                        // })'
                                        ]
                                    ) ?>
                                </div>
                                
                                <div class="col-lg-6">
                                    <?= Html::label('Municipios', 'municipios', ['class'=>'control-label']) ?>
                                    <?= Html::dropDownList(
                                        'municipios',
                                        '',
                                        // ArrayHelper::map(Municipios::find()->all(), 'idMunicipio', 'nombre'),
                                        [],
                                        [
                                            'class'=>'form-control a',
                                            'prompt'=>'[-- Seleccione Municipios --]',
                                            'value'=>'',
                                            'id'=>'municipios',
                                            'required' => 'required'
                                        ]) 
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- CARD DATOS SOLICITUD PQR -->
                    <div class="card" id="observacion" >
                        <div class="card-header bg-primary" style="color: white">Registre su Peticion,Queja o Reclamo</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12" >
                                <?= $form->field($model, 'servicio_al_cliente_observacion')->textarea(['class'=>'form-control','rows' => 6]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Enviar Solicitud' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'enviar']) ?>
                    </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
    
</div>

<script>

$("#enviar").click(function(){

var validate = false;

$('.a').each(function(){
    if ($(this).val() == "" || $(this).val() == null ) {
        $(this).css("border", "1px solid red");
        validate = false;

    }else{
        $(this).css("border", "1px solid #ccc");
        validate = true;
    }
});

if(validate) {

    return true;
    $("#enviar").submit();

}else{
    
    confirm('ERROR: se debe seleccionar Todos los campos para continuar con la asignación');
    return false;
}
});

$('#datepicker').datepicker({
    uiLibrary:'bootstrap4'
});

$('#select-departamentos').change(function(){
    var id=$(this).val();

    $.ajax({
        type: "get",
        url: "index.php?r=servicio-al-cliente/lists",
        data:{
                'id':id
            },
        success: function(data){
            $("#municipios").html(data);
        }
    });
});




</script>
