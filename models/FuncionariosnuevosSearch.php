<?php

namespace app\models;


use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Solicitudes;
/**
 * This is the model class for table "funcionarios_nuevos".
 *
 * @property integer $funcionarios_nuevos_id
 * @property string $funcionarios_nuevos_nombre
 * @property string $funcionarios_nuevos_apellidos
 * @property integer $funcionarios_nuevos_tipodocumento
 * @property string $funcionarios_nuevos_documento
 * @property string $funcionarios_nuevos_direccion
 * @property string $funcionarios_nuevos_email
 * @property integer $funcionarios_nuevos_telefono
 * @property integer $funcionarios_nuevos_tipocontrato
 * @property string $funcionarios_nuevos_fechainiciocontrato
 * @property string $funcionarios_nuevos_fechafincontrato
 * @property integer $funcionarios_nuevos_cargo
 * @property integer $funcionarios_nuevos_sucursal
 * @property string $funcionarios_nuevos_permisosrecaudo
 * @property string $funcionarios_nuevos_permisosvisita
 * @property string $funcionarios_nuevos_permisosconcertacion
 * @property string $funcionarios_nuevos_permisosdocumentos
 * @property string $funcionarios_nuevos_permisoscontabilidad
 * @property string $funcionarios_nuevos_permisosreportes
 * @property integer $funcionarios_nuevos_estado
 * @property string $funcionarios_nuevos_aplicacion
 */
class FuncionariosNuevosSearch extends Funcionariosnuevos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'funcionarios_nuevos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['funcionarios_nuevos_nombre', 'funcionarios_nuevos_apellidos', 'funcionarios_nuevos_tipodocumento', 'funcionarios_nuevos_documento', 'funcionarios_nuevos_direccion', 'funcionarios_nuevos_telefono', 'funcionarios_nuevos_tipocontrato', 'funcionarios_nuevos_cargo', 'funcionarios_nuevos_sucursal'],'safe'],
            [['funcionarios_nuevos_tipodocumento',  'funcionarios_nuevos_tipocontrato', 'funcionarios_nuevos_cargo', 'funcionarios_nuevos_sucursal', 'funcionarios_nuevos_estado'], 'integer'],
            [['funcionarios_nuevos_fechainiciocontrato', 'funcionarios_nuevos_fechafincontrato'], 'safe'],
            [['funcionarios_nuevos_nombre', 'funcionarios_nuevos_apellidos'], 'string', 'max' => 50],
            [['funcionarios_nuevos_documento'], 'string', 'max' => 20],
            [['funcionarios_nuevos_direccion', 'funcionarios_nuevos_email','funcionarios_nuevos_telefono',], 'string', 'max' => 100],
            [['funcionarios_nuevos_aplicacion','funcionarios_nuevos_permisosrecaudo', 'funcionarios_nuevos_permisosvisita', 'funcionarios_nuevos_permisosconcertacion', 'funcionarios_nuevos_permisosdocumentos', 'funcionarios_nuevos_permisoscontabilidad', 'funcionarios_nuevos_permisosreportes'], 'safe'],
            // [['funcionarios_nuevos_aplicacion'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {   
        $query = Funcionariosnuevos::find()->where(['in','funcionarios_nuevos_estado',[1,0]]);
        
        // echo "<pre>";
        // print_r($query);
        // die();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'funcionarios_nuevos_id' => $this->funcionarios_nuevos_id,
            'funcionarios_nuevos_estado' => $this->funcionarios_nuevos_estado,
            
        ]);

        $query->andFilterWhere(['like', 'funcionarios_nuevos_nombre', $this->funcionarios_nuevos_nombre])
            ->andFilterWhere(['like', 'funcionarios_nuevos_apellidos', $this->funcionarios_nuevos_apellidos])
            ->andFilterWhere(['like', 'funcionarios_nuevos_documento', $this->funcionarios_nuevos_documento]);

        return $dataProvider;
    }
}
