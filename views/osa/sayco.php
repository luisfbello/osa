<?php

use yii\helpers\Html;

$this->title="SAYCO";
$this->params['breadcrumbs'][] = $this->title;

?>

<html>
<body>

<!-- begin about -->
<section>

    <div class="card">
        <div class="card-header alert alert-primary"><h1><?= Html::encode($this->title) ?></h1></div>
            <div class="card-body" >
                <div class="container">
                    <center>
                
                    <!-- begin logo -->
                    <section>
                        <div class="container">
                            <div id="osaimg" class="col-lg-12">
                                <?=Html::img('images/Sayco.png',['width'=> '600px']) ?>
                            </div>
                        </div>
                    </section>
                    </center>
                    <!-- end logo -->
                    <br>
                    <div style="text-align: justify">
                    <div class="card-header alert alert-primary"><h2>¿Quien es SAYCO?</h2></div>
                        
                        <p>SAYCO es una Sociedad de gestión colectiva de Derechos de Autor, sin ánimo de lucro, cuyo objeto principal 
                        es la recaudación y distribución de los derechos patrimoniales de autor, en virtud del simple acto de afiliación
                         y de los contratos de representación recíproca suscritos con sus Sociedades hermanas, generados por la Comunicación 
                         Pública y/o Reproducción de las obras musicales, literarias, teatrales, audiovisuales, de bellas artes, fotográficas 
                         y de arte aplicado, de titularidad de sus mandantes Nacionales y Extranjeros.</p>
                        <br>
                        <div class="card-header alert alert-primary"><h3>Misi&oacute;n</h3></div>
                        
                        <p>
                        Licenciar, Recaudar, Administrar y Distribuir Derechos de Autor de las obras de nuestros asociados en Colombia 
                        y en el extranjero a través de contratos de representación, dentro de los más altos estándares de excelencia, 
                        rigiéndonos por la política de transparencia total, buscando el bienestar de nuestros socios y colaboradores.
                        <br> 
                        </p>

                        <br>
                        <div class="card-header alert alert-primary"><h3>Visi&oacute;n</h3></div>
                        
                        <p>
                        Ser reconocidos como una Sociedad de Gestión Colectiva de categoría mundial, por sus procesos transparentes de recaudo y 
                        distribución de los derechos de autor; posicionándose por su alto sentido de responsabilidad con sus socios a través de programas 
                        de bienestar integrales que puedan aportar al mejoramiento de su calidad de vida.
                        </p>
                        <br>
                        <div class="card-header alert alert-primary"><h3>Pol&iacute;tica de calidad</h3></div>
                        
                        <p>
                        La política de SAYCO, es recaudar y distribuir oportunamente los derechos patrimoniales de los titulares nacionales y extranjeros 
                        cuyas obras administra la sociedad, con honestidad y responsabilidad, a través de una gestión efectiva y organizada, que contribuye 
                        al mejoramiento de la calidad de vida de sus asociados desarrollando programas de promoción y previsión social. <br><br>
                        Para todos sus usuarios, SAYCO garantiza la equidad en las tarifas establecidas y la transparencia en la distribución 
                        de los dineros recaudados. SAYCO ofrece a sus afiliados y usuarios una respuesta ágil y precisa respecto a sus necesidades. <br><br>
                        En cumplimiento de la misión, SAYCO asegura la competencia de su personal brindando capacitación, estabilidad y promoviendo su participación 
                        en el desarrollo de los procesos, para la mejora continua soportada en el sistema de gestión de calidad. 
                        </p>
                        <br>
                        <div class="card-header alert alert-primary"><h3>Nuestros Principios</h3></div>
                        <p>
                            <b>Compromiso</b>: El cumplimiento de nuestro deber está enmarcado por la dedicación y el esfuerzo de hacer 
                            las cosas bien de inicio a fin. <br><br>

                            <b>Honestidad</b>: Actuamos con rectitud e integridad en el desarrollo de las labores, asegurando la 
                            transparencia de la gestión de la organización.<br><br>

                            <b>Respeto</b>: Entendemos y valoramos las relaciones interpersonales entre socios, directivos y 
                            colaboradores.<br><br>

                            <b>Amabilidad</b>: Ofrecemos un servicio cálido y humano asegurando el buen trato, mostrando empatía e 
                            interés hacia las necesidades y expectativas de nuestros socios y clientes.<br><br>

                            <b>Amabilidad</b>: Todos nuestros esfuerzos están enfocados al acercamiento entre maestras, maestros, 
                            socios y clientes con SAYCO y su administración.<br><br>
                        </p>
                        <br>
                        <div class="card-header alert alert-primary"><h3>Nuestros Valores</h3></div>
                        <p>
                            Actuamos clara y coherentemente de acuerdo con las políticas y los objetivos trazados por la organización.<br><br>

                            <b>Equidad</b>: Procedemos con imparcialidad y proporcionalidad en las actuaciones frente a los procesos para garantizar 
                            la sostenibilidad de la organización en el largo plazo propiciando un ambiente de tranquilidad entre los socios.<br><br>

                            <b>Unidad</b>: Integramos los esfuerzos de todos los interesados en pro del beneficio y mejoramiento de Sayco.<br><br>

                            <b>Armonía</b>: Compromiso activo de los socios, directivos y olaboradores de Sayco en la generación de un ambiente 
                                agradable para facilitar el fortalecimiento y crecimiento de la Sociedad.<br><br>

                            <b>Eficacia</b>: Actuar con diligencia para cumplir con lo propuesto en el tiempo previsto.<br><br>
                        </p>

                    </div>
                </div>
            </div>
    </div>

</section>

</body>

</html>