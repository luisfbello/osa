
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\db\Connection;
use app\models\Users;
use app\models\Solicitudpermisos;

/* @var $this yii\web\View */
/* @var $searchModel app\models\solicitudpermisosSearch *
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Aprobadas Nomina';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="solicitudpermisos-index">
  <div class="container-fluid">
    <h3><div class= "alert alert-info" role="alert"><?= Html::encode($this->title) ?></div></h3>
        <!-- <div class= "panel panel-primary"></div> -->
      </div>
      <div class="card-body"> 
        <div class="card border-primary mb-3">
          <div class="card-header text-white" style="background-color: #337AB7;">Datos de usuario</div>
          <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
              <div class = "table-responsive">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],


                                  [
                                   'attribute'=>'nombres',
                                   'label'=>'Nombres',    
                                   'value'=>'users.nombres'
                                ],
                                [
                                'attribute'=>'apellidos',
                                'label'=>'Apellidos',    
                                'value'=>'users.apellidos',
                                ],
                                //  [
                                // 'attribute'=>'documento',
                                // 'label'=>'Documento',    
                                // 'value'=>'users.identificacion',
                                // ],
                                [
                                'attribute'=>'motivoId',
                                'label'=>'Motivo',    
                                'value'=>'tiposdetalles.nombre',
                                ],
                                // [
                                // 'attribute'=>'Estado',
                                // 'label'=>'Estado',
                                // 'value'=>'estado.nombre',
                                // ],
                                // [
                                //  'attribute'=>'Jefeinmediato',
                                //  'label'=>'Jefe inmediato',
                                //  'value'=>'Jefeinmediato.nombre'.' '.'apellidos',
                                // ],

                                'fechaCreacion',
                                //'idsolicitudpermisos',
                                //'usuarioIdusuario',
                                //'motivoId',
                                //'fechaInicio',
                                //'fechaFin',
                                //'fechaCreacion',
                                //'tiempoRepuesto',
                                //'aprobado',
                                //'anulado',
                                //'observaciones',

                                ['class' => 'yii\grid\ActionColumn',
                                'template'=> '{view}', 
                                'buttons' => [                                
                                    'view' => function ($url, $model) {                                                        
                                            return   Html::a('<button class="btn btn-success">Ver</button>', $url, [
                                                        'title' => Yii::t('app', 'ver solictud'),
                                                        'class'=>'pull-left',                              
                                            ]);
                                        }
                                
                                
                            ], 
                                ],
                            ],
                        ]); ?>
              </div>           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
