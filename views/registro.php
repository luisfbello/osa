<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Registronegocios */
/* @var $form ActiveForm */
?>
<div class="registro">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'registro_negocio_fecha') ?>
        <?= $form->field($model, 'registro_negocio_identificacion') ?>
        <?= $form->field($model, 'registro_negocio_nit_establecimiento') ?>
        <?= $form->field($model, 'registro_negocio_observacion') ?>
        <?= $form->field($model, 'registro_negocio_nombre') ?>
        <?= $form->field($model, 'registro_negocio_apellido') ?>
        <?= $form->field($model, 'registro_negocio_email') ?>
        <?= $form->field($model, 'registro_negocio_nombre_establecimiento') ?>
        <?= $form->field($model, 'registro_negocio_zona') ?>
        <?= $form->field($model, 'registro_negocio_telefono1') ?>
        <?= $form->field($model, 'registro_negocio_telefono2') ?>
        <?= $form->field($model, 'registro_negocio_departamento') ?>
        <?= $form->field($model, 'registro_negocio_ciudad') ?>
        <?= $form->field($model, 'registro_negocio_codigo_usuario') ?>
        <?= $form->field($model, 'registro_negocio_liquidado') ?>
        <?= $form->field($model, 'registro_negocio_se_agenda_visita') ?>
        <?= $form->field($model, 'registro_negocio_dias_sin_respuesta') ?>
        <?= $form->field($model, 'registro_negocio_pago') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- registro -->
