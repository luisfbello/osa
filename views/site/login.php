<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
]); ?>

    <div class="col-lg-12 form-login">
        <div class="col-lg-4 col-lg-offset-4 content-login">
                <div class="col-lg-12" style="height:15px"></div>
            <div class="col-lg-10 col-lg-offset-1">
                <center><h1>OsaIn</h1></center>
                <div class="col-lg-12 noPadding">
                    <div class="form-group has-feedback">
                        <?= $form->field($model, 'username')->textInput(['placeholder' => 'Usuario', 'class' => 'form-control'])->label(false) ?>
                        <i class="glyphicon glyphicon-user form-control-feedback-left form-control-feedback"></i>
                    </div>
                </div>
                <div class="col-lg-12 noPadding">
                    <div class="form-group has-feedback">
                        <i class="icon-key"></i>
                        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Contraseña', 'class' => 'form-control'])->label(false) ?>
                    </div>
                </div>
                <div class="col-lg-12 noPadding">
                    <?= $form->field($model, 'rememberMe')->checkbox()->label('Keep me logged on this computer') ?>
                </div>
                <div class="form-group col-lg-12 noPadding">
                        <br>
                        <div class="col-lg-12 noPadding ol-lg-offset-2">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-default btn-block text-bold', 'name' => 'login-button']) ?>
                        </div>
                </div>
                <div class="col-lg-12" style="height:30px"></div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
