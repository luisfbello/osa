<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitudes".
 *
 * @property integer $solicitud_id
 * @property string $solicitud_observacion
 * @property integer $solicitud_importancia
 * @property string $solicitud_usuario
 * @property string $solicitud_fecha
 */
class Solicitudes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'solicitudes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['solicitud_observacion', 'solicitud_usuario', 'solicitud_fecha'], 'required'],
            [['solicitud_importancia','solicitud_estado'], 'integer'],
            [['solicitud_fecha'], 'safe'],
            [['solicitud_observacion'], 'string', 'max' => 255],
            [['solicitud_usuario'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'solicitud_id' => 'ID',
            'solicitud_observacion' => 'Observacion',
            'solicitud_importancia' => 'Importancia',
            'solicitud_usuario' => 'Usuario',
            'solicitud_fecha' => 'Fecha',
            'solicitud_estado'=>'Estado'
        ];
    }
}
