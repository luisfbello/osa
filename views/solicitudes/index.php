<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SolicitudesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitudes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitudes-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Solicitudes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'solicitud_id',
            'solicitud_observacion',
            'solicitud_importancia',
            'solicitud_usuario',
            'solicitud_fecha',
            

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{view}',
                'buttons'=> [
                    'view' => function ($url, $model) {
                        return Html::a('', ['view','id'=>$model->solicitud_id], ['class' => 'fas fa-eye']);
                        },
                ]
            
            ],
        ],
    ]); ?>

</div>
