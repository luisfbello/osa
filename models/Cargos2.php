<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cargos".
 *
 * @property integer $idCargo
 * @property string $nombre
 * @property integer $activo
 * @property string $fechaCreacion
 */
class Cargos2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cargos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'activo'], 'required'],
            [['activo'], 'integer'],
            [['fechaCreacion'], 'safe'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCargo' => 'Id Cargo',
            'nombre' => 'Nombre',
            'activo' => 'Activo',
            'fechaCreacion' => 'Fecha Creacion',
        ];
    }
}
