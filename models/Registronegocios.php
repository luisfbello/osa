<?php

namespace app\models;
use yii\web\UploadedFile;

use Yii;

/**
 * This is the model class for table "registronegocios".
 *
 * @property integer $registro_negocio_id
 * @property string $registro_negocio_fecha
 * @property string $registro_negocio_nombre
 * @property string $registro_negocio_apellido
 * @property integer $registro_negocio_identificacion
 * @property string $registro_negocio_email
 * @property string $registro_negocio_telefono1
 * @property string $registro_negocio_telefono2
 * @property string $registro_negocio_nombre_establecimiento
 * @property string $registro_negocio_zona
 * @property integer $registro_negocio_nit_establecimiento
 * @property string $registro_negocio_departamento
 * @property string $registro_negocio_ciudad
 * @property string $registro_negocio_codigo_usuario
 * @property string $registro_negocio_liquidado
 * @property string $registro_negocio_se_agenda_visita
 * @property string $registro_negocio_observacion
 * @property string $registro_negocio_dias_sin_respuesta
 * @property string $registro_negocio_pago
 * @property string $registro_negocio_file1
 * @property string $registro_negocio_file2
 */
class Registronegocios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    // public $registro_negocio_file1;
    public static function tableName()
    {
        return 'registronegocios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['registro_negocio_fecha'], 'safe'],
            [['registro_negocio_identificacion', 'registro_negocio_nit_establecimiento','registro_negocio_zona'], 'integer'],
            [['registro_negocio_observacion'], 'string'],
            [['registro_negocio_file1'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, pdf', 'maxFiles' => 2],
            [['registro_negocio_nombre', 'registro_negocio_apellido', 'registro_negocio_email', 'registro_negocio_nombre_establecimiento'], 'string', 'max' => 255],
            [['registro_negocio_telefono1', 'registro_negocio_telefono2', 'registro_negocio_departamento', 'registro_negocio_ciudad', 'registro_negocio_codigo_usuario', 'registro_negocio_liquidado', 'registro_negocio_se_agenda_visita', 'registro_negocio_dias_sin_respuesta', 'registro_negocio_pago'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'registro_negocio_id' => 'Registro Negocio ID',
            'registro_negocio_fecha' => 'Registro Negocio Fecha',
            'registro_negocio_nombre' => 'Nombres',
            'registro_negocio_apellido' => 'Apellidos',
            'registro_negocio_identificacion' => 'Identificacion',
            'registro_negocio_email' => 'Correo Electronico',
            'registro_negocio_telefono1' => 'Telefono1',
            'registro_negocio_telefono2' => 'Telefono2',
            'registro_negocio_nombre_establecimiento' => 'Nombre Establecimiento',
            'registro_negocio_zona' => 'Zona',
            'registro_negocio_nit_establecimiento' => 'Nit Establecimiento',
            'registro_negocio_departamento' => 'Departamento',
            'registro_negocio_ciudad' => 'Ciudad',
            'registro_negocio_codigo_usuario' => 'Codigo Usuario',
            'registro_negocio_liquidado' => 'Liquidado',
            'registro_negocio_se_agenda_visita' => 'Se Agenda Visita',
            'registro_negocio_observacion' => 'Observación',
            'registro_negocio_dias_sin_respuesta' => 'Dias Sin Respuesta',
            'registro_negocio_pago' => 'Pago',
            'registro_negocio_file1' => 'File1',
        ];
    }

    public function getMunicipios()
    {
        return $this->hasOne(Municipios::className(), ['idMunicipio' => 'registro_negocio_ciudad']);
    }

    public function getDepartamentos()
    {
        return $this->hasOne(Departamentos::className(), ['idDepartamento' => 'registro_negocio_departamento']);
    }
  
}
