<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Registronegocios;

/**
 * RegistronegociosSearch represents the model behind the search form about `app\models\Registronegocios`.
 */
class RegistronegociosSearch extends Registronegocios
{
    /**
     * @inheritdoc
     */

    public $departamento_nombre;
    public $ciudad_nombre;


    public function rules()
    {
        return [
            [['registro_negocio_id', 'registro_negocio_identificacion', 'registro_negocio_nit_establecimiento'], 'integer'],
            [['registro_negocio_fecha', 'registro_negocio_nombre', 'registro_negocio_apellido', 'registro_negocio_email', 'registro_negocio_telefono1', 'registro_negocio_telefono2', 'registro_negocio_nombre_establecimiento', 'registro_negocio_zona', 'registro_negocio_departamento', 'registro_negocio_ciudad', 'registro_negocio_codigo_usuario', 'registro_negocio_liquidado', 'registro_negocio_se_agenda_visita', 'registro_negocio_observacion', 'registro_negocio_dias_sin_respuesta', 'registro_negocio_pago', 'registro_negocio_file1', 'file1', 'registro_negocio_file2','departamento_nombre','ciudad_nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Registronegocios::find();

        // add conditions that should always apply here

        $query->joinWith(['departamentos', 'municipios']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        

        $dataProvider->sort->attributes['departamento_nombre'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['departamentos.nombre' => SORT_ASC],
            'desc' => ['departamentos.nombre' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ciudad_nombre'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['municipios.nombres' => SORT_ASC],
            'desc' => ['municipios.nombres' => SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'registro_negocio_id' => $this->registro_negocio_id,
            'registro_negocio_fecha' => $this->registro_negocio_fecha,
            'registro_negocio_identificacion' => $this->registro_negocio_identificacion,
            'registro_negocio_nit_establecimiento' => $this->registro_negocio_nit_establecimiento,
        ]);

        $query->andFilterWhere(['like', 'registro_negocio_nombre', $this->registro_negocio_nombre])
            ->andFilterWhere(['like', 'registro_negocio_apellido', $this->registro_negocio_apellido])
            ->andFilterWhere(['like', 'registro_negocio_email', $this->registro_negocio_email])
            ->andFilterWhere(['like', 'registro_negocio_telefono1', $this->registro_negocio_telefono1])
            ->andFilterWhere(['like', 'registro_negocio_telefono2', $this->registro_negocio_telefono2])
            ->andFilterWhere(['like', 'registro_negocio_nombre_establecimiento', $this->registro_negocio_nombre_establecimiento])
            ->andFilterWhere(['like', 'registro_negocio_zona', $this->registro_negocio_zona])
            ->andFilterWhere(['like', 'registro_negocio_departamento', $this->registro_negocio_departamento])
            ->andFilterWhere(['like', 'registro_negocio_ciudad', $this->registro_negocio_ciudad])
            ->andFilterWhere(['like', 'registro_negocio_codigo_usuario', $this->registro_negocio_codigo_usuario])
            ->andFilterWhere(['like', 'registro_negocio_liquidado', $this->registro_negocio_liquidado])
            ->andFilterWhere(['like', 'registro_negocio_se_agenda_visita', $this->registro_negocio_se_agenda_visita])
            ->andFilterWhere(['like', 'registro_negocio_observacion', $this->registro_negocio_observacion])
            ->andFilterWhere(['like', 'registro_negocio_dias_sin_respuesta', $this->registro_negocio_dias_sin_respuesta])
            ->andFilterWhere(['like', 'registro_negocio_pago', $this->registro_negocio_pago])
            ->andFilterWhere(['like', 'registro_negocio_file1', $this->registro_negocio_file1])
            ->andFilterWhere(['like', 'file1', $this->file1])
            ->andFilterWhere(['like', 'registro_negocio_file2', $this->registro_negocio_file2]);
            $query->andFilterWhere(['like', 'departamentos.nombre', $this->departamento_nombre]);
            $query->andFilterWhere(['like', 'municipios.nombre', $this->ciudad_nombre]);

        return $dataProvider;
    }
}
