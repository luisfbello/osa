<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rback".
 *
 * @property integer $idrback
 * @property integer $idRol
 * @property integer $idControlador
 * @property integer $idAccion
 * @property integer $estadoRback
 *
 * @property Rol $idRol0
 * @property Acciones $idAccion0
 * @property Controladores $idControlador0
 */
class Rback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRol', 'idControlador', 'idAccion', 'estadoRback'], 'required'],
            [['idRol', 'idControlador', 'idAccion', 'estadoRback'], 'integer'],
            [['idRol'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['idRol' => 'id']],
            [['idAccion'], 'exist', 'skipOnError' => true, 'targetClass' => Acciones::className(), 'targetAttribute' => ['idAccion' => 'idaccion']],
            [['idControlador'], 'exist', 'skipOnError' => true, 'targetClass' => Controladores::className(), 'targetAttribute' => ['idControlador' => 'idcontrolador']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idrback' => 'Idrback',
            'idRol' => 'Id Rol',
            'idControlador' => 'Id Controlador',
            'idAccion' => 'Id Accion',
            'estadoRback' => 'Estado Rback',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRol0()
    {
        return $this->hasOne(Rol::className(), ['id' => 'idRol']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAccion0()
    {
        return $this->hasOne(Acciones::className(), ['idaccion' => 'idAccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdControlador0()
    {
        return $this->hasOne(Controladores::className(), ['idcontrolador' => 'idControlador']);
    }
}
