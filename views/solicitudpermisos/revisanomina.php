
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\db\Connection;
use app\models\Users;
use app\models\Solicitudpermisos;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\solicitudpermisosSearch *
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Revision Nomina';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="solicitudpermisos-index">
    <h3><div class= "alert alert-info" role="alert"><?= Html::encode($this->title) ?></div></h3>
    <p>
    <?php echo Html::a('Generar excel', ['report'], ['class' => 'btn btn-success']);?>
    </p>
        <!-- <div class= "panel panel-primary"></div> -->
      </div>
        <div class="card border-primary mb-3">
          <div class="card-header text-white" style="background-color: #337AB7;">Datos de usuario</div>
          <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
              <div class = "table-responsive">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            // 'prowOptions'=>function($model){
                            //   if($model->apruebanomina == 136){
                            //   return ["class" => "danger"];
                            //   }
                            // },
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],


                                  [
                                   'attribute'=>'nombres',
                                   'label'=>'Nombres',    
                                   'value'=>'users.nombres'
                                ],
                                [
                                'attribute'=>'apellidos',
                                'label'=>'Apellidos',    
                                'value'=>'users.apellidos',
                                ],
                                //  [
                                // 'attribute'=>'documento',
                                // 'label'=>'Documento',    
                                // 'value'=>'users.identificacion',
                                // ],
                                [
                                'attribute'=>'motivoId',
                                'label'=>'Motivo',    
                                'value'=>'tiposdetalles.nombre',
                                ],
                                // [
                                // 'attribute'=>'Estado',
                                // 'label'=>'Estado',
                                // 'value'=>'estado.nombre',
                                // ],
                                // [
                                //  'attribute'=>'Jefeinmediato',
                                //  'label'=>'Jefe inmediato',
                                //  'value'=>'Jefeinmediato.nombre'.' '.'apellidos',
                                // ],
                                [
                                 'attribute'=>'apruebanomina',
                                 'label'=>'Estado',
                                 'value'=>'revisanomina.nombre',
                                ],
                                'fechaCreacion',
                                //'idsolicitudpermisos',
                                //'usuarioIdusuario',
                                //'motivoId',
                                //'fechaInicio',
                                //'fechaFin',
                                //'fechaCreacion',
                                //'tiempoRepuesto',
                                //'aprobado',
                                //'anulado',
                                //'observaciones',

                                ['class' => 'yii\grid\ActionColumn',
                                'template'=> '{view}', 
                                'buttons' => [                                
                                    'view' => function ($url, $model) { 
                                            return Html::button('ver', 
                                            [
                                                'class' => 'pull-left btn-xs btn-block btn-success btn-revisar',
                                                'data-revisa' => $model->idsolicitudpermisos,
                                            ]);                                                                                                   
                                        }
                                
                                
                            ], 
                                ],
                            ],
                        ]); ?>
              </div>           
          </div>        

            <div class="modal fade" id="revisanomina" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Revisar solicitud</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body " id="containerrevisar" >                          
                        </div>
                        <div class="modal-footer">
                          <div class="form-group">
                              <div class="solicitudpermisos-form">
                              <?php $form = ActiveForm::begin(['action' => ['aprobarnom'], 'method' => 'get' ]); ?>                                
                              <input type="hidden" id="revisa1" name="id">                              
                              <div class="form-group">
                                <br>
                                  <button type="button" class="btn btn-warning" data-dismiss="modal">cerrar</button>
                                  <button id="brevisa" class="btn btn-success">aprobar</button>                           
                              </div>
                              <?php ActiveForm::end(); ?>
                          </div>
                        </div>                
                      </div>
                    </div>
                  </div>                
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
          $(document).ready(function(){
            $('.btn-revisar').click(function(event){
                $('#revisa1').val($(this).data('revisa'));                
             $.ajax({
                 url: '<?php echo Yii::$app->request->baseUrl ?>/?r=solicitudpermisos/revisar',
                 type : 'get',                 
                 data : {
                     id : $(this).data('revisa')
                 },
                 success: function(data){
                     $('#containerrevisar').html(data);                     
                     $('#revisanomina').modal();
                    //  console.log(data);
                 }
             });
             $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=solicitudpermisos/revisado',
                type : 'get',
                data: {
                    id: $(this).data('revisa')
                },
                success : function(data){
                    if(data==136)
                    {
                      $('#brevisa').show();
                    }else
                    {
                      $('#brevisa').hide();
                    }
                    
                }
             });
            });              
            $('#btn-solicitudnueva').click(function(){
            $('#solicitudnueva').submit();
        });
          });           
</script>
