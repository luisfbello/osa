<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Documentos;

/**
 * DocumentosSearch represents the model behind the search form about `app\models\Documentos`.
 */
class DocumentosSearch extends Documentos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['documento_id', 'documento_proceso_id', 'documento_carpeta_id'], 'integer'],
            [['documento_nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Documentos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'documento_id' => $this->documento_id,
            'documento_proceso_id' => $this->documento_proceso_id,
            'documento_carpeta_id' => $this->documento_carpeta_id,
        ]);

        $query->andFilterWhere(['like', 'documento_nombre', $this->documento_nombre]);

        return $dataProvider;
    }
}
