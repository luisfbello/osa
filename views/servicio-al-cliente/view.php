<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioAlCliente */

$this->title = $model->servicio_al_cliente_id;
$this->params['breadcrumbs'][] = ['label' => 'Servicio Al Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-al-cliente-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->servicio_al_cliente_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->servicio_al_cliente_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'servicio_al_cliente_id',
            'servicio_al_cliente_nombre_usuario',
            'servicio_al_cliente_apellidos_usuario',
            'servicio_al_cliente_tipoIdentificacion',
            'servicio_al_cliente_numero_documento',
            'servicio_al_cliente_razonsocial',
            'servicio_al_cliente_municipio',
            'servicio_al_cliente_departamento',
            'servicio_al_cliente_zona',
            'servicio_al_cliente_direccion',
            'servicio_al_cliente_nit',
            'servicio_al_cliente_telefono1',
            'servicio_al_cliente_telefono2',
            'servicio_al_cliente_email:email',
            'servicio_al_cliente_observacion:ntext',
            'servicio_al_cliente_tipo_solicitud',
            'servicio_al_cliente_fecha_solicitud',
            'servicio_al_cliente_estado_solicitud',
            'servicio_al_cliente_operario_gestion',
            'servicio_al_cliente_fecha_gestion',
            'servicio_al_cliente_observacion_gestion:ntext',
        ],
    ]) ?>

</div>
