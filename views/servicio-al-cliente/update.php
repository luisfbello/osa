<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioAlCliente */

$this->title = 'Update Servicio Al Cliente: ' . ' ' . $model->servicio_al_cliente_id;
$this->params['breadcrumbs'][] = ['label' => 'Servicio Al Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->servicio_al_cliente_id, 'url' => ['view', 'id' => $model->servicio_al_cliente_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="servicio-al-cliente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
