<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rback */

$this->title = $model->idrback;
$this->params['breadcrumbs'][] = ['label' => 'Rbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rback-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idrback], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idrback',
            'idRol',
            'idControlador',
            'idAccion',
            'estadoRback',
        ],
    ]) ?>

</div>
