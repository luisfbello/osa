<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Solicitudes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitudes-form">

    <?php $importancia=[1=>'1',2=>'2',3=>'3',4=>'4',5=>'5']; ?>
    <?php $cerrado=[0=>'Sin gestion',1=>'Realizado']; 
        if($model->solicitud_estado==0){
            $estado='Sin gestion';
        }else{
            $estado='Realizado';
        }
    ?>


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'solicitud_observacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'solicitud_importancia')->dropDownList($importancia,['pormp'=>'-Seleccione importancia-']) ?>

    <?= $form->field($model, 'solicitud_usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'solicitud_fecha')->textInput() ?>
    <?= $form->field($model,'solicitud_estado')->dropDownList($cerrado,['pormp'=>$estado])?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
