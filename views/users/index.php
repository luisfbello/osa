<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Usuarios sistema Express';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h2 class="alert alert-info"><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Registrar Usuario', ['site/register'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
               'class' => 'table table-striped table-bordered',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            'nombres',
            'apellidos',
            'numeroIdentificacion',
            'email:email',
            // 'activate',

            // 'id',
            // 'password',
            // 'authKey',
            // 'accessToken',
            // 'role',
            // 'foto',
            // 'direccion',
            // 'telefono',
            // 'tipoContrato',
            // 'fechaInicioContrato',
            // 'fechaFinContrato',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}',
                
            ],
        ],
    ]); ?>

</div>
