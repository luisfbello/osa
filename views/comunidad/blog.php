<?php

use yii\helpers\Html;

$this->title = 'Blog OSA';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<section>
    
        <div class="card-header alert alert-primary"><h1><?= Html::encode($this->title) ?></h1></div>
        <center>  
            <!-- begin logo -->
            <section>
                <div class="container">
                    <div id="osaimg" class="col-lg-12">
                        <?=Html::img('images/Sede.jpg',['width'=> '600px']) ?>
                    </div>
                </div>
            </section>
        </center>
        
</section>
<section>
    <div class="card-body">
        <div class="container">
            <center>
            <a href="documentos/circularNo22.pdf" target="_blank">DNDA: Vía libre para el recaudo de 
                Derechos de Autor y Conexos en el Transporte Público</a>
            <?=Html::img('images/circular22.png',['width'=> '600px']) ?>
            </center>
            <br>
            <p>
            El único ente gubernamental con facultades en materia de Derechos de Autor y Conexos, la <b>Dirección Nacional de 
            Derecho de Autor</b> (DNDA) publicó su <b>circular número 22</b> con fecha 20 de mayo de 2016 en la que convalidó  la gestión de 
            <b>la Organización Sayco Acinpro</b> (OSA), para llevar a cabo los recaudos pertinentes por Comunicación Pública de Obras 
            musicales y audiovisuales en los <b>Vehículos de Transporte Público a nivel nacional</b>. <br>
            </p>
        </div> 
    </div>

</section>
    

</div>
