<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_menu".
 *
 * @property integer $id
 * @property string $action
 * @property string $etiqueta
 * @property integer $id_opcion
 * @property integer $estado
 * @property integer $orden
 *
 * @property OpcionMenu $idOpcion
 */
class ItemMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_opcion'], 'integer'],
            [['action', 'etiqueta'], 'string', 'max' => 45],
            [['id_opcion'], 'exist', 'skipOnError' => true, 'targetClass' => OpcionMenu::className(), 'targetAttribute' => ['id_opcion' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Action',
            'etiqueta' => 'Etiqueta',
            'id_opcion' => 'Id Opcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOpcion()
    {
        return $this->hasOne(OpcionMenu::className(), ['id' => 'id_opcion']);
    }
}
