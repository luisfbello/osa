<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CarteleraVirtualSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carteleravirtual-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cartelera_virtual_id') ?>

    <?= $form->field($model, 'cartelera_virtual_imagen') ?>

    <?= $form->field($model, 'cartelera_virtual_texto') ?>

    <?= $form->field($model, 'cartelera_virtual_fecha_publicacion') ?>

    <?= $form->field($model, 'cartelera_virtual_activo') ?>

    <?php // echo $form->field($model, 'cartelera_virtual_titulo') ?>

    <?php // echo $form->field($model, 'cartelera_virtual_video') ?>

    <?php // echo $form->field($model, 'cartelera_virtual_link_video') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
