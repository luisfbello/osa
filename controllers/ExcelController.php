<?php

namespace app\controllers;
use yii\db\Query;
use yii\db\connection2;
use app\models\Users;
use Yii;

require(__DIR__ . '/../vendor/phpexcel/Classes/PHPExcel.php');
require(__DIR__ . '/../vendor/barcodemaster/src/BarcodeGeneratorHTML.php');

class ExcelController extends \yii\web\Controller
{
    public function actionMasiva()
    {
    	if (yii::$app->request->post()) {
    		return $this->render('index',[
    			'masiva' => yii::$app->request->post('masiva'),
    		]);	
    	}
        
    }

    public function actionBarcode(){

         // return $this->render('barcode');

       $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
       echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode('081223897', $generator::TYPE_CODE_128)) . '">';


    } 

    public function actionFuncionarioscreados(){

      $funcionarios=utf8_decode($_REQUEST['funcionarios']);
      $funcionarios=unserialize($funcionarios);

      if (isset($funcionarios)!="") {
        return $this->render('indexfuncionarioscreados',[
          'funcionarios'=>$funcionarios,
        ]);
      }

  }

  public function actionRegistronuevos(){

      // $registronuevo=Registronegocios::find();

      $query = new Query();
        $connection = Yii::$app->db;

        $query=$connection->createCommand('SELECT  registro_negocio_fecha,registro_negocio_nombre,registro_negocio_apellido,registro_negocio_identificacion,registro_negocio_email,
                                                  registro_negocio_telefono1,registro_negocio_telefono2,registro_negocio_nombre_establecimiento,registro_negocio_zona,registro_negocio_nit_establecimiento,municipios.nombre as registro_negocio_ciudad,
                                                    departamentos.nombre as registro_negocio_departamento,file1,registro_negocio_file2
                                            FROM registronegocios
                                                  join municipios on (municipios.idMunicipio=registro_negocio_ciudad)
                                                  join departamentos on (departamentos.idDepartamento=municipios.departamento_iddepartamento)
                                                ');
        $registronuevo=$query->queryAll();

     
        if (isset($registronuevo)!="") {
          return $this->render('indexregistronuevos',[
            'registronuevo'=>$registronuevo,
          ]);
        }



  }

  public function actionSolicitudpermisos(){

    // if (yii::$app->request->post() ){

      $filtro = null;
      $filtro.=((isset($_REQUEST['select-sucursal'])&& $_REQUEST['select-sucursal']!=""))? ' and idPuntoRecaudo ='.$_REQUEST['select-sucursal'] : null;      
      $filtro.=((isset($_REQUEST['select-funcionario'])&& $_REQUEST['select-funcionario']!=""))? ' and usuarioIdusuario ='.$_REQUEST['select-funcionario'] : null;
      $filtro.=((isset($_REQUEST['select-motivo'])&& $_REQUEST['select-motivo']!=""))? ' and motivoId ='.$_REQUEST['select-motivo'] : null;
      $filtro.=((isset($_REQUEST['select-aprobadas'])&& $_REQUEST['select-aprobadas']!=""))? ' and estadoId ='.$_REQUEST['select-aprobadas'] : null;
      $filtro.=((isset($_REQUEST['select-revisadas'])&& $_REQUEST['select-revisadas']!=""))? ' and apruebanomina ='.$_REQUEST['select-revisadas'] : null;
      $filtro.=((isset($_REQUEST['fcreador'])&& $_REQUEST['fcreador']!=""))? " and solicitudpermisos.fechaCreacion >='".$_REQUEST['fcreador'].' 00:00:00'."'" : null;    
      $filtro.=((isset($_REQUEST['fechaCreacion'])&& $_REQUEST['fechaCreacion']!=""))? " and solicitudpermisos.fechaCreacion <='".$_REQUEST['fechaCreacion'].' 23:59:59'."'" : null;
      // echo'<pre>'  ;
      // print_r($filtro);
      // die(); 
      $query= new Query();
      $connection = Yii::$app->db;

      $query=$connection->createCommand("select funcionario, jefe_inmediato,sucursal,motivo,aprueba_jefe,Revisa_nomina,fechaInicio,fechaFin,
                                                case when horaInicio !=0 and horaFin !=0 and diacompleto=0 and Total_dias=1 then 0 else Total_dias end as Total_dias,
                                                horaInicio,horaFin,
                                                case when horaInicio is not null and horaFin is not null and diacompleto=0 then cast((CAST(timediff(horaFin,horaInicio) AS signed)/10000) as decimal(6,2))
                                                    when horaInicio is null || horaInicio=0 and horaFin is null || horaFin=0 and diacompleto=1 then Total_dias*9 end as Total_horas ,
                                                    Fcreacion,observaciones
                                          from (
                                                SELECT (select concat_ws (' ',users.nombres,users.apellidos) from users where users.id=usu) as funcionario,
                                                        concat_ws (' ',nombre_jefe,apellido_jefe) as jefe_inmediato,sucursal,motivo,aprueba_jefe,Revisa_nomina,
                                                        fechaInicio,fechaFin,sum(total_dias+1) as Total_dias,Ids,
                                                        Fcreacion,horaInicio,horaFin,observaciones,diacompleto
                                                from (
                                                        SELECT usuarioIdusuario as usu,estadoId as estID,fechaInicio,fechaFin,horaInicio,horaFin,idsolicitudpermisos as Ids,
                                                              DATEDIFF(fechaFin, fechaInicio) as total_dias,diacompleto,
                                                              case when usuarioAutoriza=users.id then users.nombres end as nombre_jefe,
                                                              case when usuarioAutoriza=users.id then users.nombres end as apellido_jefe,
                                                              case when idTipoDetalle=motivoId then tiposdetalles.nombre end as motivo,
                                                              case when estadoId=123 then 'Aprobado' 
                                                                    when estadoId=122 then 'Pendiente' end as aprueba_jefe,
                                                              case when apruebanomina=135 then 'Revisado'
                                                                    when apruebanomina=136 then 'Pendiente' end as Revisa_nomina,
                                                              solicitudpermisos.fechaCreacion as Fcreacion,puntosrecaudo.nombre as sucursal,observaciones
                                                      from     solicitudpermisos
                                                              join users on (usuarioAutoriza=users.id)
                                                              join tiposdetalles on (motivoId=idTipoDetalle)
                                                              join puntosrecaudo on (users.puntosrecaudo_idpuntosrecaudo=puntosrecaudo.idPuntoRecaudo)
                                                      where 1=1	
                                                      $filtro												
                                                  ) as d
                                                  group by Ids
                                          ) as t");                                       

      $solicitudnueva=$query->queryAll();
      // echo'<pre>'  ;
      // print_r($solicitudnueva);
      // die();
      
      if ($solicitudnueva) {
        return $this->render('indexsolicitudpermisos',[          
          'general'=>$solicitudnueva,
        ]);
    } else {   

            echo ("No se encontraron resultados");
                        
    }

    

    
    }
                                           
  // }

  public function actionFuncionarios($id){ 
    
      $users = Users::find()
          ->where(['puntosrecaudo_idpuntosrecaudo'=>$id]) 
          ->andwhere('jefe_inmediato not in (1)')         
          ->all();
          // echo '<pre>';
          //  print_r($users);
          // die();
              
         
      if($users){        
          echo "<option value=''>[--Seleccione funcionario --]</option>";
          foreach ($users as $user1) {               
              echo "<option value='".$user1->id."'>".$user1->nombres." $user1->apellidos</option>";            
          }
      }else{
            echo "<option value=''>[-- Seleccione Sucursales --]</option>";
      }        
  }

  public function actionReporteservicioalcliente(){
      // echo "<pre>";
      // print_r($_REQUEST);
      // die();
      $filtro=null;


      if (isset($_REQUEST)) {

        $filtro .= ((isset($_REQUEST['select-departamentos']) && $_REQUEST['select-departamentos'] != ""))? ' and servicio_al_cliente_departamento = '.$_REQUEST['select-departamentos'] : null;
        $filtro .= ((isset($_REQUEST['municipios']) && $_REQUEST['municipios'] != ""))? ' and servicio_al_cliente_municipio = '.$_REQUEST['municipios'] : null;
        $filtro .= ((isset($_REQUEST['idsolicitud']) && $_REQUEST['idsolicitud'] != ""))? ' and servicio_al_cliente_tipo_solicitud = '.$_REQUEST['idsolicitud'] : null;
        
        $query = new Query();

        $connection = Yii::$app->db;
        
        $query = $connection->createCommand("SELECT servicio_al_cliente_zona,
                                                    (select nombre
                                                    from municipios
                                                    where idMunicipio=servicio_al_cliente_municipio) as municipio,
                                                    (select nombre
                                                    from departamentos
                                                    where idDepartamento=servicio_al_cliente_departamento) as departamento,
                                                    concat(servicio_al_cliente_nombre_usuario,' ',servicio_al_cliente_apellidos_usuario) as usuario,
                                                (select nombre 
                                                from tiposdetalles
                                                where idTipoDetalle=servicio_al_cliente_tipoIdentificacion
                                                ) as tipodocumento,
                                                    servicio_al_cliente_numero_documento,
                                                    servicio_al_cliente_razonsocial,
                                                    servicio_al_cliente_nit,
                                                    servicio_al_cliente_direccion,
                                                    servicio_al_cliente_telefono1,
                                                    servicio_al_cliente_telefono2,
                                                    servicio_al_cliente_email,
                                                    servicio_al_cliente_fecha_solicitud,
                                                    servicio_al_cliente_observacion,
                                                    (select concat(nombres,' ',apellidos) as operario
                                                    from users
                                                        join observaciones_servicio_al_cliente on (observaciones_servicio_al_cliente_usuario=id)
                                                    where observaciones_servicio_al_cliente_servicio_al_cliente_id=servicio_al_cliente_id
                                                    order by 1 desc limit 1) as operarioGestiona,
                                                    (select observaciones_servicio_al_cliente_observacion 
                                                      from observaciones_servicio_al_cliente
                                                          where observaciones_servicio_al_cliente_servicio_al_cliente_id=servicio_al_cliente_id
                                                          order by 1 desc limit 1) as UltimaObservacion
                                                    
                                                    
                                                                                                                                                    
                                            FROM servicio_al_cliente
                                            where 1=1
                                                $filtro
                                              and servicio_al_cliente_estado_solicitud=1
                                            order by servicio_al_cliente_id desc");

        $solicitudes=$query->queryAll();

        // echo "<pre>";
        // // print_r($query);
        // print_r($solicitudes);
        // die();
        if ($solicitudes) {
          return $this->render('indexservicioalcliente',[
            'data'=>$solicitudes,
            'idsolicitud'=>$_REQUEST['idsolicitud'],
          ]);
        }


      }
      

  }

}
