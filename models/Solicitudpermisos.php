<?php

namespace app\models;

use Yii;
use app\models\Tiposdetalles;
use app\models\Users;
use app\models\Incapacidades;

/**
 * This is the model class for table "solicitudpermisos".
 *
 * @property int $idsolicitudpermisos
 * @property int $usuarioIdusuario
 * @property int $motivoId
 * @property string $fechaInicio
 * @property string $fechaFin
 * @property string $fechaCreacion
 * @property string $tiempoRepuesto
 * @property string $estado
 * @property int $anulado
 * @property string $observaciones
 * * @property string $incapacidadesIdincapacidad
 *
 * @property TiposDetalles $motivo
 * @property Incapacidades $ruta
 * @property Users $usuarioIdusuario0
 */
class Solicitudpermisos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'solicitudpermisos';
    }

    /**
     * {@inheritdoc
     */
    public function rules()
    {
        return [
            [['fechaCreacion','motivoId'], 'required'],
            [['idsolicitudpermisos', 'usuarioIdusuario','estadoId','usuarioAutoriza'], 'integer'],
            [['fechaInicio', 'fechaFin','horaInicio','horaFin', 'fechaCreacion', 'motivoId','diacompleto'], 'safe'],
            [['tiempoRepuesto'], 'string', 'max' => 200],
            [['observaciones','observacionJefeInmediato'], 'string', 'max' => 1000],
            [['idsolicitudpermisos'], 'unique'],
            [['usuarioIdusuario'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['usuarioIdusuario' => 'id']],
            [['usuarioAutoriza'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['usuarioAutoriza' => 'id']],
            [['incapacidad'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],            
        ];
    }
    public function upload()
    {
        if($this->validate()){
            $this->incapacidad->saveAs('images/' .$this->incapacidad->baseName.'.'.$this->incapacidad->extension);
            return true;            
        }else{
            return false;
        }
    }
    public function validacion()
    {
        if($this->incapacidad->extensions != ('png, jpg, pdf'))
        {
            echo "Archivos no validos";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idsolicitudpermisos' => 'Idsolicitudpermisos',
            'usuarioIdusuario' => 'Usuario Idusuario',
            'motivoId' => 'Motivo',
            'fechaInicio' => 'Fecha inicio',
            'fechaFin' => 'Fecha Fin',
            'fechaCreacion' => 'fecha creacion',
            'tiempoRepuesto' => 'tiempo repuesto',
            'estadoId' => 'Estado',
            'observaciones' => 'Observacion',
            'usuarioAutoriza'=> 'Jefe inmediato',
            'observacionJefeInmediato'=> 'observacion',
            'incapacidadesIdincapacidad' => 'idincapacidad',
            'horaInicio'=> 'Hora inicio',
            'horaFin'=> 'Hora fin',
            'diacompleto'=>'Dia completo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiposdetalles()
    {
        return $this->hasOne(tiposdetalles::className(), ['idTipoDetalle' => 'motivoId']);
    }

    public function getEstado()
    {
       return $this->hasOne(tiposdetalles::className(), ['idTipoDetalle' => 'estadoId']); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(users::className(), ['id' => 'usuarioIdusuario']);
    }

    public function getJefeinmediato()
    {
        return $this->hasOne(users::className(), ['id'=>'usuarioAutoriza']);
    }

    public function getRevisanomina()
    {
       return $this->hasOne(tiposdetalles::className(), ['idTipoDetalle' => 'apruebanomina']);
    }

    public function getIncapacidades()
    {
        return $this->hasOne(incapacidades::className(), ['solicitudpermisosIdsolicitudpermisos'=>'idsolicitudpermisos']);
    }

}
