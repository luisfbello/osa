<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documentos".
 *
 * @property integer $documento_id
 * @property string $documento_nombre
 * @property integer $documento_proceso_id
 * @property integer $documento_carpeta_id
 */
class Documentos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documentos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['documento_proceso_id', 'documento_carpeta_id'], 'integer'],
            [['documento_nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'documento_id' => 'Documento ID',
            'documento_nombre' => 'Documento Nombre',
            'documento_proceso_id' => 'Documento Proceso ID',
            'documento_carpeta_id' => 'Documento Carpeta ID',
        ];
    }
}
