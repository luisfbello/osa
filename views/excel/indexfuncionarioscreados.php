<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;

$objPHPExcel = new PHPExcel(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");
	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$objPHPExcel->getActiveSheet()->getStyle("A1:AB1")->getFont()->setBold(true);
   	foreach(range('A','Z') as $columnID) {
   	    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	        ->setAutoSize(true);
   	}
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A1', 'NOMBRES')
	            ->setCellValue('B1', 'APELLIDOS')
	            ->setCellValue('C1', 'DOCUMENTO')
	            ->setCellValue('D1', 'DIRECCIÓN')
	            ->setCellValue('E1', 'EMAIL')
	            ->setCellValue('F1', 'TELEFONO')
	            ->setCellValue('G1', 'FECHA INICIO CONTRATO')
				->setCellValue('H1', 'FECHA FIN CONTRATO')
				;
	$count = 2;
	foreach ($funcionarios as $m) {

		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $m['funcionarios_nuevos_nombre'])
		            ->setCellValue('B'.$count, $m['funcionarios_nuevos_apellidos'])
		            ->setCellValue('C'.$count, $m['funcionarios_nuevos_documento'])
		            ->setCellValue('D'.$count, $m['funcionarios_nuevos_direccion'])
		            ->setCellValue('E'.$count, $m['funcionarios_nuevos_email'])
		            ->setCellValue('F'.$count, $m['funcionarios_nuevos_telefono'])
		            ->setCellValue('G'.$count, $m['funcionarios_nuevos_fechainiciocontrato'])
		            ->setCellValue('H'.$count, $m['funcionarios_nuevos_fechafincontrato']);

		$count++;
	}
	$objPHPExcel->getActiveSheet()->setTitle('REPORTE_FUNCIONARIOS_CREADOS');
	$objPHPExcel->setActiveSheetIndex(0);
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="REPORTE_FUNCIONARIOS_CREADOS'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

 ?>