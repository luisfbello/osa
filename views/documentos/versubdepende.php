<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auditoria';
$this->params['breadcrumbs'][] = $this->title;
// echo "<pre>";
// print_r($depende);die;
?>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>

<div class="documentos-index">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>
    <div class="card ">
        <div class="card-header alert alert-primary"><?php echo $proceso[0]['proceso_nombre'] . " / " . $depende[0]['carpeta_nombre'] . " / " . $final[0]['carpeta_nombre'] ?></div>
        <div class="card-body">

            <?php if (!Yii::$app->user->isGuest) { ?>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                    Cargar Documentos
                </button><br>
                <!-- <div class="createFolder">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#folder">
                        Agregar carpeta <i class="fas fa-folder-plus"></i>
                    </button>
                </div> -->
            <?php } ?>

            <?php if ($documentos != null) {  ?>
                <br>
                <table class="table table-bordered table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre Archivo</th>
                            <th scope="col">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php



                            $contador = 1;
                            foreach ($documentos as $keyC => $valueC) { ?>
                            <tr>
                                <td>
                                    <?php echo $contador ?>
                                </td>
                                <td>
                                    <a download="<?php echo $valueC['documento_nombre'] ?>" href="<?php echo Url::base(); ?>/<?php echo $valueC['documento_ruta']  ?>"  class=""><?php echo $valueC['documento_nombre'] ?></a>
                                </td>
                                <td>
                                    <center>
                                        <button class="btn btn-outline-danger btnEliminar1" data-toggle="modal" id="<?php echo $valueC['documento_id'] ?>" data-target="#eliminar">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </center>
                                </td>

                            </tr>
                        <?php $contador++;
                            }
                        } else { ?>
                        <br>
                        <div class="alert alert-warning">No existen archivos en esta Ruta</div>
                    <?php } ?>
                    </tbody>
                </table>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Carga de Documentos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'options' => ["enctype" => "multipart/form-data"],
                    'action' => ['documentos/carga'],
                ]); ?>
                <div class="modal-body">
                    <input type="hidden" name="proceso" value="<?php echo $proceso[0]['proceso_id'] ?>">
                    <input type="hidden" name="carpeta" value="<?php echo $final[0]['carpeta_id'] ?> ">
                    <input type="hidden" name="esdepende" value="<?php echo $depende[0]['carpeta_id'] ?> ">

                    <div>
                        <?= $form->field($model, "documento_nombre[]")->fileInput(['multiple' => true])->label(false); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Subir Documento</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Eliminación de Documentos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Una Eliminado el archivo no se podra recuperar y debera Cargarlo nuevamente</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="idarchivo">
                <button type="button" id="eliminarArchivo" class="btn btn-danger">Eliminar Archivo</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>





<script>
    $(document).ready(function() {

        $('.btnEliminar1').click(function() {
            $('#idarchivo').val($(this).attr('id'));
        });

        $('#eliminarArchivo').click(function() {

            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=documentos/eliminardocumento',
                type: 'get',
                data: {
                    id: $('#idarchivo').val(),
                },
                success: function(data) {

                    location.reload();
                }
            }); // fin ajax
        });

        // agregar carpeta

        // $('#guardar').click(function() {

        //     var nombrecarpeta = $('#nomfolder').val();

        //     var idproceso = $('#idproceso').val();

        //     var iddepende = $('#idDepende').val();

        //     var url = '<?php echo Yii::$app->request->baseUrl ?>/index.php?r=documentos/addfolder';

        //     $.ajax({

        //         type: "get",
        //         url: url,
        //         data: {
        //             idproceso: idproceso,
        //             iddepende: iddepende,
        //             nombrecarpeta: nombrecarpeta,
        //         },
        //         success: function(data) {

        //             location.reload();
        //         }

        //     }); //fin ajax agregar carpeta
        // });

    });
</script>