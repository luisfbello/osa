<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicioAlClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitudes PQR';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-al-cliente-index">

    <div class="card">
        <div class="card-header bg-warning text-white">
            <center><h5>SOLICITUD PQR</h5></center> 
        </div>
        <div class="card-body">
            <div class="row">
            <div class="col-lg-6">
                <div style="border: solid #D82A2A; border-left: 10px solid #D82A2A;border-radius:15px;">
                    <center><h1 class="fa fa-exclamation-circle text-danger"></h1></center>
                    <div class="container">
                        <center><h2>Pendientes</h2></center>
                        <br>
                        <center><span class="badge"># <?php echo $pendientes ?></span></center>
                        <br>
                        <?php echo html::a('Ver',['servicio-al-cliente/pendientespqr', 'idsolicitud' => $idsolicitud,'estado'=>0],['class' => 'btn btn-block btn-danger','style'=>'margin: 5px']) ?>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
            <div style="border: solid #4DBC63; border-left: 10px solid #4DBC63;border-radius:15px;">
                    <center><h1 class="fa fa-file-pdf text-success"></h1></center>
                    <div class="container">
                        <center><h2>Gestionadas</h2></center>
                        <br>
                        <center><span class="badge"># <?php echo $gestionadas ?></span></center>
                        <br>
                        <?php echo html::a('Ver',['servicio-al-cliente/gestionadaspqr', 'idsolicitud' => $idsolicitud,'estado'=>1],['class' => 'btn btn-block btn-success','style'=>'margin: 5px']) ?>
                    </div>
                </div>
            </div>

            </div>
            <div>
            <?php echo html::a('Volver',['servicio-al-cliente/servicioclienteindex'],['class' => 'btn btn-block btn-info','style'=>'margin-top: 30px']) ?>
            </div>
        </div>

    </div>  

</div>
