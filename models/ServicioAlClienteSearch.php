<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ServicioAlCliente;

/**
 * ServicioAlClienteSearch represents the model behind the search form about `app\models\ServicioAlCliente`.
 */
class ServicioAlClienteSearch extends ServicioAlCliente
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['servicio_al_cliente_id', 'servicio_al_cliente_tipoIdentificacion', 'servicio_al_cliente_municipio', 'servicio_al_cliente_departamento', 'servicio_al_cliente_zona', 'servicio_al_cliente_nit', 'servicio_al_cliente_telefono1', 'servicio_al_cliente_telefono2', 'servicio_al_cliente_tipo_solicitud', 'servicio_al_cliente_estado_solicitud', 'servicio_al_cliente_operario_gestion'], 'integer'],
            [['servicio_al_cliente_nombre_usuario', 'servicio_al_cliente_apellidos_usuario', 'servicio_al_cliente_numero_documento', 'servicio_al_cliente_razonsocial', 'servicio_al_cliente_direccion', 'servicio_al_cliente_email', 'servicio_al_cliente_observacion', 'servicio_al_cliente_fecha_solicitud', 'servicio_al_cliente_fecha_gestion', 'servicio_al_cliente_observacion_gestion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$idsolicitud,$estado,$zona,$check)
    {
        if($zona != 0){
            $query = ServicioAlCliente::find()
                ->where(['servicio_al_cliente_tipo_solicitud'=>$idsolicitud])
                ->andwhere(['servicio_al_cliente_estado_solicitud'=>$estado])
                ->andWhere(['servicio_al_cliente_zona'=>$zona])
                ->andWhere(['servicio_al_cliente_check'=>$check]);
        }else{
            $query = ServicioAlCliente::find()
                ->where(['servicio_al_cliente_tipo_solicitud'=>$idsolicitud])
                ->andwhere(['servicio_al_cliente_estado_solicitud'=>$estado]);
        }
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'servicio_al_cliente_id' => $this->servicio_al_cliente_id,
            'servicio_al_cliente_tipoIdentificacion' => $this->servicio_al_cliente_tipoIdentificacion,
            'servicio_al_cliente_municipio' => $this->servicio_al_cliente_municipio,
            'servicio_al_cliente_departamento' => $this->servicio_al_cliente_departamento,
            'servicio_al_cliente_zona' => $this->servicio_al_cliente_zona,
            'servicio_al_cliente_nit' => $this->servicio_al_cliente_nit,
            'servicio_al_cliente_telefono1' => $this->servicio_al_cliente_telefono1,
            'servicio_al_cliente_telefono2' => $this->servicio_al_cliente_telefono2,
            'servicio_al_cliente_tipo_solicitud' => $this->servicio_al_cliente_tipo_solicitud,
            'servicio_al_cliente_fecha_solicitud' => $this->servicio_al_cliente_fecha_solicitud,
            'servicio_al_cliente_estado_solicitud' => $this->servicio_al_cliente_estado_solicitud,
            'servicio_al_cliente_operario_gestion' => $this->servicio_al_cliente_operario_gestion,
            'servicio_al_cliente_fecha_gestion' => $this->servicio_al_cliente_fecha_gestion,
        ]);

        $query->andFilterWhere(['like', 'servicio_al_cliente_nombre_usuario', $this->servicio_al_cliente_nombre_usuario])
            ->andFilterWhere(['like', 'servicio_al_cliente_apellidos_usuario', $this->servicio_al_cliente_apellidos_usuario])
            ->andFilterWhere(['like', 'servicio_al_cliente_numero_documento', $this->servicio_al_cliente_numero_documento])
            ->andFilterWhere(['like', 'servicio_al_cliente_razonsocial', $this->servicio_al_cliente_razonsocial])
            ->andFilterWhere(['like', 'servicio_al_cliente_direccion', $this->servicio_al_cliente_direccion])
            ->andFilterWhere(['like', 'servicio_al_cliente_email', $this->servicio_al_cliente_email])
            ->andFilterWhere(['like', 'servicio_al_cliente_observacion', $this->servicio_al_cliente_observacion])
            ->andFilterWhere(['like', 'servicio_al_cliente_observacion_gestion', $this->servicio_al_cliente_observacion_gestion]);

        return $dataProvider;
    }
}
