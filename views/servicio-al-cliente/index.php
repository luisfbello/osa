<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicioAlClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Servicio Al Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-al-cliente-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Servicio Al Cliente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'servicio_al_cliente_id',
            'servicio_al_cliente_nombre_usuario',
            'servicio_al_cliente_apellidos_usuario',
            'servicio_al_cliente_tipoIdentificacion',
            'servicio_al_cliente_numero_documento',
            // 'servicio_al_cliente_razonsocial',
            // 'servicio_al_cliente_municipio',
            // 'servicio_al_cliente_departamento',
            // 'servicio_al_cliente_zona',
            // 'servicio_al_cliente_direccion',
            // 'servicio_al_cliente_nit',
            // 'servicio_al_cliente_telefono1',
            // 'servicio_al_cliente_telefono2',
            // 'servicio_al_cliente_email:email',
            // 'servicio_al_cliente_observacion:ntext',
            // 'servicio_al_cliente_tipo_solicitud',
            // 'servicio_al_cliente_fecha_solicitud',
            // 'servicio_al_cliente_estado_solicitud',
            // 'servicio_al_cliente_operario_gestion',
            // 'servicio_al_cliente_fecha_gestion',
            // 'servicio_al_cliente_observacion_gestion:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
