<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "procesos".
 *
 * @property integer $proceso_id
 * @property string $proceso_nombre
 */
class Procesos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'procesos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proceso_nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'proceso_id' => 'Proceso ID',
            'proceso_nombre' => 'Proceso Nombre',
        ];
    }
}
