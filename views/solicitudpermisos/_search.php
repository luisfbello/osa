<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\solicitudpermisosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitudpermisos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idsolicitudpermisos') ?>

    <?= $form->field($model, 'usuarioIdusuario') ?>

    <?= $form->field($model, 'motivoId') ?>

    <?= $form->field($model, 'fechaInicio') ?>

    <?= $form->field($model, 'fechaFin') ?>

    <?php // echo $form->field($model, 'fechaCreacion') ?>

    <?php // echo $form->field($model, 'tiempoRepuesto') ?>

    <?php // echo $form->field($model, 'aprobado') ?>

    <?php // echo $form->field($model, 'anulado') ?>

    <?php // echo $form->field($model, 'observaciones') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
