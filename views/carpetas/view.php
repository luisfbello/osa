<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Carpetas */

$this->title = $model->carpeta_id;
$this->params['breadcrumbs'][] = ['label' => 'Carpetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carpetas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->carpeta_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->carpeta_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'carpeta_id',
            'carpeta_nombre',
            'carpeta_proceso_id',
        ],
    ]) ?>

</div>
