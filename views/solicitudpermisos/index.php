<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\db\Connection;
use app\models\Users;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Cargos;
use app\models\Puntosrecaudo;
use app\models\solicitudpermisos;
use app\models\Tiposdetalles;

/* @var $this yii\web\View */
/* @var $searchModel app\models\solicitudpermisosSearch *
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Solicitud de permisos';
$this->params['breadcrumbs'][] = $this->title;
?>
 <script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>
<div class="solicitudpermisos-index">                   
            <h3><div class= "alert alert-info" role="alert"><?= Html::encode($this->title) ?></div></h3>                    
            <?php if($valusu){  ?>                        
                <p>
                    <?php if(!$pendientes1 || !$model){
                            echo Html::a('Nueva solicitud', ['create'], ['class' => 'btn btn-success']); 
                        } else { 
                            ?>            
                                <div class="alert alert-warning" role="alert">
                                !Tiene solicitudes pendientes!
                                </div>
                    <?php } ?>                 
                </p>      
            
        <div class="card border-primary mb-3">
            <div class="card-header text-white" style="background-color: #337AB7;">Datos de usuario</div>
            <div class="card-body">
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                        <div class = "table-responsive">                        
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],                                
                                // [
                                // 'attribute'=>'nombres',
                                // 'label'=>'Nombres',    
                                // 'value'=>'users.nombres'
                                // ],
                                //  [
                                // 'attribute'=>'apellidos',
                                // 'label'=>'Apellidos',    
                                // 'value'=>'users.apellidos',
                                // ],
                                //  [
                                // 'attribute'=>'documento',
                                // 'label'=>'Documento',    
                                // 'value'=>'users.identificacion',
                                // ],                                
                                [                                    
                                'attribute'=>'motivoId',
                                'label'=>'Motivo',    
                                'value'=>'tiposdetalles.nombre',
                                ],
                                [
                                'attribute'=>'Estado',
                                'label'=>'Estado',
                                'value'=>'estado.nombre',
                                ],
                                [
                                'attribute'=>'apruebanomina',
                                'label'=>'Revisado nomina',
                                'value'=>'revisanomina.nombre',
                                ],
                                // [
                                //  'attribute'=>'Jefeinmediato',
                                //  'label'=>'Jefe inmediato',
                                //  'value'=>'Jefeinmediato.nombre'.' '.'apellidos',
                                // ],

                                'fechaCreacion',
                                //'idsolicitudpermisos',
                                //'usuarioIdusuario',
                                //'motivoId',
                                //'fechaInicio',
                                //'fechaFin',
                                //'fechaCreacion',
                                //'tiempoRepuesto',
                                //'aprobado',
                                //'anulado',
                                //'observaciones',

                                ['class' => 'yii\grid\ActionColumn',
                                'template'=> '{view}', 
                                'buttons' => [
                                // ====== Button ver archivo pago
                                    'view' => function ($url, $model) {                                                        
                                        return Html::button('ver', 
                                        [
                                            'class' => 'pull-left btn-xs btn-block btn-success btn-mostrar',
                                            'data-mostrar' => $model->idsolicitudpermisos,
                                        ]
                                    );
    
                                },
                            ]
                        ],
                    ],
                ]); ?>                            
                                                            
            </div>                         
        </div>   
<!--////////////////////////////////////////Modal usuario////////////////////////////////-->

        <div class="modal fade" id="verusu" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ver solicitud</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                    <div class="modal-body" id="containerver" >
                                    !Esta seguro de aprobar la solicitud¡
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <div class="solicitudpermisos-form">
                                    <?php $form = ActiveForm::begin(['action' => ['update'], 'method' => 'get' ]); ?>  
                                    <input type="hidden" id="mostrar" name="id">                                                                                                
                                    <div class="form-group">
                                    <br>
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">cerrar</button>  
                                    <button id="modif"  class="btn btn-success">Modificar</button>                                                                                                                                                                
                                                                                       
                                    </div>                            
                                    <?php ActiveForm::end(); ?>                          
                            </div>
                        </div>                
                    </div> 
                </div>
            </div>
        </div>  
<!--//////////////////////////////////////// VISTA JEFE INMEDIATO //////////////////////////////-->
            <?php } elseif(!$valusu) {?>
                <div class="card border-primary mb-3">
                    <div class="card-header text-white" style="background-color: #337AB7;">Datos de usuario</div>
                        <div class="card-body">

                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                        <div class = "table-responsive">                        
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],                                
                                [
                                'attribute'=>'nombres',
                                'label'=>'Nombres',    
                                'value'=>'users.nombres'
                                ],
                                 [
                                'attribute'=>'apellidos',
                                'label'=>'Apellidos',    
                                'value'=>'users.apellidos',
                                ],
                                //  [
                                // 'attribute'=>'documento',
                                // 'label'=>'Documento',    
                                // 'value'=>'users.identificacion',
                                // ],                                
                                [                                    
                                'attribute'=>'motivoId',
                                'label'=>'Motivo',    
                                'value'=>'tiposdetalles.nombre',
                                ],
                                [
                                'attribute'=>'Estado',
                                'label'=>'Estado',
                                'value'=>'estado.nombre',
                                ],
                                // [
                                //  'attribute'=>'Jefeinmediato',
                                //  'label'=>'Jefe inmediato',
                                //  'value'=>'Jefeinmediato.nombre'.' '.'apellidos',
                                // ],

                                'fechaCreacion',
                                //'idsolicitudpermisos',
                                //'usuarioIdusuario',
                                //'motivoId',
                                //'fechaInicio',
                                //'fechaFin',
                                //'fechaCreacion',
                                //'tiempoRepuesto',
                                //'aprobado',
                                //'anulado',
                                //'observaciones',

                                ['class' => 'yii\grid\ActionColumn',
                                'template'=> '{view}', 
                                'buttons' => [
                                // ====== Button ver archivo pago
                                    'view' => function ($url, $model) {                                                        
                                        return Html::button('ver', 
                                        [
                                            'class' => 'pull-left btn-xs btn-block btn-success btn-ver',
                                            'data-solicitud' => $model->idsolicitudpermisos,
                                        ]
                                    );
    
                                },
                            ]
                        ],
                    ],
                        ]); ?>                                             
                    </div>                    
                        <?php }?> 
            </div>                
</div>
<!--/////////////////////////////////MODAL APROBAR//////////////////////////////-->
<div class="modal fade" id="versolicitud" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ver solicitud</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body" id="containeraprobar" >
                            !Esta seguro de aprobar la solicitud¡
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <div class="solicitudpermisos-form">
                            <?php $form = ActiveForm::begin(['action' => ['aprobar'], 'method' => 'get' ]); ?>                                
                            <input type="hidden" id="ver" name="id">                               
                            <div class="form-group">
                            <br>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">cerrar</button>
                                                      
                            <button id='aprobar' type="submit" class="btn btn-success">aprobar</button>                              
                            <button id='rechazar2' type="button" class="btn btn-danger" data-toggle="modal" data-target="#rechazar">Rechazar</button>  
                                                                                
                            </div>                            
                            <?php ActiveForm::end(); ?>                          
                    </div>
                </div>                
            </div>                              
<!--/////////////////////////////////MODAL RECHZAR//////////////////////////////-->
        <div class="modal fade" id="rechazar" tabindex="-1" role="dialog" aria-labelledby="rechazarLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Rechazar solicitud</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body containerrechazar" >
                        !Esta seguro de rechazar la solicitud¡
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <div class="solicitudpermisos-form">
                                <?php $form = ActiveForm::begin(['action' => ['rechazar'], 'method' => 'get' ]); ?>
                                <label for="message-text" class="col-form-label">Observacion:</label>
                                <!-- <textarea class="form-control" name="observacion" required="required"></textarea> -->
                                <textarea name="observacion" rows="4" cols="60" required="required"></textarea> 
                                <input type="hidden" id="observacion" name="id">                                                                 
                                <div class="form-group">
                                <br>
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">cerrar</button>
                                    <button type="submit" class="btn btn-success">Rechazar</button>                           
                                </div>
                                <?php ActiveForm::end(); ?>                                
                            </div>                
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        </div>
    </div>
</div>

<script type="text/javascript">
        $(document).ready(function(){
            $('.btn-ver').click(function(event){                
                $('#ver').val($(this).data('solicitud'));
                $('#observacion').val($(this).data('solicitud'))
                // $('#versolicitud').modal();
                $.ajax({
                    url: '<?php echo Yii::$app->request->baseUrl ?>/?r=solicitudpermisos/versolicitud',
                    type : 'get',
                    data : {
                        id : $(this).data('solicitud')
                    },
                    success: function(data){
                        $('#containeraprobar').html(data);                     
                        $('#versolicitud').modal();
                    
                    }
                }); 
                ////////AJAX MOSTRAR OCULTAR BOTONES/////////
                $.ajax({
                        url: '<?php echo Yii::$app->request->baseUrl ?>/index.php?r=solicitudpermisos/mostrarboton',
                        type:'get',
                        data:{
                            id:$(this).data('solicitud')
                        },
                        success: function(data) {
                            if(data != 122){
                                $('#rechazar2').hide();
                                $('#aprobar').hide();                                
                            }else{
                                $('#rechazar2').show();
                                $('#aprobar').show();                                
                            }
                        }
                });                       
            }); 
            $('.btn-mostrar').click(function(event){
                $('#mostrar').val($(this).data('mostrar'));

                $.ajax({
                    url : '<?php echo Yii::$app->request->baseUrl ?>/?r=solicitudpermisos/vistausuario',
                    type : 'get',
                    data : {
                        id : $(this).data('mostrar')
                    },
                    success: function(data){
                        $('#containerver').html(data);
                        $('#verusu').modal();                        
                    }
                });
                $.ajax({
                    url : '<?php echo Yii::$app->request->baseUrl ?>/?r=solicitudpermisos/modificarsolicitud',
                    type : 'get',
                    data : {
                        id : $(this).data('mostrar')
                    },
                    success: function(data){
                        if(data==122){
                            $('#modif').show();
                        }else
                            $('#modif').hide();
                    }
                });
            });

                                    
        }); // document ready                   
</script>
