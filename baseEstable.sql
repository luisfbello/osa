-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: base
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesos_directos`
--

DROP TABLE IF EXISTS `accesos_directos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesos_directos` (
  `idaccesos_directos` int(11) NOT NULL AUTO_INCREMENT,
  `etiqueta` varchar(205) DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `idRol` int(11) NOT NULL,
  `estado` int(1) DEFAULT NULL,
  PRIMARY KEY (`idaccesos_directos`),
  KEY `fk_rol_idx` (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesos_directos`
--

LOCK TABLES `accesos_directos` WRITE;
/*!40000 ALTER TABLE `accesos_directos` DISABLE KEYS */;
INSERT INTO `accesos_directos` VALUES (1,'acceso prueba','acceso','info',1,1);
/*!40000 ALTER TABLE `accesos_directos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acciones`
--

DROP TABLE IF EXISTS `acciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acciones` (
  `idaccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombreAccion` varchar(45) NOT NULL,
  PRIMARY KEY (`idaccion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acciones`
--

LOCK TABLES `acciones` WRITE;
/*!40000 ALTER TABLE `acciones` DISABLE KEYS */;
INSERT INTO `acciones` VALUES (1,'create'),(2,'view'),(3,'update'),(4,'index');
/*!40000 ALTER TABLE `acciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargos`
--

DROP TABLE IF EXISTS `cargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargos` (
  `idCargo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idCargo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargos`
--

LOCK TABLES `cargos` WRITE;
/*!40000 ALTER TABLE `cargos` DISABLE KEYS */;
INSERT INTO `cargos` VALUES (1,'desarrollador',1,'2017-01-16 16:49:49');
/*!40000 ALTER TABLE `cargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controladores`
--

DROP TABLE IF EXISTS `controladores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controladores` (
  `idcontrolador` int(11) NOT NULL AUTO_INCREMENT,
  `nombreControlador` varchar(45) NOT NULL,
  PRIMARY KEY (`idcontrolador`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controladores`
--

LOCK TABLES `controladores` WRITE;
/*!40000 ALTER TABLE `controladores` DISABLE KEYS */;
INSERT INTO `controladores` VALUES (2,'opcionhasrol'),(3,'rback'),(4,'users'),(5,'accesosdirectos'),(6,'acciones');
/*!40000 ALTER TABLE `controladores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_has_rol`
--

DROP TABLE IF EXISTS `item_has_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_has_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_has_item_idx` (`id_item`),
  KEY `fk_has_rol_2_idx` (`id_rol`),
  CONSTRAINT `fk_has_item` FOREIGN KEY (`id_item`) REFERENCES `item_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_has_rol_2` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_has_rol`
--

LOCK TABLES `item_has_rol` WRITE;
/*!40000 ALTER TABLE `item_has_rol` DISABLE KEYS */;
INSERT INTO `item_has_rol` VALUES (50,5,1,1,1),(51,6,1,1,2),(52,7,1,1,3),(53,8,1,1,4);
/*!40000 ALTER TABLE `item_has_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_menu`
--

DROP TABLE IF EXISTS `item_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(45) DEFAULT NULL,
  `etiqueta` varchar(45) DEFAULT NULL,
  `id_opcion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_opcion_menu_idx` (`id_opcion`),
  CONSTRAINT `fk_opcion` FOREIGN KEY (`id_opcion`) REFERENCES `opcion_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_menu`
--

LOCK TABLES `item_menu` WRITE;
/*!40000 ALTER TABLE `item_menu` DISABLE KEYS */;
INSERT INTO `item_menu` VALUES (5,'users/index','Gestionar Usuarios',2),(6,'opcionhasrol/index','Menu',2),(7,'accesosdirectos/index','Accesos Directos',2),(8,'rback/index','Permisos (RBAC)',2);
/*!40000 ALTER TABLE `item_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion_has_rol`
--

DROP TABLE IF EXISTS `opcion_has_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcion_has_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_opcion` int(11) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_has_opcion_idx` (`id_opcion`),
  KEY `fk_has_rol_idx` (`id_rol`),
  CONSTRAINT `fk_has_opcion` FOREIGN KEY (`id_opcion`) REFERENCES `opcion_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_has_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion_has_rol`
--

LOCK TABLES `opcion_has_rol` WRITE;
/*!40000 ALTER TABLE `opcion_has_rol` DISABLE KEYS */;
INSERT INTO `opcion_has_rol` VALUES (2,2,1,1,1),(3,3,2,1,1);
/*!40000 ALTER TABLE `opcion_has_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion_menu`
--

DROP TABLE IF EXISTS `opcion_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcion_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `estado` int(1) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion_menu`
--

LOCK TABLES `opcion_menu` WRITE;
/*!40000 ALTER TABLE `opcion_menu` DISABLE KEYS */;
INSERT INTO `opcion_menu` VALUES (2,'Configuracion',NULL,NULL),(3,'ventas',NULL,NULL);
/*!40000 ALTER TABLE `opcion_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rback`
--

DROP TABLE IF EXISTS `rback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rback` (
  `idrback` int(11) NOT NULL AUTO_INCREMENT,
  `idRol` int(11) NOT NULL,
  `idControlador` int(11) NOT NULL,
  `idAccion` int(11) NOT NULL,
  `estadoRback` int(1) NOT NULL,
  PRIMARY KEY (`idrback`),
  KEY `fk_rol_idrol_idx` (`idRol`),
  KEY `fk_accion_idaccion_idx` (`idAccion`),
  KEY `fk_controlador_idcontrolador_idx` (`idControlador`),
  CONSTRAINT `fk_accion_idaccion` FOREIGN KEY (`idAccion`) REFERENCES `acciones` (`idaccion`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_controlador_idcontrolador` FOREIGN KEY (`idControlador`) REFERENCES `controladores` (`idcontrolador`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rol_idrol` FOREIGN KEY (`idRol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rback`
--

LOCK TABLES `rback` WRITE;
/*!40000 ALTER TABLE `rback` DISABLE KEYS */;
INSERT INTO `rback` VALUES (4,1,2,4,1),(5,1,2,3,1),(6,1,3,4,1),(7,1,3,1,1),(8,1,3,2,1),(9,1,2,2,1),(10,1,4,4,1),(11,1,4,3,1),(12,1,4,1,1),(13,1,4,2,1),(14,2,2,4,1),(15,2,2,3,1),(16,2,2,2,1),(17,2,3,4,1),(18,2,3,1,1),(19,2,3,2,1),(20,2,4,4,1),(21,2,4,3,1),(22,2,4,1,1),(23,2,4,2,1),(24,2,5,4,1),(25,2,5,1,1),(26,2,5,3,1),(27,2,5,2,1),(28,1,5,4,1),(29,1,5,1,1),(30,1,5,3,1),(31,1,5,2,1),(32,1,6,4,1),(33,1,6,1,1),(34,1,6,3,1),(35,1,6,2,1);
/*!40000 ALTER TABLE `rback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'administrador'),(2,'vendedor');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subitem_has_rol`
--

DROP TABLE IF EXISTS `subitem_has_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subitem_has_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_subitem` int(11) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_subitem_has_idx` (`id_subitem`),
  KEY `fk_has_rol_3_idx` (`id_rol`),
  CONSTRAINT `fk_has_rol_3` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_subitem_has` FOREIGN KEY (`id_subitem`) REFERENCES `subitem_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subitem_has_rol`
--

LOCK TABLES `subitem_has_rol` WRITE;
/*!40000 ALTER TABLE `subitem_has_rol` DISABLE KEYS */;
/*!40000 ALTER TABLE `subitem_has_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subitem_menu`
--

DROP TABLE IF EXISTS `subitem_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subitem_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(45) DEFAULT NULL,
  `etiqueta` varchar(45) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_idx` (`id_item`),
  CONSTRAINT `fk_item` FOREIGN KEY (`id_item`) REFERENCES `item_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subitem_menu`
--

LOCK TABLES `subitem_menu` WRITE;
/*!40000 ALTER TABLE `subitem_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `subitem_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(250) NOT NULL,
  `authKey` varchar(250) NOT NULL,
  `accessToken` varchar(250) NOT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT '1',
  `role` varchar(45) DEFAULT NULL,
  `nombres` varchar(60) DEFAULT NULL,
  `apellidos` varchar(60) DEFAULT NULL,
  `foto` varchar(250) DEFAULT 'default.png',
  `numeroIdentificacion` varchar(45) NOT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `telefono` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rol_idx` (`id`,`role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'vitrul','cbautista@gmail.com','fsbqobwqC.aMo','cf11061916902f8ee555a61f171d667949930c87469453e5dadb176fb11405c2a80ce8ca6b7e7d22b4366f9641e19af2fe5aabe3664e6d4465689cb2b794f508197f43f247e4423dfb8fafa4032d45f31b89c1d7fe60fe1c687d998c259c6cc5f27a2593','9bc290d0ca50683a3ee1696dc03060441e0a087bf3191dcf2c1c5cfb64ec80cb772756fc02a9a849f02d156eed6f8638a62b9952f62e1a999a14c3b84dd7637bcb05a39fd17a61cf2af048e882262594c6c5a3b281bb9eeb90a3b3fdbe4acda4976b79aa',1,'1','super administrador','servimatica','cbautista.png','454554','dg super administrador',5736612),(2,'mjerez','monicapatricia1030@gmail.com','fsbqobwqC.aMo','16f7e4d775aa75c6f7fb97a06896053273566d49f6e99a8f8b0a57b1008451ab3f145408c5762ca67e76753f16c00b715820aa36f2620d2bce31e10c7a6b13a5d0aa4db29dbb4c0e7d6acc474c638837b5825eab8672e94f89f104df6cd95c27f9c8cde3','ed88e3eef4e69ad6fa9e9e759a6c8a9892fb4cebcc399e3ad6b02e219a5db6aca5dd1347f3daf1ed10401b3be7bd4f0e260c370087e8a10ec82f67a3a37b26ddf599ba277a55cf7cce6b118acc977bfb32959bacca204b59320629be44012f86cdeeda8f',1,'2','Monica Patricia','Jerez Arevalo','default.png','123456798','direccion prueba',1234567);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-21 11:05:53
