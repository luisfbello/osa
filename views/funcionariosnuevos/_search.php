<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FuncionariosnuevosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="funcionariosnuevos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'funcionarios_nuevos_id') ?>

    <?= $form->field($model, 'funcionarios_nuevos_nombre') ?>

    <?= $form->field($model, 'funcionarios_nuevos_apellidos') ?>

    <?= $form->field($model, 'funcionarios_nuevos_tipodocumento') ?>

    <?= $form->field($model, 'funcionarios_nuevos_documento') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_direccion') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_email') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_telefono') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_tipocontrato') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_fechainiciocontrato') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_fechafincontrato') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_cargo') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_sucursal') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_permisosrecaudo') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_permisosvisita') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_permisosconcertacion') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_permisosdocumentos') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_permisoscontabilidad') ?>

    <?php // echo $form->field($model, 'funcionarios_nuevos_permisosreportes') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
