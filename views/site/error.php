<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>
<div class="col-lg-12">

    <div class="jumbotron">
        <img width="250" src="<?php echo Url::base(); ?>/images/f52610_b3fa64f9ff4d4ac0a9d8f745fa6b0a82.png" alt="..." class="">
    </div>

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>


    <p>
        Por favor contactese con el area de Tecnologia. Gracias
    </p>

</div>
