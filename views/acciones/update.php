<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Acciones */

$this->title = 'Update Acciones: ' . ' ' . $model->idaccion;
$this->params['breadcrumbs'][] = ['label' => 'Acciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idaccion, 'url' => ['view', 'id' => $model->idaccion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="acciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
