<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioAlClienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servicio-al-cliente-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'servicio_al_cliente_id') ?>

    <?= $form->field($model, 'servicio_al_cliente_nombre_usuario') ?>

    <?= $form->field($model, 'servicio_al_cliente_apellidos_usuario') ?>

    <?= $form->field($model, 'servicio_al_cliente_tipoIdentificacion') ?>

    <?= $form->field($model, 'servicio_al_cliente_numero_documento') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_razonsocial') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_municipio') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_departamento') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_zona') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_direccion') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_nit') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_telefono1') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_telefono2') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_email') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_observacion') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_tipo_solicitud') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_fecha_solicitud') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_estado_solicitud') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_operario_gestion') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_fecha_gestion') ?>

    <?php // echo $form->field($model, 'servicio_al_cliente_observacion_gestion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
