<?php

use yii\helpers\Html;

$this->title="Organizacion sayco-acinpro";
$this->params['breadcrumbs'][] = $this->title;

?>

<html>
<body>

<!-- begin about -->
<section>

    <div class="card">
        <div class="card-header alert alert-primary"><h1><?= Html::encode($this->title) ?></h1></div>
            <div class="card-body" >
                <div class="container">
                    <center>
                
                    <!-- begin logo -->
                    <section>
                        <div class="container">
                            <div id="osaimg" class="col-lg-12">
                                <?=Html::img('images/osa.jpg',['width'=> '600px']) ?>
                            </div>
                        </div>
                    </section>
                    </center>
                    <!-- end logo -->
                    <br>
                    <div style="text-align: justify">
                    <div class="card-header alert alert-primary"><h2>¿Que es la OSA?</h2></div>
                        
                        <p>La OSA es una Organización privada, con personería jurídica y autorización de funcionamiento otorgada por la 
                        <b>Dirección Nacional de Derechos de Autor</b>; cuyo objeto social es el recaudo de las remuneraciones provenientes 
                        de la comunicación y el almacenamiento digital de obras musicales, y producciones fonográficas a efectos de la 
                        comunicación pública, a nivel nacional.</p>
                        <br>
                        <div class="card-header alert alert-primary"><h2>¿Como Funciona la OSA?</h2></div>
                        
                        <p>Cuando un comerciante quiere ejecutar o almacenar la música a efectos de la comunicación al público, 
                            dándole un valor agregado a su establecimiento o actividad comercial debe acudir a la <b>OSA</b>, 
                            quien hará el estudio concerniente y definirá con el usuario la tarifa anual para el pago de los Derecho de Autor y Conexos, 
                            según el reglamento tarifario vigente. <br>
                            Una vez realizado el pago, contara con su soporte, el cual le dará total legalidad y autorizará el uso del catálogo universal 
                            de las obras musicales que representamos. Este dinero recaudado se entrega a las Sociedades de Gestión Colectiva <b>SAYCO o ACINPRO</b>,
                            quienes se encargan de distribuir a sus afiliados, autores, compositores, intérpretes , productores fonográficos. <br> 
                        </p>

                        <br>
                        <div class="card-header alert alert-primary"><h2>¿Que beneficios ofrece la OSA?</h2></div>
                        
                        <p>Antes que nada, estar al día con la <b>OSA</b>  significa apoyar a los autores, artistas, intérpretes, productores fonográficos 
                            creadores de la cultura del nuestro País. <br>
                            Adicionalmente, nuestras licencias y/o autorizaciones le garantizan la legalidad de su establecimiento, sitio o lugar 
                            ante las autoridades administrativas, policivas y judiciales y le brindan acceso a la comunicación pública de la música 
                            de un catálogo universal de más de 30.000.000 de obras.  <br>
                            Para el usuario, nuestra gestión brinda la ventaja de ofrecer en un sólo pago la autorización para hacer uso de un repertorio 
                            que contiene la mayoría de obras nacionales y extranjeras que se oyen en el país.
                        </p>
                    </div>
                </div>
            </div>
    </div>

</section>

</body>

</html>