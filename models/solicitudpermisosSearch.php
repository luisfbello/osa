<?php

namespace app\models;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Solicitudpermisos;
use app\models\Tiposdetalles;

/**
 * solicitudpermisosSearch represents the model behind the search form of `app\models\solicitudpermisos`.
 */
class solicitudpermisosSearch extends Solicitudpermisos
{
    /**
     * {@inheritdoc}


     */
   public $nombres;
   public $apellidos;
   public $documento;
   public $idTipoDetalle;
   public $Estado;
   public $apruebanomina;

    public function rules()
    {
        return [
            [['idsolicitudpermisos', 'usuarioIdusuario', 'motivoId'], 'integer'],
            [['fechaInicio', 'fechaFin', 'fechaCreacion', 'tiempoRepuesto', 'observaciones','idTipoDetalle','nombres','apellidos','documento','Estado','apruebanomina'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = solicitudpermisos::find();
        $query->joinWith(['users','tiposdetalles']);               
        $query->where(['usuarioIdusuario' =>(Yii::$app->user->identity->id)]);
        $query->orwhere(['usuarioAutoriza'=>(Yii::$app->user->identity->id)]);
        

        // add conditions that should always apply her

           
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['nombres'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['users.nombres' => SORT_ASC],
            'desc' => ['users.nombres' => SORT_DESC],
        ];  
        
        $dataProvider->sort->attributes['apellidos'] = [  

            'asc' => ['users.apellidos' => SORT_ASC],
            'desc' => ['users.apellidos' => SORT_DESC],

        ];
        $dataProvider->sort->attributes['documento'] = [  

            'asc' => ['users.identificacion' => SORT_ASC],
            'desc' => ['users.identificacion'=> SORT_DESC],
        ];

        $dataProvider ->sort->attributes ['Estado']=[
           
           'asc'=> ['tiposdetalles.nombre' => SORT_ASC],
           'desc'=> ['tiposdetalles.nombre'=> SORT_DESC],
        ];
        $dataProvider ->sort->attributes ['Revisado nomina']=[

            'asc'=>['tiposdetalles.nombre'=>SORT_ASC],
            'desc'=>['tiposdetalles.nombre'=>SORT_DESC],
        ];

        // $dataProvider->pagination->pagesize=10;
        //     $this->widget('zii-widgets.ClistView',array(
        //            'dataProvider'=>$dataProvider,
        //             'itemView'=>'_view',));


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idsolicitudpermisos' => $this->idsolicitudpermisos,
            'usuarioIdusuario' => $this->usuarioIdusuario,
            'motivoId' => $this->motivoId,
            'fechaInicio' => $this->fechaInicio,
            'fechaFin' => $this->fechaFin,
            'fechaCreacion' => $this->fechaCreacion,
            'estadoId' => $this->Estado,
            // 'anulado' => $this->anulado,
        ]);

        $query->andFilterWhere(['like', 'tiempoRepuesto', $this->tiempoRepuesto])                     
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'users.nombres', $this->nombres])
            ->andFilterWhere(['like', 'users.apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'users.identificacion', $this->documento])
            ->andFilterWhere(['like', 'tiposdetalles.nombre', $this->idTipoDetalle])
            ->andFilterWhere(['like', 'tiposdetalles.nombre', $this->Estado])
            ->andFilterWhere(['like', 'tiposdetalles.nombre', $this->apruebanomina]);
        return $dataProvider;
    }
}
