<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.7.9.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker',
    ),
  ),
  'kartik-v/yii2-dropdown-x' => 
  array (
    'name' => 'kartik-v/yii2-dropdown-x',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/dropdown' => $vendorDir . '/kartik-v/yii2-dropdown-x',
    ),
  ),
  'kartik-v/yii2-nav-x' => 
  array (
    'name' => 'kartik-v/yii2-nav-x',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/nav' => $vendorDir . '/kartik-v/yii2-nav-x',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf/src',
    ),
  ),
  'codemix/yii2-excelexport' => 
  array (
    'name' => 'codemix/yii2-excelexport',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@codemix/excelexport' => $vendorDir . '/codemix/yii2-excelexport/src',
    ),
  ),
  'kartik-v/yii2-slider' => 
  array (
    'name' => 'kartik-v/yii2-slider',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/slider' => $vendorDir . '/kartik-v/yii2-slider',
    ),
  ),
  'barcode/yii2-barcode' => 
  array (
    'name' => 'barcode/yii2-barcode',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@barcode/barcode' => $vendorDir . '/barcode/yii2-barcode',
    ),
  ),
  'vilochane/yii2-barcode-generator' => 
  array (
    'name' => 'vilochane/yii2-barcode-generator',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@barcode/barcode' => $vendorDir . '/vilochane/yii2-barcode-generator',
    ),
  ),
  'zyx/zyx-phpmailer' => 
  array (
    'name' => 'zyx/zyx-phpmailer',
    'version' => '0.9.4.0',
    'alias' => 
    array (
      '@zyx/phpmailer' => $vendorDir . '/zyx/zyx-phpmailer',
    ),
  ),
);
