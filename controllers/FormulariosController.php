<?php

namespace app\controllers;

    use Yii;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;
    use yii\data\Pagination;
    use yii\data\ActiveDataProvider;
    use yii\filters\AccessControl;
    use app\models\Registronegocios;
    use app\models\Solicitudliquidacion;
    use app\models\ConsultasYReclamos;
    use yii\web\UploadedFile;
    use app\models\RegistronegociosSearch;
    use app\models\Municipios;
    use app\models\Sucursales;
    use yii\db\Query;


/**
 * NegociosController implements the CRUD actions for Negocios model.
 */
class FormulariosController extends Controller
{

    // // SELECCON DE ACTIVIDAD Y SUBACTIVIDAD
    // public function actionIndex()
    // {   
    //     $this->layout = 'formulario1';
    //     return $this->render('index', [
    //         // 'actividades' => $actividades,
    //     ]);
    // }



    public function actionSolicitud()

    {
        $this->layout = 'formulario1';
        $model = new Solicitudliquidacion();


        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {

                // form inputs are valid, do something here

                return;

            }

        }else {
            return $this->render('solicitud', [

                'model' => $model,
    
            ]);
        }

    }


    public function actionRegistro()

    {
        $this->layout = 'formulario1';

        $model = new Registronegocios();
        $query = new Query();
        $connection = Yii::$app->db;


        if ($model->load(Yii::$app->request->post())) {

            // echo "<pre>";
            // print_r($_REQUEST);
            
            $model->registro_negocio_file1 = UploadedFile::getInstances($model, 'registro_negocio_file1');
            // echo "<pre>";
            
            $arrayFinal = $model->registro_negocio_file1;
            // print_r($arrayFinal);
            // die;
            

            $municipios = Municipios::findOne($_REQUEST['select-municipios']);
            $sucursales = Sucursales::findOne($municipios->sucursales_idsucursal);
           

            $model->registro_negocio_departamento = $_REQUEST['select-departamentos'];
            $model->registro_negocio_ciudad = $_REQUEST['select-municipios'];
            $model->registro_negocio_zona = $sucursales->zona;

            

            $model->registro_negocio_fecha  = $_REQUEST['fechaEntrega'];
            $model->registro_negocio_file1 = null;
            
            if($model->save()){
                
                $modelUpdate = Registronegocios::findOne($model->registro_negocio_id);

                // echo $model->registro_negocio_id.'_1';
                // // print_r($modelUpdate);
                // die();

                $modelUpdate->file1 = $model->registro_negocio_id.'_1';
                $modelUpdate->registro_negocio_file2 = $model->registro_negocio_id.'_2';
                $modelUpdate->registro_negocio_file3 = $model->registro_negocio_id.'_3';

                $arrayExtension = array();
                $contador=1;

                foreach ($arrayFinal as $file ) {

                        

                        $file->saveAs('formularioregistro/' . $model->registro_negocio_id."_".$contador.'.'. $file->extension);
                        // $arrayExtension = $file->extension;
                        array_push($arrayExtension,$file->extension);
                        $contador++;
                }

                // echo "<pre>";
                // print_r($arrayExtension);
                // die;

               $query = $connection->createCommand("UPDATE registronegocios
                                                        SET file1 ='".$model->registro_negocio_id."_1.".$arrayExtension[0]."',
                                                            registro_negocio_file2 = '".$model->registro_negocio_id."_2.".$arrayExtension[1]."',
                                                            registro_negocio_file3 = '".$model->registro_negocio_id."_3.".$arrayExtension[2]."'
                                                    where   1=1
                                                            and registro_negocio_id =".$model->registro_negocio_id);
                                                            
                $actualiza = $query->execute();
                
                if ($actualiza) {
                    // echo 'actualizar';
                    return $this->render('viewok', [
                        'model' => null,
                    ]);

                }else{

                    // echo "entra al else";
                    print_r($model->getErrors());
                }
            }else{
                print_r($model->getErrors());
            }
            // echo "<pre>";
            // print_r($model);
        }else {
            return $this->render('registro', [
                'model' => $model,
            ]);
        }

    }

    public function actionPqr()

    {
        $this->layout = 'formulario1';
        $model = new ConsultasYReclamos();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {

                // form inputs are valid, do something here

                return;

            }

        }else {
            return $this->render('pqr', [

                'model' => $model,
    
            ]);
        }
    }

    public function actionViewnuevos()
    {   
        $model = new Registronegocios();

        
        $searchModel = new RegistronegociosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('viewnuevos', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    public function actionViewliquidaciones()
    {
        return $this->render('viewliquidaciones', [
            
        ]);
    }

    public function actionViewpqr()
    {
        return $this->render('viewpqr', [
            
        ]);
    }


    public function actionLists($id){
     
        $countMunicipios = Municipios::find()->where(['departamento_iddepartamento'=>$id])->count();

        $municipios = Municipios::find()->where(['departamento_iddepartamento'=>$id])->all();

        if($countMunicipios > 0){
            echo "<option value=''> [--Seleccione Municipio--]</option>";
            foreach ($municipios as $municipio) {
                echo "<option value='".$municipio->idMunicipio."'>".$municipio->nombre."</option>";
            }
        }else{
            echo "<option value=''>Opción Inválida</option>";
        }        
    }




}

?>