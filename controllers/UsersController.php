<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\User;
use app\models\Rol;
use app\models\Controladores;
use app\models\Rback;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{

    public function rback(){

        $Rback = array('none');

        $controlador = Controladores::find()->where(['nombreControlador' => Yii::$app->controller->id])->one();

        $arrayRback = Rback::find()
                        ->where(['idRol' => yii::$app->user->identity->role])
                        ->andWhere(['idControlador' => $controlador->idcontrolador])
                        ->andWhere(['estadoRback' => 1])
                        ->all();

        foreach ($arrayRback as $r) {
            array_push($Rback, $r->idAccion0->nombreAccion);
        }

        return $Rback;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','update','delete','view'],
                'rules' => [
                    // Acciones rol ADMINISTRADOR
                    [
                        'actions' => $this->rback(),
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    // Acciones rol VENDEDOR
                    [
                        'actions' => $this->rback(),
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserVentas(Yii::$app->user->identity->id);
                        },
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $roles = Rol::find()->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'roles' => $roles
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (yii::$app->user->identity->role == 1) {
            $model = $this->findModel($id);
            $roles = Rol::find()->all();
            $actualPassword = $model->password;
            if ($model->load(Yii::$app->request->post())) {
                if ($actualPassword != $model->password) {
                    $model->password = crypt($model->password, Yii::$app->params["salt"]);
                }
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'roles' => $roles,
                ]);
            }
        }else{
        if (yii::$app->user->identity->id == $id) {
            $roles = Rol::find()->all();
            $model = $this->findModel($id);
                $actualPassword = $model->password;
                if ($model->load(Yii::$app->request->post())) {
                    if ($actualPassword != $model->password) {
                        $model->password = crypt($model->password, Yii::$app->params["salt"]);
                    }
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'roles' => $roles,
                    ]);
                }
            }else{
                $model = $this->findModel(yii::$app->user->identity->id);
                return $this->render('update', [
                    'model' => $model,
                    'roles' => $roles,
                ]);
            }
        }

    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
