<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="users-view">

  <?php $rolesPerfil = ''; ?>
  <?php $init = true; ?>
  <?php $idRolesPerfil = explode(',', $model->role); ?>
  <?php foreach ($roles as $r) { ?>
    <?php if (in_array($r->id, $idRolesPerfil)) { ?>
      <?php if ($init) { ?>
        <?php $rolesPerfil = $r->nombre; ?>
        <?php $init = false; ?>
      <?php }else{ ?>
        <?php $rolesPerfil = $rolesPerfil . ',' . $r->nombre; ?>
      <?php } ?>
    <?php } ?>
  <?php } ?>

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <div class="jumbotron">
        <img width="150" src="<?php echo Url::base(); ?>/images/fotos/cbautista.png" alt="..." class="img-circle">
        <p class="lead"><?php echo $model->nombres.' '.$model->apellidos; ?></p>
    </div>

    <!-- informacion personal -->
    <div class="panel panel-primary">
        <div class="panel-heading">Informacion personal</div>
        <div class="panel-body">
            <table class="table table-striped">
                <tbody>
                  <tr>
                    <td><strong>Identificacion: </strong><?php echo $model->numeroIdentificacion; ?></td>
                  </tr>
                  <tr>
                    <td><strong>Nombres: </strong><?php echo $model->nombres; ?></td>
                    <td><strong>Apellidos: </strong><?php echo $model->apellidos; ?></td>
                  </tr>
                  <tr>
                    <td><strong>Direccion: </strong><?php echo $model->direccion; ?></td>
                    <td><strong>Telefono: </strong><?php echo $model->telefono; ?></td>
                  </tr>
                  <tr>
                    <td colspan="2"><strong>Email: </strong><?php echo $model->email; ?></td>
                  </tr>
                </tbody>
              </table>
        </div>
    </div>

    <!-- informacion laboral -->
    <div class="panel panel-primary">
        <div class="panel-heading">Informacion laboral</div>
        <div class="panel-body">
            <table class="table table-striped">
                <tbody>
                  <tr>
                    <td><strong>Username: </strong><?php echo $model->username; ?></td>
                    <td><strong>Rol usuario: </strong><?php echo $rolesPerfil; ?></td>
                </tbody>
              </table>
        </div>
    </div>
    <p>
        <?= Html::a('Actualizar informacion', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p> 
</div>
