<?php 
	use yii\helpers\Html;
	use yii\widgets\DetailView;
	use yii\helpers\Url;
?>

<div class="alert alert-<?php echo $alert ?>" role="alert">
  <span class="glyphicon glyphicon-<?php echo $graphicon;  ?>" aria-hidden="true"></span>
  <?php echo $msg; ?>
</div>
<?php if ($model) { ?>
	<div class="users-view">

	    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
	    <div class="jumbotron">
	        <img width="150" src="<?php echo Url::base(); ?>/images/fotos/<?php echo $model->foto; ?>" alt="..." class="img-circle">
	        <p class="lead"><?php echo $model->nombres.' '.$model->apellidos; ?></p>
	    </div>

	    <!-- informacion personal -->
	    <div class="panel panel-primary">
	        <div class="panel-heading">Informacion personal</div>
	        <div class="panel-body">
	            <table class="table table-striped">
	                <tbody>
	                  <tr>
	                    <td><strong>Nombres: </strong><?php echo $model->nombres; ?></td>
	                    <td><strong>Apellidos: </strong><?php echo $model->apellidos; ?></td>
	                  </tr>
	                  <tr>
	                    <td><strong>Tipo de estado: </strong><?php echo $model->numeroIdentificacion; ?></td>
	                  </tr>
	                  <tr>
	                    <td><strong>Direccion: </strong><?php echo $model->direccion; ?></td>
	                    <td><strong>Telefono: </strong><?php echo $model->telefono; ?></td>
	                  </tr>
	                  <tr>
	                    <td colspan="2"><strong>Email: </strong><?php echo $model->email; ?></td>
	                  </tr>
	                </tbody>
	              </table>
	        </div>
	    </div>

	    <!-- informacion laboral -->
	    <div class="panel panel-primary">
	        <div class="panel-heading">Informacion laboral</div>
	        <div class="panel-body">
	            <table class="table table-striped">
	                <tbody>
	                  <tr>
	                    <td><strong>Username: </strong><?php echo $model->username; ?></td>
	                    <td><strong>Rol usuario: </strong><?php echo $model->rol->nombre; ?></td>
	                  </tr>
	                </tbody>
	              </table>
	        </div>
	    </div>
	    <p>
	        <?= Html::a('Actualizar informacion', ['funcionarios/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	        <?= Html::a('Registrar funcionario', ['site/register'], ['class' => 'btn btn-primary']) ?>
	        <?= Html::a('Volver', ['site/index'], ['class' => 'btn btn-primary']) ?>
	    </p> 
	</div>

<?php }else{ ?>
	<p>
	    <?= Html::a('Registrar funcionario', ['site/register'], ['class' => 'btn btn-primary']) ?>
	    <?= Html::a('Volver', ['site/index'], ['class' => 'btn btn-primary']) ?>
	</p> 
<?php } ?>