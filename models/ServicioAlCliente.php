<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio_al_cliente".
 *
 * @property integer $servicio_al_cliente_id
 * @property string $servicio_al_cliente_nombre_usuario
 * @property string $servicio_al_cliente_apellidos_usuario
 * @property integer $servicio_al_cliente_tipoIdentificacion
 * @property string $servicio_al_cliente_numero_documento
 * @property string $servicio_al_cliente_razonsocial
 * @property integer $servicio_al_cliente_municipio
 * @property integer $servicio_al_cliente_departamento
 * @property integer $servicio_al_cliente_zona
 * @property string $servicio_al_cliente_direccion
 * @property integer $servicio_al_cliente_nit
 * @property integer $servicio_al_cliente_telefono1
 * @property integer $servicio_al_cliente_telefono2
 * @property string $servicio_al_cliente_email
 * @property string $servicio_al_cliente_observacion
 * @property integer $servicio_al_cliente_tipo_solicitud
 * @property string $servicio_al_cliente_fecha_solicitud
 * @property integer $servicio_al_cliente_estado_solicitud
 * @property integer $servicio_al_cliente_operario_gestion
 * @property string $servicio_al_cliente_fecha_gestion
 * @property string $servicio_al_cliente_observacion_gestion
 * @property string $servicio_al_cliente_check
 *
 * @property Municipios $servicioAlClienteMunicipio
 * @property Departamentos $servicioAlClienteDepartamento
 * @property Tiposdetalles $servicioAlClienteTipoSolicitud
 * @property Users $servicioAlClienteOperarioGestion
 */
class ServicioAlCliente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio_al_cliente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['servicio_al_cliente_nombre_usuario', 'servicio_al_cliente_apellidos_usuario', 'servicio_al_cliente_tipoIdentificacion', 'servicio_al_cliente_numero_documento', 'servicio_al_cliente_municipio', 'servicio_al_cliente_departamento', 'servicio_al_cliente_zona', 'servicio_al_cliente_tipo_solicitud', 'servicio_al_cliente_fecha_solicitud','servicio_al_cliente_check'], 'required'],
            [['servicio_al_cliente_tipoIdentificacion', 'servicio_al_cliente_municipio', 'servicio_al_cliente_departamento', 'servicio_al_cliente_zona', 'servicio_al_cliente_nit', 'servicio_al_cliente_telefono1', 'servicio_al_cliente_telefono2', 'servicio_al_cliente_tipo_solicitud', 'servicio_al_cliente_estado_solicitud','servicio_al_cliente_check', 'servicio_al_cliente_operario_gestion'], 'integer'],
            [['servicio_al_cliente_observacion', 'servicio_al_cliente_observacion_gestion'], 'string'],
            [['servicio_al_cliente_fecha_solicitud', 'servicio_al_cliente_fecha_gestion'], 'safe'],
            [['servicio_al_cliente_nombre_usuario', 'servicio_al_cliente_apellidos_usuario'], 'string', 'max' => 45],
            [['servicio_al_cliente_numero_documento'], 'string', 'max' => 60],
            [['servicio_al_cliente_razonsocial', 'servicio_al_cliente_direccion', 'servicio_al_cliente_email'], 'string', 'max' => 150],
            [['servicio_al_cliente_municipio'], 'exist', 'skipOnError' => true, 'targetClass' => Municipios::className(), 'targetAttribute' => ['servicio_al_cliente_municipio' => 'idMunicipio']],
            [['servicio_al_cliente_departamento'], 'exist', 'skipOnError' => true, 'targetClass' => Departamentos::className(), 'targetAttribute' => ['servicio_al_cliente_departamento' => 'idDepartamento']],
            [['servicio_al_cliente_tipo_solicitud'], 'exist', 'skipOnError' => true, 'targetClass' => Tiposdetalles::className(), 'targetAttribute' => ['servicio_al_cliente_tipo_solicitud' => 'idTipoDetalle']],
            [['servicio_al_cliente_operario_gestion'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['servicio_al_cliente_operario_gestion' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'servicio_al_cliente_id' => 'Nº Ticket',
            'servicio_al_cliente_nombre_usuario' => ' Nombre Usuario',
            'servicio_al_cliente_apellidos_usuario' => ' Apellidos Usuario',
            'servicio_al_cliente_tipoIdentificacion' => ' Tipo Identificacion',
            'servicio_al_cliente_numero_documento' => ' Numero Documento',
            'servicio_al_cliente_razonsocial' => ' Razon social',
            'servicio_al_cliente_municipio' => ' Municipio',
            'servicio_al_cliente_departamento' => ' Departamento',
            'servicio_al_cliente_zona' => ' Zona',
            'servicio_al_cliente_direccion' => ' Direccion',
            'servicio_al_cliente_nit' => ' Nit',
            'servicio_al_cliente_telefono1' => ' Telefono',
            'servicio_al_cliente_telefono2' => ' Telefono 2',
            'servicio_al_cliente_email' => ' Email',
            'servicio_al_cliente_observacion' => ' Solicitud',
            'servicio_al_cliente_tipo_solicitud' => ' Solicitud',
            'servicio_al_cliente_fecha_solicitud' => ' Fecha Solicitud',
            'servicio_al_cliente_estado_solicitud' => 'Estado Solicitud',
            'servicio_al_cliente_operario_gestion' => ' Operario Gestion',
            'servicio_al_cliente_fecha_gestion' => ' Fecha Gestion',
            'servicio_al_cliente_observacion_gestion' => ' Observacion Gestion',
            'servicio_al_cliente_check'=>'Check',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicioAlClienteMunicipio()
    {
        return $this->hasOne(Municipios::className(), ['idMunicipio' => 'servicio_al_cliente_municipio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicioAlClienteDepartamento()
    {
        return $this->hasOne(Departamentos::className(), ['idDepartamento' => 'servicio_al_cliente_departamento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicioAlClienteTipoSolicitud()
    {
        return $this->hasOne(Tiposdetalles::className(), ['idTipoDetalle' => 'servicio_al_cliente_tipo_solicitud']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicioAlClienteOperarioGestion()
    {
        return $this->hasOne(Users::className(), ['id' => 'servicio_al_cliente_operario_gestion']);
    }
    public function getServicioAlClienteTipoIdentificacion()
    {
        return $this->hasOne(Tiposdetalles::className(), ['idTipoDetalle' => 'servicio_al_cliente_tipoIdentificacion']);
    }
}
