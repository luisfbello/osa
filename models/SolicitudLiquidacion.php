<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitud_liquidacion".
 *
 * @property integer $solicitud_liquidacion_id
 * @property string $solicitud_liquidacion_fecha
 * @property string $solicitud_liquidacion_nombres
 * @property string $solicitud_liquidacion_apellidos
 * @property integer $solicitud_liquidacion_identificacion
 * @property string $solicitud_liquidacion_nombre_establecimiento
 * @property string $solicitud_liquidacion_departamento
 * @property string $solicitud_liquidacion_direccion
 * @property integer $solicitud_liquidacion_nit
 * @property string $solicitud_liquidacion_telefono1
 * @property string $solicitud_liquidacion_telefono2
 * @property string $solicitud_liquidacion_email
 */
class SolicitudLiquidacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'solicitud_liquidacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['solicitud_liquidacion_fecha'], 'safe'],
            [['solicitud_liquidacion_identificacion', 'solicitud_liquidacion_nit'], 'integer'],
            [['solicitud_liquidacion_nombres', 'solicitud_liquidacion_apellidos', 'solicitud_liquidacion_nombre_establecimiento', 'solicitud_liquidacion_direccion', 'solicitud_liquidacion_email'], 'string', 'max' => 255],
            [['solicitud_liquidacion_departamento', 'solicitud_liquidacion_telefono1', 'solicitud_liquidacion_telefono2'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'solicitud_liquidacion_id' => 'Solicitud Liquidacion ID',
            'solicitud_liquidacion_fecha' => 'Solicitud Liquidacion Fecha',
            'solicitud_liquidacion_nombres' => 'Solicitud Liquidacion Nombres',
            'solicitud_liquidacion_apellidos' => 'Solicitud Liquidacion Apellidos',
            'solicitud_liquidacion_identificacion' => 'Solicitud Liquidacion Identificacion',
            'solicitud_liquidacion_nombre_establecimiento' => 'Solicitud Liquidacion Nombre Establecimiento',
            'solicitud_liquidacion_departamento' => 'Solicitud Liquidacion Departamento',
            'solicitud_liquidacion_direccion' => 'Solicitud Liquidacion Direccion',
            'solicitud_liquidacion_nit' => 'Solicitud Liquidacion Nit',
            'solicitud_liquidacion_telefono1' => 'Solicitud Liquidacion Telefono1',
            'solicitud_liquidacion_telefono2' => 'Solicitud Liquidacion Telefono2',
            'solicitud_liquidacion_email' => 'Solicitud Liquidacion Email',
        ];
    }
}
