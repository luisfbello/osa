<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioAlCliente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servicio-al-cliente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'servicio_al_cliente_nombre_usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'servicio_al_cliente_apellidos_usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'servicio_al_cliente_tipoIdentificacion')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_numero_documento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'servicio_al_cliente_razonsocial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'servicio_al_cliente_municipio')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_departamento')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_zona')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'servicio_al_cliente_nit')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_telefono1')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_telefono2')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'servicio_al_cliente_observacion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'servicio_al_cliente_tipo_solicitud')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_fecha_solicitud')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_estado_solicitud')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_operario_gestion')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_fecha_gestion')->textInput() ?>

    <?= $form->field($model, 'servicio_al_cliente_observacion_gestion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
