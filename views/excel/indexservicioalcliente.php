<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;


$objPHPExcel = new PHPExcel(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");
	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$objPHPExcel->getActiveSheet()->getStyle("A1:AB1")->getFont()->setBold(true);
   	foreach(range('A','Z') as $columnID) {
   	    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	        ->setAutoSize(true);
   	}
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'ZONA')
	            ->setCellValue('B1', 'DEPARTAMENTO')
	            ->setCellValue('C1', 'MUNICIPIO')
	            ->setCellValue('D1', 'USUARIO')
	            ->setCellValue('E1', 'TIPO DOCUMENTO')
	            ->setCellValue('F1', 'NUMERO DOCUMENTO')
	            ->setCellValue('G1', 'RAZON SOCIAL')
	            ->setCellValue('H1', 'NIT')
				->setCellValue('I1', 'DIRECCION')
				->setCellValue('J1', 'TELEFONO')
				->setCellValue('K1', 'TELEFONO 2')
				->setCellValue('L1', 'EMAIL')
				->setCellValue('M1', 'OBSERVACION CLIENTE')
				->setCellValue('N1', 'FECHA SOLICITUD')
				->setCellValue('O1', 'OPERARIO GESTIONA')
				->setCellValue('P1', 'ULTIMA OBSERVACION GESTION');
	$count = 2;

	

	foreach ($data as $value) {

		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $value['servicio_al_cliente_zona'])
		            ->setCellValue('B'.$count, $value['departamento'])
		            ->setCellValue('C'.$count, $value['municipio'])
		            ->setCellValue('D'.$count, $value['usuario'])
		            ->setCellValue('E'.$count, $value['tipodocumento'])
		            ->setCellValue('F'.$count, $value['servicio_al_cliente_numero_documento'])
		            ->setCellValue('G'.$count, $value['servicio_al_cliente_razonsocial'])
					->setCellValue('H'.$count, $value['servicio_al_cliente_nit'])
					->setCellValue('I'.$count, $value['servicio_al_cliente_direccion'])
		            ->setCellValue('J'.$count, $value['servicio_al_cliente_telefono1'])
		            ->setCellValue('K'.$count, $value['servicio_al_cliente_telefono2'])
					->setCellValue('L'.$count, $value['servicio_al_cliente_email'])
					->setCellValue('M'.$count, $value['servicio_al_cliente_observacion'])
					->setCellValue('N'.$count, $value['servicio_al_cliente_fecha_solicitud'])
					->setCellValue('O'.$count, $value['operarioGestiona'])
					->setCellValue('P'.$count, $value['UltimaObservacion']);
			
			

		$count++;
	}

	if($idsolicitud==139){
		$titulo="REPORTE_SOLICITUDES_PQR";
	}else{
		$titulo="REPORTE_SOLICITUDES_LIQUIDACION";
	}
	$objPHPExcel->getActiveSheet()->setTitle($titulo);
	$objPHPExcel->setActiveSheetIndex(0);
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$titulo.''.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

 ?>