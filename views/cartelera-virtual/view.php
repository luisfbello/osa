<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Carteleravirtual */

$this->title = $model->cartelera_virtual_id;
$this->params['breadcrumbs'][] = ['label' => 'Carteleravirtuals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carteleravirtual-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->cartelera_virtual_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->cartelera_virtual_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cartelera_virtual_id',
            'cartelera_virtual_imagen',
            'cartelera_virtual_texto:ntext',
            'cartelera_virtual_fecha_publicacion',
            'cartelera_virtual_activo',
            'cartelera_virtual_titulo',
            'cartelera_virtual_video',
            'cartelera_virtual_link_video:ntext',
        ],
    ]) ?>

</div>
