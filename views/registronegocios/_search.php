<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RegistronegociosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="registronegocios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'registro_negocio_id') ?>

    <?= $form->field($model, 'registro_negocio_fecha') ?>

    <?= $form->field($model, 'registro_negocio_nombre') ?>

    <?= $form->field($model, 'registro_negocio_apellido') ?>

    <?= $form->field($model, 'registro_negocio_identificacion') ?>

    <?php // echo $form->field($model, 'registro_negocio_email') ?>

    <?php // echo $form->field($model, 'registro_negocio_telefono1') ?>

    <?php // echo $form->field($model, 'registro_negocio_telefono2') ?>

    <?php // echo $form->field($model, 'registro_negocio_nombre_establecimiento') ?>

    <?php // echo $form->field($model, 'registro_negocio_zona') ?>

    <?php // echo $form->field($model, 'registro_negocio_nit_establecimiento') ?>

    <?php // echo $form->field($model, 'registro_negocio_departamento') ?>

    <?php // echo $form->field($model, 'registro_negocio_ciudad') ?>

    <?php // echo $form->field($model, 'registro_negocio_codigo_usuario') ?>

    <?php // echo $form->field($model, 'registro_negocio_liquidado') ?>

    <?php // echo $form->field($model, 'registro_negocio_se_agenda_visita') ?>

    <?php // echo $form->field($model, 'registro_negocio_observacion') ?>

    <?php // echo $form->field($model, 'registro_negocio_dias_sin_respuesta') ?>

    <?php // echo $form->field($model, 'registro_negocio_pago') ?>

    <?php // echo $form->field($model, 'registro_negocio_file1') ?>

    <?php // echo $form->field($model, 'file1') ?>

    <?php // echo $form->field($model, 'registro_negocio_file2') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
