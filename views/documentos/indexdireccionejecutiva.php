<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dirección Ejecutiva';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentos-index">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="card " >
        <div class="card-header alert alert-primary">Dirección Ejecutiva</div>
        <div class="card-body">
        <?php if(!Yii::$app->user->isGuest){ ?>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModalLong">
                Cargar Documentos
            </button><br>
        <?php } ?>
        <?php if ($result!="") { ?>
                <br><ul class="list-group">
                <?php
                    $contador=1; 
                    foreach ($result as $keyC => $valueC) { ?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <?= Html::a($contador.'- '.$valueC['carpeta_nombre'], ['documentos/ver' , 'proceso'=>'3', 'carpeta' => $valueC['carpeta_id']], ['class' => '']) ?>
                        <span class="badge badge-primary badge-pill"><?php  echo $valueC['documentos'] ?></span>
                    </li>
                <?php  $contador++; } ?>
                
                </ul>
            <?php } ?>
          
            <?php if(Yii::$app->user->isGuest){ ?>

                <?php if ($archivos!=null) { ?>
                    <br><ul class="list-group">
                    <?php
                        $contador=1; 
                        foreach ($archivos as $keyC => $valueC) { ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo Url::base(); ?>/<?php echo $valueC['documento_ruta'] ?>" target="_blank" class=""><?php echo $valueC['documento_nombre'] ?></a>
                        </li>
                    <?php  $contador++; } ?>
                    
                    </ul>
                <?php } ?>      

        <?php }else{ if ($archivos!=null) {?>
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre Archivo</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $contador=1; 
                    foreach ($archivos as $keyC => $valueC) { ?>
                    <tr>
                        <td>
                        <?php echo $contador ?>
                        </td>
                        <td>
                            <a href="<?php echo Url::base(); ?>/<?php echo $valueC['documento_ruta'] ?>" target="_blank" class=""><?php echo $valueC['documento_nombre'] ?></a>
                        </td>
                        <td>
                        <center>
                            <button class="btn btn-outline-danger btnEliminar1" data-toggle="modal" id="<?php echo $valueC['documento_id'] ?>" data-target="#eliminar">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </center>
                        </td>
                        
                    </tr>
                <?php  $contador++; } ?>

                </tbody>
            </table>


        <?php } } ?>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Carga de Documentos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php $form = ActiveForm::begin([
                                  'method' => 'post',
                                  'options' => ["enctype" => "multipart/form-data"],
                                  'action' => ['documentos/carga'],
                                ]); ?>
          <div class="modal-body">
              <input type="hidden" name="proceso" value="3">
              <div >
                  <?= $form->field($model, "documento_nombre[]")->fileInput(['multiple' => true])->label(false); ?>
              </div>
          </div>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Subir Documento</button>
      </div>
      <?php ActiveForm::end(); ?>     
    </div>
  </div>
</div>





<!-- Modal -->
<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Eliminación de Documentos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <p>Una Eliminado el archivo no se podra recuperar y debera Cargarlo nuevamente</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="idarchivo">
                <button type="button" id="eliminarArchivo" class="btn btn-danger" >Eliminar Archivo</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<script>

$( document ).ready(function() {

 $('.btnEliminar1').click(function(){
     $('#idarchivo').val($(this).attr('id'));
 })

 $('#eliminarArchivo').click(function(){
    
    $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl ?>/?r=documentos/eliminardocumento',
            type: 'get',
            data: {
                id : $('#idarchivo').val(), 
            },
            success: function (data) {
                location.reload();
            }
        }); // fin ajax
 });

});


</script>