<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "accesos_directos".
 *
 * @property integer $idaccesos_directos
 * @property string $etiqueta
 * @property string $action
 * @property string $color
 * @property integer $idRol
 * @property integer $estado
 */
class AccesosDirectos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accesos_directos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRol'], 'required'],
            [['idRol', 'estado'], 'integer'],
            [['etiqueta'], 'string', 'max' => 205],
            [['action', 'color'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idaccesos_directos' => 'Idaccesos Directos',
            'etiqueta' => 'Etiqueta',
            'action' => 'Action',
            'color' => 'Color',
            'idRol' => 'Id Rol',
            'estado' => 'Estado',
        ];
    }
}
