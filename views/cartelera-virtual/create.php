<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Carteleravirtual */

$this->title = 'Registrar Evento';
$this->params['breadcrumbs'][] = ['label' => 'Carteleravirtuals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carteleravirtual-create">

    <!-- begin header -->
    <section>
    <div id="hederRegistro">
        <h1><?= Html::encode($this->title) ?></h1>
        
        <hr>
        <br>
    </div>
    <div class="container "> 
        
        <?php $form= ActiveForm::begin([
            'method'=> 'post',
            'action'=>['carteleraVirtual/create'],
            'options'=>['class'=>'form-group'],
        ]);
        ?>
        <div class='form-group col-lg-10 '>
            <?= $form->field($model,'cartelera_virtual_titulo')->label('Titulo de la Publicacion')?>
            <?= $form->field($model,'cartelera_virtual_texto')->textArea()->label('Descripcion')?>

            <a class="btn btn-success" data-toggle="collapse" href="#colaplImg" >Agregar imagen</a>
            <a class="btn btn-success"  data-toggle="collapse" href="#colapVid">Agregar video</a>

        </div>
        
        

        <div class='collapse' id='colaplImg' >
            <div class="custom-file mb-3">
                <input type="file" class="custom-file-input" id="customFileImg" name="filename">
                <label class="custom-file-label" for="customFile">Eliga un archivo</label>
            </div>
        </div>
        <div class='collapse' id='colapVid' >
            <div class="custom-file mb-3">
                <input type="file" class="custom-file-input" id="customFileVideo" name="filename" accept="video/*">
                <label class="custom-file-label" for="customFile">Eliga un Video</label>
            </div>
        </div>

        <div>
            <input type="submit" class="col-lg-12 btn btn-info" value="Enviar" >
        </div>
        <?php ActiveForm::end(); ?>
        
    </div>
        
        
    </section>
    


    

</div>
<script>
    $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
