<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'SG SST';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentos-index">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>
    <div class="card ">
        <div class="card-header alert alert-primary">SG SST</div>
        <div class="card-body">
            <?php if (!Yii::$app->user->isGuest) { ?>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModalLong">
                    Cargar Documentos
                </button><br>
                <div class="createFolder">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#folder">
                        Agregar carpeta <i class="fas fa-folder-plus"></i>
                    </button>
                </div>

            <?php } ?>
            <!-- view de las carpetas -->
            <?php if ($result != "") { ?>
                <br>
                <ul class="list-group">
                    <?php
                        $contador = 1;
                        foreach ($result as $keyC => $valueC) { ?>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                        <?= Html::a($contador . '- ' . $valueC['carpeta_nombre'], ['documentos/ver', 'proceso' => '11', 'carpeta' => $valueC['carpeta_id']], ['class' => '']) ?>    
                        <div class="float-right"> 
                                <span class="badge badge-primary badge-pill" ><?php echo $valueC['documentos'] ?></span>
                                <button class="btn btn-outline-danger btneliminarcarpeta"  data-toggle="modal" id="<?php echo $valueC['carpeta_id'] ?>" data-target="#eliminarCarpeta">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>    
                            </div>
                            
                            
                        </li>
                    <?php $contador++;
                        } ?>

                </ul>
            <?php } ?>

            <?php if (Yii::$app->user->isGuest) { ?>

                <?php if ($archivos != null) { ?>
                    <br>
                    <ul class="list-group">
                        <?php
                                $contador = 1;
                                foreach ($archivos as $keyC => $valueC) { ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="<?php echo Url::base(); ?>/<?php echo $valueC['documento_ruta'] ?>" target="_blank" class=""><?php echo $valueC['documento_nombre'] ?></a>
                            </li>
                        <?php $contador++;
                                } ?>

                    </ul>
                <?php } ?>

                <?php } else {
                    if ($archivos != null) { ?>
                    <table class="table table-bordered table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre Archivo</th>
                                <th scope="col">Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $contador = 1;
                                    foreach ($archivos as $keyC => $valueC) { ?>
                                <tr>
                                    <td>
                                        <?php echo $contador ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo Url::base(); ?>/<?php echo $valueC['documento_ruta'] ?>" target="_blank" class=""><?php echo $valueC['documento_nombre'] ?></a>
                                    </td>
                                    <td>
                                        <center>
                                            <button class="btn btn-outline-danger btnEliminar1" data-toggle="modal" id="<?php echo $valueC['documento_id'] ?>" data-target="#eliminar">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </center>
                                    </td>

                                </tr>
                            <?php $contador++;
                                    } ?>

                        </tbody>
                    </table>


            <?php }
            } ?>




        </div>
    </div>
</div>





<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Carga de Documentos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'options' => ["enctype" => "multipart/form-data"],
                    'action' => ['documentos/carga'],
                ]); ?>
                <div class="modal-body">
                    <input type="hidden" name="proceso" value="1">
                    <div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Subir Documento</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Eliminación de Documentos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Una Eliminado el archivo no se podra recuperar y debera Cargarlo nuevamente</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="idarchivo">
                <button type="button" id="eliminarArchivo" class="btn btn-danger">Eliminar Archivo</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal agregar folder -->

<div class="modal fade" id="folder">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header alert alert-primary">
                <h4 class="modal-title">Agregar Carpeta</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <label>Ingresa El Nombre De la Carpeta</label>
                <input class="form-control" type="text" name="nombreFolder" id="nomfolder">
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" id="guardar">Guardar</button>
            </div>

        </div>
    </div>
</div>

<!-- Modal eliminar carpeta -->

<div class="modal fade" id="eliminarCarpeta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Eliminación de carpeta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Al Eliminar la carpeta no se podra recuperar y debera Crearla nuevamente</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="idcarpeta">
                <button type="button" id="eliminarfolder" class="btn btn-danger">Eliminar Carpeta</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!-- modal respuesta -->

<div class="modal fade" id="respuestacarpeta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Creacion Carpeta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="idrespuestacarpeta"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" id="cerrarModal">Cerrar</button>
            </div>
        </div>
    </div>
</div>




<script>
    $(document).ready(function() {

        $('.btnEliminar1').click(function() {
            $('#idarchivo').val($(this).attr('id'));
        })
        
        $('.btneliminarcarpeta').click(function() {
            $('#idcarpeta').val($(this).attr('id'));
        })

        $('#eliminarArchivo').click(function() {

            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=documentos/eliminardocumento',
                type: 'get',
                data: {
                    id: $('#idarchivo').val(),
                },
                success: function(data) {
                    location.reload();
                }
            }); // fin ajax
        });

        // agregar carpeta

        $('#guardar').click(function() {

            var nombrecarpeta = $('#nomfolder').val();

            var idproceso = 11;
            var iddepende = 0;
            var url = '<?php echo Yii::$app->request->baseUrl ?>/index.php?r=documentos/addfolder';

            $.ajax({

                type: "get",
                url: url,
                data: {
                    idproceso: idproceso,
                    iddepende: iddepende,
                    nombrecarpeta: nombrecarpeta,
                },
                success: function(data) {

                    location.reload();
                }

            }); //fin ajax agregar carpeta
        });

        // Eliminar Carpeta

        $('#eliminarfolder').click(function() {

            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=documentos/eliminarfolder',
                type: 'get',
                data: {
                    id: $('#idcarpeta').val(),
                },
                success: function(data) {
                    $('#respuestacarpeta').modal('show');
                    $('#idrespuestacarpeta').text(data);
                }
            }); // fin ajax
        });

        $('#cerrarModal').click(function(){
            location.reload();
        })

    });
</script>