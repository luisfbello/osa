<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opcion_has_rol".
 *
 * @property integer $id
 * @property integer $id_opcion
 * @property integer $id_rol
 * @property integer $estado
 * @property integer $orden
 *
 * @property OpcionMenu $idOpcion
 * @property Rol $idRol
 */
class OpcionHasRol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opcion_has_rol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_rol', 'estado', 'orden'], 'required'],
            [['id_opcion', 'id_rol', 'estado', 'orden'], 'integer'],
            [['id_opcion'], 'exist', 'skipOnError' => true, 'targetClass' => OpcionMenu::className(), 'targetAttribute' => ['id_opcion' => 'id']],
            [['id_rol'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['id_rol' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_opcion' => 'Id Opcion',
            'id_rol' => 'Id Rol',
            'estado' => 'Estado',
            'orden' => 'Orden',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOpcion()
    {
        return $this->hasOne(OpcionMenu::className(), ['id' => 'id_opcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRol()
    {
        return $this->hasOne(Rol::className(), ['id' => 'id_rol']);
    }
}
