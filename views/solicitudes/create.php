<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    
    $this->title = 'Registrar Solicitud de Mejora';
    $this->params['breadcrumbs'][] = $this->title;
?>

    <div class="card" >
    <div class="cards-header alert alert-primary"><h1><?=Html::encode($this->title) ?></h1></div>
    <div class="card-body">
        <div class="container">
            <?php if(isset($model->solicitud_id)){?> 
                    
                    <div class="alert alert-primay">
                        <center><h1>Muchas gracias Por tus Observaciones.</h1></center>
                        
                    </div>
                    <?= Html::a('Regresar',['cartelera-virtual/index'],['class'=>'btn btn-success'])?>

            <?php }else{?>
            <?php   
                $form= ActiveForm::begin([
                    'method'=>'post',
                    'action'=>['solicitudes/create'],
                    'options'=>['class'=>'form-group'],
                ]);
            ?>
            <?= $form->field($model,'solicitud_usuario')->label('Ingrese el nombre del Solicitante') ?>
            <?= $form->field($model,'solicitud_observacion')->textArea()->label('Ingrese la solicitud') ?>
            <?= Html::submitButton('Enviar',['class'=>'btn btn-primary','id'=>'enviarSolicitud']) ?>
            <?php ActiveForm::end()?>

                <?php }?>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function(){

           
            
        });
    </script>
