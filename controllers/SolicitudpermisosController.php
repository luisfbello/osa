<?php

namespace app\controllers;

use Yii;
use app\models\Solicitudpermisos;
use app\models\Revisanominasearch;
use app\models\Aprobadasnominasearch;
use app\models\solicitudpermisosSearch;
use app\models\PendientesSearch;
use app\models\AprobadasSearch;
use app\models\RechazadasSearch;
use app\models\Users;
use app\models\Cargos;
use app\models\Tiposdetalles;
use app\models\Puntosrecaudo;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\time;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\db\Query;





/**
 * SolicitudpermisosController implements the CRUD actions for solicitudpermisos model
 */
class SolicitudpermisosController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all solicitudpermisos models.
     * @return mixed
     */
    public function actionIndex()

    {        
        $searchModel = new solicitudpermisosSearch();
        $model = Solicitudpermisos::find()->where(['usuarioIdusuario'=>(Yii::$app->user->identity->id)])->all();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $usuarios = Solicitudpermisos::find()->where(['usuarioIdusuario'=>(Yii::$app->user->identity->id)])->all();
        $jefeinmediato = Solicitudpermisos::find()->where(['usuarioAutoriza'=>(Yii::$app->user->identity->id)] && ['estadoId'=>(123)])->all(); 
        $validaU = Users::find()->where(['id'=>(Yii::$app->user->identity->id)])->all();                                   
        $pendientes1=false;
        $usuarios1=false; 
        $jefeinmediato1=false;    
        $jefeinmediato2=false;
        $valusu=false; 
              
        // echo $sinSolcitudes; die ();
        //      echo '<pre>';
        //    print_r($validaU);
        //   die();
        foreach ($validaU as $value) 
        {
            if($value->id==(Yii::$app->user->identity->id) && $value->jefe_inmediato!=1){
                $valusu=true;
            }
        }

        foreach ($model as $pendientes) {            
               if ($pendientes->usuarioIdusuario ==(Yii::$app->user->identity->id) && $pendientes->estadoId ==122 || $pendientes->apruebanomina==136){
                $pendientes1=true;
            }                                   
        }
     
        foreach ($usuarios as $usuario) {        
            if ($usuario->usuarioIdusuario ==(Yii::$app->user->identity->id)){
            $usuarios1=true;
            }                                   
        }

        foreach ($jefeinmediato as $jefe) {        
            if ($jefe->usuarioAutoriza ==(Yii::$app->user->identity->id)){
            $jefeinmediato1=true;
            }                                   
        }

        foreach ($jefeinmediato as $jefe) {        
            if ((Yii::$app->user->identity->id==2)&& $jefe->estadoId ==123){
            $jefeinmediato2=true;
            }                                   
        }     
     
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pendientes1' => $pendientes1, 
            'usuarios1' => $usuarios1, 
            'jefeinmediato1' => $jefeinmediato1, 
            'model'=> $model,
            'valusu'=>$valusu,

        ]);
    
    }
    public function actionReport()
    {  
        return $this->render('reporte');                       
    
    }
    /**
     * Displays a single solicitudpermisos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        // $model= Solicitudpermisos::find()->where(['usuarioAutoriza=>'=>(Yii::$app->user->identity->id)])->all();
        // $gestionar=false;


        //   foreach ($model as $gestionar) {

        //     if ($gestionar->usuarioAutoriza==(Yii::$app->user->identity->id) && $pendientes->estadoId ==4){

        //         $gestionar=true;
        //     }
        //     # code...
        // }
        

        
        return $this->render('view', [
            'model' => $this->findModel($id),            
        ]);

        return $this->render('revisanomina', [
            'model' => $this->findModel($id),            
        ]);
    }

    /**
     * Creates a new solicitudpermisos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()

    
    {
            $model = new Solicitudpermisos();

            if ($model->load(Yii::$app->request->post()) ) {

                // echo "<pre>";
                // print_r($_REQUEST);die;

                $model->fechaInicio = $_REQUEST['fechaInicio'];
                $model->fechaFin = $_REQUEST['fechaFin'];
                $model->horaInicio=$_REQUEST['horaInicio'];
                $model->horaFin=$_REQUEST['horaFin'];
                $model->incapacidad=UploadedFile::getInstance($model,'incapacidad');
                // echo "<pre>";
                // print_r($model->incapacidad);die;


                if($model->save()){   
                    if ($model->incapacidad) {
                        if ($model->upload()) {
                            // file is uploaded successfully
                        }else{
                            echo 'no guardo';
                            die();
                        }
                    }               
                    // $model->incapacidad->saveAs('intranetosa/images/' .$model->incapacidad->name.'.'.$model->incapacidad->extension);
                    return $this->redirect(['view', 'id' => $model->idsolicitudpermisos]);

                }else{
                    echo '<pre>';
                    print_r( $model->getErrors() );
                    die();
                }
            }

            //$users=users::model()->findByPk($id);
            $users = users::findOne(Yii::$app->user->identity->id);
            $cargos = cargos::findOne(1);
            $puntosrecaudo= puntosrecaudo::findOne(1);
            $tiposdetalles= tiposdetalles::findOne(1);
            // echo "<pre>";
            // print_r($cargos); die;
            //$usuar = users::find()->where(['id' => $id])->all();

            return $this->render('create', [
                'model' => $model,
                'users'=>$users,
                'cargos'=>$cargos,
                'puntosrecaudo'=>$puntosrecaudo,
                'tiposdetalles'=>$tiposdetalles,




            ]);
            
          
    }

    /**
     * Updates an existing solicitudpermisos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) ) { 
                $model->fechaInicio = $_REQUEST['fechaInicio'];
                $model->fechaFin = $_REQUEST['fechaFin'];
                $model->horaInicio=$_REQUEST['horaInicio'];
                $model->horaFin=$_REQUEST['horaFin'];
                $model->save();
        //  echo '<pre>'       ;
        //  print_r ($model);
        //  die();

        if    ($model->incapacidad=UploadedFile::getInstance($model,'incapacidad')){
            
            if($model->save()){                   
                        if ($model->incapacidad) {                        
                            if ($model->upload()) {
                                // file is uploaded successfully
                            }else{
                                echo 'no guardo';
                                die();
                            }
                        }               
                        // $model->incapacidad->saveAs('intranetosa/images/' .$model->incapacidad->name.'.'.$model->incapacidad->extension);
                        // return $this->redirect(['view', 'id' => $model->idsolicitudpermisos]);
    
                    }else{
                        echo '<pre>';
                        print_r( $model->getErrors() );
                        die();
                    }
        }           

            return $this->redirect(['view', 'id' => $model->idsolicitudpermisos]);            
    }
         
        $users = users::findOne(Yii::$app->user->identity->id);

        return $this->render('update', [
            'model' => $model,
            'users'=> $users,
        ]);
    }

    /**
     * Deletes an existing solicitudpermisos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();

        // return $this->redirect(['index']);
        
    }

    /**
     * Finds the solicitudpermisos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return solicitudpermisos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = solicitudpermisos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionEliminardoc($id)
    {


        // echo '<pre>';
        //     print_r($id.'no trae nada');
        //     die();
        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT *
                                            from    solicitudpermisos
                                            where   idsolicitudpermisos =' .$id);
        
        $incapacidad2 = $query->queryAll();


        foreach ($incapacidad2 as $key => $value) {

            unlink('images/'.$value ['incapacidad']);
        }

        $query = $connection->createCommand('UPDATE 
                                                    solicitudpermisos
                                            set     incapacidad=""
                                            where   1=1
                                                    and idsolicitudpermisos = ' . $id);
        $result = $query->execute();
        echo $id; 
    }

    public function actionAprobar($id)
    {
        $model=solicitudpermisos::findOne($id);
        $model->estadoId='123';
        if ($model->save()) {
            return $this->redirect(['index']);
        }else{
            echo '<pre>';
            print_r( $model->getErrors() );
            die();
        }
        // $model->updateAttributes(['estadoId' => 5]);
   
    }        
           

    public function actionRechazar($id)

    {
            $model=solicitudpermisos::findOne($id);
          // echo '<pre>';
          //  print_r($model);
          // die();
            if(isset($_REQUEST['observacion'])) {

                $model->estadoId='124';
                $model->apruebanomina='137';
                $model->observacionJefeInmediato=$_REQUEST['observacion'];

                if ($model->save()){
                    
                    return $this->redirect(['index']); 
                }


            } 
        
    }
    public function actionAprobarnom($id)
    {
        $model=solicitudpermisos::findOne($id);
        $model->apruebanomina='135';
        if ($model->save()) {
            return $this->redirect(['revisanomina']);
        }else{
            echo '<pre>';
            print_r( $model->getErrors() );
            die();
        }
        // $model->updateAttributes(['estadoId' => 5]);
   
    }
  
    public function actionRevisanomina()
    {
        $searchModel = new Revisanominasearch();    
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);  
            # code...
            
        return $this->render('revisanomina',[            
            'dataProvider' => $dataProvider,  
            'searchModel' => $searchModel, 

        ]); 
    }

    public function actionVersolicitud($id)
    {
       
       ////////////////////////////////////////Datos del usuario////////////////////////////////       
       $solicitud=solicitudpermisos::findOne($id);
       $inca=$solicitud->motivoId; 
       $color=$solicitud->estadoId==124;
       
           ///////////Total dias/////////////////////////////////
           $fecha_inicio = strtotime($solicitud->fechaInicio); 
           $fecha_fin = strtotime($solicitud->fechaFin); 
           $diff = $fecha_fin - $fecha_inicio; 
           $total=round(($diff / 86400)+1). " dias";
           
   
           // echo $total; 
   
           // die();
       $html = '';
       $html .= '<div class="card border-primary mb-3">';
       $html .= '<div class="card-header text-white" style="background-color: #337AB7;">Datos de la solicitud</div>';
       $html .= '<div class="card-body">';
       $html .= '<div class="col-lg-12">';  
       $html .= '<div class="table-responsive">';       
       $html .= '<table class="table table-striped table-hover table-bordered" >';
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Nombres</th>';
       $html .=  '<th>Apellidos</th>';
       $html .=  '<th>Documento</th>';
       $html .=  '<th>Horas</th>';       
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$solicitud->users->nombres. '</td>';
       $html .= '<td>'.$solicitud->users->apellidos. '</td>'; 
       $html .= '<td>'.$solicitud->users->numeroIdentificacion. '</td>';               
       $html .= '</tr>';
       $html .= '</tbody>';
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Telefono</th>';
       $html .=  '<th>Cargo</th>';
       $html .=  '<th>Sucursal</th>';       
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$solicitud->users->telefono. '</td>';
       $html .= '<td>'.$solicitud->users->cargo->nombre. '</td>'; 
       $html .= '<td>'.$solicitud->users->puntorecaudo->nombre. '</td>';        
       $html .= '</tr>';
       $html .= '</tbody>';       
       $html .= '<div>';
       $html .= '<div>';
       $html .= '<div>';        
       ////////////////////////////////////////Datos de solicitud////////////////////////////////
    //    $html = '';              
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Motivo</th>';
       $html .=  '<th>Jefe autoriza</th>';
       $html .=  '<th>Total dias</th>';       
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$solicitud->tiposdetalles->nombre. '</td>';
       $html .= '<td>'.$solicitud->jefeinmediato->nombres.''.$solicitud->jefeinmediato->apellidos. '</td>'; 
       $html .= '<td>'.$total. '</td>';        
       $html .= '</tr>';
       $html .= '</tbody>';
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Fecha inicio</th>';
       $html .=  '<th>Fecha fin</th>';
       $html .=  '<th>Sucursal</th>';       
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$solicitud->fechaInicio. '</td>';
       $html .= '<td>'.$solicitud->fechaFin. '</td>'; 
       $html .= '<td>'.$solicitud->users->puntorecaudo->nombre. '</td>';        
       $html .= '</tr>';
       $html .= '</tbody>';
       if ($solicitud->horaInicio && $solicitud->horaFin>0)
       {
           $html .='<thead>';
           $html .='<tr>';
           $html .='<th>Hora Inicio</th>';
           $html .='<th>Hora Fin</th>';                     
           $html .='</tr>';
           $html .='</thead>';
           $html .='<tbody>';
           $html .='<tr>';
           $html .='<th>'.$solicitud->horaInicio.'</th>';
           $html .='<th>'.$solicitud->horaFin.'</th>';           
           $html .='</tr>';
           $html .='</tbody>';
       }
       $html .= '</table>';
    //    $html .= '<div class="col-lg-12">';       
       $html .= '<table class="table table-striped table-hover table-bordered" >';
       $html .= '<table class="table table-striped table-hover table-bordered" >';
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Observacion solicitud</th>'; 
       if($color==124)
        {
            $html .=  '<th>Observacion jefe inmediato</th>'; 
        }          
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$solicitud->observaciones. '</td>';  
       if ($color==124)
       {
       $html .= '<td>'.$solicitud->observacionJefeInmediato. '</td>';
       }
       $html .= '</tr>';
       $html .= '</tbody>';
       $html .= '<table class="table table-striped table-hover table-bordered" >';
       if ($inca==127) 
       {
           $html .= '<thead>';
           $html .= '<th>Incapacidad</th>';
           $html .= '</thead>';
           $html .= '<tbody>';
           $html .= '<tr>';
           $html .= '<td><a download="'.$solicitud->incapacidad.'" href="http://35.175.13.101/OsaIntranet/web/images/'.$solicitud->incapacidad.'"> <img src="images/'.$solicitud->incapacidad.'"style="width: 700px; height: 600px;"> </a></td>'; 
           $html .= '</tr>';  
           $html .= '</tbody>';
       }            
       echo $html;
       
    }

    public function actionRevisar($id)
    {
       
       ////////////////////////////////////////Datos del usuario////////////////////////////////       
       $revisa=solicitudpermisos::findOne($id);
       $colorp=$revisa->apruebanomina; 
       $inca=$revisa->motivoId;               
           ///////////Total dias/////////////////////////////////
           $fecha_inicio = strtotime($revisa->fechaInicio); 
           $fecha_fin = strtotime($revisa->fechaFin); 
           $diff = $fecha_fin - $fecha_inicio; 
           $total=round(($diff / 86400)+1). " dias";
   
           // echo $total; 
   
           // die();
       $html = '';
       if ($colorp==136)
       {
        $html .= '<div class="card border-primary mb-3">';
        $html .= '<div class="card-header text-white" style="background-color: #337AB7;">Datos de la solicitud</div>';
       }else
       {
        $html .= '<div class="card border-success mb-3">';
        $html .= '<div class="card-header text-white bg-success">Datos de la solicitud &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<i class="fas fa-check-circle"></i></div>';
       }       
       $html .= '<div class="card-body">';
       $html .= '<div class="col-lg-12">'; 
       $html .= '<div class="table-responsive">';       
       $html .= '<table class="table table-striped table-hover table-bordered" >';
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Nombres</th>';
       $html .=  '<th>Apellidos</th>';
       $html .=  '<th>Documento</th>';       
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$revisa->users->nombres. '</td>';
       $html .= '<td>'.$revisa->users->apellidos. '</td>'; 
       $html .= '<td>'.$revisa->users->numeroIdentificacion. '</td>';        
       $html .= '</tr>';
       $html .= '</tbody>';
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Telefono</th>';
       $html .=  '<th>Cargo</th>';
       $html .=  '<th>Sucursal</th>';       
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$revisa->users->telefono. '</td>';
       $html .= '<td>'.$revisa->users->cargo->nombre. '</td>'; 
       $html .= '<td>'.$revisa->users->puntorecaudo->nombre. '</td>';        
       $html .= '</tr>';
       $html .= '</tbody>';       
       $html .= '<div>';
       $html .= '<div>';
       $html .= '<div>';
       $html .= '<div>';
       ////////////////////////////////////////Datos de solicitud////////////////////////////////                 
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Motivo</th>';
       $html .=  '<th>Jefe autoriza</th>';
       $html .=  '<th>Total dias</th>';       
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$revisa->tiposdetalles->nombre. '</td>';
       $html .= '<td>'.$revisa->jefeinmediato->nombres.''.$revisa->jefeinmediato->apellidos. '</td>'; 
       $html .= '<td>'.$total. '</td>';        
       $html .= '</tr>';
       $html .= '</tbody>';
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Fecha inicio</th>';
       $html .=  '<th>Fecha fin</th>';
       $html .=  '<th>Sucursal</th>';       
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$revisa->fechaInicio. '</td>';
       $html .= '<td>'.$revisa->fechaFin. '</td>'; 
       $html .= '<td>'.$revisa->users->puntorecaudo->nombre. '</td>';        
       $html .= '</tr>';
       $html .= '</tbody>';
       if ($revisa->horaInicio && $revisa->horaFin>0)
       {
           $html .='<thead>';
           $html .='<tr>';
           $html .='<th>Hora Inicio</th>';
           $html .='<th>Hora Fin</th>';                     
           $html .='</tr>';
           $html .='</thead>';
           $html .='<tbody>';
           $html .='<tr>';
           $html .='<th>'.$revisa->horaInicio.'</th>';
           $html .='<th>'.$revisa->horaFin.'</th>';           
           $html .='</tr>';
           $html .='</tbody>';
       }
       $html .= '</table>';          
       $html .= '<table class="table table-striped table-hover table-bordered" >';
       $html .= '<thead>';
       $html .= '<tr>';
       $html .=  '<th>Observacion</th>';       
       $html .= '<tr>';
       $html .= '</thead>';
       $html .= '<tbody>';
       $html .= '<tr>';
       $html .= '<td>'.$revisa->observaciones. '</td>';            
       $html .= '</tr>';
       $html .= '</tbody>';  
       if ($inca==127) 
       {
           $html .= '<thead>';
           $html .= '<th>Incapacidad</th>';
           $html .= '</thead>';
           $html .= '<tbody>';
           $html .= '<tr>';
           $html .= '<td><a download="'.$revisa->incapacidad.'" href="http://35.175.13.101/OsaIntranet/web/images/'.$revisa->incapacidad.'"> <img src="images/'.$revisa->incapacidad.'"style="width: 700px; height: 600px;"> </a></td>'; 
           $html .= '</tr>';  
           $html .= '</tbody>';
       }          
       echo $html;
       
    }

    public function actionVistausuario($id)
    {
        $vistausu=solicitudpermisos::findOne($id);
        $colorpanel=$vistausu->estadoId;
        $inca=$vistausu->motivoId;
        $color=$vistausu->apruebanomina;
        //  echo $colorpanel; 

        //  die();
        ///////////Total dias/////////////////////////////////
        $fecha_inicio = strtotime($vistausu->fechaInicio); 
        $fecha_fin = strtotime($vistausu->fechaFin); 
        $diff = $fecha_fin - $fecha_inicio; 
        $total=round(($diff / 86400)+1). " dias";

        
    $html = '';
    if($colorpanel==122)
    {
        $html .= '<div class="card border-primary mb-3">'; 
        $html .= '<div class="card-header text-white bg-primary ">Datos de la solicitud</div>'; 
    }elseif($colorpanel==123 && $color==136)
    {
        $html .= '<div class="card border-warning mb-3">';
        $html .= '<div class="card-header text-white bg-warning">Datos de la solicitud</div>';
    }elseif($colorpanel==124)
    {
        $html .= '<div class="card border-danger mb-3">';        
        $html .= '<div class="card-header text-white bg-danger">Datos de la solicitud &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<i style="font-size:40px" class="fas fa-times-circle"></i></div>';
    }elseif($colorpanel==123 && $color==135)
    {
        $html .= '<div class="card border-success mb-3">';
        $html .= '<div class="card-header text-white bg-success">Datos de la solicitud &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<i style="font-size:40px" class="fas fa-check-circle"></i></div>';        
    }            
    $html .= '<div class="card-body">';
    $html .= '<div class="col-lg-12">'; 
    $html .= '<div class="table-responsive">';      
    $html .= '<table class="table table-striped table-hover table-bordered" >';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .=  '<th>Nombres</th>';
    $html .=  '<th>Apellidos</th>';
    $html .=  '<th>Documento</th>';       
    $html .= '<tr>';
    $html .= '</thead>';
    $html .= '<tbody>';
    $html .= '<tr>';
    $html .= '<td>'.$vistausu->users->nombres. '</td>';
    $html .= '<td>'.$vistausu->users->apellidos. '</td>'; 
    $html .= '<td>'.$vistausu->users->numeroIdentificacion. '</td>';        
    $html .= '</tr>';
    $html .= '</tbody>';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .=  '<th>Telefono</th>';
    $html .=  '<th>Cargo</th>';
    $html .=  '<th>Sucursal</th>';       
    $html .= '<tr>';
    $html .= '</thead>';
    $html .= '<tbody>';
    $html .= '<tr>';
    $html .= '<td>'.$vistausu->users->telefono. '</td>';
    $html .= '<td>'.$vistausu->users->cargo->nombre. '</td>'; 
    $html .= '<td>'.$vistausu->users->puntorecaudo->nombre. '</td>';        
    $html .= '</tr>';
    $html .= '</tbody>';       
    $html .= '<div>';
    $html .= '<div>';
    $html .= '<div>';        
    ////////////////////////////////////////Datos de solicitud////////////////////////////////                 
    $html .= '<thead>';
    $html .= '<tr>';
    $html .=  '<th>Motivo</th>';
    $html .=  '<th>Jefe autoriza</th>';
    $html .=  '<th>Total dias</th>';       
    $html .= '<tr>';
    $html .= '</thead>';
    $html .= '<tbody>';
    $html .= '<tr>';
    $html .= '<td>'.$vistausu->tiposdetalles->nombre. '</td>';
    $html .= '<td>'.$vistausu->jefeinmediato->nombres.''.$vistausu->jefeinmediato->apellidos. '</td>'; 
    $html .= '<td>'.$total. '</td>';        
    $html .= '</tr>';
    $html .= '</tbody>';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .=  '<th>Fecha inicio</th>';
    $html .=  '<th>Fecha fin</th>';
    $html .=  '<th>Sucursal</th>';       
    $html .= '<tr>';
    $html .= '</thead>';
    $html .= '<tbody>';
    $html .= '<tr>';
    $html .= '<td>'.$vistausu->fechaInicio. '</td>';
    $html .= '<td>'.$vistausu->fechaFin. '</td>'; 
    $html .= '<td>'.$vistausu->users->puntorecaudo->nombre. '</td>';        
    $html .= '</tr>';
    $html .= '</tbody>';
    if ($vistausu->horaInicio && $vistausu->horaFin>0)
       {
           $html .='<thead>';
           $html .='<tr>';
           $html .='<th>Hora Inicio</th>';
           $html .='<th>Hora Fin</th>';                     
           $html .='</tr>';
           $html .='</thead>';
           $html .='<tbody>';
           $html .='<tr>';
           $html .='<th>'.$vistausu->horaInicio.'</th>';
           $html .='<th>'.$vistausu->horaFin.'</th>';           
           $html .='</tr>';
           $html .='</tbody>';
       }     
    $html .= '</table>';          
    $html .= '<table class="table table-striped table-hover table-bordered" >';
    $html .= '<table class="table table-striped table-hover table-bordered" >';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .=  '<th>Observacion solicitud</th>'; 
    if($colorpanel==124)
    {
        $html .=  '<th>Observacion jefe inmediato</th>'; 
    }          
    $html .= '<tr>';
    $html .= '</thead>';
    $html .= '<tbody>';
    $html .= '<tr>';
    $html .= '<td>'.$vistausu->observaciones. '</td>'; 
    if($colorpanel==124)
    {
        $html .= '<td>'.$vistausu->observacionJefeInmediato. '</td>';
    }                
    $html .= '</tr>';              
    $html .= '</tbody>'; 
    $html .= '<table class="table table-striped table-hover table-bordered" >'; 
    if ($inca==127) 
    {
        $html .= '<thead>';
        $html .= '<th>Incapacidad</th>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $html .= '<tr>';
        $html .= '<td><a download="'.$vistausu->incapacidad.'" href="http://35.175.13.101/OsaIntranet/web/images/'.$vistausu->incapacidad.'"> <img src="images/'.$vistausu->incapacidad.'"style="width: 700px; height: 600px;"> </a></td>'; 
        $html .= '</tr>';  
        $html .= '</tbody>';
    }               
    echo $html; 
     
    }

    public function actionMostrarboton($id)
    {
        $muestra=solicitudpermisos::findOne($id); 
        echo $muestra->estadoId;
        
    }
    public function actionModificarsolicitud($id)
    {
        $ver=solicitudpermisos::findOne($id);
        echo $ver->estadoId;
    }
    public function actionRevisado($id)       
    {
        $revisa=solicitudpermisos::findOne($id);
        echo $revisa->apruebanomina;
    }  

   
}
?>


    

