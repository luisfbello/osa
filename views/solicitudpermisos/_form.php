<div class="container-fluid">
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Users;
use kartik\date\DatePicker;
use yii\db\Connection;
use app\models\Cargos;
use app\models\Puntosrecaudo;
use app\models\Tiposdetalles;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\solicitudpermisos */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>
<div class="solicitudpermisos-form">
       <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id'=>'form']); ?>            
       <?php echo $form->field($model, 'usuarioIdusuario')->hiddenInput(['value'=> (Yii::$app->user->identity->id)])->label(false); ?>
  <!-- //////////////////////////////////////PANEL DATOS EMPLEADO////////////////////////////////////////////////////// -->    
       <div class="card border-primary mb-3">
            <div class="card-header text-white" style="background-color: #337AB7;">Datos de usuario</div>
                <div class="card-body">                                         
                    <div class="col-lg-12">  
                        <div class="table-responsive">                                                   
                            <table class="table">                                
                                     <thead><tr>                                            
                                            <th scope="col">Nombres</th>
                                            <th scope="col">Apellidos</th>
                                            <th scope="col">Documento</th>                                            
                                            </tr></thead>   
                                    <tbody><tr>
                                            <td> <?php echo $users->nombres; ?></td>                                                                                                        
                                            <td> <?php echo $users->apellidos; ?></td>                                                                                        
                                            <td> <?php echo $users->numeroIdentificacion; ?></td>                                                                                   
                                           </tr></tbody>                                                                                                                                                         
                                    <thead><tr>                                            
                                            <th scope="col">Telefono</th>
                                            <th scope="col">Cargo</th>
                                            <th scope="col">Regional</th>                                            
                                            </tr></thead>   
                                    <tbody><tr>
                                          <td> <?php echo $users->telefono; ?></td>                                                                                        
                                          <td> <?php echo $users->cargo->nombre; ?></td>                                                                                          
                                          <td> <?php echo  $users->puntorecaudo->nombre; ?></td>                                                                                            
                                          </tr></tbody>                                         
                              </table> 
                           </div>   
                    </div>                                                                         
                </div>
        </div>          
<!-- //////////////////////////////////////PANEL DATOS DE SOLICITUD////////////////////////////////////////////////////// -->
        <div class="card border-primary mb-3">
            <div class="card-header text-white" style="background-color: #337AB7;">Datos de solicitud</div>
                  <div class="card-body">                                                                                                                               
                        <div class="col-lg-12" > 
                         <div class="table-responsive"> 
                            <table class="table">                                                                  
                                  <tbody><tr>
                                  <td>
                                    <?= $form->field($model, 'motivoId')->dropDownList(ArrayHelper::map(tiposdetalles::find()
                                                              ->where(['tipo_idtipo' => 15 ])
                                                              ->orderBy(['nombre'=>SORT_ASC])->all(), 'idTipoDetalle', 'nombre'), 
                                                       [
                                                      'class'=>'form-control a',
                                                      'prompt'=>'[-- Seleccione motivo--]',  
                                                      'required'=>'required',                                                    
                                                       ]                                                      
                                                  ) ?>
                                          <div class="help-block"></div>                                                                                                    
                                   </td>
                                   <td>
                                        <?= $form->field($model, 'usuarioAutoriza')->dropDownList(ArrayHelper::map(users::find()
                                                              ->where(['jefe_inmediato'=>[1]])
                                                              ->orderBy(['nombres'=>SORT_ASC])->all(), 'id',function ($model, $defaultValue){
                                                                return $model->nombres .' '. $model->apellidos;
                                                              }), 
                                                      [
                                                      'required'=>'required',
                                                      'class'=>'form-control a',
                                                      'prompt'=>'[-- Seleccione usuario--]',                                                    
                                                      ]
                                          ) ?>
                                    </td>
                                    <td>
                                    <?php
                                       echo $form->field($model, 'diacompleto')->dropDownList(['1' => 'Si', '0' => 'No'],['required'=>'required','class'=>'form-control a','prompt'=>'[-- Seleccione--]','id'=>'diacompleto' ]);
                                       ?>                                                                                                
                                   </td>
                                   <td><input type="button" class="btn btn-primary" value="Agregar horas" class="form-control"></td>      
                                   </tbody></tr>  
                                
                                  <tbody><tr>
                                      <td>       
                                        <label for="start">Fecha Inicio</label>             
                                        <input type="date" class="form-control" id="start" value="<?php echo $model->fechaInicio ?>" name="fechaInicio" id="finicio" required="required">
                                     </td> 
                                     <td>                                            
                                       <label for="start">Fecha Fin</label>                                                   
                                       <input type="date" class="form-control" id="start" value="<?php echo $model->fechaFin ?>" name="fechaFin" id="ffin" required="required">                                            
                                     </td>                                        
                                       <td> <?= $form->field($model, 'tiempoRepuesto')->textInput() ?></td>                                                                             
<!--//////////////////////////////////////CARGAR ARCHIVO//////////////////////////////////////-->                                       
                                       <td id="incapacidad" class="d-none">                                         
                                         <?= $form->field($model, "incapacidad")->fileInput(['multiple' => false, 'class'=> 'a'])->label('Adjuntar incapacidad') ?>                                         
                                       </td>                                       
                                       <?php echo $form->field($model, 'fechaCreacion')->hiddenInput(['value'=> date('Y-m-d')])->label(false); ?>
                                       <td><?php echo $form->field($model, 'estadoId')->hiddenInput(['value'=>122 ])->label(false); ?></td>                                        
                                  </tr></tbody>                                                                                                                                                                                         
                                  <tbody><tr id="hora" class="d-none">
                                       <td>
                                          <label for="start">Hora inicio</label>  
                                          <input type="time" class="form-control" name="horaInicio" value="00:00">
                                       </td> 
                                       <td>
                                          <label for="start">Hora fin</label>  
                                          <input type="time" class="form-control" name="horaFin" value="00:00">
                                       </td> 
                                  </tr></tbody>
                                </div></div></div> 
                          </table>        
                       </div>   
                                <div class="col-lg-12" >
                                <div class="table-responsive"> 
                                <table class="table">                                 
                                  <tbody><tr>                                                                                                                                                                                                                                                                             
                                        <div class="help-block"></div>  
                                        <td><?= $form->field($model, 'observaciones')->textarea(['maxlength' => true,'style'=>'height: 100px'])?></td>                                                                                                                                                                  
                                   </tr></tbody>
                                  </div>                                     
                                </table>
                                </div>
                       </div>           
                                </div>                                                                                                                           
                    </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        </div>                    
                <div class="form-group">
                    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success','id'=>'guardar']) ?>
                </div>

                <?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">
        $(document).ready(function(){
                // alert("fff");
                $('#solicitudpermisos-motivoid').on('change',function(){           
                        if($(this).val()==127){            
                                $('#incapacidad').removeClass('d-none');      
                        }else{
                                $("#incapacidad").addClass('d-none');
                        }        
                });                  
                $(".btn").click(function(){
                        $('#hora').removeClass('d-none');
                         $("#hora").collapse('toggle');                                       
                });
                $('#diacompleto').on('change',function(){           
                        if($(this).val()==0){            
                                $('#hora').removeClass('d-none');      
                        }else{
                                $("#hora").addClass('d-none');
                        }        
                });                                                                                        
        }); // document ready                   
</script>