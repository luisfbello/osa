<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cargos;
use app\models\Rol;
use kartik\date\DatePicker;
use yii\helpers\Url;
?>
  <script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>
<?php $form = ActiveForm::begin([
    'method' => 'post',
 'id' => 'formulario',
 'enableClientValidation' => false,
 'enableAjaxValidation' => true,
]);
?>

<div class="jumbotron">
    <img width="250" src="<?php echo Url::base(); ?>/images/f52610_b3fa64f9ff4d4ac0a9d8f745fa6b0a82.png" alt="..." class="">
    <!-- <p class="lead">Registro Usuarios OsaNet</p> -->
</div>
<center><h2>Registro usuarios OsaNet</h2></center>
<!-- ====== INFORMACION DE LA APLICACION ============ -->
<div class="panel panel-primary">
    <div class="panel-heading">Informacion de la aplicacion</div>
    <div class="panel-body">
        <div class="col-lg-12 noPadding">
            <div class="form-group col-lg-6">
             <?= $form->field($model, "username")->input("text") ?>   
            </div>
            <div class="form-group col-lg-6">
             <?= $form->field($model, "email")->input("email") ?>   
            </div>
        </div>
        <div class="col-lg-12 noPadding">
            <div class="form-group col-lg-6">
             <?= $form->field($model, "password")->input("password") ?>   
            </div>

            <div class="form-group col-lg-6">
             <?= $form->field($model, "password_repeat")->input("password") ?>   
            </div>
        </div>
    </div>
</div>
<!-- ====== INFORMACION PERSONAL ============ -->
<div class="panel panel-primary">
    <div class="panel-heading">Informacion personal</div>
    <div class="panel-body">
       <div class="col-lg-12 noPadding">
           <div class="form-group col-lg-6">
            <?= $form->field($model, "nombres") ?>   
           </div>

           <div class="form-group col-lg-6">
            <?= $form->field($model, "apellidos") ?>   
           </div>
       </div>
       <div class="col-lg-12 noPadding">
           <div class="form-group col-lg-6">
                <?= $form->field($model, "numeroIdentificacion") ?>   
           </div>
       </div> 
       <div class="col-lg-12 noPadding">
            <div class="form-group col-lg-6">
             <?= $form->field($model, "telefono") ?>   
            </div>
           <div class="form-group col-lg-6">
            <?= $form->field($model, "direccion") ?>   
           </div>
       </div>
    </div>
</div>

<!-- ============== ROLES ASOCIADOS AL PERFIL ============ -->
<div class="panel panel-primary">
  <div class="panel-heading">Roles Asociados al perfil</div>
  <div class="panel-body">
    <div class="col-lg-12 noPadding">
        <?php foreach ($roles as $r) { ?>
          <button type="button" class="btn btn-primary addRole" id="<?php echo $r->id; ?>">
            <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php echo $r->nombre; ?>
          </button>
        <?php } ?>
        <?= $form->field($model, "role")->textInput(['readonly'=>'readonly']) ?>   
    </div>
  </div>
</div>

<?= Html::submitButton("Registrar usuario", ["class" => "btn btn-primary"]) ?>

<?php $form->end() ?>

<!-- ================ JS =========== -->
<script>
  $(document).ready(function(){
    $(".addRole").click(function(){
      if ($(this).hasClass('select')) {
        $(this).removeClass('select');
        $(this).addClass('btn-primary');
        $(this).removeClass('btn-success');
      }else{
        $(this).addClass('btn-success');
        $(this).addClass('select')
        $(this).removeClass('btn-primary');
      }
      validateRoleActive();
    });
  })
  // =============== FUNCIONES ==================
  function validateRoleActive(){
    var concatRole = '';
    var init = false;
    $(".addRole").each(function(){
      console.log($(this).attr('class'));
      if ($(this).hasClass('select')) {
        if (init) {
          concatRole = concatRole + ',' + $(this).attr('id');
        }else{
          concatRole = $(this).attr('id');
          init = true;
        }
      }
    });
    $("#formregister-role").val(concatRole);
  }
</script>