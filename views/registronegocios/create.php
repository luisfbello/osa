<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Registronegocios */

$this->title = 'Create Registronegocios';
$this->params['breadcrumbs'][] = ['label' => 'Registronegocios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registronegocios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
