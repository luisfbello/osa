<?php

use kartik\mpdf\Pdf;
$params = require(__DIR__ . '/params.php');

$config = [
    'name' => 'Base',
    'defaultRoute' => 'site/indexweb',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'es',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'AmDQLA3UQ8iYPIgNIDMRjSqMRq7M7L7A',
        ],
        
        

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true, // debe estar en false para que expire la sesion
            // 'authTimeout' => 30, // tiempo en segundos de expiracion de sesion

        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mail' => [
                    'class'            => 'zyx\phpmailer\Mailer',
                    'useFileTransport' => false,
                    'config'           => [
                        'mailer'     => 'smtp',
                        'host'       => 'smtp.gmail.com',
                        'port'       => '465',
                        'smtpsecure' => 'ssl',
                        'smtpauth'   => true,
                        'username'   => 'cristian921215@gmail.com',
                        'password'   => 'gato6612',
                    ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'pdf' => [
                'class' => Pdf::classname(),
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_BROWSER,
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                'marginTop' => 2,
                'marginHeader' => 2,
                'marginLeft' => 2,
                // refer settings section for all configuration options
            ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*']
    ];
}

return $config;
