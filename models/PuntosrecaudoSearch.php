<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Puntosrecaudo;

/**
 * PuntosrecaudoSearch represents the model behind the search form about `app\models\Puntosrecaudo`.
 */
class PuntosrecaudoSearch extends Puntosrecaudo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPuntoRecaudo', 'sucursales_idsucursales', 'localidades_idlocalidades'], 'integer'],
            [['nombre', 'fechaCreacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Puntosrecaudo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idPuntoRecaudo' => $this->idPuntoRecaudo,
            'sucursales_idsucursales' => $this->sucursales_idsucursales,
            'localidades_idlocalidades' => $this->localidades_idlocalidades,
            'fechaCreacion' => $this->fechaCreacion,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
