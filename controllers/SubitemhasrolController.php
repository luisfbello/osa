<?php

namespace app\controllers;

use Yii;
use app\models\SubitemHasRol;
use app\models\SubitemMenu;
use app\models\Rol;
use app\models\SubitemHasRolSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SubitemhasrolController implements the CRUD actions for SubitemHasRol model.
 */
class SubitemhasrolController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubitemHasRol models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubitemHasRolSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSubitem($id,$estado){
        $model = $this->findModel($id);
        $model->estado = $estado;
        $model->update();
    }

    /**
     * Displays a single SubitemHasRol model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SubitemHasRol model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idRol,$idItem)
    {
        $model = new SubitemHasRol();
        $rol = Rol::findOne($idRol);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                return $this->redirect(['opcionhasrol/index']);
            }else{
                echo 'falied save sub item has rol';
            }


        } else {
            return $this->render('create', [
                'model' => $model,
                'rol' => $rol,
                'item' => $idItem,
            ]);
        }
    }

    /**
     * Updates an existing SubitemHasRol model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($idRol,$idSubitem)
    {
        $rol = Rol::findOne($idRol);

        $model = $this->findModel($idSubitem);
        $item = $model->idSubitem->idItem->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

                return $this->redirect(['opcionhasrol/index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'rol' => $rol,
                'item' => $item,
            ]);
        }
    }

    /**
     * Deletes an existing SubitemHasRol model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SubitemHasRol model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubitemHasRol the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubitemHasRol::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
