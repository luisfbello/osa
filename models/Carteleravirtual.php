<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "carteleravirtual".
 *
 * @property string $cartelera_virtual_id
 * @property string $cartelera_virtual_imagen
 * @property string $cartelera_virtual_texto
 * @property string $cartelera_virtual_fecha_publicacion
 * @property integer $cartelera_virtual_activo
 * @property string $cartelera_virtual_titulo
 * @property integer $cartelera_virtual_video
 * @property string $cartelera_virtual_link_video
 */
class Carteleravirtual extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carteleravirtual';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cartelera_virtual_texto', 'cartelera_virtual_link_video'], 'string'],
            [['cartelera_virtual_fecha_publicacion'], 'safe'],
            [['cartelera_virtual_activo', 'cartelera_virtual_video'], 'integer'],
            [['cartelera_virtual_imagen', 'cartelera_virtual_titulo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cartelera_virtual_id' => 'Cartelera Virtual ID',
            'cartelera_virtual_imagen' => 'Cartelera Virtual Imagen',
            'cartelera_virtual_texto' => 'Cartelera Virtual Texto',
            'cartelera_virtual_fecha_publicacion' => 'Cartelera Virtual Fecha Publicacion',
            'cartelera_virtual_activo' => 'Cartelera Virtual Activo',
            'cartelera_virtual_titulo' => 'Cartelera Virtual Titulo',
            'cartelera_virtual_video' => 'Cartelera Virtual Video',
            'cartelera_virtual_link_video' => 'Cartelera Virtual Link Video',
        ];
    }
}
