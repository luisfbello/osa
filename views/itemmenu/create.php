<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ItemMenu */

$this->title = 'Crear Item';
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['opcionhasrol/index']];
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-menu-create">

    <h2 class="alert alert-info"><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
