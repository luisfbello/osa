<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Procesos */

$this->title = 'Update Procesos: ' . ' ' . $model->proceso_id;
$this->params['breadcrumbs'][] = ['label' => 'Procesos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->proceso_id, 'url' => ['view', 'id' => $model->proceso_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="procesos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
