<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\nav\NavX;
use yii\helpers\Url;
use app\models\OpcionHasRol;
use app\models\ItemHasRol;
use app\models\SubitemHasRol;


AppAsset::register($this);
?>
<?php

$this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Osa Intranet</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo Url::base(); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?php echo Url::base(); ?>/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo Url::base(); ?>/css/sb-admin.css" rel="stylesheet">
  <link href="<?php echo Url::base(); ?>/css/site.css" rel="stylesheet">

  <script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>


</head>

<body id="page-top">


  <nav class="navbar navbar-expand navbar-dark bg-info static-top">

    <a class="navbar-brand mr-1" href="index.html">Organizacion Sayco-Acinpro</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <!-- <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">
            <i class="fas fa-search"></i>
          </button>
        </div> -->
      </div>
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">

      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">


          <?php if (Yii::$app->user->isGuest) { ?>
            <?= Html::a('Login', ['site/login'], ['class' => 'dropdown-item']) ?>
          <?php } else { ?>
            <?= Html::a('Editar', ['users/update', 'id' => Yii::$app->user->identity->id], ['class' => 'dropdown-item']) ?>
            <div class="dropdown-divider"></div>
            <?= Html::a('Salir', ['site/logout'], ['class' => 'dropdown-item'], ['data-method' => 'post']) ?>

            <!-- <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Salir</a> -->
          <?php } ?>


        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper" class="alert-light">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">


        <?= Html::a('<i class="fas fa-fw fa-tachometer-alt"></i> Cartelera Virtual', ['site/indexweb'], ['class' => 'nav-link']) ?>
        <!-- <a class="nav-link" href="<?php Url::base(); ?>/site/index">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Inicio</span>
        </a> -->
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>La Organización</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">La Organización</h6>
          <?= Html::a('OSA', ['osa/index'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('SAYCO', ['osa/sayco'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('ACINPRO', ['osa/acinpro'], ['class' => 'dropdown-item']) ?>

        </div>
      </li>

      <li id="listasolicitud" class="nav-item dropdown ">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Solicitudes</span>
        </a>
        <div id="submenuSolicitud" class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Solicitudes</h6>

          <a id="mejora" style="color:black" class="nav-link dropdown-toggle" data-toggle="collapse" data-target="#demo">Mejoras en Osaplus</a>
          <div id="demo" class="collapse">
            <?= Html::a('Mejoras a Osaplus', ['solicitudes/create'], ['class' => 'dropdown-item']) ?>
            <?php
            if (Yii::$app->user->isGuest) {
              //not logged user
            } else {
              echo Html::a('Listado Solicitudes', ['solicitudes/index'], ['class' => 'dropdown-item']);
            }
            ?>
          </div>

          <a style="color:black" class="nav-link dropdown-toggle" data-toggle="collapse" data-target="#demo2">Gestion Funcionarios</a>
          <div id="demo2" class="collapse">
            <?= Html::a('Gestion funcionarios', ['funcionariosnuevos/create'], ['class' => 'dropdown-item']) ?>
            <?php
            if (Yii::$app->user->isGuest) {
              //not logged user
            } else {
              echo Html::a('Listado funcionarios', ['funcionariosnuevos/index'], ['class' => 'dropdown-item']);
              if (Yii::$app->user->identity->id == 1) {
                echo Html::a('Funcionarios creados', ['funcionariosnuevos/funcionarioscreados'], ['class' => 'dropdown-item']);
              }
            }
            ?>
          </div>

          <!-- <a class="dropdown-item" href="forgot-password.html">Mejoras a OsaPlus</a> -->
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Aplicaciones</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Aplicaciones</h6>
          <!-- <?= Html::a('Informacion SGC', ['documentossgc/index'], ['class' => 'dropdown-item']) ?> -->
          <!-- <a class="dropdown-item" href="login.html">Informacion SGC</a> -->
          <!-- <a class="dropdown-item" href="register.html">Gestion de Calidad</a> -->
          <a class="dropdown-item" href="https://saycoacinpro.org/OsaPlus/web/index.php?r=site%2Flogin" target="_blank">OsaPlus</a>
        </div>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span></a>
      </li> -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>OSA Comunidad</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Osa Comunidad</h6>
          <?= Html::a('Blog Osa', ['comunidad/blog'], ['class' => 'dropdown-item']) ?>
          <!-- <a class="dropdown-item" href="forgot-password.html">Blog OSA</a> -->
          <a class="dropdown-item" href="forgot-password.html">Comunicaciones</a>
          <?= Html::a('Boletines', ['comunidad/boletines'], ['class' => 'dropdown-item']) ?>
          <!-- <a class="dropdown-item" href="forgot-password.html">Boletines</a> -->
          <?= Html::a('Circulares', ['comunidad/circulares'], ['class' => 'dropdown-item']) ?>
          <!-- <a class="dropdown-item" href="forgot-password.html">Circulares</a> -->
          <?= Html::a('Equipo Humano', ['comunidad/equipo'], ['class' => 'dropdown-item']) ?>
          <!-- <a class="dropdown-item" href="forgot-password.html">Equipo Humano</a> -->
          <?= Html::a('Videos Internos', ['comunidad/videos'], ['class' => 'dropdown-item']) ?>
          <!-- <a class="dropdown-item" href="forgot-password.html">Videos Internos</a> -->
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Gestión de Calidad</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Gestion De calidad</h6>
          <?= Html::a('Auditoria', ['documentos/auditoria'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('Contabilidad', ['documentos/contabilidad'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('Dirección Ejecutiva', ['documentos/direccionejecutiva'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('Gestión de Calidad', ['documentos/calidad'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('Información SGC', ['documentos/infosgc'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('Gestión Humana', ['documentos/gestionhumana'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('Gestión Juridica', ['documentos/juridica'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('Recaudo', ['documentos/recaudo'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('Tecnología', ['documentos/tecnologia'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('Tesorería', ['documentos/tesoreria'], ['class' => 'dropdown-item']) ?>
          <?= Html::a('SG SST', ['documentos/sgsst'], ['class' => 'dropdown-item']) ?>

          <!-- <a class="dropdown-item" href="#">SG SST</a> -->

        </div>
      </li>
      
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Comunicaciones</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Comunicaciones</h6>
            <?php if (Yii::$app->user->id == 2) { ?>
            <?= Html::a('Registro Nuevos', ['formularios/viewnuevos'], ['class' => 'dropdown-item']) ?>
            <?php } ?>
            <?= Html::a('Servicio al Cliente', ['servicio-al-cliente/servicioclienteindex'], ['class' => 'dropdown-item']) ?>
            <!-- <?=Html::a('P.Q.R.', ['servicio-al-cliente/pqrindex'], ['class' => 'dropdown-item'])?> -->

          </div>
        </li>


      

      <?php if (!Yii::$app->user->isGuest) { ?>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Gestión Humana</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Gestión Humana</h6>
            <?= Html::a('Solicitud Permisos', ['solicitudpermisos/index'], ['class' => 'dropdown-item']) ?>
            <?php if (Yii::$app->user->identity->id == 112) { ?>
              <?= Html::a('Revision Nomina', ['solicitudpermisos/revisanomina'], ['class' => 'dropdown-item']) ?>

          </div>
        </li>
      <?php } ?>
    <?php } ?>
|



    </ul>
    <div id="content-wrapper">
      <div class="container ">

        <?php echo $content ?>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer ">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © <p class="pull-left"> OSA <?= date('Y') ?></p></span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="vendor/chart.js/Chart.min.js"></script>
  <script src="vendor/datatables/jquery.dataTables.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="js/demo/chart-area-demo.js"></script>

  <script>
    $(document).ready(function() {
      $('#listasolicitud').mouseover(function() {
        $('#submenuSolicitud').show();

      });

      $('#listasolicitud').mouseout(function() {
        $('#submenuSolicitud').hide();

      });

    });
  </script>

</body>

</html>

<?php $this->endPage() ?>