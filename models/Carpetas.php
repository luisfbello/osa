<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "carpetas".
 *
 * @property integer $carpeta_id
 * @property string $carpeta_nombre
 * @property integer $carpeta_proceso_id
 */
class Carpetas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carpetas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['carpeta_proceso_id'], 'integer'],
            [['carpeta_nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'carpeta_id' => 'Carpeta ID',
            'carpeta_nombre' => 'Carpeta Nombre',
            'carpeta_proceso_id' => 'Carpeta Proceso ID',
        ];
    }
}
