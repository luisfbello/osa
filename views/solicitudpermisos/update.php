<?php

use yii\helpers\Html;
use app\models\users;

/* @var $this yii\web\View */
/* @var $model app\models\solicitudpermisos */

$this->title = 'Modificar solicitud : ' . $model->idsolicitudpermisos;
$this->params['breadcrumbs'][] = ['label' => 'Solicitudpermisos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idsolicitudpermisos, 'url' => ['view', 'id' => $model->idsolicitudpermisos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="solicitudpermisos-update">
	<div class="solicitudpermisos-create">
		<div class="container-fluid">
		 <div class= "alert alert-info" role="alert"><h4><?= Html::encode($this->title) ?></h4></div>
		</div> 
	</div>	
	        <!-- <div class= "panel panel-primary"></div> -->
	            <div class= "panel-body">
<div class = "table-responsive">

   

    <?= $this->render('formupdate', [
        'model' => $model,
        'users'=> $users,       
    ]) ?>

</div>
