<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OpcionMenu */

$this->title = 'Crear Opcion';
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['opcionhasrol/index']];
$this->params['breadcrumbs'][] = ['label' => 'Opciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcion-menu-create">

    <h2 class="alert alert-info"><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
