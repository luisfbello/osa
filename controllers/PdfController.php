<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\helpers\Url;
use app\models\Negocios;
use app\models\Actas;
use app\models\Users;
use app\models\User;
use app\models\Pagos;
use app\models\PuntosRecaudo;
use app\models\Documentos;
use app\models\Liquidaciones;
use app\models\LiquidacionesDetalle;
use app\models\EstadosCuenta;
use app\models\Actacompromisodetalles;

class PdfController extends \yii\web\Controller

{

    // ============================    PDF CNU                                 =============================================
        public function actionCnu($id)
        {

            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $documento = Documentos::find()
                        ->where(['idDocumento' => $id])
                        ->andWhere(['tipoDocumento' => 7])->One();
            $nombreCompleto = $documento->negociosIdnegocios->tercerosIdterceros->nombres.' '.$documento->negociosIdnegocios->tercerosIdterceros->apellidos;
            $identificacion = $documento->negociosIdnegocios->tercerosIdterceros->identificacion;


            switch (date('m')) {
                case 01: $mes = 'ENERO'; break;
                case 02: $mes = 'FEBRERO'; break;
                case 03: $mes = 'MARZO'; break;
                case 04: $mes = 'ABRIL'; break;
                case 05: $mes = 'MAYO'; break;
                case 06: $mes = 'JUNIO'; break;
                case 07: $mes = 'JULIO'; break;
                case 08: $mes = 'AGOSTO'; break;
                case 09: $mes = 'SEPTIEMBRE'; break;
                case 10: $mes = 'OCUBRE'; break;
                case 11: $mes = 'NOVIEMBRE'; break;
                case 12: $mes = 'DICIEMBRE'; break;
            }


            // # ================= INICIO CONSTRUCCION DE PDF
                $content = '';
                $content .= '<div style="font-family: arial;">';
                    // ==== logo OSA
                    $content .= '<div style="width:250px; float:left;">
                                    <img src="'.Url::base().'/images/f52610_b3fa64f9ff4d4ac0a9d8f745fa6b0a82.png" width="210"/>
                                </div>';
                    // ==== Informacion CNU
                    $content .= '<div style="width:370px; text-align:right;float:right;">
                                    <p style="float:right;">RJC 002</p>
                                    <h2 style="float:right; padding-bottom:0px; margin:0px; margin-top:5px; padding-right: 25px;">
                                       CNU - '.$documento->idDocumento.'
                                    </h2>
                                    <i style="font-weight:bold; font-size: 9px;">Personeria Juridica Res. No. 291 DNDA 18/10/2011</i>
                                </div>';
                    // ==== titulo prohibicion
                    $content .= '<h3 style="width: 100%; text-align:center; margin-top:30px;">
                                    PROHIBICIÓN DE COMUNICACIÓN PÚBLICA DE LA MÚSICA <br> '.$documento->negociosIdnegocios->identificador.'
                                </h3>';
                   // ==== primer parrafo
                   $content .= '<p style="width: 100%; text-align:justify; font-size: 11px; margin-top: 30px;">
                                    El señor (a) '.$nombreCompleto.', identificado con cedula Ciudadania No. '.$identificacion.' en su calidad de propietario del
                                    establecimiento denominado '.$documento->negociosIdnegocios->razonSocial.', ubicado en la '.$documento->negociosIdnegocios->direccion.' de la Ciudad de 
                                    '.$documento->negociosIdnegocios->municipios->nombre.', manifiesta bajo la gravedad del juramento que en el citado estalecimiento NO posee medios idòneos como Radio,
                                    televisiòn, equipos elèctricos o electrònicos, o dispositivos digitales aptos para la comunicaciòn pùblica de la mùsica, 
                                    almacenamiento digital de obras musicales, fonograficas y obras musicales
                                </p>';
                   // ==== segundo parrafo
                   $content .= '<p style="width: 100%; text-align:justify; font-size: 11px; margin-top: 20px;">
                                    Por lo tanto y haciendo uso de las facultades asignadas a los titulares de los derechos de autor establecida en el artìculo 163,
                                    numeral 4 de la Ley 23 de 1982, concordante con el artìculo 76, numeral 1, de la misma disposiciòn, el artìculo 8 del Decreto 3942 
                                    del 25 de octubre de 2010, esta comunicaciòn constituye formalmente una NOTIFICACIÒN DE PROHIBICIÒN DE USO DEL 
                                    PERETORIO DE NUESTROS MANDANTES. En caso de comprobarse despuès de la fecha de expediciòn de esta prohibiciòn, que 
                                    viene ejecutando pùblicamente las obras musicales y fonogramas de nuestro repertorio, esta Organizaciòn emprenderà las acciones 
                                    legales que correspondan por la infracciòn de los derechos patrimoniales de autor y derechos conexos, establecidos en las Leyes 23 
                                    de 1982, 44 de 1993, 232 de 1995 y artìculo 271 del Còdigo Penal.
                                </p>';
                   // ==== tercer parrafo
                   $content .= '<p style="width: 100%;text-align:justify; font-size: 8px; margin-top: 50px;">
                                    Conforme a lo previsto por la Ley 1581 de 2012 (Ley de protección de datos personales) le informamos que los datos obtenidos por la Organización Sayco Acinpro, son datos públicos (Artículo 10 
                                    Literal b) de la citada Ley por lo tanto para su recolección no es necesaria la autorización del titular de la información; por cuanto tal información podrá ser tomada del registro del certificado de Cámara 
                                    de Comercio, en cumplimiento de la ley 232 de 1995, que como comerciante debe cumplir.

                                </p>';
                    // ==== cuarto parrafo
                   $content .= '<p style="width: 100%;text-align:justify; font-size: 8px; margin-top: 20px;">
                                    Yo, '.$nombreCompleto.', mayor de edad, domiciliado en la ciudad de '.$documento->negociosIdnegocios->tercerosIdterceros->municipios->nombre.', obrando como representante legal del , autorizo a la ORGANIZACIÓN SAYCO ACINPRO de manera escrita, expresa, concreta, suficiente, 
                                    voluntaria e informada, para que toda la información personal actual y la que se genere en el futuro fruto de las relaciones comerciales y/o contractuales establecidas en las leyes 23 de 1982 y 232 de 1995, sea manejada 
                                    en los tórminos de la Ley 1581 de 2012 referente al Tratamiento de Datos Personales.
                                </p>';
                    // ==== cuarto parrafo
                   $content .= '<p style="width: 100%;text-align:justify; font-size: 8px; margin-top: 20px;">
                                    La Organización igualmente, le informa que sus datos serán incorporados en un registro automatizado con una finalidad exclusivamente administrativa y  en cumplimiento del objeto social de la Organización, cual es, el 
                                    recaudo por la autorización pública cuando se almacenen digitalmente obras musicales, fonogramas y videos musicales y se comuniquen al público obras musicales, fonogramas, y videos musicales, así como la reproducción 
                                    parcial de material editorial, realizado principalmente a través del sistema de fotocopiado, en establecimientos, conforme a las Leyes 23 de 1982 y  232 de 1995. Los datos estarán almacenos en una entidad idónea, en donde 
                                    serán custodiados mediante el empleo de herramientas de seguridad de la información, razonablemente aceptadas en la industria, procedimientos de control de acceso y mecanismos criptográficos, entre otros. Todo lo anterior 
                                    con el objetivo de evitar el acceso no autorizado, por parte de terceros, a los datos personales almacenados.
                                </p>';
                    // ==== quinto parrafo
                   $content .= '<p style="width: 100%;text-align:justify; font-size: 8px; margin-top: 20px;">
                                    Usted tiene derecho a conocer, actualizar, rectificar y eliminar los datos personales (Cuando ya no ejerza su actividad comercial) de los cuales es titular, mediante los procedimientos establecidos en la Ley 1581/2012 
                                    (Ley de Protección de Datos Personales). Así mismo, por ser un Secreto Empresarial cuya confidencialidad está protegida en los términos del Artículo 260 y siguientes de la Decisión Andina 486 de 2000, no podrá ser entregada 
                                    a terceros, diferentes a realizar la gestión de recaudo o por solicitud de autoridad competente.
                                </p>';
                    // ==== sexto parrafo
                   $content .= '<p style="width: 100%;text-align:justify; font-size: 8px; margin-top: 20px;">
                                    Igualmente, me comprometo a leer el Manual de Protección de Bases de Datos que posee la Organización, en la página web institucional www.saycoacinpro.org.co.
                                </p>';
                    // ==== texto NOTA
                   $content .= '<p style="width: 100%;text-align:justify; font-size: 12px; margin-top: 20px;">
                                    NOTA: Este Documento se expide sin costo alguno Art. 8 Decreto 3942 de 2010.
                                </p>';
                    // ==== fecha de expedicion
                   $content .= '<div style="float:left; font-size:11px; width:50%; text-align:center; margin-top:15px;">
                                    Fecha de Expediciòn <br> '.date('d/m/Y').'
                                </div>';
                    // ==== fecha de expiracion
                   $content .= '<p style="float:right; font-size:11px; width:50%; text-align:center; margin-top: 0px;">
                                    Fecha de Expiraciòn <br> 31/12/'.date('Y').'
                                </p>';
                    // ==== texto de firmado a los dias n
                    $content .= '<p style="width: 100%;text-align:justify; font-size: 12px; margin-top: 20px;">
                                     Firmado en la ciudad de Bogotá a los '.date('d').' días del mes de '.$mes.' del año 2015.
                                 </p>';
                    // ==== firma sayco acinpro
                    $content .= '<div style="float:left; font-size:10px; width:50%; text-align:center; margin-top:45px;">
                                     ____________________________________________ <br> 
                                     '.$userIdentity->nombres.' '.$userIdentity->apellidos.' <br> 
                                     Organizaciòn Sayco Acinpro
                                 </div>';
                    // ==== firma propietario
                    $content .= '<p style="float:right; font-size:10px; width:50%; text-align:center; margin-top: 0px;">
                                     ____________________________________________ <br> 
                                     '.$nombreCompleto.' <br> 
                                     CC: '.$documento->negociosIdnegocios->tercerosIdterceros->identificacion.' <br> 
                                     Propietario
                                 </p>';
                    // ==== titulo sedes y direcciones
                    $content .= '<p style="width:100%; font-size: 11px; text-align:center; margin:0px; padding:0px; margin-top: 25px;">
                                    SEDES Y DIRECCIONES
                                 </p>';
                    // ==== informacion sedes y direcciones
                    $content .= '<p style="width:100%; text-align:center; margin: 0px; padding: 0px; font-size: 10px;">
                                    Villavicencio 6622163-6727144. Tunja 7442048-3214492941. Bogotá - Restrepo 3615286-2094612. 
                                    Bogotá - Teusaquillo 3230899-3102040740. Medellín 5132750-5111290. Pereira 3244209-3244380. 
                                    Manizales 8977818. Armenia 7140782-3113217338. Cali 6601090-6674444.Pasto 7314083-3105669245.
                                    Tulua 2258565-2258103. Barranquilla 3512782-3514051. Cartagena 6642633-3105582204. Santa Marta 4234500. 
                                    Monteria 7810489-3114486031. Sincelejo 2825318-3012611917. Bucaramanga 6429175-6422088. 
                                    Cúcuta 5730225-5724163. Ibagué 2633713-3105853363. Neiva 8710053.
                                 </p>';


                $content .= '</div>';
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 15,
                'marginLeft' => 15,
                'marginRight' => 15,
                'marginBottom' => 8,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('CNU.pdf', 'D'); // call the mpdf api output as needed

            // return $this->render('index');
        }
    // ============================    PDF LIQUIDACION                         =============================================

        public function actionLq($id){


            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $documento = Liquidaciones::find()
                        ->where(['idliquidacion' => $id])
                        ->One();
            $nombreCompleto = $documento->negociosIdnegocios->tercerosIdterceros->nombres.' '.$documento->negociosIdnegocios->tercerosIdterceros->apellidos;
            $identificacion = $documento->negociosIdnegocios->tercerosIdterceros->identificacion;

            $liquidacionesDetalle = LiquidacionesDetalle::find()
                                    ->Where(['liquidaciones_idliquidacion' => $id])
                                    ->all();
            $entidadesLiquidacion = LiquidacionesDetalle::find()
                                    ->Where(['liquidaciones_idliquidacion' => $id])
                                    ->groupBy(['entidades_identidades'])
                                    ->all();
            $pagos = Pagos::find()
                    ->Where(['liquidaciones_idliquidacion' => $id])
                    ->andWhere(['anulado' => 0])
                    ->orderBy(['periodo' => SORT_DESC])
                    ->all();

            $arrayLiquidacion = array();
            $valorLiquidacion = 0;
            $valorPago = 0;
            foreach ($liquidacionesDetalle as $lqd) {
                $valorLiquidacion = $valorLiquidacion + $lqd->valor;
            }
            foreach ($pagos as $p) {
                $valorPago = $valorPago + $p->valor;
            }
            foreach ($entidadesLiquidacion as $eld) {
                array_push($arrayLiquidacion, $eld->entidades_identidades);
            }

            $fechaVencimiento = date_create($documento->fechaVencimiento);
            $fechaVencimiento = date_format($fechaVencimiento,'Y-m-d');

            $fechaExpedicion = date_create($documento->fechaExpedicion);
            $fechaExpedicion = date_format($fechaExpedicion,'Y-m-d');


            // # ================= INICIO CONSTRUCCION DE PDF
                $content = '';
                $content .= '<div style="font-family: arial;">';

                    $content .= '<p style="width: 100%; text-align:right; font-size: 12px; color: red; font-weight:bold;">"VÁLIDO UNICAMENTE CON EL <br> TIMBRE O SELLO DEL BANCO"</p>';
                    $content .= '<div style="width: 70%; margin:0px auto; text-align: center;">
                                    <img src="'. Url::base()  .'/images/logoPdf.png" />
                                </div>';
                    $content .= '<div style="width: 50%; float:left; margin-top: 0px; text-align:center; height: 45px;">
                                    <span style="font-size: 10px;" >AUTORIZACIÓN POR LA COMUNICACIÓN DE OBRAS AL PÚBLICO</span> <br>
                                    |||||||||||||||||||||||||||||||
                                </div>';
                    // bloque fecha limite de pago
                    $content .= '<div style="width: 50%; float:left; height: 45px; font-size: 11px;">
                                    <div style="width: 90%; float:right; background: #ccc; border: 1px solid black; height: 20px;">
                                        <div style="width: 58%; margin-left: 2%; float: left; font-style: italic;">
                                            FECHA LIMITE DE PAGO: 
                                        </div>
                                        <div style="width: 38%; float: left; margin-right: 2%; text-align: right;">
                                            '.$fechaVencimiento.'
                                        </div>
                                    </div>
                                    <div style="width: 90%; margin-top: 5px; float:right; background: #ccc; border: 1px solid black; height: 20px;">
                                        <div style="width: 58%; margin-left: 2%; float: left; font-style: italic;">
                                            TOTAL A PAGAR: 
                                        </div>
                                        <div style="width: 38%; float: left; margin-right: 2%; text-align: right;">
                                            '.'$' . number_format($valorLiquidacion,0) .'
                                        </div>
                                    </div>
                                </div>';
                    // bloque informacion del negocio
                    $content .= '<div style="width: 100%; font-size: 8px; padding: 2px; float: left; margin-top: 5px; border: 1px solid black; text-align:center;">
                                    <div style="width: 100%; font-size: 11px; float: left; background: #141414; color: white; text-align: center;">INFORMACIÓN DEL NEGOCIO</div>
                                    <div style="width: 20%; float:left;" >
                                        <div style="width: 100%;">CÓDIGO</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->identificador.'
                                        </div>
                                    </div>
                                    <div style="width: 15%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">NIT</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->identificador.'
                                        </div>
                                    </div>
                                    <div style="width: 23%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">RAZÓN SOCIAL</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->razonSocial.'
                                        </div>
                                    </div>
                                    <div style="width: 20%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">ACTIVIDAD</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'
                                        </div>
                                    </div>
                                    <div style="width: 21%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">PROPIETARIO AFILIADO</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$nombreCompleto.'
                                        </div>
                                    </div>

                                    <div style="width: 19%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">DIRECCIÓN</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->direccion.'
                                        </div>
                                    </div>
                                    <div style="width: 12%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">TELÉFONO</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->telefono.'
                                        </div>
                                    </div>
                                    <div style="width: 15%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">CIUDAD</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->municipios->nombre.'
                                        </div>
                                    </div>
                                    <div style="width: 18%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">BARRIO</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->barrios->nombre.'
                                        </div>
                                    </div>
                                    <div style="width: 15%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">C. DE COSTOS</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'
                                        </div>
                                    </div>
                                    <div style="width: 18%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">FECHA DE EXPEDICIÓN</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->fechaExpedicion.'
                                        </div>
                                    </div>
                                </div>';
                    // bloque informacion de la cuenta
                    // height total por entidad es 350px
                    $content .= '<div style="width: 100%; font-size: 8px; padding: 2px; float: left; margin-top: 5px; border: 1px solid black;">                        <div style="width: 100%; font-size: 11px; float: left; background: #141414; color: white; text-align: center;">INFORMACIÓN DE LA CUENTA</div>';
                            // foreach entidades
                            $content .= '<div style="width: 100%; margin-bottom: 5px; float: left; padding: 2px; border: 1px solid black;">';
                            foreach ($arrayLiquidacion as $e) {
                                switch ($e) {
                                    case 1:
                                        $nombreEntidad = 'Comunicacion - Musica';
                                    break;
                                    case 4:
                                        $nombreEntidad = 'Almacenamiento';
                                    break;
                                    case 5:
                                        $nombreEntidad = 'Reprografia';
                                    break;
                                    case 6:
                                        $nombreEntidad = 'Peliculas - MPLC';
                                    break;
                                    case 7:
                                        $nombreEntidad = 'Comunicacion - Tranporte';
                                    break;
                                }
                                // ENTIDAD
                                $content .=  '<div style="width: 100%; float: left; background: #141414; text-align: center; color:white;">'.$nombreEntidad.'</div>';
                                // PERIODOS
                                $content .=  '<div style="width: 24%; float: left; background: gray; color:white; text-align: center;  border: 1px solid black;">AÑOS ANTERIORES</div>
                                            <div style="width: 24%; float: left; background: gray; color:white; text-align: center;  margin-left: 6px;  border: 1px solid black;">AÑO 2014</div>
                                            <div style="width: 24%; float: left; background: gray; color:white; text-align: center;  margin-left: 6px;  border: 1px solid black;">AÑO 2015</div>
                                            <div style="width: 24%; float: left; background: gray; color:white; text-align: center;  margin-left: 6px;  border: 1px solid black;">AÑO 2016</div>
                                            <div style="width: 24%; float: left; border-left: 1px solid black; border-right: 1px solid black;;">
                                                <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                <div style="width:57%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                <div style="width:42%; min-width: 42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2013') {
                                                            $content .= $ld->mesInicio . ' - ' . $ld->mesFin;
                                                        }else{
                                                            $content .= '.';
                                                        }
                                                    }
                                            
                                $content .= '</div>
                                                <div style="width:57%; text-align: center; float:left;"> ';
                                                foreach ($liquidacionesDetalle as $ld) {
                                                    if ($ld->entidades_identidades == $e && $ld->periodo == '2013') {
                                                        $content .= number_format($ld->valor,0);
                                                    }else{
                                                        $content .= '.';
                                                    }
                                                }   
                                $content .=   '</div>
                                            </div>
                                            <div style="width: 24%; float: left; margin-left: 6px; border-left: 1px solid black; border-right: 1px solid black;;">
                                                <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                <div style="width:56%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                <div style="width:42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2014') {
                                                            $content .= $ld->mesInicio . ' - ' . $ld->mesFin;
                                                        }else{
                                                            $content .= '.';
                                                        }
                                                    }

                                $content .=    '</div>
                                                <div style="width:56%; text-align: center; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2014') {
                                                            $content .= number_format($ld->valor,0);
                                                        }else{
                                                            $content .= '.';
                                                        }
                                                    }

                                $content .=     '</div>
                                            </div>
                                            <div style="width: 24%; float: left; margin-left: 6px; border-left: 1px solid black; border-right: 1px solid black;;">
                                                <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                <div style="width:56%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                <div style="width:42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2015') {
                                                            $content .= $ld->mesInicio . ' - ' . $ld->mesFin;
                                                        }else{
                                                            $content .= '.';
                                                        }
                                                    }

                                $content .=     '</div>
                                                <div style="width:56%; text-align: center; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2015') {
                                                            $content .= number_format($ld->valor,0);
                                                        }else{
                                                            $content .= '.';
                                                        }
                                                    }

                                $content .=     '</div>
                                            </div>
                                            <div style="width: 24%; float: left; margin-left: 6px; border-left: 1px solid black; border-right: 1px solid black;;">
                                                <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                <div style="width:56%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                <div style="width:42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2016') {
                                                            $content .= $ld->mesInicio . ' - ' . $ld->mesFin;
                                                        }else{
                                                            $content .= '.';
                                                        }
                                                    }

                                $content .=     '</div>
                                                <div style="width:56%; text-align: center; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2016') {
                                                            $content .= number_format($ld->valor,0);
                                                        }else{
                                                            $content .= '.';
                                                        }
                                                    }

                                $content .=     '</div>
                                            </div>
                                </div>';
                            }
                    $content .= '</div>';    
                    // bloque valor total entidades 
                    $content .= '<div style="width: 100%; float: left; font-size:10px; padding: 2px; border: 1px solid black;">
                        <div style="width: 77%; float:left; background: gray; text-align: right; font-weight: bold; padding-right: 10px;">TOTAL</div>
                            <div style="width: 20%; float:left; text-align: center; font-weight: bold; padding-right: 10px;">
                                $'.number_format($valorLiquidacion,0).'
                            </div>
                        </div>';
                        // bloque informacion pagos
                        $content .= '<div style="width: 100%; font-size: 8px; padding: 2px; float: left; margin-top: 5px; border: 1px solid black;">
                            <div style="width: 100%; font-size: 11px; float: left; background: #141414; color: white; text-align: center;">INFORMACIÓN DE PAGOS</div>';
                            $content .= '<div style="width:166px; border-right: 1px solid black; float:left; text-align: center; background: gray; color: white;">FECHA PAGO</div>';
                            $content .= '<div style="width:165px; border-right: 1px solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>';
                            $content .= '<div style="width:170px; border-right: 1px solid black; float:left; text-align: center; background: gray; color: white;">ENTIDAD</div>';
                            $content .= '<div style="width:170px; float:left; text-align: center; background: gray; color: white;">VALOR</div>';
                            // foreach entidades
                            foreach ($pagos as $p) {
                                switch ($p->entidades_identidades) {
                                    case 1:
                                        $nombreEntidad = 'Comunicacion - Musica';
                                    break;
                                    case 4:
                                        $nombreEntidad = 'Almacenamiento';
                                    break;
                                    case 5:
                                        $nombreEntidad = 'Reprografia';
                                    break;
                                    case 6:
                                        $nombreEntidad = 'Peliculas - MPLC';
                                    break;
                                    case 7:
                                        $nombreEntidad = 'Comunicacion - Tranporte';
                                    break;
                                // FOREACH PAGOS
                                }
                                $content .= '<div style="width:166px; border-right: 1px solid black; float:left; text-align: center;">'.$p->fechaPago.'</div>';
                                $content .= '<div style="width:165px; border-right: 1px solid black; float:left; text-align: center;">'.$p->periodo.'</div>';
                                $content .= '<div style="width:170px; border-right: 1px solid black; float:left; text-align: center;">'.$nombreEntidad.'</div>';
                                $content .= '<div style="width:170px; float:left; text-align: center;">'.number_format($p->valor,0).'</div>';
                            }
                        $content .= '</div>';
                        // bloque valor total pago 
                        $content .= '<div style="width: 100%; float: left; font-size:10px; padding: 2px; border: 1px solid black;">
                            <div style="width: 77%; float:left; background: gray; text-align: right; font-weight: bold; padding-right: 10px;">TOTAL PAGO</div>
                                <div style="width: 20%; float:left; text-align: center; font-weight: bold; padding-right: 10px;">
                                    $'.number_format($valorPago,0).'
                                </div>
                            </div>';
                    // bloque colilla      
                    $content .= '<div style="width:100%; margin: 3px 0px; float:left;">----------------------------------------------------------------------------------------------------------------------------------------------</div>'; 
                    $content .= '<div style="width:70%; border:1px solid black; font-size:8px;">
                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">CÓDIGO</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">RAZÓN SOCIAL</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">ACTIVIDAD</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">DIRECCIÓN</div>

                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->identificador.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->razonSocial.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->direccion.'
                        </div>

                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">NIT</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CENTRO COSTO</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CIUDAD</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">TELÉFONO</div>

                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->identificador.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->municipios->nombre.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->telefono.'
                        </div>
                    </div>'; 
                    $content .= '<div style="width:70%; float:left; border:1px solid black; padding-top:5px; border-top:2px solid black; font-size:10px;">
                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">FECHA LIMITE</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">VALOR A CANCELAR</div>
                        <div style="width:100%; float:left;"></div>
                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$fechaVencimiento.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            $'.number_format($valorLiquidacion,0).'
                        </div>
                    </div>';
                    $content .= '<div style="padding-left:2px; width:29%; font-size:10px; float:left; border: 1px solid black; border-left: 0px;">LQ-'.$documento->idliquidacion.' | Expredido '.$fechaExpedicion.'</div>';
                    $content .= '<div style="width:50%; margin-top:5px; font-size:10px; float:left;">
                        <div style="width:80%; font-size:10px; background: #ccc; text-align:center; margin:0 auto;">
                            '.$userIdentity->puntorecaudo->nombre.' ('.$userIdentity->puntorecaudo->sucursalesIdsucursales->nombre.') - '.$userIdentity->nombres. ' ' .$userIdentity->apellidos.'
                        </div>
                    </div>';
                    $content .= '<div style="width:50%; font-size:10px; float:left; text-align:center;">||||||||||||||||||||||</div>';

                    // SEGUNDO BLOQUE COLILLA
                    $content .= '<div style="width:100%; margin: 3px 0px; float:left;">----------------------------------------------------------------------------------------------------------------------------------------------</div>'; 
                    $content .= '<div style="width:70%; border:1px solid black; font-size:8px;">
                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">CÓDIGO</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">RAZÓN SOCIAL</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">ACTIVIDAD</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">DIRECCIÓN</div>

                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->identificador.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->razonSocial.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->direccion.'
                        </div>

                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">NIT</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CENTRO COSTO</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CIUDAD</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">TELÉFONO</div>

                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->identificador.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->municipios->nombre.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->telefono.'
                        </div>
                    </div>'; 
                    $content .= '<div style="width:70%; float:left; border:1px solid black; padding-top:5px; border-top:2px solid black; font-size:10px;">
                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">FECHA LIMITE</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">VALOR A CANCELAR</div>
                        <div style="width:100%; float:left;"></div>
                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$fechaVencimiento.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            $'.number_format($valorLiquidacion,0).'
                        </div>
                    </div>';
                    $content .= '<div style="padding-left:2px; width:29%; font-size:10px; float:left; border: 1px solid black; border-left: 0px;">LQ-'.$documento->idliquidacion.' | Expredido '.$fechaExpedicion.'</div>';
                    $content .= '<div style="width:50%; margin-top:5px; font-size:10px; float:left;">
                        <div style="width:80%; font-size:10px; background: #ccc; text-align:center; margin:0 auto;">
                            '.$userIdentity->puntorecaudo->nombre.' ('.$userIdentity->puntorecaudo->sucursalesIdsucursales->nombre.') - '.$userIdentity->nombres. ' ' .$userIdentity->apellidos.'
                        </div>
                    </div>';
                    $content .= '<div style="width:50%; font-size:10px; float:left; text-align:center;">||||||||||||||||||||||</div>';


                // bloque fin container
                $content .= '</div>';
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 5,
                'marginLeft' => 15,
                'marginRight' => 15,
                'marginBottom' => 5,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->WriteHtml($content); // call mpdf write html
            $mpdf->Output('Liquidacion.pdf', 'D'); // call the mpdf api output as needed
            return true;
        }
    // ============================    PDF PAGO BANCO                          =============================================

        public function actionPagobanco($id){


            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $documento = Documentos::find()
                        ->where(['idDocumento' => $id])
                        ->One();

            $date=date_create($documento->fechaLimite);
            $fechaLimite = date_format($date,"Y-m-d");

            $nombreCompleto = $documento->negociosIdnegocios->tercerosIdterceros->nombres.' '.$documento->negociosIdnegocios->tercerosIdterceros->apellidos;
            $identificacion = $documento->negociosIdnegocios->tercerosIdterceros->identificacion;

            $liquidacionesDetalle = EstadosCuenta::find()
                                    ->Where(['documentos_iddocumento' => $id])
                                    ->all();
            $entidadesLiquidacion = EstadosCuenta::find()
                                    ->Where(['documentos_iddocumento' => $id])
                                    ->groupBy(['entidades_identidades'])
                                    ->all();

            $arrayLiquidacion = array();
            $valorLiquidacion = 0;
            foreach ($liquidacionesDetalle as $lqd) {
                $valorLiquidacion = $valorLiquidacion + $lqd->valor;
            }

            foreach ($entidadesLiquidacion as $eld) {
                array_push($arrayLiquidacion, $eld->entidades_identidades);
            }

            // $fechaVencimiento = date_create($documento->fechaVencimiento);
            // $fechaVencimiento = date_format($fechaVencimiento,'Y-m-d');

            $fechaLimite = date_create($documento->fechaLimite);
            $fechaLimite = date_format($fechaLimite,'Y-m-d');

            $fechaExpedicion = date_create($documento->fechaExpedicion);
            $fechaExpedicion = date_format($fechaExpedicion,'Y-m-d');

            $content = '';
            // # ================= INICIO CONSTRUCCION DE PDF
                
                $content .= '<div style="font-family: arial;">';

                    $content .= '<p style="width: 100%; text-align:right; font-size: 10px; color: red; font-weight:bold;">"VÁLIDO UNICAMENTE CON EL <br> TIMBRE O SELLO DEL BANCO"</p>';
                    $content .= '<div style="width: 87%; margin:0px auto; text-align: center;">';
                        if ($documento->printLogo == 0) {
                            $content .= '<img src="'. Url::base()  .'/images/logoPdf.png"/>';
                        }else{
                            $content .= '<img src="'. Url::base()  .'/images/logoPdf.png" style="visibility: hidden;"/>';
                        }
                    $content .= '</div>';
                    $content .= '<div style="width: 100%; float:left; margin-top: -30px; font-size:11px; text-align:right;">
                                    LQ-'.$documento->idDocumento.'
                                </div>';
                    $content .= '<div style="width: 50%; float:left; margin-top: 0px; text-align:center;">
                                    <span style="font-size: 10px;" >AUTORIZACIÓN POR LA COMUNICACIÓN DE OBRAS AL PÚBLICO</span> <br>';
                        $content .= '<img src="data:image/png;base64,' . $this->generarBarcode($documento->idDocumento,$documento->valor,$documento->fechaLimite,1,30) . '">';
                        $content .= "<p style='font-size:8px; width:100%; margin:0px; padding:0px; float:left;'>".$this->generarCadena($documento->idDocumento,$documento->valor,$documento->fechaLimite)."</p>";
                    $content .= '</div>';
                    // bloque fecha limite de pago
                    $content .= '<div style="width: 50%; float:left; font-size: 11px;">
                                    <div style="width: 90%; float:right; border: 1px solid black; height: 10px;">
                                        <div style="width: 58%; margin-left: 2%; float: left; font-style: italic;">
                                            FECHA LIMITE DE PAGO: 
                                        </div>
                                        <div style="width: 38%; float: left; margin-right: 2%; text-align: right;">
                                            '.$fechaLimite.'
                                        </div>
                                    </div>
                                    <div style="width: 90%; margin-top: 5px; float:right; border: 1px solid black; height: 10px;">
                                        <div style="width: 58%; margin-left: 2%; float: left; font-style: italic;">
                                            TOTAL A PAGAR: 
                                        </div>
                                        <div style="width: 38%; float: left; margin-right: 2%; text-align: right;">
                                            '.'$' . number_format($valorLiquidacion,0) .'
                                        </div>
                                    </div>
                                </div>';
                    // bloque informacion del negocio
                    $content .= '<div style="width: 100%; font-size: 8px; padding: 2px; float: left; margin-top: 5px; border: 1px solid black; text-align:center;">
                                    <div style="width: 100%; font-size: 11px; float: left; background: #141414; color: white; text-align: center;">INFORMACIÓN DEL NEGOCIO</div>
                                    <div style="width: 20%; float:left;" >
                                        <div style="width: 100%;">CÓDIGO</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->identificador.'
                                        </div>
                                    </div>
                                    <div style="width: 15%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">NIT</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->identificador.'
                                        </div>
                                    </div>
                                    <div style="width: 23%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">RAZÓN SOCIAL</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->razonSocial.'
                                        </div>
                                    </div>
                                    <div style="width: 20%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">ACTIVIDAD</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'
                                        </div>
                                    </div>
                                    <div style="width: 21%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">PROPIETARIO AFILIADO</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$nombreCompleto.'
                                        </div>
                                    </div>

                                    <div style="width: 19%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">DIRECCIÓN</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->direccion.'
                                        </div>
                                    </div>
                                    <div style="width: 12%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">TELÉFONO</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->telefono.'
                                        </div>
                                    </div>
                                    <div style="width: 15%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">CIUDAD</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->municipios->nombre.'
                                        </div>
                                    </div>
                                    <div style="width: 18%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">BARRIO</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->barrios->nombre.'
                                        </div>
                                    </div>
                                    <div style="width: 15%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">C. DE COSTOS</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'
                                        </div>
                                    </div>
                                    <div style="width: 18%; margin-left: 2px; float:left;" >
                                        <div style="width: 100%; text-align: center;">FECHA DE EXPEDICIÓN</div>
                                        <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                            '.$documento->fechaExpedicion.'
                                        </div>
                                    </div>
                                </div>';
                    // bloque informacion de la cuenta
                    $content .= '<div style="width: 100%; font-size: 8px; padding: 2px; float: left; margin-top: 5px; height: 205px; border: 1px solid black;">
                        <div style="width: 100%; font-size: 11px; float: left; background: #141414; color: white; text-align: center;">INFORMACIÓN DE LA CUENTA</div>';
                            // foreach entidades
                            $content .= '<div style="width: 100%; margin-bottom: 5px; float: left; padding: 2px; border: 1px solid black;">';
                            foreach ($arrayLiquidacion as $e) {
                                switch ($e) {
                                    case 1:
                                        $nombreEntidad = 'Comunicacion - Musica';
                                    break;
                                    case 4:
                                        $nombreEntidad = 'Almacenamiento';
                                    break;
                                    case 5:
                                        $nombreEntidad = 'Reprografia';
                                    break;
                                    case 6:
                                        $nombreEntidad = 'Peliculas - MPLC';
                                    break;
                                    case 7:
                                        $nombreEntidad = 'Comunicacion - Tranporte';
                                    break;
                                }
                                // ENTIDAD
                                $content .=  '<div style="width: 100%; float: left; background: #141414; text-align: center; color:white;">'.$nombreEntidad.'</div>';
                                // PERIODOS
                                $content .=  '<div style="width: 24%; float: left; background: gray; color:white; text-align: center;  border: 1px solid black;">AÑOS ANTERIORES</div>
                                            <div style="width: 24%; float: left; background: gray; color:white; text-align: center;  margin-left: 6px;  border: 1px solid black;">AÑO 2014</div>
                                            <div style="width: 24%; float: left; background: gray; color:white; text-align: center;  margin-left: 6px;  border: 1px solid black;">AÑO 2015</div>
                                            <div style="width: 24%; float: left; background: gray; color:white; text-align: center;  margin-left: 6px;  border: 1px solid black;">AÑO 2016</div>
                                            <div style="width: 24%; float: left; border-left: 1px solid black; border-right: 1px solid black;;">
                                                <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                <div style="width:57%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                <div style="width:42%; min-width: 42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                    // $isAnteriores = false;
                                                    // foreach ($liquidacionesDetalle as $ld) {
                                                    //     if ($ld->entidades_identidades == $e && $ld->periodo <= 2013) {
                                                    //         $isAnteriores = true;
                                                    //     }
                                                    // }

                                                    // if ($isAnteriores) {
                                                        $content .= '1 - 12';
                                                    // }
                                            
                                $content .= '</div>
                                                <div style="width:57%; text-align: center; float:left;"> ';
                                                $anteriores = 0;
                                                foreach ($liquidacionesDetalle as $ld) {
                                                    if ($ld->entidades_identidades == $e && $ld->periodo <= 2013) {
                                                        $anteriores = $anteriores + $ld->valor;
                                                    }
                                                } 
                                                $content .= number_format($anteriores,0);

                                $content .=   '</div>
                                            </div>
                                            <div style="width: 24%; float: left; margin-left: 6px; border-left: 1px solid black; border-right: 1px solid black;;">
                                                <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                <div style="width:56%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                <div style="width:42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2014') {
                                                            $content .= $ld->fechaInicial . ' - ' . $ld->fechaFinal;
                                                        }
                                                    }

                                $content .=    '</div>
                                                <div style="width:56%; text-align: center; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2014') {
                                                            $content .= number_format($ld->valor,0);
                                                        }
                                                    }

                                $content .=     '</div>
                                            </div>
                                            <div style="width: 24%; float: left; margin-left: 6px; border-left: 1px solid black; border-right: 1px solid black;;">
                                                <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                <div style="width:56%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                <div style="width:42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2015') {
                                                            $content .= $ld->fechaInicial . ' - ' . $ld->fechaFinal;
                                                        }
                                                    }

                                $content .=     '</div>
                                                <div style="width:56%; text-align: center; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2015') {
                                                            $content .= number_format($ld->valor,0);
                                                        }
                                                    }

                                $content .=     '</div>
                                            </div>
                                            <div style="width: 24%; float: left; margin-left: 6px; border-left: 1px solid black; border-right: 1px solid black;;">
                                                <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                <div style="width:56%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                <div style="width:42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2016') {
                                                            $content .= $ld->fechaInicial . ' - ' . $ld->fechaFinal;
                                                        }
                                                    }

                                $content .=     '</div>
                                                <div style="width:56%; text-align: center; float:left;">';

                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo == '2016') {
                                                            $content .= number_format($ld->valor,0);
                                                        }
                                                    }

                                $content .=     '</div>
                                            </div>
                                </div>';
                            }


                    $content .= '</div>';        
                    // bloque valor total entidades 
                    $content .= '<div style="width: 100%; float: left; font-size:10px; padding: 2px; border: 1px solid black;">
                        <div style="width: 77%; float:left; background: gray; text-align: right; font-weight: bold; padding-right: 10px;">TOTAL</div>
                            <div style="width: 20%; float:left; text-align: center; font-weight: bold; padding-right: 10px;">
                                $'.number_format($valorLiquidacion,0).'
                            </div>
                        </div>';
                    // bloque firma de usuario
                    $content .= '<div style="margin-top:5px; width: 100%; font-size: 8px;height:30px; float: left; border: 1px solid black; padding: 2px;">
                        <div style="width:69%; float:left; background: #D8D8D8; ">OBSERVACIONES: '.$documento->observacion.'</div>      
                        <div style="width:30%; float:left; font-weight: bold; text-align:center;">FIRMA USUARIO</div>
                    </div>';  
                    // bloque colilla      
                    $content .= '<div style="width:100%; margin: 10px 0px; float:left;">----------------------------------------------------------------------------------------------------------------------------------------------</div>'; 
                    $content .= '<div style="width:70%; border:1px solid black; font-size:8px; margin-top:20px;">
                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">CÓDIGO</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">RAZÓN SOCIAL</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">ACTIVIDAD</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">DIRECCIÓN</div>

                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->identificador.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->razonSocial.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->direccion.'
                        </div>

                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">NIT</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CENTRO COSTO</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CIUDAD</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">TELÉFONO</div>

                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->identificador.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->municipios->nombre.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->telefono.'
                        </div>
                    </div>'; 
                    $content .= '<div style="width:70%; float:left; border:1px solid black; padding-top:5px; border-top:2px solid black; font-size:10px;">
                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">FECHA LIMITE</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">VALOR A CANCELAR</div>
                        <div style="width:100%; float:left;"></div>
                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            fechaVencimiento
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            $'.number_format($valorLiquidacion,0).'
                        </div>
                    </div>';
                    $content .= '<div style="padding-left:2px; width:29%; font-size:10px; float:left; border: 1px solid black; border-left: 0px;">LQ-'.$documento->idDocumento.' | Expredido '.$fechaExpedicion.'</div>';
                    $content .= '<div style="width:50%; margin-top:5px; font-size:10px; float:left;">
                        <div style="width:80%; font-size:10px; background: #ccc; text-align:center; margin:0 auto;">
                            '.$userIdentity->puntorecaudo->nombre.' ('.$userIdentity->puntorecaudo->sucursalesIdsucursales->nombre.') - '.$userIdentity->nombres. ' ' .$userIdentity->apellidos.'
                        </div>
                    </div>';
                    $content .= '<div style="width:50%; font-size:10px; float:left; text-align:center;">';
                        $content .= '<img src="data:image/png;base64,' . $this->generarBarcode($documento->idDocumento,$documento->valor,$documento->fechaLimite,0.8,20) . '">';
                        $content .= "<p style='font-size:8px; width:100%; margin:0px; padding:0px; float:left;'>".$this->generarCadena($documento->idDocumento,$documento->valor,$documento->fechaLimite)."</p>";
                    $content .= '</div>';

                    // SEGUNDO BLOQUE COLILLA
                    $content .= '<div style="width:100%; margin: 3px 0px; margin-top:25px; float:left;">----------------------------------------------------------------------------------------------------------------------------------------------</div>'; 
                    $content .= '<div style="width:70%; border:1px solid black; font-size:8px; margin-top:10px;">
                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">CÓDIGO</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">RAZÓN SOCIAL</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">ACTIVIDAD</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">DIRECCIÓN</div>

                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->identificador.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->razonSocial.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->direccion.'
                        </div>

                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">NIT</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CENTRO COSTO</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CIUDAD</div>
                        <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">TELÉFONO</div>

                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->identificador.'
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->municipios->nombre.'
                        </div>
                        <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            '.$documento->negociosIdnegocios->telefono.'
                        </div>
                    </div>'; 
                    $content .= '<div style="width:70%; float:left; border:1px solid black; padding-top:5px; border-top:2px solid black; font-size:10px;">
                        <div style="width:22%; text-align:center; font-weight:bold; float:left;">FECHA LIMITE</div>
                        <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">VALOR A CANCELAR</div>
                        <div style="width:100%; float:left;"></div>
                        <div style="width:22%; text-align:center; float:left; background: #ccc;">
                            fechaVencimiento
                        </div>
                        <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                            $'.number_format($valorLiquidacion,0).'
                        </div>
                    </div>';
                    $content .= '<div style="padding-left:2px; width:29%; font-size:10px; float:left; border: 1px solid black; border-left: 0px;">LQ-'.$documento->idDocumento.' | Expredido '.$fechaExpedicion.'</div>';
                    $content .= '<div style="width:50%; margin-top:5px; font-size:10px; float:left;">
                        <div style="width:80%; font-size:10px; background: #ccc; text-align:center; margin:0 auto;">
                            '.$userIdentity->puntorecaudo->nombre.' ('.$userIdentity->puntorecaudo->sucursalesIdsucursales->nombre.') - '.$userIdentity->nombres. ' ' .$userIdentity->apellidos.'
                        </div>
                    </div>';
                    $content .= '<div style="width:50%; font-size:10px; float:left; text-align:center;">';
                        $content .= '<img src="data:image/png;base64,' . $this->generarBarcode($documento->idDocumento,$documento->valor,$documento->fechaLimite,0.8,20) . '">';
                        $content .= "<p style='font-size:8px; width:100%; margin:0px; padding:0px; float:left;'>".$this->generarCadena($documento->idDocumento,$documento->valor,$documento->fechaLimite)."</p>";
                    $content .= '</div>';


                // bloque fin container
                $content .= '</div>';
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 5,
                'marginLeft' => 15,
                'marginRight' => 15,
                'marginBottom' => 5,
                'content' => $content,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->WriteHtml($content); // call mpdf write html
            $mpdf->Output('pagobanco.pdf', 'D'); // call the mpdf api output as needed



            return true;
        }
    // ============================    PDF LIQUIDACION MASIVA                  =============================================

        public function actionLqmasiva(){
            $content = '';
            $i = 1;
            $countMasivaPage = count(Yii::$app->request->post('masiva'));
            foreach (Yii::$app->request->post('masiva') as $idMnegocio) {
                $userIdentity = Users::findOne(yii::$app->user->identity->id);
                $documento = Documentos::find()
                            ->where(['tipoDocumento' => 245])
                            ->andWhere(['negocios_idnegocios' => $idMnegocio])
                            ->One();

                $date=date_create($documento->fechaLimite);
                $fechaLimite = date_format($date,"Y-m-d");

                $nombreCompleto = $documento->negociosIdnegocios->tercerosIdterceros->nombres.' '.$documento->negociosIdnegocios->tercerosIdterceros->apellidos;
                $identificacion = $documento->negociosIdnegocios->tercerosIdterceros->identificacion;

                $liquidacionesDetalle = EstadosCuenta::find()
                                        ->Where(['documentos_iddocumento' => $documento->idDocumento])
                                        ->all();
                $entidadesLiquidacion = EstadosCuenta::find()
                                        ->Where(['documentos_iddocumento' => $documento->idDocumento])
                                        ->groupBy(['entidades_identidades'])
                                        ->all();

                $arrayLiquidacion = array();
                $valorLiquidacion = 0;
                foreach ($liquidacionesDetalle as $lqd) {
                    $valorLiquidacion = $valorLiquidacion + $lqd->valor;
                }

                foreach ($entidadesLiquidacion as $eld) {
                    array_push($arrayLiquidacion, $eld->entidades_identidades);
                }

                // $fechaVencimiento = date_create($documento->fechaVencimiento);
                // $fechaVencimiento = date_format($fechaVencimiento,'Y-m-d');

                $fechaExpedicion = date_create($documento->fechaExpedicion);
                $fechaExpedicion = date_format($fechaExpedicion,'Y-m-d');


                // # ================= INICIO CONSTRUCCION DE PDF
                    
                    $content .= '<div style="font-family: arial;">';

                        $content .= '<p style="width: 100%; text-align:right; font-size: 10px; color: red; font-weight:bold;">"VÁLIDO UNICAMENTE CON EL <br> TIMBRE O SELLO DEL BANCO"</p>';
                        $content .= '<div style="width: 87%; margin:0px auto; text-align: center;">';
                            if ($documento->printLogo == 0) {
                                $content .= '<img src="'. Url::base()  .'/images/logoPdf.png"/>';
                            }else{
                                $content .= '<img src="'. Url::base()  .'/images/logoPdf.png" style="visibility: hidden;"/>';
                            }
                        $content .= '</div>';
                        $content .= '<div style="width: 100%; float:left; margin-top: -30px; font-size:11px; text-align:right;">
                                        LQ-'.$documento->idDocumento.'
                                    </div>';
                        $content .= '<div style="width: 50%; float:left; margin-top: 0px; text-align:center;">
                                        <span style="font-size: 10px;" >AUTORIZACIÓN POR LA COMUNICACIÓN DE OBRAS AL PÚBLICO</span> <br>';
                                        $content .= '<img src="data:image/png;base64,' . $this->generarBarcode($documento->idDocumento,$documento->valor,$documento->fechaLimite,1,30) . '">';
                                        $content .= "<p style='font-size:8px; width:100%; margin:0px; padding:0px; float:left;'>".$this->generarCadena($documento->idDocumento,$documento->valor,$documento->fechaLimite)."</p>";
                        $content .= '</div>';
                        // bloque fecha limite de pago
                        $content .= '<div style="width: 50%; float:left; font-size: 11px;">
                                        <div style="width: 90%; float:right; border: 1px solid black; height: 10px;">
                                            <div style="width: 58%; margin-left: 2%; float: left; font-style: italic;">
                                                FECHA LIMITE DE PAGO: 
                                            </div>
                                            <div style="width: 38%; float: left; margin-right: 2%; text-align: right;">
                                                '.$fechaLimite.'
                                            </div>
                                        </div>
                                        <div style="width: 90%; margin-top: 5px; float:right; border: 1px solid black; height: 10px;">
                                            <div style="width: 58%; margin-left: 2%; float: left; font-style: italic;">
                                                TOTAL A PAGAR: 
                                            </div>
                                            <div style="width: 38%; float: left; margin-right: 2%; text-align: right;">
                                                '.'$' . number_format($valorLiquidacion,0) .'
                                            </div>
                                        </div>
                                    </div>';
                        // bloque informacion del negocio
                        $content .= '<div style="width: 100%; font-size: 8px; padding: 2px; float: left; margin-top: 5px; border: 1px solid black; text-align:center;">
                                        <div style="width: 100%; font-size: 11px; float: left; background: #141414; color: white; text-align: center;">INFORMACIÓN DEL NEGOCIO</div>
                                        <div style="width: 20%; float:left;" >
                                            <div style="width: 100%;">CÓDIGO</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->negociosIdnegocios->identificador.'
                                            </div>
                                        </div>
                                        <div style="width: 15%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">NIT</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->negociosIdnegocios->identificador.'
                                            </div>
                                        </div>
                                        <div style="width: 23%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">RAZÓN SOCIAL</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->negociosIdnegocios->razonSocial.'
                                            </div>
                                        </div>
                                        <div style="width: 20%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">ACTIVIDAD</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'
                                            </div>
                                        </div>
                                        <div style="width: 21%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">PROPIETARIO AFILIADO</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$nombreCompleto.'
                                            </div>
                                        </div>

                                        <div style="width: 19%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">DIRECCIÓN</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->negociosIdnegocios->direccion.'
                                            </div>
                                        </div>
                                        <div style="width: 12%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">TELÉFONO</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->negociosIdnegocios->telefono.'
                                            </div>
                                        </div>
                                        <div style="width: 15%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">CIUDAD</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->negociosIdnegocios->municipios->nombre.'
                                            </div>
                                        </div>
                                        <div style="width: 18%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">BARRIO</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->negociosIdnegocios->barrios->nombre.'
                                            </div>
                                        </div>
                                        <div style="width: 15%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">C. DE COSTOS</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'
                                            </div>
                                        </div>
                                        <div style="width: 18%; margin-left: 2px; float:left;" >
                                            <div style="width: 100%; text-align: center;">FECHA DE EXPEDICIÓN</div>
                                            <div style="width: 100%; height: 15px; background: #ccc; padding-left: 5px;">
                                                '.$documento->fechaExpedicion.'
                                            </div>
                                        </div>
                                    </div>';
                        // bloque informacion de la cuenta
                        $content .= '<div style="width: 100%; font-size: 8px; padding: 2px; float: left; margin-top: 5px; height: 205px; border: 1px solid black;">
                            <div style="width: 100%; font-size: 11px; float: left; background: #141414; color: white; text-align: center;">INFORMACIÓN DE LA CUENTA</div>';
                                // foreach entidades
                                $content .= '<div style="width: 100%; margin-bottom: 5px; float: left; padding: 2px; border: 1px solid black;">';
                                foreach ($arrayLiquidacion as $e) {
                                    switch ($e) {
                                        case 1:
                                            $nombreEntidad = 'Comunicacion - Musica';
                                        break;
                                        case 4:
                                            $nombreEntidad = 'Almacenamiento';
                                        break;
                                        case 5:
                                            $nombreEntidad = 'Reprografia';
                                        break;
                                        case 6:
                                            $nombreEntidad = 'Peliculas - MPLC';
                                        break;
                                        case 7:
                                            $nombreEntidad = 'Comunicacion - Tranporte';
                                        break;
                                    }
                                    // ENTIDAD
                                    $content .=  '<div style="width: 100%; float: left; background: #141414; text-align: center; color:white;">'.$nombreEntidad.'</div>';
                                    // PERIODOS
                                    $content .=  '<div style="width: 24%; float: left; background: gray; color:white; text-align: center;  border: 1px solid black;">AÑOS ANTERIORES</div>
                                                <div style="width: 24%; float: left; background: gray; color:white; text-align: center;  margin-left: 6px;  border: 1px solid black;">AÑO 2014</div>
                                                <div style="width: 24%; float: left; background: gray; color:white; text-align: center;  margin-left: 6px;  border: 1px solid black;">AÑO 2015</div>
                                                <div style="width: 24%; float: left; background: gray; color:white; text-align: center;  margin-left: 6px;  border: 1px solid black;">AÑO 2016</div>
                                                <div style="width: 24%; float: left; border-left: 1px solid black; border-right: 1px solid black;;">
                                                    <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                    <div style="width:57%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                    <div style="width:42%; min-width: 42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                        // $isAnteriores = false;
                                                        // foreach ($liquidacionesDetalle as $ld) {
                                                        //     if ($ld->entidades_identidades == $e && $ld->periodo <= 2013) {
                                                        //         $isAnteriores = true;
                                                        //     }
                                                        // }

                                                        // if ($isAnteriores) {
                                                            $content .= '1 - 12';
                                                        // }
                                                
                                    $content .= '</div>
                                                    <div style="width:57%; text-align: center; float:left;"> ';
                                                    $anteriores = 0;
                                                    foreach ($liquidacionesDetalle as $ld) {
                                                        if ($ld->entidades_identidades == $e && $ld->periodo <= 2013) {
                                                            $anteriores = $anteriores + $ld->valor;
                                                        }
                                                    } 
                                                    $content .= number_format($anteriores,0);

                                    $content .=   '</div>
                                                </div>
                                                <div style="width: 24%; float: left; margin-left: 6px; border-left: 1px solid black; border-right: 1px solid black;;">
                                                    <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                    <div style="width:56%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                    <div style="width:42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                        foreach ($liquidacionesDetalle as $ld) {
                                                            if ($ld->entidades_identidades == $e && $ld->periodo == '2014') {
                                                                $content .= $ld->fechaInicial . ' - ' . $ld->fechaFinal;
                                                            }
                                                        }

                                    $content .=    '</div>
                                                    <div style="width:56%; text-align: center; float:left;">';

                                                        foreach ($liquidacionesDetalle as $ld) {
                                                            if ($ld->entidades_identidades == $e && $ld->periodo == '2014') {
                                                                $content .= number_format($ld->valor,0);
                                                            }
                                                        }

                                    $content .=     '</div>
                                                </div>
                                                <div style="width: 24%; float: left; margin-left: 6px; border-left: 1px solid black; border-right: 1px solid black;;">
                                                    <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                    <div style="width:56%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                    <div style="width:42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                        foreach ($liquidacionesDetalle as $ld) {
                                                            if ($ld->entidades_identidades == $e && $ld->periodo == '2015') {
                                                                $content .= $ld->fechaInicial . ' - ' . $ld->fechaFinal;
                                                            }
                                                        }

                                    $content .=     '</div>
                                                    <div style="width:56%; text-align: center; float:left;">';

                                                        foreach ($liquidacionesDetalle as $ld) {
                                                            if ($ld->entidades_identidades == $e && $ld->periodo == '2015') {
                                                                $content .= number_format($ld->valor,0);
                                                            }
                                                        }

                                    $content .=     '</div>
                                                </div>
                                                <div style="width: 24%; float: left; margin-left: 6px; border-left: 1px solid black; border-right: 1px solid black;;">
                                                    <div style="width:42%; border-right: 1% solid black; float:left; text-align: center; background: gray; color: white;">PERIODO</div>
                                                    <div style="width:56%; float:left; text-align: center; background: gray; color: white;">MONTO</div>
                                                    <div style="width:42%; text-align: center; border-right: 1% solid black; float:left;">';

                                                        foreach ($liquidacionesDetalle as $ld) {
                                                            if ($ld->entidades_identidades == $e && $ld->periodo == '2016') {
                                                                $content .= $ld->fechaInicial . ' - ' . $ld->fechaFinal;
                                                            }
                                                        }

                                    $content .=     '</div>
                                                    <div style="width:56%; text-align: center; float:left;">';

                                                        foreach ($liquidacionesDetalle as $ld) {
                                                            if ($ld->entidades_identidades == $e && $ld->periodo == '2016') {
                                                                $content .= number_format($ld->valor,0);
                                                            }
                                                        }

                                    $content .=     '</div>
                                                </div>
                                    </div>';
                                }


                        $content .= '</div>';        
                        // bloque valor total entidades 
                        $content .= '<div style="width: 100%; float: left; font-size:10px; padding: 2px; border: 1px solid black;">
                            <div style="width: 77%; float:left; background: gray; text-align: right; font-weight: bold; padding-right: 10px;">TOTAL</div>
                                <div style="width: 20%; float:left; text-align: center; font-weight: bold; padding-right: 10px;">
                                    $'.number_format($valorLiquidacion,0).'
                                </div>
                            </div>';
                        // bloque firma de usuario
                        $content .= '<div style="margin-top:5px; width: 100%; font-size: 8px;height:30px; float: left; border: 1px solid black; padding: 2px;">
                            <div style="width:69%; float:left; background: #D8D8D8; ">OBSERVACIONES: '.$documento->observacion.'</div>      
                            <div style="width:30%; float:left; font-weight: bold; text-align:center;">FIRMA USUARIO</div>
                        </div>';  
                        // bloque colilla      
                        $content .= '<div style="width:100%; margin: 10px 0px; float:left;">----------------------------------------------------------------------------------------------------------------------------------------------</div>'; 
                        $content .= '<div style="width:70%; border:1px solid black; font-size:8px; margin-top:20px;">
                            <div style="width:22%; text-align:center; font-weight:bold; float:left;">CÓDIGO</div>
                            <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">RAZÓN SOCIAL</div>
                            <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">ACTIVIDAD</div>
                            <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">DIRECCIÓN</div>

                            <div style="width:22%; text-align:center; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->identificador.'
                            </div>
                            <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->razonSocial.'
                            </div>
                            <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'
                            </div>
                            <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->direccion.'
                            </div>

                            <div style="width:22%; text-align:center; font-weight:bold; float:left;">NIT</div>
                            <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CENTRO COSTO</div>
                            <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CIUDAD</div>
                            <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">TELÉFONO</div>

                            <div style="width:22%; text-align:center; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->identificador.'
                            </div>
                            <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'
                            </div>
                            <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->municipios->nombre.'
                            </div>
                            <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->telefono.'
                            </div>
                        </div>'; 
                        $content .= '<div style="width:70%; float:left; border:1px solid black; padding-top:5px; border-top:2px solid black; font-size:10px;">
                            <div style="width:22%; text-align:center; font-weight:bold; float:left;">FECHA LIMITE</div>
                            <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">VALOR A CANCELAR</div>
                            <div style="width:100%; float:left;"></div>
                            <div style="width:22%; text-align:center; float:left; background: #ccc;">
                                fechaVencimiento
                            </div>
                            <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                $'.number_format($valorLiquidacion,0).'
                            </div>
                        </div>';
                        $content .= '<div style="padding-left:2px; width:29%; font-size:10px; float:left; border: 1px solid black; border-left: 0px;">LQ-'.$documento->idDocumento.' | Expredido '.$fechaExpedicion.'</div>';
                        $content .= '<div style="width:50%; margin-top:5px; font-size:10px; float:left;">
                            <div style="width:80%; font-size:10px; background: #ccc; text-align:center; margin:0 auto;">
                                '.$userIdentity->puntorecaudo->nombre.' ('.$userIdentity->puntorecaudo->sucursalesIdsucursales->nombre.') - '.$userIdentity->nombres. ' ' .$userIdentity->apellidos.'
                            </div>
                        </div>';
                        $content .= '<div style="width:50%; font-size:10px; float:left; text-align:center;">';
                            $content .= '<img src="data:image/png;base64,' . $this->generarBarcode($documento->idDocumento,$documento->valor,$documento->fechaLimite,0.8,20) . '">';
                            $content .= "<p style='font-size:8px; width:100%; margin:0px; padding:0px; float:left;'>".$this->generarCadena($documento->idDocumento,$documento->valor,$documento->fechaLimite)."</p>";
                        $content .= '</div>';


                        // SEGUNDO BLOQUE COLILLA
                        $content .= '<div style="width:100%; margin: 3px 0px; margin-top:25px; float:left;">----------------------------------------------------------------------------------------------------------------------------------------------</div>'; 
                        $content .= '<div style="width:70%; border:1px solid black; font-size:8px; margin-top:10px;">
                            <div style="width:22%; text-align:center; font-weight:bold; float:left;">CÓDIGO</div>
                            <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">RAZÓN SOCIAL</div>
                            <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">ACTIVIDAD</div>
                            <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">DIRECCIÓN</div>

                            <div style="width:22%; text-align:center; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->identificador.'
                            </div>
                            <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->razonSocial.'
                            </div>
                            <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'
                            </div>
                            <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->direccion.'
                            </div>

                            <div style="width:22%; text-align:center; font-weight:bold; float:left;">NIT</div>
                            <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CENTRO COSTO</div>
                            <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">CIUDAD</div>
                            <div style="width:24%; text-align:center; font-weight:bold; margin-left:1%; float:left;">TELÉFONO</div>

                            <div style="width:22%; text-align:center; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->identificador.'
                            </div>
                            <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'
                            </div>
                            <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->municipios->nombre.'
                            </div>
                            <div style="width:24%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                '.$documento->negociosIdnegocios->telefono.'
                            </div>
                        </div>'; 
                        $content .= '<div style="width:70%; float:left; border:1px solid black; padding-top:5px; border-top:2px solid black; font-size:10px;">
                            <div style="width:22%; text-align:center; font-weight:bold; float:left;">FECHA LIMITE</div>
                            <div style="width:27%; text-align:center; font-weight:bold; margin-left:1%; float:left;">VALOR A CANCELAR</div>
                            <div style="width:100%; float:left;"></div>
                            <div style="width:22%; text-align:center; float:left; background: #ccc;">
                                fechaVencimiento
                            </div>
                            <div style="width:27%; text-align:center; margin-left:1%; float:left; background: #ccc;">
                                $'.number_format($valorLiquidacion,0).'
                            </div>
                        </div>';
                        $content .= '<div style="padding-left:2px; width:29%; font-size:10px; float:left; border: 1px solid black; border-left: 0px;">LQ-'.$documento->idDocumento.' | Expredido '.$fechaExpedicion.'</div>';
                        $content .= '<div style="width:50%; margin-top:5px; font-size:10px; float:left;">
                            <div style="width:80%; font-size:10px; background: #ccc; text-align:center; margin:0 auto;">
                                '.$userIdentity->puntorecaudo->nombre.' ('.$userIdentity->puntorecaudo->sucursalesIdsucursales->nombre.') - '.$userIdentity->nombres. ' ' .$userIdentity->apellidos.'
                            </div>
                        </div>';
                        $content .= '<div style="width:50%; font-size:10px; float:left; text-align:center;">';
                            $content .= '<img src="data:image/png;base64,' . $this->generarBarcode($documento->idDocumento,$documento->valor,$documento->fechaLimite,0.8,20) . '">';
                            $content .= "<p style='font-size:8px; width:100%; margin:0px; padding:0px; float:left;'>".$this->generarCadena($documento->idDocumento,$documento->valor,$documento->fechaLimite)."</p>";
                        $content .= '</div>';



                    // bloque fin container
                    $content .= '</div>';
                // # ================= FIN CONSTRUCCION DE PDF
                //  Objeto pdf instanceado con margenes definidas
                $pdf = new Pdf([
                    'marginTop' => 5,
                    'marginLeft' => 15,
                    'marginRight' => 15,
                    'marginBottom' => 5,
                ]); 
                $mpdf = $pdf->api; // fetches mpdf api
                $mpdf->WriteHtml($content); // call mpdf write html
                if ($countMasivaPage != $i) {
                    $mpdf->AddPage();
                }
                $i++;
            }

            $mpdf->Output('pagobanco.pdf', 'D'); // call the mpdf api output as needed
        }
    // ============================    PDF ACTA DE COMPROMISO                  =============================================
        public function actionActacompromiso($id){

            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $acta = Actas::findOne($id);

            $cartera = Yii::$app->db->createCommand(
                '
                    select  case when entidades_identidades = 1 then "Comunicacion - Musica"
                                WHEN entidades_identidades = 4 THEN "Almacenamiento"
                                WHEN entidades_identidades = 5 THEN "Reprografia"
                                WHEN entidades_identidades = 6 THEN "Peliculas - MPLC"
                                WHEN entidades_identidades = 7 THEN "Musica - Transporte"
                             END AS derecho,   
                                
                            CONCAT(min(periodo),"-",min(mesInicio)) as periodoMinimo ,CONCAT(max(periodo),"-",min(mesFin)) as periodoMaximo, 
                            sum(carteras.saldo) as total
                    from    carteras
                    where   carteras.negocios_idnegocios = '.$acta->negociosIdnegocios->idNegocio.'
                    group by entidades_identidades
                '
            )->queryAll();

            $nombreCompleto = $acta->negociosIdnegocios->tercerosIdterceros->nombres.' '.$acta->negociosIdnegocios->tercerosIdterceros->apellidos;
            $identificacion = $acta->negociosIdnegocios->tercerosIdterceros->identificacion;
            if ($acta->negociosIdnegocios->tercerosIdterceros->lugarExpedicionDocumento) {
                $localizacionIdentificacion = $acta->negociosIdnegocios->tercerosIdterceros->lugarExpedicionDocumento;
            }else{
                $localizacionIdentificacion = 'SIN INFORMACIÓN';
            }
            $razonSocial = $acta->negociosIdnegocios->razonSocial;
            $direccion = $acta->negociosIdnegocios->direccion;
            $ciudad = $acta->negociosIdnegocios->municipios->nombre;

            switch (date('m')) {
                case '01': $mes = 'ENERO'; break;
                case '02': $mes = 'FEBRERO'; break;
                case '03': $mes = 'MARZO'; break;
                case '04': $mes = 'ABRIL'; break;
                case '05': $mes = 'MAYO'; break;
                case '06': $mes = 'JUNIO'; break;
                case '07': $mes = 'JULIO'; break;
                case '08': $mes = 'AGOSTO'; break;
                case '09': $mes = 'SEPTIEMBRE'; break;
                case '10': $mes = 'OCUBRE'; break;
                case '11': $mes = 'NOVIEMBRE'; break;
                case '12': $mes = 'DICIEMBRE'; break;
            }

            $numeroCuotas = Actacompromisodetalles::find()->where(['actas_idActa' => $acta->idActa])->count();
            $actaCompromiso = Actacompromisodetalles::find()->where(['actas_idActa' => $acta->idActa])->all();

            // # ================= INICIO CONSTRUCCION DE PDF
                $content = '';
                $content .= '<div style="font-family: arial;">';
                    // ==== logo OSA
                    $content .= '<div style="width:250px; float:left;">
                                    <img src="'.Url::base().'/images/f52610_b3fa64f9ff4d4ac0a9d8f745fa6b0a82.png" width="210"/>
                                </div>';
                   // ==== Informacion CNU
                   $content .= '<div style="width:370px; text-align:right;float:right;">
                                   <p style="float:right;">RJC 002</p>
                                   <h2 style="float:right; padding-bottom:0px; margin:0px; margin-top:5px; padding-right: 25px;">
                                      CC - '.$acta->idActa.'
                                   </h2>
                                   <i style="font-weight:bold; font-size: 9px;">Personeria Juridica Res. No. 291 DNDA 18/10/2011</i>
                               </div>';
                    // ==== titulo prohibicion
                    $content .= '<h3 style="width: 100%; text-align:center; margin-top:30px;">
                                   CONTRATO DE CONCERTACIÓN USUARIOS <br> '.$acta->negociosIdnegocios->identificador.'
                               </h3>';
                    // ==== primer parrafo
                    $content .= '<p style="width: 100%; text-align:justify; font-size: 11px; margin-top: 30px;">
                                   Entre la ORGANIZACIÓN SAYCO ACINPRO con Personería jurìdica y licencia de funcionamiento otorgada por la Dirección 
                                   Nacional de Derecho de Autor, mediante la Resolución No. 291 del 18 de Octubre de 2011; obrando conforme a los Mandatos 
                                   conferidos por usus mandantes y el(la) señor(a) '.$nombreCompleto.' identificado(a) con CÉDULA CIUDADANIA No. 
                                   '.$identificacion.' de '.$localizacionIdentificacion.' mayor de edad edad, quien obra como propietario y/o representante legal del establecimiento denominado 
                                   '.$razonSocial.' ubicado en la '.$direccion.' de la ciudad de '.$ciudad.' y quien manifiesta que firma el presente 
                                   ACUERDO y/o CONCERTACIÓN de manera libre y voluntaria, por la autorización para la COMUNICACIÓN DE OBRAS 
                                   MUSICALES. interpretaciones artísticas musicales, fonogramas 
                                   videos musicales y de la comunicación de las obras musicales con contenido audiovisual con destino a la comunicación de la obra al 
                                   público en el citado establecimiento, a través de medios como la radio, televisión, equipos eléctricos, electrónicos o dispositivos 
                                   digitales, conocidos o por conocersen y que sirvan para tal fin (Derechos de Autor y derechos Conexos); Obligación legal dispuesta 
                                   en las Leyes 23/82, artículos 72, 76 literal D, 158, 159, Ley 232/95 artículo 2, literal C, por la suma de $'.$acta->valor.'  correspondiente a ';
                                foreach ($cartera as $c) {
                                    $content .= $c['derecho'].' por el periodo de '.$c['periodoMinimo'].' AL '.$c['periodoMaximo'].' por un valor de '.$c['total'].', ';
                                }

                                $content .= ' (ESTA AUTORIZACIÓN y/o LICNCIA SOLO ES VÁLIDO POR EL DERECHO AQUÍ ACORDADO). Valor que sera cancelado en '.$numeroCuotas.' cuotas de la siguiente manera: <br><br>';
                             
                                $content .= '<div style="width:100%: float:left; font-size:11px; text-align:center; height:200px;">';
                                    $content .= '<div style="width:33%; float:left; border:1px solid black; background:#00A2FF;">No Cuota</div>';
                                    $content .= '<div style="width:33%; float:left; border:1px solid black; background:#00A2FF;">Fecha Expediciòn</div>';
                                    $content .= '<div style="width:33%; float:left; border:1px solid black; background:#00A2FF;">Valor</div>';

                                    $colorBolean = true;
                                    foreach ($actaCompromiso as $a ) {
                                        
                                        


                                        $date = date_create($a->fechaLimite);
                                        $fechaFormateada = date_format($date, 'd/m/Y');

                                        if ($colorBolean) {
                                            $content .= '<div style="width:33%; float:left; border:1px solid black; border-top:0px;">'.$a->numeroCuota.'</div>';
                                            $content .= '<div style="width:33%; float:left; border:1px solid black; border-top:0px;">'.$fechaFormateada.'</div>';
                                            $content .= '<div style="width:33%; float:left; border:1px solid black; border-top:0px;">$'.number_format($a->valor,2).'</div>';
                                            $colorBolean = false;
                                        }else{
                                            $content .= '<div style="width:33%; float:left; border:1px solid black; border-top:0px; background:#CCC;">'.$a->numeroCuota.'</div>';
                                            $content .= '<div style="width:33%; float:left; border:1px solid black; border-top:0px; background:#CCC;">'.$fechaFormateada.'</div>';
                                            $content .= '<div style="width:33%; float:left; border:1px solid black; border-top:0px; background:#CCC;">$'.number_format($a->valor,2).'</div>';
                                            $colorBolean = true;
                                        }
                                    }

                                $content .= '</div>';


                    $content .= '</p>';
                    // ==== segundo parrafo
                    $content .= '<p style="width: 100%; text-align:justify; font-size: 10px; margin-top: 30px;">';
                                    $content .= 'Cuando el abligado incumpla una o varias de las cuotas acordadas en el presente documento, la Organización podrá declarar 
                                    vencidos la totalidad de los plazos de esta Obligación o de las cuotas que constituyan el saldo de lo debido y exigir su pago
                                    inmediato ya sea judicial o extrajudicialmente, sin que para ello sea necesario requerimiento previo; aceptando que el presente 
                                    documento de acuerdo con los artículos 1494 del código civil y el 422 del código codigó general al proceso, establece una 
                                    Obligación EXPRESA, CLARA Y EXIGIBLE y presta MERITO EJECUTIVO. así mismo constituye plena prueba y su aceptación
                                    información referente con mi incumplimiento de la obligación que por medio de este documento, adquiero frente a esta Entidad 
                                    (Art. 15 C. N). La falta de autorización previa y expresa del titular o sus representantes constituye una infrección a los derechos 
                                    de autor sancionada por la ley y se encuentra tipificada como delito (Art. 271 del código Penal)';
                    $content .= '</p>';
                    // ==== tercer parrafo
                    $content .= '<p style="width: 100%; text-align:justify; font-size: 9px; margin-top: 30px;">';
                                    $content .= 'Yo '.$nombreCompleto.' autorizo a la ORGANIZACIÓN SAYCO ACINPRO de manera escrita, expresa, concreta, suficiente, voluntaria e informada, para 
                                    que toda información personal actual y la que se genere en el futuro fruto de las relaciones comerciales y/o contractuales establecidas en las leyes 23 de 1982 y 232 de 
                                    1995, sea manejado en los términos de la Ley 1581 de 2012 referente al Tratamiento de datos personales <br> 
                                    Le informamos que sus datos serán incorporados en un registro automatizado con una finalidad exclusivamente administrativa, y en cumplimiento de su objeto social.';
                    $content .= '</p>';
                    // ==== cuarto parrafo
                    $content .= '<p style="width: 100%; text-align:justify; font-size: 11px; margin-top: 10px;">';
                                    $content .= 'Firmado en la ciudad de Bogotá a los '.date('d').' días del mes de '.$mes.' del año '.date('Y');
                    $content .= '</p>';
                    $content .= '<div style="width:50%; float:left; margin-top: 50px; font-size:11px; text-align:center;">____________________________________________<br>'.$userIdentity->nombres.' '.$userIdentity->apellidos.'<br>Organización Sayco Acinpro</div>';
                    $content .= '<div style="width:50%; float:left; text-align:center; font-size:11px;">____________________________________________<br>'.$nombreCompleto.'<br>CC: '.$acta->negociosIdnegocios->tercerosIdterceros->identificacion.'<br>Teléfono: '.$acta->negociosIdnegocios->tercerosIdterceros->telefono.'</div>';
                    $content .= '<p style="width: 100%; text-align:center; font-size: 10px; margin: 0px; padding:0px;">SEDES Y DIRECCIÓNES</p>';
                    $content .= '<p style="width: 100%; text-align:center; font-size: 9px; margin: 0px; padding:0px;">
                                    Villavicencio 6622163-6727144. Tunja 7442048-3214492941. Bogotá - Restrepo 3615286-2094612. Bogotá - Teusaquillo 3230899-3102040740. Medellín
                                    5132750-5111290. Pereira 3244209-3244380. Manizales 8977818. Armenia 7140782-3113217338. Cali 6601090-6674444.Pasto 7314083-3105669245.Tulua
                                    2258565-2258103. Barranquilla 3512782-3514051. Cartagena 6642633-3105582204. Santa Marta 4234500. Monteria 7810489-3114486031. Sincelejo 2825318-3012611917.
                                    Bucaramanga 6429175-6422088. Cúcuta 5730225-5724163. Ibagué 2633713-3105853363. Neiva 8710053. 
                                </p>';
                $content .= '</div>';
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 15,
                'marginLeft' => 15,
                'marginRight' => 15,
                'marginBottom' => 8,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('acta_compromiso.pdf', 'D'); // call the mpdf api output as needed
        }
    // ============================    PDF PAGO COMPLETO                       =============================================
        public function actionPagocompleto($id){
            $pago = Documentos::findOne($id);
            $nombreCompleto = $pago->negociosIdnegocios->tercerosIdterceros->nombres.' '.$pago->negociosIdnegocios->tercerosIdterceros->apellidos;
            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $detallePago = EstadosCuenta::find()->Where(['documentos_iddocumento' => $id])->all();

            // # ================= INICIO CONSTRUCCION DE PDF
            $content = '';
            $content .= '<div style="font-family: arial;">';
                // ==== logo OSA
                $content .= '<div style="width:250px; float:left;">
                                <img src="'.Url::base().'/images/f52610_b3fa64f9ff4d4ac0a9d8f745fa6b0a82.png" width="210"/>
                            </div>';
               // ==== Informacion CNU
               $content .= '<div style="width:370px; text-align:right;float:right;">
                               <p style="float:right;">RJC 002</p>
                               <h2 style="float:right; padding-bottom:0px; margin:0px; margin-top:5px; padding-right: 25px;">
                                  PC - '.$pago->idDocumento.'
                               </h2>
                               <i style="font-weight:bold; font-size: 9px;">Personeria Juridica Res. No. 291 DNDA 18/10/2011</i>
                           </div>';
                // ==== titulo prohibicion
                $content .= '<h3 style="width: 100%; text-align:center; margin-top:30px;">
                               PAGO COMPLETO <br> '.$pago->negociosIdnegocios->identificador.'
                           </h3>';
                // ==== Informacion negocio
                $content .= '<div style="width:100%: float:left; font-size:11px; text-align:center; margin-top:20px;">';

                    $content .= '<div style="width:100%; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Información del Negocio</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Codigó</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Razon Social</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->identificador.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->razonSocial.'</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Ubicación</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Dirección</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->barrios->localidades->municipios->nombre.' - '.$pago->negociosIdnegocios->barrios->localidades->nombre.' - '.$pago->negociosIdnegocios->barrios->nombre.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->direccion.'</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Identificación Propietario</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Nombre Propietario</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->tercerosIdterceros->identificacion.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->tercerosIdterceros->nombres.' '.$pago->negociosIdnegocios->tercerosIdterceros->apellidos.'</div>';

                $content .= '</div>';
                // ==== Informacion pago
                $content .= '<div style="width:100%: float:left; font-size:11px; text-align:center; margin-top:50px;">';

                    // Bloque detalle pago
                    $content .= '<div style="width:100%; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Detalle del Pago</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Periodo</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Valor</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Entidad</div>';

                    foreach ($detallePago as $d) {

                        switch ($d->entidades_identidades) {
                            case 1:
                                $nombreEntidad = 'Comunicacion - Musica';
                            break;
                            case 4:
                                $nombreEntidad = 'Almacenamiento';
                            break;
                            case 5:
                                $nombreEntidad = 'Reprografia';
                            break;
                            case 6:
                                $nombreEntidad = 'Peliculas - MPLC';
                            break;
                            case 7:
                                $nombreEntidad = 'Comunicacion - Tranporte';
                            break;
                        }

                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">'.$d->periodo.'</div>';
                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">$'.number_format($d->valor,0).'</div>';
                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">'.$nombreEntidad.'</div>';
                    }
                    
                    // Bloque pago
                    $content .= '<div style="width:100%; top:50px; margin-top:50px; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Información del Abono</div>';
                    $content .= '<div style="width:50%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Fecha de expediciòn</div>';
                    $content .= '<div style="width:49.3%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Valor</div>';
                    $content .= '<div style="width:50%; padding: 10px 0px; float:left; border:1px solid black; border-top:0px;">'.$pago->fechaExpedicion.'</div>';
                    $content .= '<div style="width:49.3%; padding: 10px 0px; float:left; border:1px solid black; border-top:0px;">$'.number_format($pago->valor,0).'</div>';

                    $content .= '<br><br><br><br>';

                    // Informacion Funcionario
                    $content .= '<div style="width:50%; float:left; margin-top: 50px; font-size:11px; text-align:center;">____________________________________________<br>'.$userIdentity->nombres.' '.$userIdentity->apellidos.'<br>Organización Sayco Acinpro</div>';
                    // Informacion Tercero
                    $content .= '<div style="width:50%; float:left; text-align:center; font-size:11px;">____________________________________________<br>'.$nombreCompleto.'<br>CC: '.$pago->negociosIdnegocios->tercerosIdterceros->identificacion.'<br>Teléfono: '.$pago->negociosIdnegocios->tercerosIdterceros->telefono.'</div>';
                    // Sedes y Direcciones
                    $content .= '<p style="width: 100%; text-align:center; font-size: 10px; margin: 0px; padding:0px;">SEDES Y DIRECCIÓNES</p>';
                    $content .= '<p style="width: 100%; text-align:center; font-size: 9px; margin: 0px; padding:0px;">
                                    Villavicencio 6622163-6727144. Tunja 7442048-3214492941. Bogotá - Restrepo 3615286-2094612. Bogotá - Teusaquillo 3230899-3102040740. Medellín
                                    5132750-5111290. Pereira 3244209-3244380. Manizales 8977818. Armenia 7140782-3113217338. Cali 6601090-6674444.Pasto 7314083-3105669245.Tulua
                                    2258565-2258103. Barranquilla 3512782-3514051. Cartagena 6642633-3105582204. Santa Marta 4234500. Monteria 7810489-3114486031. Sincelejo 2825318-3012611917.
                                    Bucaramanga 6429175-6422088. Cúcuta 5730225-5724163. Ibagué 2633713-3105853363. Neiva 8710053. 
                                </p>';

                $content .= '</div>';
            $content .= '</div>';
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 15,
                'marginLeft' => 15,
                'marginRight' => 15,
                'marginBottom' => 8,
            ]);


            $mpdf = $pdf->api; // fetches mpdf api
            // $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
            $mpdf->WriteHtml($content); // call mpdf write html


            $mpdf->Output('pago_completo_'.$pago->negociosIdnegocios->identificador.'.pdf', 'D'); // call the mpdf api output as needed
        }
    // ============================    PDF PAGO ABONO                          =============================================
        public function actionPagoabono($id){
            $pago = Documentos::findOne($id);
            $nombreCompleto = $pago->negociosIdnegocios->tercerosIdterceros->nombres.' '.$pago->negociosIdnegocios->tercerosIdterceros->apellidos;
            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $detallePago = EstadosCuenta::find()->Where(['documentos_iddocumento' => $id])->all();
            

            // # ================= INICIO CONSTRUCCION DE PDF
            $content = '';
            $content .= '<div style="font-family: arial;">';
                // ==== logo OSA
                $content .= '<div style="width:250px; float:left;">
                                <img src="'.Url::base().'/images/f52610_b3fa64f9ff4d4ac0a9d8f745fa6b0a82.png" width="210"/>
                            </div>';
               // ==== Informacion ABONO
               $content .= '<div style="width:370px; text-align:right;float:right;">
                               <p style="float:right;">RJC 002</p>
                               <h2 style="float:right; padding-bottom:0px; margin:0px; margin-top:5px; padding-right: 25px;">
                                    AB - '.$pago->idDocumento.'
                               </h2>
                               <i style="font-weight:bold; font-size: 9px;">Personeria Juridica Res. No. 291 DNDA 18/10/2011</i>
                           </div>';
                // ==== titulo prohibicion
                $content .= '<h3 style="width: 100%; text-align:center; margin-top:30px;">
                               ABONO <br> '.$pago->negociosIdnegocios->identificador.'
                           </h3>';
                // ==== Informacion negocio
                $content .= '<div style="width:100%: float:left; font-size:11px; text-align:center; margin-top:20px;">';

                    $content .= '<div style="width:100%; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Información del Negocio</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Codigó</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Razon Social</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->identificador.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->razonSocial.'</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Ubicación</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Dirección</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->barrios->localidades->municipios->nombre.' - '.$pago->negociosIdnegocios->barrios->localidades->nombre.' - '.$pago->negociosIdnegocios->barrios->nombre.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->direccion.'</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Identificación Propietario</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Nombre Propietario</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->tercerosIdterceros->identificacion.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->tercerosIdterceros->nombres.' '.$pago->negociosIdnegocios->tercerosIdterceros->apellidos.'</div>';

                $content .= '</div>';
                // ==== Informacion abono
                $content .= '<div style="width:100%: float:left; font-size:11px; text-align:center; margin-top:50px;">';

                    // Bloque detalle abono
                    $content .= '<div style="width:100%; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Detalle del Pago</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Periodo</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Valor</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Entidad</div>';

                    foreach ($detallePago as $d) {

                        switch ($d->entidades_identidades) {
                            case 1:
                                $nombreEntidad = 'Comunicacion - Musica';
                            break;
                            case 4:
                                $nombreEntidad = 'Almacenamiento';
                            break;
                            case 5:
                                $nombreEntidad = 'Reprografia';
                            break;
                            case 6:
                                $nombreEntidad = 'Peliculas - MPLC';
                            break;
                            case 7:
                                $nombreEntidad = 'Comunicacion - Tranporte';
                            break;
                        }

                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">'.$d->periodo.'</div>';
                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">$'.number_format($d->valor,0).'</div>';
                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">'.$nombreEntidad.'</div>';
                    }

                    // bloque abono
                    $content .= '<div style="width:100%; top:50px; margin-top:50px; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Información del Abono</div>';
                    $content .= '<div style="width:50%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Fecha de expediciòn</div>';
                    $content .= '<div style="width:49.3%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Valor</div>';
                    $content .= '<div style="width:50%; padding: 10px 0px; float:left; border:1px solid black; border-top:0px;">'.$pago->fechaExpedicion.'</div>';
                    $content .= '<div style="width:49.3%; padding: 10px 0px; float:left; border:1px solid black; border-top:0px;">$'.number_format($pago->valor,0).'</div>';

                    $content .= '<br><br><br><br>';

                    // informacion funcionario
                    $content .= '<div style="width:50%; float:left; margin-top: 50px; font-size:11px; text-align:center;">____________________________________________<br>'.$userIdentity->nombres.' '.$userIdentity->apellidos.'<br>Organización Sayco Acinpro</div>';
                    // informacion tercero
                    $content .= '<div style="width:50%; float:left; text-align:center; font-size:11px;">____________________________________________<br>'.$nombreCompleto.'<br>CC: '.$pago->negociosIdnegocios->tercerosIdterceros->identificacion.'<br>Teléfono: '.$pago->negociosIdnegocios->tercerosIdterceros->telefono.'</div>';
                    // sedes y direcciones
                    $content .= '<p style="width: 100%; text-align:center; font-size: 10px; margin: 0px; padding:0px;">SEDES Y DIRECCIÓNES</p>';
                    $content .= '<p style="width: 100%; text-align:center; font-size: 9px; margin: 0px; padding:0px;">
                                    Villavicencio 6622163-6727144. Tunja 7442048-3214492941. Bogotá - Restrepo 3615286-2094612. Bogotá - Teusaquillo 3230899-3102040740. Medellín
                                    5132750-5111290. Pereira 3244209-3244380. Manizales 8977818. Armenia 7140782-3113217338. Cali 6601090-6674444.Pasto 7314083-3105669245.Tulua
                                    2258565-2258103. Barranquilla 3512782-3514051. Cartagena 6642633-3105582204. Santa Marta 4234500. Monteria 7810489-3114486031. Sincelejo 2825318-3012611917.
                                    Bucaramanga 6429175-6422088. Cúcuta 5730225-5724163. Ibagué 2633713-3105853363. Neiva 8710053. 
                                </p>';

                $content .= '</div>';
            $content .= '</div>';
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 15,
                'marginLeft' => 15,
                'marginRight' => 15,
                'marginBottom' => 8,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('abono_'.$pago->negociosIdnegocios->identificador.'.pdf', 'D'); // call the mpdf api output as needed
        }
    // ============================    PDF PAGO PERIODO                        =============================================
        public function actionPagoperiodo($id){
            $pago = Documentos::findOne($id);
            $nombreCompleto = $pago->negociosIdnegocios->tercerosIdterceros->nombres.' '.$pago->negociosIdnegocios->tercerosIdterceros->apellidos;
            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $detallePago = EstadosCuenta::find()->Where(['documentos_iddocumento' => $id])->all();
            

            // # ================= INICIO CONSTRUCCION DE PDF
            $content = '';
            $content .= '<div style="font-family: arial;">';
                // ==== logo OSA
                $content .= '<div style="width:250px; float:left;">
                                <img src="'.Url::base().'/images/f52610_b3fa64f9ff4d4ac0a9d8f745fa6b0a82.png" width="210"/>
                            </div>';
               // ==== Informacion ABONO
               $content .= '<div style="width:370px; text-align:right;float:right;">
                               <p style="float:right;">RJC 002</p>
                               <h2 style="float:right; padding-bottom:0px; margin:0px; margin-top:5px; padding-right: 25px;">
                                    PPE - '.$pago->idDocumento.'
                               </h2>
                               <i style="font-weight:bold; font-size: 9px;">Personeria Juridica Res. No. 291 DNDA 18/10/2011</i>
                           </div>';
                // ==== titulo prohibicion
                $content .= '<h3 style="width: 100%; text-align:center; margin-top:30px;">
                               PAGO PERIODO <br> '.$pago->negociosIdnegocios->identificador.'
                           </h3>';
                // ==== Informacion negocio
                $content .= '<div style="width:100%: float:left; font-size:11px; text-align:center; margin-top:20px;">';

                    $content .= '<div style="width:100%; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Información del Negocio</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Codigó</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Razon Social</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->identificador.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->razonSocial.'</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Ubicación</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Dirección</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->barrios->localidades->municipios->nombre.' - '.$pago->negociosIdnegocios->barrios->localidades->nombre.' - '.$pago->negociosIdnegocios->barrios->nombre.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->direccion.'</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Identificación Propietario</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Nombre Propietario</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->tercerosIdterceros->identificacion.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->tercerosIdterceros->nombres.' '.$pago->negociosIdnegocios->tercerosIdterceros->apellidos.'</div>';

                $content .= '</div>';
                // ==== Informacion abono
                $content .= '<div style="width:100%: float:left; font-size:11px; text-align:center; margin-top:50px;">';

                    // Bloque detalle abono
                    $content .= '<div style="width:100%; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Detalle del Pago</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Periodo</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Valor</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Entidad</div>';

                    foreach ($detallePago as $d) {

                        switch ($d->entidades_identidades) {
                            case 1:
                                $nombreEntidad = 'Comunicacion - Musica';
                            break;
                            case 4:
                                $nombreEntidad = 'Almacenamiento';
                            break;
                            case 5:
                                $nombreEntidad = 'Reprografia';
                            break;
                            case 6:
                                $nombreEntidad = 'Peliculas - MPLC';
                            break;
                            case 7:
                                $nombreEntidad = 'Comunicacion - Tranporte';
                            break;
                        }

                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">'.$d->periodo.'</div>';
                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">$'.number_format($d->valor,0).'</div>';
                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">'.$nombreEntidad.'</div>';
                    }

                    // bloque abono
                    $content .= '<div style="width:100%; top:50px; margin-top:50px; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Información del Abono</div>';
                    $content .= '<div style="width:50%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Fecha de expediciòn</div>';
                    $content .= '<div style="width:49.3%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Valor</div>';
                    $content .= '<div style="width:50%; padding: 10px 0px; float:left; border:1px solid black; border-top:0px;">'.$pago->fechaExpedicion.'</div>';
                    $content .= '<div style="width:49.3%; padding: 10px 0px; float:left; border:1px solid black; border-top:0px;">$'.number_format($pago->valor,0).'</div>';

                    $content .= '<br><br><br><br>';

                    // informacion funcionario
                    $content .= '<div style="width:50%; float:left; margin-top: 50px; font-size:11px; text-align:center;">____________________________________________<br>'.$userIdentity->nombres.' '.$userIdentity->apellidos.'<br>Organización Sayco Acinpro</div>';
                    // informacion tercero
                    $content .= '<div style="width:50%; float:left; text-align:center; font-size:11px;">____________________________________________<br>'.$nombreCompleto.'<br>CC: '.$pago->negociosIdnegocios->tercerosIdterceros->identificacion.'<br>Teléfono: '.$pago->negociosIdnegocios->tercerosIdterceros->telefono.'</div>';
                    // sedes y direcciones
                    $content .= '<p style="width: 100%; text-align:center; font-size: 10px; margin: 0px; padding:0px;">SEDES Y DIRECCIÓNES</p>';
                    $content .= '<p style="width: 100%; text-align:center; font-size: 9px; margin: 0px; padding:0px;">
                                    Villavicencio 6622163-6727144. Tunja 7442048-3214492941. Bogotá - Restrepo 3615286-2094612. Bogotá - Teusaquillo 3230899-3102040740. Medellín
                                    5132750-5111290. Pereira 3244209-3244380. Manizales 8977818. Armenia 7140782-3113217338. Cali 6601090-6674444.Pasto 7314083-3105669245.Tulua
                                    2258565-2258103. Barranquilla 3512782-3514051. Cartagena 6642633-3105582204. Santa Marta 4234500. Monteria 7810489-3114486031. Sincelejo 2825318-3012611917.
                                    Bucaramanga 6429175-6422088. Cúcuta 5730225-5724163. Ibagué 2633713-3105853363. Neiva 8710053. 
                                </p>';

                $content .= '</div>';
            $content .= '</div>';
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 15,
                'marginLeft' => 15,
                'marginRight' => 15,
                'marginBottom' => 8,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('pago_periodo_'.$pago->negociosIdnegocios->identificador.'.pdf', 'D'); // call the mpdf api output as needed
        }
    // ============================    PDF PAGO PARCIAL                        =============================================
        public function actionPagoparcial($id){
            $pago = Documentos::findOne($id);
            $nombreCompleto = $pago->negociosIdnegocios->tercerosIdterceros->nombres.' '.$pago->negociosIdnegocios->tercerosIdterceros->apellidos;
            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $detallePago = EstadosCuenta::find()->Where(['documentos_iddocumento' => $id])->all();
            

            // # ================= INICIO CONSTRUCCION DE PDF
            $content = '';
            $content .= '<div style="font-family: arial;">';
                // ==== logo OSA
                $content .= '<div style="width:250px; float:left;">
                                <img src="'.Url::base().'/images/f52610_b3fa64f9ff4d4ac0a9d8f745fa6b0a82.png" width="210"/>
                            </div>';
               // ==== Informacion ABONO
               $content .= '<div style="width:370px; text-align:right;float:right;">
                               <p style="float:right;">RJC 002</p>
                               <h2 style="float:right; padding-bottom:0px; margin:0px; margin-top:5px; padding-right: 25px;">
                                    pp - '.$pago->idDocumento.'
                               </h2>
                               <i style="font-weight:bold; font-size: 9px;">Personeria Juridica Res. No. 291 DNDA 18/10/2011</i>
                           </div>';
                // ==== titulo prohibicion
                $content .= '<h3 style="width: 100%; text-align:center; margin-top:30px;">
                               PAGO PARCIAL <br> '.$pago->negociosIdnegocios->identificador.'
                           </h3>';
                // ==== Informacion negocio
                $content .= '<div style="width:100%: float:left; font-size:11px; text-align:center; margin-top:20px;">';

                    $content .= '<div style="width:100%; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Información del Negocio</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Codigó</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Razon Social</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->identificador.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->razonSocial.'</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Ubicación</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Dirección</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->barrios->localidades->municipios->nombre.' - '.$pago->negociosIdnegocios->barrios->localidades->nombre.' - '.$pago->negociosIdnegocios->barrios->nombre.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->direccion.'</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Identificación Propietario</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Nombre Propietario</div>';

                    $content .= '<div style="width:49.7%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->tercerosIdterceros->identificacion.'</div>';
                    $content .= '<div style="width:49.6%; float:left; border:1px solid black; border-top:0px;">'.$pago->negociosIdnegocios->tercerosIdterceros->nombres.' '.$pago->negociosIdnegocios->tercerosIdterceros->apellidos.'</div>';

                $content .= '</div>';
                // ==== Informacion abono
                $content .= '<div style="width:100%: float:left; font-size:11px; text-align:center; margin-top:50px;">';

                    // Bloque detalle abono
                    $content .= '<div style="width:100%; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Detalle del Pago</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Periodo</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Valor</div>';
                    $content .= '<div style="width:33%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Entidad</div>';

                    foreach ($detallePago as $d) {

                        switch ($d->entidades_identidades) {
                            case 1:
                                $nombreEntidad = 'Comunicacion - Musica';
                            break;
                            case 4:
                                $nombreEntidad = 'Almacenamiento';
                            break;
                            case 5:
                                $nombreEntidad = 'Reprografia';
                            break;
                            case 6:
                                $nombreEntidad = 'Peliculas - MPLC';
                            break;
                            case 7:
                                $nombreEntidad = 'Comunicacion - Tranporte';
                            break;
                        }

                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">'.$d->periodo.'</div>';
                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">$'.number_format($d->valor,0).'</div>';
                        $content .= '<div style="width:33%; padding: 0px; float:left; border:1px solid black; border-top:0px;">'.$nombreEntidad.'</div>';
                    }

                    // bloque abono
                    $content .= '<div style="width:100%; top:50px; margin-top:50px; float:left; border:1px solid black; color:white; background:#007CC3; text-align:center;">Información del Abono</div>';
                    $content .= '<div style="width:50%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Fecha de expediciòn</div>';
                    $content .= '<div style="width:49.3%; float:left; border:1px solid black; border-bottom: 0px; background:#CCC;">Valor</div>';
                    $content .= '<div style="width:50%; padding: 10px 0px; float:left; border:1px solid black; border-top:0px;">'.$pago->fechaExpedicion.'</div>';
                    $content .= '<div style="width:49.3%; padding: 10px 0px; float:left; border:1px solid black; border-top:0px;">$'.number_format($pago->valor,0).'</div>';

                    $content .= '<br><br><br><br>';

                    // informacion funcionario
                    $content .= '<div style="width:50%; float:left; margin-top: 50px; font-size:11px; text-align:center;">____________________________________________<br>'.$userIdentity->nombres.' '.$userIdentity->apellidos.'<br>Organización Sayco Acinpro</div>';
                    // informacion tercero
                    $content .= '<div style="width:50%; float:left; text-align:center; font-size:11px;">____________________________________________<br>'.$nombreCompleto.'<br>CC: '.$pago->negociosIdnegocios->tercerosIdterceros->identificacion.'<br>Teléfono: '.$pago->negociosIdnegocios->tercerosIdterceros->telefono.'</div>';
                    // sedes y direcciones
                    $content .= '<p style="width: 100%; text-align:center; font-size: 10px; margin: 0px; padding:0px;">SEDES Y DIRECCIÓNES</p>';
                    $content .= '<p style="width: 100%; text-align:center; font-size: 9px; margin: 0px; padding:0px;">
                                    Villavicencio 6622163-6727144. Tunja 7442048-3214492941. Bogotá - Restrepo 3615286-2094612. Bogotá - Teusaquillo 3230899-3102040740. Medellín
                                    5132750-5111290. Pereira 3244209-3244380. Manizales 8977818. Armenia 7140782-3113217338. Cali 6601090-6674444.Pasto 7314083-3105669245.Tulua
                                    2258565-2258103. Barranquilla 3512782-3514051. Cartagena 6642633-3105582204. Santa Marta 4234500. Monteria 7810489-3114486031. Sincelejo 2825318-3012611917.
                                    Bucaramanga 6429175-6422088. Cúcuta 5730225-5724163. Ibagué 2633713-3105853363. Neiva 8710053. 
                                </p>';

                $content .= '</div>';
            $content .= '</div>';
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 15,
                'marginLeft' => 15,
                'marginRight' => 15,
                'marginBottom' => 8,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('pago_parcial_'.$pago->negociosIdnegocios->identificador.'.pdf', 'D'); // call the mpdf api output as needed
        }
    // ============================    PDF REPORTE INGRESOS ARQUEO CAJA        =============================================
        public function actionArqueocaja(){
            $puntorecaudo = PuntosRecaudo::findOne(Yii::$app->request->post('puntorecaudo'));
            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $documentos = Documentos::find()
                                    ->Where(['puntosrecaudo_idpuntosrecaudo' => Yii::$app->request->post('puntorecaudo')])
                                    ->andWhere(['between', 'fechaExpedicion', Yii::$app->request->post('fechaInicial'), Yii::$app->request->post('fechaFinal') ])
                                    ->All();
            // # ================= INICIO CONSTRUCCION DE PDF
                $content = '';
                $content .= '<div style="font-family: arial; font-size:11px;">'; //INICIO CONTAINER
                    // bloque titulo
                    $content .= '<div style="width:100%; font-size:12px; float:left; text-align:center; font-weight:bold;">ORGANIZACION SAYCO ACINPRO</div>';
                    // bloque subtitulo
                    $content .= '<div style="width:720px; text-align:right; font-weight:bold; float:left;">ARQUEO DE CAJA No. 0</div>';
                    // bloque titulos fecha, punto recaudo, sucursal, zona y responsable
                    $content .= '<div style="width:720px; margin-top:10px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                        $content .= '<div style="width:144px; float:left; ">FECHA</div>';
                        $content .= '<div style="width:144px; float:left; ">PUNTO DE RECAUDO</div>';
                        $content .= '<div style="width:143px; float:left; ">SUCURSAL</div>';
                        $content .= '<div style="width:103px; float:left; ">ZONA</div>';
                        $content .= '<div style="width:183px; float:left; ">RESPONSABLE</div>';
                    $content .= '</div>';
                    $content .= '<div style="width:720px; padding:0px; text-align:center; border-bottom: 2px solid black; float:left;">';
                        $content .= '<div style="width:144px; float:left; ">'.Yii::$app->request->post('fechaInicial').'/'.Yii::$app->request->post('fechaFinal').'</div>';
                        $content .= '<div style="width:144px; float:left; ">'.$puntorecaudo->idPuntoRecaudo.' - '.$puntorecaudo->nombre.'</div>';
                        $content .= '<div style="width:143px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->nombre.'</div>';
                        $content .= '<div style="width:103px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->zona.'</div>';
                        $content .= '<div style="width:183px; float:left; ">'.$userIdentity->nombres.' '.$userIdentity->apellidos.'</div>';
                    $content .= '</div>';
                    // bloque informacion consecutivo, forma de pago, descripcion negocio, detalle pago y valor
                    $content .= '<div style="width:720px; margin-top:15px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                        $content .= '<div style="width:124px; float:left; ">CONSECUTIVO</div>';
                        $content .= '<div style="width:330px; float:left; text-align:left;">DESCRIPCION DEL NEGOCIO</div>';
                        $content .= '<div style="width:123px; float:left; text-align:left;">DETALLE PAGO</div>';
                        $content .= '<div style="width:83px; float:left; ">VALOR</div>';
                    $content .= '</div>';
                    $content .= '<div style="width:720px; height:200px; font-size:9px; padding:0px; text-align:center; float:left;">';
                        $consignacionEfectivo = 0;
                        $efectivo = 0;
                        $tarjetaDebito = 0;
                        $tarjetaCredito = 0;
                        $consignacionCheque = 0;
                        foreach ($documentos as $doc) {
                            $content .= '<div style="width:720px; font-size:9px; padding:0px; text-align:center; float:left;">';
                                $content .= '<div style="width:124px; float:left; text-align:center;">'.$doc->idDocumento.'</div>';
                                $content .= '<div style="width:330px; float:left; text-align:left;">'.$doc->negociosIdnegocios->identificador.' - '.$doc->negociosIdnegocios->razonSocial.'</div>';
                                $content .= '<div style="width:123px; float:left; text-align:left;">'.$doc->observacion.'</div>';
                                $content .= '<div style="width:83px; float:left; text-align:right;">'.number_format($doc->valor,0).'</div>';
                            $content .= '</div>';

                            $pagos = Pagos::find()->where(['documentos_iddocumento' => $doc->idDocumento])->All();
                            foreach ($pagos as $p) {
                                // vr Recaudo
                                switch ($p->tipoPago) {
                                    case 46:
                                        $consignacionCheque = $consignacionCheque + $p->valor;
                                    break;
                                    case 47:
                                        $efectivo = $efectivo + $p->valor;
                                    break;
                                    case 48:
                                        $consignacionEfectivo = $consignacionEfectivo + $p->valor;
                                    break;
                                    case 52:
                                        $tarjetaDebito = $tarjetaDebito + $p->valor;
                                    break;
                                    case 53:
                                        $tarjetaCredito = $tarjetaCredito + $p->valor;
                                    break;
                                    
                                }
                                // tipos de pago
                            }
                        }

                    $content .= '</div>';
                    // bloque titulo resumen arqueo de caja
                    $content .= '<div style="width:100%; margin-top:20px; font-size:12px; float:left; text-align:center; font-weight:bold;">RESUMEN ARQUEO DE CAJA</div>';
                    // resumen arqueo de caja
                    $content .= '<div style="width:340px; height:50px; margin:20 auto;">';
                        $content .= '<div style="width:170px; font-weight:bold; float:left;">EFECTIVO</div>';
                        $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($efectivo,0).'</div>';
                        $content .= '<div style="width:170px; font-weight:bold; float:left;">CONSIGNACION EN EFECTIVO</div>';
                        $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($consignacionEfectivo,0).'</div>';
                        $content .= '<div style="width:170px; font-weight:bold; float:left;">CONSIGNACION EN CHEQUE</div>';
                        $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($consignacionCheque,0).'</div>';
                        $content .= '<div style="width:170px; font-weight:bold; float:left;">TARJETA DE CREDITO</div>';
                        $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($tarjetaCredito,0).'</div>';
                        $content .= '<div style="width:170px; font-weight:bold; float:left;">TARJETA DEBITO</div>';
                        $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($tarjetaDebito,0).'</div>';
                    $content .= '</div>';


                $content .= '</div>'; //CIERRE CONTAINER
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 20,
                'marginLeft' => 10,
                'marginRight' => 10,
                'marginBottom' => 10,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->SetHeader(date('d-m-Y'));
            $mpdf->SetFooter('|Pagina {PAGENO}|');

            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('reporte_ingreso_arqueo.pdf', 'D'); // call the mpdf api output as needed
        }
    // ============================    PDF REPORTE INGRESOS CENTRO COSTOS      =============================================
        public function actionCentrocostos(){
            $centroCostoArray = array();
            $totalCentroCosto = 0;
            $puntorecaudo = PuntosRecaudo::findOne(Yii::$app->request->post('puntorecaudo'));
            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $documentos = Documentos::find()
                                    ->Where(['puntosrecaudo_idpuntosrecaudo' => Yii::$app->request->post('puntorecaudo')])
                                    ->andWhere(['between', 'fechaExpedicion', Yii::$app->request->post('fechaInicial'), Yii::$app->request->post('fechaFinal') ])
                                    ->All();
            foreach ($documentos as $d) { 
                if (!in_array(intval($d->negociosIdnegocios->barrios->localidades->codigoCentroCosto), $centroCostoArray)) {
                    array_push($centroCostoArray, intval($d->negociosIdnegocios->barrios->localidades->codigoCentroCosto));
                }
            }
            // # ================= INICIO CONSTRUCCION DE PDF
                $content = '';
                $content .= '<div style="font-family: arial; font-size:11px;">'; //INICIO CONTAINER
                    // bloque titulo
                    $content .= '<div style="width:100%; font-size:12px; float:left; text-align:center; font-weight:bold;">ORGANIZACION SAYCO ACINPRO</div>';
                    // bloque subtitulo
                    $content .= '<div style="width:720px; text-align:right; font-weight:bold; float:left;">RELACION DE INGRESOS No. 0</div>';
                    // bloque titulos fecha, punto recaudo, sucursal, zona y responsable
                    $content .= '<div style="width:720px; margin-top:10px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                        $content .= '<div style="width:144px; float:left; ">FECHA</div>';
                        $content .= '<div style="width:144px; float:left; ">PUNTO DE RECAUDO</div>';
                        $content .= '<div style="width:143px; float:left; ">SUCURSAL</div>';
                        $content .= '<div style="width:103px; float:left; ">ZONA</div>';
                        $content .= '<div style="width:183px; float:left; ">RESPONSABLE</div>';
                    $content .= '</div>';
                    $content .= '<div style="width:720px; padding:0px; text-align:center; border-bottom: 2px solid black; float:left;">';
                        $content .= '<div style="width:144px; float:left; ">'.Yii::$app->request->post('fechaInicial').'/'.Yii::$app->request->post('fechaFinal').'</div>';
                        $content .= '<div style="width:144px; float:left; ">'.$puntorecaudo->idPuntoRecaudo.' - '.$puntorecaudo->nombre.'</div>';
                        $content .= '<div style="width:143px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->nombre.'</div>';
                        $content .= '<div style="width:103px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->zona.'</div>';
                        $content .= '<div style="width:183px; float:left; ">'.$userIdentity->nombres.' '.$userIdentity->apellidos.'</div>';
                    $content .= '</div>';
                    // bloque informacion consecutivo, consecutivo, razon social, actividad, prpietario, direcciion, c. costo, meses y valor
                    $content .= '<div style="width:720px; font-size:9px; margin-top:15px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                        $content .= '<div style="width:90px; float:left; ">CONSECUTIVO</div>';
                        $content .= '<div style="width:145px; float:left; ">RAZON SOCIAL</div>';
                        $content .= '<div style="width:90px; float:left; ">ACTIVIDAD</div>';
                        $content .= '<div style="width:130px; float:left; ">PROPIETARIO</div>';
                        $content .= '<div style="width:90px; float:left; ">DIRECCION</div>';
                        $content .= '<div style="width:60px; float:left; ">C. COSTO</div>';
                        $content .= '<div style="width:60px; float:left; ">VALOR</div>';
                    $content .= '</div>';
                    $content .= '<div style="width:720px; font-size:8px; padding:0px; text-align:center; float:left;">';

                        // INICIO FOREACH
                        foreach ($centroCostoArray as $c) {
                            foreach ($documentos as $doc) {
                                if ($c == $doc->negociosIdnegocios->barrios->localidades->codigoCentroCosto) {
                                    $content .= '<div style="width:720px; padding:0px; text-align:center; float:left;">';
                                        $content .= '<div style="width:90px; float:left; ">'.$doc->idDocumento.'</div>';
                                        $content .= '<div style="width:145px; float:left; text-align:left;">'.$doc->negociosIdnegocios->identificador.'-'.$doc->negociosIdnegocios->razonSocial.'</div>';
                                        $content .= '<div style="width:90px; float:left; text-align:left;">'.$doc->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'</div>';
                                        $content .= '<div style="width:130px; float:left; text-align:left;">'.$doc->negociosIdnegocios->tercerosIdterceros->nombres.' '.$doc->negociosIdnegocios->tercerosIdterceros->apellidos.'</div>';
                                        $content .= '<div style="width:90px; float:left; text-align:left;">'.$doc->negociosIdnegocios->direccion.'</div>';
                                        $content .= '<div style="width:60px; float:left; ">'.$doc->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'</div>';
                                        $content .= '<div style="width:60px; float:left; text-align:right;">'.number_format($doc->valor,0).'</div>';
                                    $content .= '</div>';
                                    $totalCentroCosto = $totalCentroCosto + $doc->valor;
                                }
                            }
                            // BLOQUE TOTAL CENTRO DE COSTOS
                            $content .= '<div style="width:720px; float:left;">';
                                $content .= '<div style="width:620px; height:20px; float:left; text-align:right; font-weight:bold;">Total Centro Costo '.$c.' </div>';
                                $content .= '<div style="width:95px; height:20px; float:left; text-align:right; font-weight:bold;">'.number_format($totalCentroCosto,0).'</div>';
                            $content .= '</div>';
                            $totalCentroCosto = 0;
                        }
                        // FIN FOREACH
                        

                    $content .= '</div>';
                    // bloque titulo resumen arqueo de caja
                $content .= '</div>'; //CIERRE CONTAINER
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 20,
                'marginLeft' => 10,
                'marginRight' => 10,
                'marginBottom' => 10,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->SetHeader(date('d-m-Y'));
            $mpdf->SetFooter('|Pagina {PAGENO}|');
            $mpdf->WriteHtml($content); // call mpdf write html
            $mpdf->AddPage();
            $content = '';
            $content .= '<div style="font-family: arial; font-size:11px;">'; //INICIO CONTAINER
                // bloque titulo
                $content .= '<div style="width:100%; font-size:12px; float:left; text-align:center; font-weight:bold;">ORGANIZACION SAYCO ACINPRO</div>';
                // bloque subtitulo
                $content .= '<div style="width:720px; text-align:right; font-weight:bold; float:left;">RELACION DE INGRESOS No. 0</div>';
                // bloque titulos fecha, punto recaudo, sucursal, zona y responsable
                $content .= '<div style="width:720px; margin-top:10px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                    $content .= '<div style="width:144px; float:left; ">FECHA</div>';
                    $content .= '<div style="width:144px; float:left; ">PUNTO DE RECAUDO</div>';
                    $content .= '<div style="width:143px; float:left; ">SUCURSAL</div>';
                    $content .= '<div style="width:103px; float:left; ">ZONA</div>';
                    $content .= '<div style="width:183px; float:left; ">RESPONSABLE</div>';
                $content .= '</div>';
                $content .= '<div style="width:720px; padding:0px; text-align:center; border-bottom: 2px solid black; float:left;">';
                    $content .= '<div style="width:144px; float:left; ">'.Yii::$app->request->post('fechaInicial').'/'.Yii::$app->request->post('fechaFinal').'</div>';
                    $content .= '<div style="width:144px; float:left; ">'.$puntorecaudo->idPuntoRecaudo.' - '.$puntorecaudo->nombre.'</div>';
                    $content .= '<div style="width:143px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->nombre.'</div>';
                    $content .= '<div style="width:103px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->zona.'</div>';
                    $content .= '<div style="width:183px; float:left; ">'.$userIdentity->nombres.' '.$userIdentity->apellidos.'</div>';
                $content .= '</div>';
                // bloque informacion consecutivo, consecutivo, razon social, actividad, prpietario, direcciion, c. costo, meses y valor
                $content .= '<div style="width:715px; font-size:9px; margin-top:15px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                    $content .= '<div style="width:145px; float:left; text-align:left;">CENTRO DE COSTO</div>';
                    $content .= '<div style="width:55px; float:left; ">2010</div>';
                    $content .= '<div style="width:55px; float:left; ">2011</div>';
                    $content .= '<div style="width:55px; float:left; ">2012</div>';
                    $content .= '<div style="width:55px; float:left; ">2013</div>';
                    $content .= '<div style="width:55px; float:left; ">2014</div>';
                    $content .= '<div style="width:55px; float:left; ">2015</div>';
                    $content .= '<div style="width:55px; float:left; ">2016</div>';
                    $content .= '<div style="width:90px; float:left; text-align:right;">VR RECAUDO</div>';
                    $content .= '<div style="width:90px; float:left; text-align:right;">VR DOCUMENTO</div>';
                $content .= '</div>';
                // INICIO FOREACH CENTRO COSTOS
                $periodo2010 = 0;
                $periodo2011 = 0;
                $periodo2012 = 0;
                $periodo2013 = 0;
                $periodo2014 = 0;
                $periodo2015 = 0;
                $periodo2016 = 0;
                $vrDocumento = 0;
                foreach ($centroCostoArray as $c) {
                    foreach ($documentos as $doc) {
                        if ($c == $doc->negociosIdnegocios->barrios->localidades->codigoCentroCosto) {
                            $pagos = Pagos::find()->where(['documentos_iddocumento' => $doc->idDocumento])->All();
                            foreach ($pagos as $p) {
                                // vr Recaudo
                                switch ($p->periodo) {
                                    case 2010:
                                        $periodo2010 = $periodo2010 + $p->valor;
                                    break;
                                    case 2011:
                                        $periodo2011 = $periodo2011 + $p->valor;
                                    break;
                                    case 2012:
                                        $periodo2012 = $periodo2012 + $p->valor;
                                    break;
                                    case 2013:
                                        $periodo2013 = $periodo2013 + $p->valor;
                                    break;
                                    case 2014:
                                        $periodo2014 = $periodo2014 + $p->valor;
                                    break;
                                    case 2015:
                                        $periodo2015 = $periodo2015 + $p->valor;
                                    break;
                                    case 2016:
                                        $periodo2016 = $periodo2016 + $p->valor;
                                    break;
                                }
                                // tipos de pago
                            }
                            $vrDocumento = $vrDocumento + $doc->valor;
                        }
                    }
                    $vrRecaudo = $periodo2010 + $periodo2011 + $periodo2012 + $periodo2013 + $periodo2014 + $periodo2015 + $periodo2016;
                    $content .= '<div style="width:715px; font-size:9px; padding:0px; text-align:center; float:left;">';
                        $content .= '<div style="width:145px; float:left; text-align:left;">'.$c.'</div>';
                        $content .= '<div style="width:55px; float:left; ">'.number_format($periodo2010,0).'</div>';
                        $content .= '<div style="width:55px; float:left; ">'.number_format($periodo2011,0).'</div>';
                        $content .= '<div style="width:55px; float:left; ">'.number_format($periodo2012,0).'</div>';
                        $content .= '<div style="width:55px; float:left; ">'.number_format($periodo2013,0).'</div>';
                        $content .= '<div style="width:55px; float:left; ">'.number_format($periodo2014,0).'</div>';
                        $content .= '<div style="width:55px; float:left; ">'.number_format($periodo2015,0).'</div>';
                        $content .= '<div style="width:55px; float:left; ">'.number_format($periodo2016,0).'</div>';
                        $content .= '<div style="width:90px; float:left; text-align:right;">'.number_format($vrRecaudo,0).'</div>';
                        if (intval($vrRecaudo) == intval($vrDocumento)) {
                            $content .= '<div style="width:90px; float:left; text-align:right;">'.number_format($vrDocumento,0).'</div>';
                        }else{
                            $content .= '<div style="width:90px; color:red; float:left; text-align:right;">'.number_format($vrDocumento,0).'</div>';
                        }
                    $content .= '</div>';

                    $periodo2010 = 0;
                    $periodo2011 = 0;
                    $periodo2012 = 0;
                    $periodo2013 = 0;
                    $periodo2014 = 0;
                    $periodo2015 = 0;
                    $periodo2016 = 0;
                    $vrDocumento = 0;

                }
                // FIN FOREACH CENTRO COSTOS
                $efectivo = 0;
                $consignacionEfectivo = 0;
                $tarjetaDebito = 0;
                $tarjetaCredito = 0;
                foreach ($documentos as $doc) {
                    $pagos = Pagos::find()->where(['documentos_iddocumento' => $doc->idDocumento])->All();
                    foreach ($pagos as $p) {
                        // vr Recaudo
                        switch ($p->tipoPago) {
                            case 47:
                                $consignacionEfectivo = $consignacionEfectivo + $p->valor;
                            break;
                            case 48:
                                $efectivo = $efectivo + $p->valor;
                            break;
                            case 52:
                                $tarjetaDebito = $tarjetaDebito + $p->valor;
                            break;
                            case 53:
                                $tarjetaCredito = $tarjetaCredito + $p->valor;
                            break;
                            default:
                                $sintipos =  $p->tipoPago;
                            break;
                            
                        }
                        // tipos de pago
                    }
                }
            $content .= '</div>'; //CIERRE CONTAINER
            $mpdf->WriteHtml($content); // call mpdf write html
            $mpdf->AddPage();
            $content = '';
            $content .= '<div style="font-family: arial; font-size:11px;">'; //INICIO CONTAINER
                // bloque titulo
                $content .= '<div style="width:100%; font-size:12px; float:left; text-align:center; font-weight:bold;">ORGANIZACION SAYCO ACINPRO</div>';
                // bloque subtitulo
                $content .= '<div style="width:720px; text-align:right; font-weight:bold; float:left;">RELACION DE INGRESOS No. 0</div>';
                // bloque titulos fecha, punto recaudo, sucursal, zona y responsable
                $content .= '<div style="width:720px; margin-top:10px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                    $content .= '<div style="width:144px; float:left; ">FECHA</div>';
                    $content .= '<div style="width:144px; float:left; ">PUNTO DE RECAUDO</div>';
                    $content .= '<div style="width:143px; float:left; ">SUCURSAL</div>';
                    $content .= '<div style="width:103px; float:left; ">ZONA</div>';
                    $content .= '<div style="width:183px; float:left; ">RESPONSABLE</div>';
                $content .= '</div>';
                $content .= '<div style="width:720px; padding:0px; text-align:center; border-bottom: 2px solid black; float:left;">';
                    $content .= '<div style="width:144px; float:left; ">'.Yii::$app->request->post('fechaInicial').'/'.Yii::$app->request->post('fechaFinal').'</div>';
                    $content .= '<div style="width:144px; float:left; ">'.$puntorecaudo->idPuntoRecaudo.' - '.$puntorecaudo->nombre.'</div>';
                    $content .= '<div style="width:143px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->nombre.'</div>';
                    $content .= '<div style="width:103px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->zona.'</div>';
                    $content .= '<div style="width:183px; float:left; ">'.$userIdentity->nombres.' '.$userIdentity->apellidos.'</div>';
                $content .= '</div>';
                // bloque titulo resumen ingresos por tipo de pago
                $content .= '<div style="width:100%; margin-top:50px; font-size:12px; float:left; text-align:center; font-weight:bold;">RESUMEN RELACION DE INGRESOS POR TIPO DE PAGO</div>';
                // resumen ingresos por tipo de pago
                $content .= '<div style="width:340px; height:50px; margin:20 auto;">';
                    $content .= '<div style="width:170px; font-weight:bold; float:left;">Efectivo</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($efectivo,0).'</div>';
                    $content .= '<div style="width:170px; font-weight:bold; float:left;">Consignacion en efectivo</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($consignacionEfectivo,0).'</div>';
                    $content .= '<div style="width:170px; font-weight:bold; float:left;">Tarjeta Credito</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($tarjetaCredito,0).'</div>';
                    $content .= '<div style="width:170px; font-weight:bold; float:left; border-bottom:1px solid black;">Tarjeta Debito</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right; border-bottom:1px solid black;">'.number_format($tarjetaDebito,0).'</div>';
                    // total ingresos
                    $content .= '<div style="width:170px; font-weight:bold; float:left;">TOTAL INGRESOS</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($efectivo + $tarjetaDebito + $tarjetaCredito + $consignacionEfectivo,0).'</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.$sintipos.'</div>';
                    
                $content .= '</div>';
                // bloque titulo resumen pagos con tarjeta
                $content .= '<div style="width:100%; margin-top:30px; font-size:12px; float:left; text-align:center; font-weight:bold;">RESUMEN DE PAGOS CON TARJETA</div>';
                // resumen ingresos pagos con tarjeta
                $content .= '<div style="width:340px; height:50px; margin:20 auto;">';
                    $content .= '<div style="width:340px;">';
                        $content .= '<div style="width:85px; font-weight:bold; float:left; text-align:left;">PORCENTAJE</div>';
                        $content .= '<div style="width:85px; font-weight:bold; float:left; text-align:right;">VALOR TOTAL</div>';
                        $content .= '<div style="width:85px; font-weight:bold; float:left; text-align:right;">COMISION</div>';
                        $content .= '<div style="width:80px; font-weight:bold; float:left; text-align:right">VALOR NETO</div>';
                    $content .= '</div>';
                    foreach ($documentos as $doc) {
                        $pagos = Pagos::find()->where(['documentos_iddocumento' => $doc->idDocumento])->All();
                        $valortotal = 0;
                        $valortotalcomision = 0;
                        $valortotalneto = 0;
                        foreach ($pagos as $p) {
                            if ($p->tipoPago == 52 || $p->tipoPago == 55) {
                                $content .= '<div style="width:340px;">';
                                    $content .= '<div style="width:85px; float:left; text-align:left;">'.$p->tipostarjetasIdtipostarjetas->porcentaje.'%</div>';
                                    $content .= '<div style="width:85px; float:left; text-align:right;">'.number_format($p->valor,0).'</div>';
                                    $valorPorcentaje = ($p->tipostarjetasIdtipostarjetas->porcentaje * $p->valor) / 100;
                                    $valorNeto = $p->valor + $valorPorcentaje;
                                    $content .= '<div style="width:85px; float:left; text-align:right;">'.number_format(($p->tipostarjetasIdtipostarjetas->porcentaje * $p->valor)/100,0).'</div>';
                                    $content .= '<div style="width:80px; float:left; text-align:right;">'.number_format($valorNeto,0).'</div>';
                                $content .= '</div>';

                                $valortotal = $valortotal + $p->valor;
                                $valortotalcomision = $valortotalcomision + $valorPorcentaje;
                                $valortotalneto = $valortotalneto + $valorNeto;
                            }
                        }
                    }

                    // total ingresos
                    $content .= '<div style="width:340px; border-top:1px solid black; font-weight:bold;">';
                        $content .= '<div style="width:85px; float:left; text-align:left;">TOTALES</div>';
                        $content .= '<div style="width:85px; float:left; text-align:right;">'.number_format($valortotal,0).'</div>';
                        $content .= '<div style="width:85px; float:left; text-align:right;">'.number_format($valortotalcomision,0).'</div>';
                        $content .= '<div style="width:80px; float:left; text-align:right;">'.number_format($valortotalneto,0).'</div>';
                    $content .= '</div>';
                $content .= '</div>';
            $content .= '</div>'; //CIERRE CONTAINER
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('reporte_ingreso_centro_costo.pdf', 'D'); // call the mpdf api output as needed
        }
    // ============================    PDF REPORTE INGRESOS POR FUNCIONARIO    =============================================
        public function actionFuncionario(){
            $funcionariosArray = array();
            $puntorecaudo = PuntosRecaudo::findOne(Yii::$app->request->post('puntorecaudo'));
            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            $documentos = Documentos::find()
                                    ->Where(['puntosrecaudo_idpuntosrecaudo' => Yii::$app->request->post('puntorecaudo')])
                                    ->andWhere(['between', 'fechaExpedicion', Yii::$app->request->post('fechaInicial'), Yii::$app->request->post('fechaFinal') ])
                                    ->All();
            foreach ($documentos as $d) { 
                if (!in_array(intval($d->usuarios_idusuarios), $funcionariosArray)) {
                    array_push($funcionariosArray, intval($d->usuarios_idusuarios));
                }
            }
            // # ================= INICIO CONSTRUCCION DE PDF
                $content = '';
                $content .= '<div style="font-family: arial; font-size:11px;">'; //INICIO CONTAINER
                    // bloque titulo
                    $content .= '<div style="width:100%; font-size:12px; float:left; text-align:center; font-weight:bold;">ORGANIZACION SAYCO ACINPRO</div>';
                    // bloque subtitulo
                    $content .= '<div style="width:720px; text-align:right; font-weight:bold; float:left;">RELACION DE INGRESOS No. 0</div>';
                    // bloque titulos fecha, punto recaudo, sucursal, zona y responsable
                    $content .= '<div style="width:720px; margin-top:10px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                        $content .= '<div style="width:144px; float:left; ">FECHA</div>';
                        $content .= '<div style="width:144px; float:left; ">PUNTO DE RECAUDO</div>';
                        $content .= '<div style="width:143px; float:left; ">SUCURSAL</div>';
                        $content .= '<div style="width:103px; float:left; ">ZONA</div>';
                        $content .= '<div style="width:183px; float:left; ">RESPONSABLE</div>';
                    $content .= '</div>';
                    $content .= '<div style="width:720px; padding:0px; text-align:center; border-bottom: 2px solid black; float:left;">';
                        $content .= '<div style="width:144px; float:left; ">'.Yii::$app->request->post('fechaInicial').'/'.Yii::$app->request->post('fechaFinal').'</div>';
                        $content .= '<div style="width:144px; float:left; ">'.$puntorecaudo->idPuntoRecaudo.' - '.$puntorecaudo->nombre.'</div>';
                        $content .= '<div style="width:143px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->nombre.'</div>';
                        $content .= '<div style="width:103px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->zona.'</div>';
                        $content .= '<div style="width:183px; float:left; ">'.$userIdentity->nombres.' '.$userIdentity->apellidos.'</div>';
                    $content .= '</div>';
                    // bloque informacion consecutivo, consecutivo, razon social, actividad, prpietario, direcciion, c. costo, meses y valor
                    $content .= '<div style="width:720px; font-size:9px; margin-top:15px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                        $content .= '<div style="width:90px; float:left; ">CONSECUTIVO</div>';
                        $content .= '<div style="width:205px; float:left; ">RAZON SOCIAL</div>';
                        $content .= '<div style="width:90px; float:left; ">ACTIVIDAD</div>';
                        $content .= '<div style="width:130px; float:left; ">PRPIETARIO</div>';
                        $content .= '<div style="width:80px; float:left; ">DIRECCION</div>';
                        $content .= '<div style="width:60px; float:left; ">C. COSTO</div>';
                        $content .= '<div style="width:60px; float:left; ">VALOR</div>';
                    $content .= '</div>';
                    $content .= '<div style="width:720px; font-size:8px; padding:0px; text-align:center; float:left;">';

                        // INICIO FOREACH
                        foreach ($funcionariosArray as $f) {
                            $funcionario = Users::findOne($f);
                            $totalValorFuncionario = 0;
                            foreach ($documentos as $doc) {
                                if ($f == $doc->usuarios_idusuarios) {
                                    $content .= '<div style="width:720px; padding:0px; text-align:center; float:left;">';
                                        $content .= '<div style="width:90px; float:left; ">'.$doc->idDocumento.'</div>';
                                        $content .= '<div style="width:205px; float:left; text-align:left;">'.$doc->negociosIdnegocios->identificador.'-'.$doc->negociosIdnegocios->razonSocial.'</div>';
                                        $content .= '<div style="width:90px; float:left; text-align:left;">'.$doc->negociosIdnegocios->subactividades->actividadesIdactividades->nombre.'</div>';
                                        $content .= '<div style="width:130px; float:left; text-align:left;">'.$doc->negociosIdnegocios->tercerosIdterceros->nombres.' '.$doc->negociosIdnegocios->tercerosIdterceros->apellidos.'</div>';
                                        $content .= '<div style="width:80px; float:left; text-align:left;">'.$doc->negociosIdnegocios->direccion.'</div>';
                                        $content .= '<div style="width:60px; float:left; ">'.$doc->negociosIdnegocios->barrios->localidades->codigoCentroCosto.'</div>';
                                        $content .= '<div style="width:60px; float:left; text-align:right;">'.number_format($doc->valor,0).'</div>';
                                    $content .= '</div>';
                                    $totalValorFuncionario = $totalValorFuncionario + $doc->valor;
                                }


                            }
                            // BLOQUE TOTAL FUNCIONARIO
                            $content .= '<div style="width:720px; float:left;">';
                                $content .= '<div style="width:620px; height:20px; float:left; text-align:right; font-weight:bold;">Total '.$funcionario->nombres.' '.$funcionario->apellidos.'</div>';
                                $content .= '<div style="width:95px; height:20px; float:left; text-align:right; font-weight:bold;">'.number_format($totalValorFuncionario,0).'</div>';
                            $content .= '</div>';
                        }
                        // FIN FOREACH

                        

                    $content .= '</div>';
                    // bloque titulo resumen arqueo de caja
                $content .= '</div>'; //CIERRE CONTAINER
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 20,
                'marginLeft' => 10,
                'marginRight' => 10,
                'marginBottom' => 10,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->SetHeader(date('d-m-Y'));
            $mpdf->SetFooter('|Pagina {PAGENO}|');
            $mpdf->WriteHtml($content); // Insertar estructura html a pdf
            $mpdf->AddPage();
            $content = '';
            $content .= '<div style="font-family: arial; font-size:11px;">'; //INICIO CONTAINER
                // bloque titulo
                $content .= '<div style="width:100%; font-size:12px; float:left; text-align:center; font-weight:bold;">ORGANIZACION SAYCO ACINPRO</div>';
                // bloque subtitulo
                $content .= '<div style="width:720px; text-align:right; font-weight:bold; float:left;">RELACION DE INGRESOS No. 0</div>';
                // bloque titulos fecha, punto recaudo, sucursal, zona y responsable
                $content .= '<div style="width:720px; margin-top:10px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                    $content .= '<div style="width:144px; float:left; ">FECHA</div>';
                    $content .= '<div style="width:144px; float:left; ">PUNTO DE RECAUDO</div>';
                    $content .= '<div style="width:143px; float:left; ">SUCURSAL</div>';
                    $content .= '<div style="width:103px; float:left; ">ZONA</div>';
                    $content .= '<div style="width:183px; float:left; ">RESPONSABLE</div>';
                $content .= '</div>';
                $content .= '<div style="width:720px; padding:0px; text-align:center; border-bottom: 2px solid black; float:left;">';
                    $content .= '<div style="width:144px; float:left; ">'.Yii::$app->request->post('fechaInicial').'/'.Yii::$app->request->post('fechaFinal').'</div>';
                    $content .= '<div style="width:144px; float:left; ">'.$puntorecaudo->idPuntoRecaudo.' - '.$puntorecaudo->nombre.'</div>';
                    $content .= '<div style="width:143px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->nombre.'</div>';
                    $content .= '<div style="width:103px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->zona.'</div>';
                    $content .= '<div style="width:183px; float:left; ">'.$userIdentity->nombres.' '.$userIdentity->apellidos.'</div>';
                $content .= '</div>';
                // bloque titulo resumen ingresos por tipo de pago
                $content .= '<div style="width:100%; margin-top:50px; font-size:12px; float:left; text-align:center; font-weight:bold;">RESUMEN POR FUNCIONARIO</div>';
                // titulo tabla resumen por funcionario
                $content .= '<div style="width:715px; padding:0px; height:50px; border:1px solid black; text-align:center;  float:left; font-weight:bold;">';
                    $content .= '<div style="width:250px; padding-top:10px; border-right:1px solid black; height:40px; float:left;" >FUNCIONARIO</div>';
                    $content .= '<div style="width:59px; padding-top:10px; border-right:1px solid black; height:40px; float:left;" >VALOR</div>';
                    $content .= '<div style="width:403px; height:50px; float:left;">';
                        $content .= '<div style="width:202px; height:19px; border-bottom:1px solid black; border-right:1px solid black; padding-top:5px; float:left;">PAGOS OFICINA</div>';
                        $content .= '<div style="width:199px; height:19px; border-bottom:1px solid black; padding-top:5px; float:left;">LIQUIDACIONES</div>';
                        $content .= '<div style="width:67px; height:20px; font-size:8px; border-right:1px solid black; float:left; padding-top:5px;">ACTIVOS</div>';
                        $content .= '<div style="width:67px; height:20px; font-size:8px; border-right:1px solid black; float:left; padding-top:5px;">ANULADOS</div>';
                        $content .= '<div style="width:67px; height:20px; font-size:8px; border-right:1px solid black; float:left; padding-top:5px;">TOTAL</div>';
                        $content .= '<div style="width:65px; height:20px; font-size:8px; border-right:1px solid black; float:left; padding-top:5px;">ACTIVOS</div>';
                        $content .= '<div style="width:65px; height:20px; font-size:8px; border-right:1px solid black; float:left; padding-top:5px;">ANULADOS</div>';
                        $content .= '<div style="width:65px; height:20px; font-size:8px; float:left; padding-top:5px;">TOTAL</div>';
                    $content .= '</div>';
                $content .= '</div>';
                // tabla resumen por funcionario
                foreach ($funcionariosArray as $f) {
                    $pagosOficina = 0;
                    $pagosOficinaAnulado = 0;
                    $pagosOficinaActivo = 0;
                    $liquidacion = 0;
                    $liquidacionAnulado = 0;
                    $liquidacionActivo = 0;
                    $valortotal = 0;
                    foreach ($documentos as $doc) {
                        if ($f == $doc->usuarios_idusuarios) {
                            $valortotal = $valortotal + $doc->valor;
                        }

                        // PAGOS DE OFICINA
                        if ($doc->tipoDocumento == 74) {
                            if ($doc->anulado) {
                                $pagosOficinaAnulado++;
                            }else{
                                $pagosOficinaActivo++;
                            }
                            $pagosOficina++;
                        }
                        // LIQUIDACIONES
                        if ($doc->tipoDocumento == 6) {
                            if ($doc->anulado) {
                                $liquidacionAnulado++;
                            }else{
                                $liquidacionActivo++;
                            }
                            $liquidacion++;
                        }
                    }
                    $content .= '<div style="width:715px; padding:0px; font-size:8px; border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; float:left;">';
                        $content .= '<div style="width:250px; border-right:1px solid black; float:left; text-align:left;" >AYALA MARTIN JEISSON STEVEN</div>'; 
                        $content .= '<div style="width:59px; border-right:1px solid black; float:left; text-align:right;" >'.number_format($valortotal,0).'</div>'; 
                        $content .= '<div style="width:67px; border-right:1px solid black; float:left; text-align:right;" >'.$pagosOficinaActivo.'</div>'; 
                        $content .= '<div style="width:67px; border-right:1px solid black; float:left; text-align:right;" >'.$pagosOficinaAnulado.'</div>'; 
                        $content .= '<div style="width:67px; border-right:1px solid black; float:left; text-align:right;" >'.$pagosOficina.'</div>'; 
                        $content .= '<div style="width:65px; border-right:1px solid black; float:left; text-align:right;" >'.$liquidacionActivo.'</div>'; 
                        $content .= '<div style="width:65px; border-right:1px solid black; float:left; text-align:right;" >'.$liquidacionAnulado.'</div>'; 
                        $content .= '<div style="width:65px; float:left; text-align:right;" >'.$liquidacion.'</div>'; 
                    $content .= '</div>'; 
                }
                
            $content .= '</div>'; //CIERRE CONTAINER
            $mpdf->WriteHtml($content); // Insertar estructura html al pdf
            $mpdf->AddPage();
            $content = '';
            $content .= '<div style="font-family: arial; font-size:11px;">'; //INICIO CONTAINER
                // bloque titulo
                $content .= '<div style="width:100%; font-size:12px; float:left; text-align:center; font-weight:bold;">ORGANIZACION SAYCO ACINPRO</div>';
                // bloque subtitulo
                $content .= '<div style="width:720px; text-align:right; font-weight:bold; float:left;">RELACION DE INGRESOS No. 0</div>';
                // bloque titulos fecha, punto recaudo, sucursal, zona y responsable
                $content .= '<div style="width:720px; margin-top:10px; font-weight:bold; padding:0px; text-align:center; float:left;">';
                    $content .= '<div style="width:144px; float:left; ">FECHA</div>';
                    $content .= '<div style="width:144px; float:left; ">PUNTO DE RECAUDO</div>';
                    $content .= '<div style="width:143px; float:left; ">SUCURSAL</div>';
                    $content .= '<div style="width:103px; float:left; ">ZONA</div>';
                    $content .= '<div style="width:183px; float:left; ">RESPONSABLE</div>';
                $content .= '</div>';
                $content .= '<div style="width:720px; padding:0px; text-align:center; border-bottom: 2px solid black; float:left;">';
                    $content .= '<div style="width:144px; float:left; ">'.Yii::$app->request->post('fechaInicial').'/'.Yii::$app->request->post('fechaFinal').'</div>';
                    $content .= '<div style="width:144px; float:left; ">'.$puntorecaudo->idPuntoRecaudo.' - '.$puntorecaudo->nombre.'</div>';
                    $content .= '<div style="width:143px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->nombre.'</div>';
                    $content .= '<div style="width:103px; float:left; ">'.$puntorecaudo->sucursalesIdsucursales->zona.'</div>';
                    $content .= '<div style="width:183px; float:left; ">'.$userIdentity->nombres.' '.$userIdentity->apellidos.'</div>';
                $content .= '</div>';
                // bloque titulo resumen ingresos por tipo de pago
                $content .= '<div style="width:100%; margin-top:50px; font-size:12px; float:left; text-align:center; font-weight:bold;">RESUMEN RELACION DE INGRESOS POR TIPO DE PAGO</div>';
                // resumen ingresos por tipo de pago
                $content .= '<div style="width:340px; height:50px; margin:20 auto;">';
                    $content .= '<div style="width:170px; font-weight:bold; float:left;">Efectivo</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($efectivo,0).'</div>';
                    $content .= '<div style="width:170px; font-weight:bold; float:left;">Consignacion en efectivo</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($consignacionEfectivo,0).'</div>';
                    $content .= '<div style="width:170px; font-weight:bold; float:left;">Tarjeta Credito</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($tarjetaCredito,0).'</div>';
                    $content .= '<div style="width:170px; font-weight:bold; float:left; border-bottom:1px solid black;">Tarjeta Debito</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right; border-bottom:1px solid black;">'.number_format($tarjetaDebito,0).'</div>';
                    // total ingresos
                    $content .= '<div style="width:170px; font-weight:bold; float:left;">TOTAL INGRESOS</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.number_format($efectivo + $tarjetaDebito + $tarjetaCredito + $consignacionEfectivo,0).'</div>';
                    $content .= '<div style="width:170px; float:left; text-align:right;">'.$sintipos.'</div>';
                $content .= '</div>';
                // bloque titulo resumen pagos con tarjeta
                $content .= '<div style="width:100%; margin-top:30px; font-size:12px; float:left; text-align:center; font-weight:bold;">RESUMEN DE PAGOS CON TARJETA</div>';
                // resumen ingresos pagos con tarjeta
                $content .= '<div style="width:340px; height:50px; margin:20 auto;">';
                    $content .= '<div style="width:340px;">';
                        $content .= '<div style="width:85px; font-weight:bold; float:left; text-align:left;">PORCENTAJE</div>';
                        $content .= '<div style="width:85px; font-weight:bold; float:left; text-align:right;">VALOR TOTAL</div>';
                        $content .= '<div style="width:85px; font-weight:bold; float:left; text-align:right;">COMISION</div>';
                        $content .= '<div style="width:80px; font-weight:bold; float:left; text-align:right">VALOR NETO</div>';
                    $content .= '</div>';
                    foreach ($documentos as $doc) {
                        $pagos = Pagos::find()->where(['documentos_iddocumento' => $doc->idDocumento])->All();
                        $valortotal = 0;
                        $valortotalcomision = 0;
                        $valortotalneto = 0;
                        foreach ($pagos as $p) {
                            if ($p->tipoPago == 52 || $p->tipoPago == 55) {
                                $content .= '<div style="width:340px;">';
                                    $content .= '<div style="width:85px; float:left; text-align:left;">'.$p->tipostarjetasIdtipostarjetas->porcentaje.'%</div>';
                                    $content .= '<div style="width:85px; float:left; text-align:right;">'.number_format($p->valor,0).'</div>';
                                    $valorPorcentaje = ($p->tipostarjetasIdtipostarjetas->porcentaje * $p->valor) / 100;
                                    $valorNeto = $p->valor + $valorPorcentaje;
                                    $content .= '<div style="width:85px; float:left; text-align:right;">'.number_format(($p->tipostarjetasIdtipostarjetas->porcentaje * $p->valor)/100,0).'</div>';
                                    $content .= '<div style="width:80px; float:left; text-align:right;">'.number_format($valorNeto,0).'</div>';
                                $content .= '</div>';

                                $valortotal = $valortotal + $p->valor;
                                $valortotalcomision = $valortotalcomision + $valorPorcentaje;
                                $valortotalneto = $valortotalneto + $valorNeto;
                            }
                        }
                    }

                    // total ingresos
                    $content .= '<div style="width:340px; border-top:1px solid black; font-weight:bold;">';
                        $content .= '<div style="width:85px; float:left; text-align:left;">TOTALES</div>';
                        $content .= '<div style="width:85px; float:left; text-align:right;">'.number_format($valortotal,0).'</div>';
                        $content .= '<div style="width:85px; float:left; text-align:right;">'.number_format($valortotalcomision,0).'</div>';
                        $content .= '<div style="width:80px; float:left; text-align:right;">'.number_format($valortotalneto,0).'</div>';
                    $content .= '</div>';
                $content .= '</div>';
            $content .= '</div>'; //CIERRE CONTAINER
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('reporte_ingreso_funcionario.pdf', 'D'); // call the mpdf api output as needed
        }
    // ============================    PDF COMPROBANTES                        =============================================
        public function actionComprobante($id){
            $content = 'PDF COMPROBANTE';
            // # ================= FIN CONSTRUCCION DE PDF
            //  Objeto pdf instanceado con margenes definidas
            $pdf = new Pdf([
                'marginTop' => 15,
                'marginLeft' => 15,
                'marginRight' => 15,
                'marginBottom' => 8,
            ]); 
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('comprobante_'.$id.'.pdf', 'D'); // call the mpdf api output as needed
        }
    // =========== funcion para generar codigo de barras =================
        public function generarBarcode($consecutivo,$valor,$fechaLimite,$anchoBarcode,$alturaBarcode){
            $Fvalor = $valor * 100;

            $fechaBarcode = str_replace('-','',$fechaLimite);
            $valorBarcode = substr_replace("0000000000",$Fvalor,-(strlen($Fvalor)));

            $cadena = "415770999800412280200".$consecutivo."3902".$valorBarcode."96".$fechaBarcode;

            $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
            return base64_encode($generator->getBarcode($cadena, $generator::TYPE_CODE_128,$widthFactor = $anchoBarcode, $totalHeight = $alturaBarcode ));
        }
    // =========== funcion para generar cadena del codigo de barras =================
        public function generarCadena($consecutivo,$valor,$fechaLimite){
            $Fvalor = $valor * 100;

            $fechaBarcode = str_replace('-','',$fechaLimite);
            $valorBarcode = substr_replace("0000000000",$Fvalor,-(strlen($Fvalor)));

            $cadena = "(415)7709998004122(8020)0".$consecutivo."(3902)".$valorBarcode."(96)".$fechaBarcode;
            return $cadena;
        }

}