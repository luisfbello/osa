<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Carteleravirtual */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carteleravirtual-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cartelera_virtual_imagen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cartelera_virtual_texto')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cartelera_virtual_fecha_publicacion')->textInput() ?>

    <?= $form->field($model, 'cartelera_virtual_activo')->textInput() ?>

    <?= $form->field($model, 'cartelera_virtual_titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cartelera_virtual_video')->textInput() ?>

    <?= $form->field($model, 'cartelera_virtual_link_video')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
