<?php

use yii\helpers\Html;

$this->title="ACINPRO";
$this->params['breadcrumbs'][] = $this->title;

?>

<html>
<body>

<!-- begin about -->
<section>

    <div class="card">
        <div class="card-header alert alert-primary"><h1><?= Html::encode($this->title) ?></h1></div>
            <div class="card-body" >
                <div class="container">
                <center>
                
                <!-- begin logo -->
                <section>
                    <div class="container">
                        <div id="osaimg" class="col-lg-12">
                        <?= Html::img('images/acinpro.jpg',['width'=> '500px'])?>
                        </div>
                    </div>
                </section>
                </center>
                <!-- end logo -->
                <br>
                    <div class="container">
                        <div class="card-header alert alert-primary"><h3>¿Qu&iacute;enes son?</h3></div>
                        <p>
                        Asociación Colombiana de Intérpretes y Productores Fonográficos, Acinpro (Fundada en Medellín, 1978), 
                        sociedad de Gestión Colectiva, legalmente reconocida por el Estado Colombiano a través de la Dirección 
                        Nacional de Derecho de Autor, que gestiona los derechos de comunicación pública de la música fonograbada 
                        en nombre y representación de sus socios. <a target="_blank" href="https://www.acinpro.org.co/nosotros-acinpro/quienes-somos/">Leer mas</a> <br>
                        </p>
                    </div>
                    
                    
                    <div class="container">
                        <div class="card-header alert alert-primary"><h3>Leyes y reglamentos</h3></div>
                        <p>
                        Es indispensable para Acinpro que los socios y los usuarios de la música de la entidad conozcan qué leyes, 
                        convenios, acuerdos, estatutos y reglamentos debemos cumplir como entidad, al igual que cada uno de nuestros 
                        socios y usuarios se enteren también de las normas que deben cumplir para que todo funcione correctamente. <a target="_blank" href="https://www.acinpro.org.co/nosotros-acinpro/leyes-reglamentos/">Leer mas</a>  <br>
                        </p>
                    </div>
                    
                    
                    <div class="container">
                        <div class="card-header alert alert-primary"><h3>Situacion Financiera</h3></div>
                        <p>
                        Entre nuestros valores corporativos, la transparencia, es uno de las más importantes. Los balances y nuestra situación 
                        financiera son de interés público, tanto para nuestros socios como para los usuarios de los fonogramas. Encuentre aquí los 
                        informes sobre este tema, y sobre el manejo que le damos en nuestra entidad. <a target="_blank" href="https://www.acinpro.org.co/nosotros-acinpro/situacion-financiera/">Leer mas</a> <br>
                        </p>
                        </div>
                    </div>

                </div>
                
            </div>
    </div>

</section>

</body>

</html>