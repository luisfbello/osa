<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Funcionariosnuevos */


$estado=$model->funcionarios_nuevos_estado;
// echo "<pre>";
// echo $estado;
// die();



$this->title = $model->funcionarios_nuevos_id;
$this->params['breadcrumbs'][] = ['label' => 'Funcionariosnuevos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funcionariosnuevos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->funcionarios_nuevos_id], ['class' => 'btn btn-primary']) ?>
        <input id="idEliminar"  type="hidden" value=<?=$model->funcionarios_nuevos_id?> >
        <button class="btn btn-danger" onclick=eliminar()>Eliminar</button>
       
        <?php 
            if (Yii::$app->user->identity->id==112) {
                if ($estado==0) {
                    // echo "<a href='' class='btn btn-info'>Aprobar</a> ";
                   echo Html::a('Aprobar', ['aprobar', 'id' => $model->funcionarios_nuevos_id], ['class' => 'btn btn-info']);
                }

            }else{
                if ($estado==1) {
                    echo Html::a('Gestionar', ['gestionar', 'id' => $model->funcionarios_nuevos_id], ['class' => 'btn btn-info']);
                }
            }
         
        ?>
    </p>

    <div class="container">
        <div class="card">
            <div class="card-header alert alert-primary">
            <h3>Datos Usuario Nuevo</h3>
            <p><?php
                if (Yii::$app->user->identity->id==4) {
                    if($model->funcionarios_nuevos_estado==0){
                        echo "<div class='alert alert-danger'><b>Estado</b>: Sin Aprobar</div>";
                    }elseif($model->funcionarios_nuevos_estado==1){
                        echo "<div class='alert alert-success'><b>Estado</b>: En gestion</div>";
                    }
                }else{
                    if($model->funcionarios_nuevos_estado==0){
                        echo "<div class='alert alert-danger'><b>Estado</b>: Sin Aprobar</div>";
                    }elseif($model->funcionarios_nuevos_estado==1){
                        echo "<div class='alert alert-success'><b>Estado</b>:Aprobado</div>";
                        }
                }
                ?></p>
            </div>
            
            <div class="card-body">
                <div class="card">
                    <div class="card-header alert alert-primary">
                        <h4 for="">Informacion Personal</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class=" table table-striped" >
                                    <thead>
                                        <tr>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>Tipo Documento</th>
                                            <th>Numero Documento</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                            <?= $model->funcionarios_nuevos_nombre?>
                                            </td>
                                            <td>
                                            <?= $model->funcionarios_nuevos_apellidos?>
                                            </td>
                                            <td>
                                            <?= $model->tiposDocumento->nombre?>
                                            </td>
                                            <td>
                                            <?= $model->funcionarios_nuevos_documento?>
                                            </td>
                                            <td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table class=" table table-striped" >
                                    <thead>
                                        <tr>
                                            <th>Direccion</th>
                                            <th>Telefono</th>
                                            <th>Email</th>
                                            <th>Aplicacion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                            <?= $model->funcionarios_nuevos_direccion?>
                                            </td>
                                            <td>
                                            <?= $model->funcionarios_nuevos_telefono?>
                                            </td>
                                            <td>
                                            <?= $model->funcionarios_nuevos_email?>
                                            </td>
                                            <td>
                                            <?= $model->funcionarios_nuevos_aplicacion?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               <div class="card">
                    <div class="card-header alert alert-primary"><h4>Informacion Laboral</h4></div>
                    <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Tipo Contrato</th>
                                        <th>Fecha Inicio Contrato</th>
                                        <th>Fecha Fin contrato</th>
                                        <th>Cargo</th>
                                        <th>Sucursal</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?= $model->tiposContrato->nombre?>
                                        </td>
                                        <td>
                                            <?= $model->funcionarios_nuevos_fechainiciocontrato?>
                                        </td>
                                        <td>
                                            <?= $model->funcionarios_nuevos_fechafincontrato?>
                                        </td>
                                        <td>
                                            <?= $model->tiposCargo->nombre?>
                                        </td>
                                        <td>
                                            <?= $model->tiposSucursal->nombre?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               </div>
               <div class="card">
                    <div class="card-header alert alert-primary"><h4>Permisos</h4></div>
                    <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Recaudo</th>
                                        <th>Visitas</th>
                                        <th>Concertacion</th>
                                        <th>Documentos</th>
                                        <th>Contabilidad</th>
                                        <th>Reportes</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php $recaudo =explode(",",$model->funcionarios_nuevos_permisosrecaudo);

                                                foreach ($recaudo as $value) {
                                                    echo $value.'<br>';
                                                }
                                            
                                            ?>
                                        </td>
                                        <td>
                                        <?php $visitas =explode(",",$model->funcionarios_nuevos_permisosvisita);

                                                foreach ($visitas as $value) {
                                                    echo $value.'<br>';
                                                }

                                                ?>
                                        </td>
                                        <td>
                                        <?php $concertacion =explode(",",$model->funcionarios_nuevos_permisosconcertacion);

                                            foreach ($concertacion as $value) {
                                                echo $value.'<br>';
                                            }

                                            ?>   
                                           
                                        </td>
                                        <td>
                                        <?php $documentos =explode(",",$model->funcionarios_nuevos_permisosdocumentos);

                                                foreach ($documentos as $value) {
                                                    echo $value.'<br>';
                                                }

                                                ?>
                                            
                                        </td>
                                        <td>
                                        <?php $contabilidad =explode(",",$model->funcionarios_nuevos_permisoscontabilidad);

                                                foreach ($contabilidad as $value) {
                                                    echo $value.'<br>';
                                                }

                                                ?>
                                        </td>
                                        <td>
                                        <?php $reportes =explode(",",$model->funcionarios_nuevos_permisosreportes);

                                                foreach ($reportes as $value) {
                                                    echo $value.'<br>';
                                                }

                                                ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               </div>
                <div class="card">
                    <div class="cards-header alert alert-primary"><h3>Adicionales</h3></div>
                    <div class="card-body">
                    <table class="table table-striped">
                    <thead>
                            <tr>
                                <th>Quien Solicita</th>
                                <th>Observacion</th>
                            </tr>
                    </thead>
                    <tbody>
                            <tr>
                                <td><?= $model->funcionarios_nuevos_quiensolicita ?></td>
                                <td><?= $model->funcionarios_nuevos_observacion ?></td>

                            </tr>
                    </tbody>
                    </table>
                    </div>
                </div>

            </div>
        </div>
    </div>



</div>
<script>
    function eliminar(){
        
        http_request = new XMLHttpRequest();
        http_request.onreadyStatechange=()=>{
            if(this.readyState===4 && this.status==200){
                
            }
        }
        
    }
    
    
</script>