<?php

use yii\helpers\Html;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\solicitudpermisos */

$this->title = 'Descargar Solicitudes';
$this->params['breadcrumbs'][] = ['label' => 'Solicitudpermisos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idsolicitudpermisos, 'url' => ['reporte']];
$this->params['breadcrumbs'][] = 'report';
?>
<div class="solicitudpermisos-report">
	<div class="solicitudpermisos-revisanomina">
		<div class="container-fluid">
		 <div class= "alert alert-info" role="alert"><h4><?= Html::encode($this->title) ?></h4></div>
		</div> 
	</div>	
	        <!-- <div class= "panel panel-primary"></div> -->
	            <div class= "panel-body">
<div class = "table-responsive">

   

    <?= $this->render('reporte', [
        'model' => $model,               
    ]) ?>

</div>
