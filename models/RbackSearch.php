<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rback;

/**
 * RbackSearch represents the model behind the search form about `app\models\Rback`.
 */
class RbackSearch extends Rback
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idrback', 'idRol', 'idControlador', 'idAccion', 'estadoRback'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rback::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idrback' => $this->idrback,
            'idRol' => $this->idRol,
            'idControlador' => $this->idControlador,
            'idAccion' => $this->idAccion,
            'estadoRback' => $this->estadoRback,
        ]);

        return $dataProvider;
    }
}
