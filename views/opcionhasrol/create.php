<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OpcionHasRol */

$this->title = 'Asignar opcion a Rol';
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcion-has-rol-create">

    <h2 class="alert alert-info"><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
