<?php

namespace app\models;

use app\models\Tiposdetalles;
use Yii;

/**
 * This is the model class for table "funcionarios_nuevos".
 *
 * @property integer $funcionarios_nuevos_id
 * @property string $funcionarios_nuevos_nombre
 * @property string $funcionarios_nuevos_apellidos
 * @property integer $funcionarios_nuevos_tipodocumento
 * @property string $funcionarios_nuevos_documento
 * @property string $funcionarios_nuevos_direccion
 * @property string $funcionarios_nuevos_email
 * @property integer $funcionarios_nuevos_telefono
 * @property integer $funcionarios_nuevos_tipocontrato
 * @property string $funcionarios_nuevos_fechainiciocontrato
 * @property string $funcionarios_nuevos_fechafincontrato
 * @property integer $funcionarios_nuevos_cargo
 * @property integer $funcionarios_nuevos_sucursal
 * @property string $funcionarios_nuevos_permisosrecaudo
 * @property string $funcionarios_nuevos_permisosvisita
 * @property string $funcionarios_nuevos_permisosconcertacion
 * @property string $funcionarios_nuevos_permisosdocumentos
 * @property string $funcionarios_nuevos_permisoscontabilidad
 * @property string $funcionarios_nuevos_permisosreportes
 * @property integer $funcionarios_nuevos_estado
 * @property string $funcionarios_nuevos_aplicacion
 * @property string $funcionarios_nuevos_quiensolicita
 * @property string $funcionarios_nuevos_observacion
 */
class FuncionariosNuevos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'funcionarios_nuevos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['funcionarios_nuevos_nombre', 'funcionarios_nuevos_apellidos', 'funcionarios_nuevos_tipodocumento', 'funcionarios_nuevos_documento', 'funcionarios_nuevos_direccion', 'funcionarios_nuevos_telefono', 'funcionarios_nuevos_tipocontrato', 'funcionarios_nuevos_cargo', 'funcionarios_nuevos_sucursal'], 'required'],
            [['funcionarios_nuevos_tipodocumento', 'funcionarios_nuevos_tipocontrato', 'funcionarios_nuevos_cargo', 'funcionarios_nuevos_sucursal', 'funcionarios_nuevos_estado'], 'integer'],
            [['funcionarios_nuevos_fechainiciocontrato', 'funcionarios_nuevos_fechafincontrato'], 'safe'],
            [['funcionarios_nuevos_nombre', 'funcionarios_nuevos_apellidos','funcionarios_nuevos_telefono',], 'string'],
            [['funcionarios_nuevos_documento'], 'string', 'max' => 20],
            [['funcionarios_nuevos_direccion', 'funcionarios_nuevos_email','funcionarios_nuevos_quiensolicita','funcionarios_nuevos_observacion'], 'string'],
            [['funcionarios_nuevos_aplicacion','funcionarios_nuevos_permisosrecaudo', 'funcionarios_nuevos_permisosvisita', 'funcionarios_nuevos_permisosconcertacion', 'funcionarios_nuevos_permisosdocumentos', 'funcionarios_nuevos_permisoscontabilidad', 'funcionarios_nuevos_permisosreportes'], 'safe'],
            // [['funcionarios_nuevos_aplicacion'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'funcionarios_nuevos_id' => 'ID',
            'funcionarios_nuevos_nombre' => 'Nombre',
            'funcionarios_nuevos_apellidos' => ' Apellidos',
            'funcionarios_nuevos_tipodocumento' => 'Tipo documento',
            'funcionarios_nuevos_documento' => 'Documento',
            'funcionarios_nuevos_direccion' => ' Direccion',
            'funcionarios_nuevos_email' => 'Email',
            'funcionarios_nuevos_telefono' => ' Telefono',
            'funcionarios_nuevos_tipocontrato' => 'Tipo contrato',
            'funcionarios_nuevos_fechainiciocontrato' => 'Fecha inicio contrato',
            'funcionarios_nuevos_fechafincontrato' => 'Fecha fin contrato',
            'funcionarios_nuevos_cargo' => 'Cargo',
            'funcionarios_nuevos_sucursal' => 'Sucursal',
            'funcionarios_nuevos_permisosrecaudo' => 'Permisos recaudo',
            'funcionarios_nuevos_permisosvisita' => 'Permisos visita',
            'funcionarios_nuevos_permisosconcertacion' => 'Permisos concertacion',
            'funcionarios_nuevos_permisosdocumentos' => 'Permisos documentos',
            'funcionarios_nuevos_permisoscontabilidad' => 'Permisos contabilidad',
            'funcionarios_nuevos_permisosreportes' => 'Permisos reportes',
            'funcionarios_nuevos_estado' => 'Estado',
            'funcionarios_nuevos_aplicacion' => 'Aplicacion',
            'funcionarios_nuevos_quiensolicita'=>'Quien Solicita',
            'funcionarios_nuevos_observacion'=> 'Observacion',
        ];
    }

     /**
       * @return \yii\db\ActiveQuery
       */
      public function getTiposDocumento()
      {
          return $this->hasOne(TiposDetalles::className(), ['idTipoDetalle' => 'funcionarios_nuevos_tipodocumento']);
      }

      public function getTiposContrato()
      {
          return $this->hasOne(TiposDetalles::className(), ['idTipoDetalle' => 'funcionarios_nuevos_tipocontrato']);
      }

      public function getTiposCargo()
      {
          return $this->hasOne(TiposDetalles::className(), ['idTipoDetalle' => 'funcionarios_nuevos_cargo']);
      }
      public function getTiposSucursal()
      {
          return $this->hasOne(TiposDetalles::className(), ['idTipoDetalle' => 'funcionarios_nuevos_sucursal']);
      }


}
