<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Carteleravirtual;

/**
 * CarteleraVirtualSearch represents the model behind the search form about `app\models\Carteleravirtual`.
 */
class CarteleraVirtualSearch extends Carteleravirtual
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cartelera_virtual_id', 'cartelera_virtual_activo', 'cartelera_virtual_video'], 'integer'],
            [['cartelera_virtual_imagen', 'cartelera_virtual_texto', 'cartelera_virtual_fecha_publicacion', 'cartelera_virtual_titulo', 'cartelera_virtual_link_video'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Carteleravirtual::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cartelera_virtual_id' => $this->cartelera_virtual_id,
            'cartelera_virtual_fecha_publicacion' => $this->cartelera_virtual_fecha_publicacion,
            'cartelera_virtual_activo' => $this->cartelera_virtual_activo,
            'cartelera_virtual_video' => $this->cartelera_virtual_video,
        ]);

        $query->andFilterWhere(['like', 'cartelera_virtual_imagen', $this->cartelera_virtual_imagen])
            ->andFilterWhere(['like', 'cartelera_virtual_texto', $this->cartelera_virtual_texto])
            ->andFilterWhere(['like', 'cartelera_virtual_titulo', $this->cartelera_virtual_titulo])
            ->andFilterWhere(['like', 'cartelera_virtual_link_video', $this->cartelera_virtual_link_video]);

        return $dataProvider;
    }
}
