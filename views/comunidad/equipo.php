<?php

use yii\helpers\Html;

$this->title = 'Equipo Humano';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<section>
    
        <div class="card-header alert alert-primary"><h1><?= Html::encode($this->title) ?></h1></div>
        <center>  
            <!-- begin logo -->
            <section>
                <div class="container">
                    <div id="osaimg" class="col-lg-12">
                        <?=Html::img('images/osa.jpg',['width'=> '600px']) ?>
                    </div>
                </div>
            </section>
        </center>
        
</section>
<section>

    <div class="card-body">
        <div class="container">
            <div class="card-header alert alert-primary"><h3>Lista de Directores y Delegados</h3></div>
                <div class="container">
                    
                    <div id="accordion" style="border-color:#9CC9FE;border-style:solid">
                        <div class="card" >
                        <div class="card-header " id="headerzona1">
                            <a class="card-link" data-toggle="collapse" href="#zona1">
                            <b>ZONA 1</b>
                            </a>
                        </div>
                        <div id="zona1" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="container">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Ciudad</th>
                                                <th>Direccion</th>
                                                <th>Nombre Encargado</th>
                                                <th>Cargo</th>
                                                <th>Telefono</th>
                                                <th>Correo electronico</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Teusaquillo</td>
                                                <td>Cr 17 # 35-70</td>
                                                <td>Maria Cristina Rodiguez</td>
                                                <td>Directora de zona</td>
                                                <td>3102040740</td>
                                                <td>crodriguez@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Villavicencio</td>
                                                <td>Cl 38 # 30a-25</td>
                                                <td>Ivan Sanchez</td>
                                                <td>Delegado de zona</td>
                                                <td>3105853356-3105545493</td>
                                                <td>isanchez.ext@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Tunja</td>
                                                <td>Cr 12 # 18-33 Of 305</td>
                                                <td>Rodrigo Meneses</td>
                                                <td>Delegado de zona</td>
                                                <td>3103044688-3214492941</td>
                                                <td>rmeneses@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Restrepo</td>
                                                <td>Cl 16 sur N.22 19 Of 403</td>
                                                <td>Cesar Muñoz</td>
                                                <td>Coordinador de zona</td>
                                                <td>3125836469</td>
                                                <td>cmunoz@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Municipios</td>
                                                <td>Chia - Calle 12 N° 12 - 22 2do piso Oficina 215 Soacha - Centro Calle 13 N° 5 - 95 Oficina 204</td>
                                                <td>Manuel Camargo</td>
                                                <td>Coordinador de zona</td>
                                                <td>3204200774-3213630097</td>
                                                <td>mcamargo@saycoacinpro.org.co</td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="card">
                        <div class="card-header  " id="headerzona2">
                            <a class="collapsed card-link" data-toggle="collapse" href="#zona2">
                            <b>ZONA 2</b>
                        </a>
                        </div>
                        <div id="zona2" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="container">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Ciudad</th>
                                                <th>Direccion</th>
                                                <th>Nombre Encargado</th>
                                                <th>Cargo</th>
                                                <th>Telefono</th>
                                                <th>Correo electronico</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Medellin</td>
                                                <td>Cl 53 # 45 - 112 Of 1403 Edificio Colseguros</td>
                                                <td>Hugo Fernando Franco</td>
                                                <td>Director de zona</td>
                                                <td>3115922093</td>
                                                <td>hfranco@saycoacinpro.org.co</td> 
                                            </tr>
                                            <tr>
                                                <td>Caucasia</td>
                                                <td>Cr 2 # 21 Esquina Of 20-11</td>
                                                <td>Daniela Diaz</td>
                                                <td>Delegado de zona</td>
                                                <td>3105890212</td>
                                                <td>caucasia@saycoacinpro.org.co</td> 
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="card">
                            <div class="card-header  " id="headerzona3">
                                <a class="collapsed card-link" data-toggle="collapse" href="#zona3">
                                <b>ZONA 3</b>
                                </a>
                            </div>
                            <div id="zona3" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                <div class="container">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Ciudad</th>
                                                <th>Direccion</th>
                                                <th>Nombre Encargado</th>
                                                <th>Cargo</th>
                                                <th>Telefono</th>
                                                <th>Correo electronico</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Pereira</td>
                                                <td>Cl 20 6- 30 Of 802</td>
                                                <td>Lucy Adriana Villanueva</td>
                                                <td>Delegado de zona</td>
                                                <td>3113217334</td>
                                                <td>lvillanueva@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Manizalez</td>
                                                <td>Cl 20 # 22 - 27 Of 602 Edificio Cumanday</td>
                                                <td>Carolina Toro</td>
                                                <td>Delegado de zona</td>
                                                <td>3102590616</td>
                                                <td>ctoro.ext@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Armenia</td>
                                                <td>Cl 20 18- 44 Of 205</td>
                                                <td>Nayleth Triviño</td>
                                                <td>Delegado de zona</td>
                                                <td>3102593901-3113217338</td>
                                                <td>ntribino@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Cali</td>
                                                <td>Av 5 norte 19N- 04 Of 301</td>
                                                <td>Luz Helena Zapata</td>
                                                <td>Director de zona</td>
                                                <td>3108567762-3102600424-3108567758</td>
                                                <td>lzapata@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Popayan</td>
                                                <td>Cr 10 # 5-29 - centro</td>
                                                <td>Maria Alexandra Gonzalez</td>
                                                <td>Delegado de zona</td>
                                                <td>3105582935</td>
                                                <td>popayan@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Tulua</td>
                                                <td>Cr 26 # 26-39 Of 201</td>
                                                <td>Fredy Arley Gonzalez</td>
                                                <td>Delegado de zona</td>
                                                <td>3113345996</td>
                                                <td>fgonzalez@saycoacinpro.org.co</td>
                                            </tr>
                                            <tr>
                                                <td>Pasto</td>
                                                <td>Cl 18 28- 84 Oficina 710</td>
                                                <td>Luis Caicedo</td>
                                                <td>Delegado de zona</td>
                                                <td>3105669245</td>
                                                <td>lcaicedo@saycoacinpro.org.co</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headerzona4">
                                <a class="collapsed card-link" data-toggle="collapse" href="#zona4">
                                <b>ZONA 4</b>
                                </a>
                            </div>
                            <div id="zona4" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                <div class="container">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Ciudad</th>
                                                <th>Direccion</th>
                                                <th>Nombre Encargado</th>
                                                <th>Cargo</th>
                                                <th>Telefono</th>
                                                <th>Correo electronico</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Barranquilla</td>
                                                <td>Cl 46 # 46- 36 Piso 2 Of 205</td>
                                                <td>Katherine Villadiego</td>
                                                <td>Directora de zona</td>
                                                <td>3105582204-3115922099</td>
                                                <td>kvilladiego@saycoacinpro.org.co</td> 
                                            </tr>
                                            <tr>
                                                <td>Cartagena</td>
                                                <td>Cr 10a # 35 Of 308, Plazoleta Telecom Edificio Comodoro</td>
                                                <td>Nayith Mendoza</td>
                                                <td>Delegado de zona</td>
                                                <td>3203898505</td>
                                                <td>nmendoza.ext@saycoacinpro.org.co</td> 
                                            </tr>
                                            <tr>
                                                <td>Santa Marta</td>
                                                <td>Cr 2B # 14 - 21 Piso 7 Of 712 Edificio de los bancos</td>
                                                <td>Tatiana Correa</td>
                                                <td>Delegado de zona</td>
                                                <td>3102590683</td>
                                                <td>tcorrea@saycoacinpro.org.co</td> 
                                            </tr>
                                            <tr>
                                                <td>Monteria</td>
                                                <td>Cr 1 # 27 - 40 Piso 2 Edificio Sayco</td>
                                                <td>Mayra Hernandez</td>
                                                <td>Delegado de zona</td>
                                                <td>3114486031</td>
                                                <td>jperez@saycoacinpro.org.co</td> 
                                            </tr>
                                            <tr>
                                                <td>Valledupar</td>
                                                <td>Cr 9 # 14 29 Of 206</td>
                                                <td>Manuel Marriaga</td>
                                                <td>Delegado de zona</td>
                                                <td>3228171112</td>
                                                <td>mmarriaga@saycoacinpro.org.co</td> 
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header " id="headerzona5">
                                <a class="collapsed card-link" data-toggle="collapse" href="#zona5">
                                <b>ZONA 5</b>
                                </a>
                            </div>
                            <div id="zona5" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                <div class="container">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Ciudad</th>
                                                <th>Direccion</th>
                                                <th>Nombre Encargado</th>
                                                <th>Cargo</th>
                                                <th>Telefono</th>
                                                <th>Correo electronico</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Bucaramanga</td>
                                                <td>Cl 36 # 13-51 Of 303</td>
                                                <td>Ginna Paola caceres Torres</td>
                                                <td>Director de zona</td>
                                                <td>3115922095-3102598284</td>
                                                <td>gcaceres@saycoacinpro.org.co</td> 
                                            </tr>
                                            <tr>
                                                <td>Cucuta</td>
                                                <td>Av 5 # 9-58 Of 203</td>
                                                <td>Marisol Ramirez</td>
                                                <td>Delegado de zona</td>
                                                <td>3115929600</td>
                                                <td>dramirez@saycoacinpro.org.co</td> 
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header " id="headerzona6">
                                <a class="collapsed card-link" data-toggle="collapse" href="#zona6">
                                <b>ZONA 6</b>
                                </a>
                            </div>
                            <div id="zona6" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                <div class="container">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Ciudad</th>
                                                <th>Direccion</th>
                                                <th>Nombre Encargado</th>
                                                <th>Cargo</th>
                                                <th>Telefono</th>
                                                <th>Correo electronico</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Ibague</td>
                                                <td>Cr 4 # 12-47 Of 306</td>
                                                <td>Jose Alonzo Runza</td>
                                                <td>Director de zona</td>
                                                <td>3108565303-3105853363</td>
                                                <td>arunza@saycoacinpro.org.co</td> 
                                            </tr>
                                            <tr>
                                                <td>Caucasia</td>
                                                <td>Cr 5 # 9–18 Of 404</td>
                                                <td>Lady Katherine Ariza</td>
                                                <td>Delegado de zona</td>
                                                <td>3108567755</td>
                                                <td>lariza.ext@saycoacinpro.org.co</td> 
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  

        </div> 
    </div>

</section>
    

</div>
<script>
    $(document).ready(function(){
        $('#headerzona1').hover(function(){
            $('#headerzona1').css('background-color','#d0e4fc');
        },function(){
            $('#headerzona1').css('background-color','white');
        })

        $('#headerzona2').hover(function(){
            $('#headerzona2').css('background-color','#d0e4fc');
        },function(){
            $('#headerzona2').css('background-color','white');
        })
        $('#headerzona3').hover(function(){
            $('#headerzona3').css('background-color','#d0e4fc');
        },function(){
            $('#headerzona3').css('background-color','white');
        })
        $('#headerzona4').hover(function(){
            $('#headerzona4').css('background-color','#d0e4fc');
        },function(){
            $('#headerzona4').css('background-color','white');
        })
        $('#headerzona5').hover(function(){
            $('#headerzona5').css('background-color','#d0e4fc');
        },function(){
            $('#headerzona5').css('background-color','white');
        })
        $('#headerzona6').hover(function(){
            $('#headerzona6').css('background-color','#d0e4fc');
        },function(){
            $('#headerzona6').css('background-color','white');
        })
    });

</script>