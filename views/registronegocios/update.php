<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Registronegocios */

$this->title = 'Update Registronegocios: ' . ' ' . $model->registro_negocio_id;
$this->params['breadcrumbs'][] = ['label' => 'Registronegocios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->registro_negocio_id, 'url' => ['view', 'id' => $model->registro_negocio_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="registronegocios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
