<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Carteleravirtual */

$this->title = 'Update Carteleravirtual: ' . ' ' . $model->cartelera_virtual_id;
$this->params['breadcrumbs'][] = ['label' => 'Carteleravirtuals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cartelera_virtual_id, 'url' => ['view', 'id' => $model->cartelera_virtual_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="carteleravirtual-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
