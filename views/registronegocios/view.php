<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Registronegocios */

$this->title = $model->registro_negocio_id;
$this->params['breadcrumbs'][] = ['label' => 'Registronegocios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registronegocios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->registro_negocio_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->registro_negocio_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'registro_negocio_id',
            'registro_negocio_fecha',
            'registro_negocio_nombre',
            'registro_negocio_apellido',
            'registro_negocio_identificacion',
            'registro_negocio_email:email',
            'registro_negocio_telefono1',
            'registro_negocio_telefono2',
            'registro_negocio_nombre_establecimiento',
            'registro_negocio_zona',
            'registro_negocio_nit_establecimiento',
            'registro_negocio_departamento',
            'registro_negocio_ciudad',
            'registro_negocio_codigo_usuario',
            'registro_negocio_liquidado',
            'registro_negocio_se_agenda_visita',
            'registro_negocio_observacion:ntext',
            'registro_negocio_dias_sin_respuesta',
            'registro_negocio_pago',
            'registro_negocio_file1',
            'file1',
            'registro_negocio_file2',
        ],
    ]) ?>

</div>
