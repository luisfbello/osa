<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Response;
use app\models\FormRegister; 
use app\models\Users; 
use app\models\AccesosDirectos; 
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Rol;


// Yii::import('application.vendors.*');
// require(__DIR__ . '/../vendor/phpmailer2/PHPMailerAutoload.php');
// require(__DIR__ . '/../vendor/phpmailer2/class.phpmailer.php');

Yii::$classMap['phpmailer2'] = __DIR__ . '/../vendor/phpmailer2/PHPMailerAutoload.php';
use phpmailer2;

class SiteController extends Controller
{
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        //'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'logout' => [''],
                ],
            ],
        ];

    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {

        $userIdentity = Users::findOne(yii::$app->user->identity->id);
        $accesosDirectos = AccesosDirectos::find()
                            ->where(['IN','idRol', explode(',', yii::$app->user->identity->role)])
                            ->andWhere(['estado'=>1])
                            ->all();
        $roles = Rol::find()->all();

        return $this->render('index',[
            'userIdentity' => $userIdentity,
            'accesosDirectos' => $accesosDirectos,
            'roles' => $roles
        ]);
        return $this->render('cartelera-virtual/index');
    }

    public function actionIndexweb()
    {
        return $this->redirect(["cartelera-virtual/index"]);
    }

     public function actionLogin()
     {
         if (!\Yii::$app->user->isGuest) {
             return $this->redirect(["site/index"]);
         }

        $model = new LoginForm();
         if ($model->load(Yii::$app->request->post()) && $model->login()) {

             return $this->redirect(["site/index"]);

         } else {
             return $this->render('login', [
                 'model' => $model,
             ]);
        }
     }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionUser(){
        return $this->render("user");
    }

    public function actionAdmin(){
        return $this->render("admin");
    }

 

    private function randKey($str='', $long=0){
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;

        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }

        return $key;
    }

       

     public function actionConfirm(){

        $table = new Users;

        if (Yii::$app->request->get())
        {
            //Obtenemos el valor de los parámetros get
            $id = Html::encode($_GET["id"]);
            $authKey = $_GET["authKey"];
        
            if ((int) $id)
            {
                //Realizamos la consulta para obtener el registro
                $model = $table
                ->find()
                ->where("id=:id", [":id" => $id])
                ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
      
                //Si el registro existe
                if ($model->count() == 1)
                {
                    $activar = Users::findOne($id);
                    $activar->activate = 1;

                    if ($activar->update())
                    {
                        echo "Enhorabuena registro llevado a cabo correctamente, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";
                    }
                    else
                    {
                        echo "Ha ocurrido un error al realizar el registro, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";

                    }
                }
                else //Si no existe redireccionamos a login
                {
                    return $this->redirect(["site/login"]);
                }
            }
            else //Si id no es un número entero redireccionamos a login
            {
                return $this->redirect(["site/login"]);
            }
        }
    }

      
    public function actionRegister()
    { 
        //Creamos la instancia con el model de validación
        $model = new FormRegister;

        //Mostrará un mensaje en la vista cuando el usuario se haya registrado
        $msg = null;

        //Validación mediante ajax
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);

        }

        if ($model->load(Yii::$app->request->post()))
        {

            if($model->validate())
            {

                //Preparamos la consulta para guardar el usuario
                $user = new Users;
                $user->username = $model->username;
                $user->email = $model->email;
                $user->role = $model->role; 
                $user->nombres = $model->nombres; 
                $user->apellidos = $model->apellidos; 
                $user->numeroIdentificacion = $model->numeroIdentificacion; 
                $user->direccion = $model->direccion; 
                $user->telefono = $model->telefono; 
                // $user->grupo_idgrupo = $model->grupo_idgrupo; 

                //Encriptamos el password
                $user->password = crypt($model->password, Yii::$app->params["salt"]);
                // $user->password = md5($model->password);

                //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
                //clave será utilizada para activar el usuario
                $user->authKey = $this->randKey("abcdef0123456789", 200);
                //Creamos un token de acceso único para el usuario
                $user->accessToken = $this->randKey("abcdef0123456789", 200);
                //Si el registro es guardado correctamente
                if ($user->insert())
                {
                    $newUser = Users::findOne($user->id);
                    $msg = "El funcionario ".$newUser->username.' ha sido registrado';
                    $alert = 'success';
                    $graphicon = 'ok';
                    return $this->render("newUser", 
                        [
                            "model" => $newUser, 
                            "msg" => $msg,
                            "alert" => $alert,
                            "graphicon" => $graphicon
                        ]
                    );
                }
                else
                {
                    $newUser = false;
                    $msg = "Ha ocurrido un error al llevar a cabo tu registro";
                    $alert = 'danger';
                    $graphicon = 'remove';

                    return $this->render("newUser", 
                        [
                            "model" => $newUser, 
                            "msg" => $msg,
                            "alert" => $alert,
                            "graphicon" => $graphicon
                        ]
                    );
                }
            }
            else
            {
                $model->getErrors();

            }
        }
        $roles = Rol::find()->all();
        return $this->render("register", ["model" => $model, "msg" => $msg,"roles"=>$roles]);
    }


}
