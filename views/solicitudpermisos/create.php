<?php

use yii\helpers\Html;
use app\models\Users;
use app\models\cargos;
use app\models\Puntosrecaudo;
use app\models\tiposdetalles;

/* @var $this yii\web\View */
/* @var $model app\models\solicitudpermisos */

$this->title = 'Solicitud de permiso';
$this->params['breadcrumbs'][] = ['label' => 'Solicitudpermisos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitudpermisos-create">
    <div class="container-fluid">
	   <div class= "alert alert-info" role="alert"><h4><?= Html::encode($this->title) ?></h4></div>
        <!-- <div class= "panel panel-primary"></div> -->
    </div>
<div class= "panel-body">
    <div class = "table-responsive">

    <!-- <h1><?= Html::encode($this->title) ?></h1>
 -->
    <?= $this->render('_form', [
        'model' => $model,
        'users'=> $users,
        'cargos'=>$cargos,
        'puntosrecaudo'=>$puntosrecaudo,
        'tiposdetalles'=>$tiposdetalles,        
    ]) ?>


   </div>
 </div>  
