<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Funcionariosnuevos */

$this->title = 'Update Funcionariosnuevos: ' . ' ' . $model->funcionarios_nuevos_id;
$this->params['breadcrumbs'][] = ['label' => 'Funcionariosnuevos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->funcionarios_nuevos_id, 'url' => ['view', 'id' => $model->funcionarios_nuevos_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="funcionariosnuevos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
