<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NegociosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Funcionarios Gestionados';
$this->params['breadcrumbs'][] = $this->title;


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1><?= Html::encode($this->title) ?></h1>
<div class="container">
    <div class="card">
        <div class="card-header alert alert-primary"><h3>Funcionarios Gestionados</h3>
            <?php 
                $form= ActiveForm::begin([
                    'action' => ['excel/funcionarioscreados'],
                    'method' => 'post',
                    'id' => 'funcionarios',
                ]);
            ?>
            <img id="btn-funcionarios" src="images/excel.png" alt="" title="Reporte Excel" style="cursor:pointer;margin-left: 20px;">
            <input type="hidden" name="funcionarios" value="<?php echo htmlentities(serialize($funcionarios))?>">
            <?php ActiveForm::end()?> 
        </div>
        <div class="card-body">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Documentos</th>
                <th>Direccion</th>
                <th>Correo</th>
                <th>Telefono</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($funcionarios as $value) { ?>
               <tr>
                    <td><?php echo $value['funcionarios_nuevos_nombre'] ?></td>
                    <td><?php echo $value['funcionarios_nuevos_apellidos'] ?></td>
                    <td><?php echo $value['funcionarios_nuevos_documento'] ?></td>
                    <td><?php echo $value['funcionarios_nuevos_direccion'] ?></td>
                    <td><?php echo $value['funcionarios_nuevos_email'] ?></td>
                    <td><?php echo $value['funcionarios_nuevos_telefono'] ?></td>

                </tr>
            <?php }
            ?>
            
            
            </tbody>
        </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#btn-funcionarios').click(function(){
            $('#funcionarios').submit();
        });
    });
</script>
</body>
</html>