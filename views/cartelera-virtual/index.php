<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CarteleraVirtualSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cartelera Virtual';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cartelera-virtual-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <?php if(!Yii::$app->user->isGuest){?> -->
    <p>
        <?= Html::a('Registrar Evento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <!-- <?php } ?> -->
    <center>
        <div class="card" style="width: 50rem;" >
        <img src="<?php echo Url::base();?>/carteleravirtual/cumpleanosMarzo2020.png" class="figure-img img-fluid rounded" alt="...">
        <!-- <video src="<?php echo Url::base();?>/carteleravirtual/NOVIEMBRE.mp4"  width="640" height="480"> -->
            <div class="card-body">
                <!-- <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
            </div>
        </div>
    </center>
    <br>
    <center>
        <div class="card" style="width: 58rem;" >
            <div class="card-body">
                <h5 class="card-title alert alert-primary">Gu&iacute;a de la Policita Frente a la gesti&oacute;n que realiza la OSA</h5>
                <div class="embed-responsive embed-responsive-21by9" >
                    <embed class="embed-responsive-item" src="<?php echo Url::base(); ?>/documentos/GuiadelaPoliciafrentealagestionquerealizalaOSA.pdf#toolbar=0&navpanes=0&scrollbar=0" width=100%>
                </div>
                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
            </div>
        </div>
    </center>
    <br>
    <center>
        <div class="card" style="width: 58rem;" >
            <div class="card-body">
                <h5 class="card-title alert alert-primary">Conoce los objetivos de Calidad</h5>
                <div >
                    <img src="<?php echo Url::base(); ?>/images/objetivos.png"  width=600 height=100%>
                </div>
                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
            </div>
        </div>
    </center>
    <center>
        <div class="card" style="width: 58rem;" >
            <div class="card-body">
                <h5 class="card-title alert alert-primary">Nuestro comit&eacute; de seguridad y salud para el trabajo</h5>
                <div >
                    <img src="<?php echo Url::base(); ?>/images/comitedeseguridad.jpg"  width=600>
                </div>
                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
            </div>
        </div>
    </center>

    <center>
        <div class="card" style="width: 58rem;" >
            <div class="card-body">
                <h5 class="card-title alert alert-primary">Nuestro comit&eacute; de Convivencia</h5>
                <div >
                    <img src="<?php echo Url::base(); ?>/images/convivencia.jpg"  width=600>
                </div>
                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
            </div>
        </div>
    </center>

</div>
