<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;

$objPHPExcel = new PHPExcel(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");
	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$objPHPExcel->getActiveSheet()->getStyle("A1:AB1")->getFont()->setBold(true);
   	foreach(range('A','Z') as $columnID) {
   	    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	        ->setAutoSize(true);
   	}
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A1', 'RAZÓN SOCIAL')
	            ->setCellValue('B1', 'CÓDIGO')
	            ->setCellValue('C1', 'NIT')
	            ->setCellValue('D1', 'DIRECCIÓN')
	            ->setCellValue('E1', 'VALOR LIQUIDADO')
	            ->setCellValue('F1', 'CATEGORÍA')
	            ->setCellValue('G1', 'ACTIVIDAD')
	            ->setCellValue('H1', 'CONVENIO')
	            ->setCellValue('I1', 'ÚLTIMO PAGO')
	            ->setCellValue('J1', 'MUNICIPIO')
	            ->setCellValue('K1', 'LOCALIDAD')
	            ->setCellValue('L1', 'SECTOR')
	            ->setCellValue('M1', 'PROPIETARIO')
	            ->setCellValue('N1', 'ENTIDAD')
	            ->setCellValue('O1', 'PERIODO')
	            ->setCellValue('P1', 'TARIFA')
	            ->setCellValue('Q1', 'ENTIDAD')
	            ->setCellValue('R1', 'PERIODO')
	            ->setCellValue('S1', 'TARIFA')
	            ->setCellValue('T1', 'ENTIDAD')
	            ->setCellValue('U1', 'PERIODO')
	            ->setCellValue('V1', 'TARIFA')
	            ->setCellValue('W1', 'ENTIDAD')
	            ->setCellValue('X1', 'PERIODO')
	            ->setCellValue('Y1', 'TARIFA')
	            ->setCellValue('Z1', 'ENTIDAD')
	            ->setCellValue('AA1', 'PERIODO')
	            ->setCellValue('AB1', 'TARIFA');
	$count = 2;
	foreach ($masiva as $m) {
		$negocio = Negocios::findOne($m);
		$documento = Documentos::find()
					->where(['tipoDocumento' => 245])
					->andWhere(['negocios_idnegocios' => $m])
					->one();
		$tarifasBase = TarifasBase::find()
					->andWhere(['negocios_idnegocio' => $m])
					->groupBy(['entidades_identidad'])
					->orderBy(['periodo' => SORT_ASC])
					->all();

		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $negocio->razonSocial)
		            ->setCellValue('B'.$count, strval($negocio->identificador))
		            ->setCellValue('C'.$count, strval($negocio->tercerosIdterceros->identificacion))
		            ->setCellValue('D'.$count, $negocio->direccion)
		            ->setCellValue('E'.$count, number_format($documento->valor,0))
		            ->setCellValue('F'.$count, $negocio->categoria)
		            ->setCellValue('G'.$count, $negocio->subactividades->actividades->nombre)
		            ->setCellValue('H'.$count, $negocio->conveniosIdconvenios->nombre)
		            ->setCellValue('I'.$count, 'FALTA')
		            ->setCellValue('J'.$count, $negocio->municipios->nombre)
		            ->setCellValue('K'.$count, $negocio->barrios->localidades->nombre)
		            ->setCellValue('L'.$count, $negocio->barrios->nombre)
		            ->setCellValue('M'.$count, $negocio->tercerosIdterceros->nombres.' '.$negocio->tercerosIdterceros->apellidos);

		$columnTarifas = array(
							1 => array(
										1 => 'N', 
         								2 => 'O', 
         								3 => 'P', 
     						),
     						2 => array(
     									1 => 'Q', 
     									2 => 'R', 
     									3 => 'S', 
     						),
     						3 => array(
     									1 => 'T', 
     									2 => 'U', 
     									3 => 'V', 
     						),
     						4 => array(
     									1 => 'W', 
     									2 => 'X', 
     									3 => 'Y', 
     						),
     						5 => array(
     									1 => 'Z', 
     									2 => 'AA', 
     									3 => 'AB', 
     						),
		);
		$countTarifa = 1;

		foreach ($tarifasBase as $t) {
			$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue($columnTarifas[$countTarifa][1].$count, $t->entidades->nombre)
			            ->setCellValue($columnTarifas[$countTarifa][2].$count, $t->periodo)
			            ->setCellValue($columnTarifas[$countTarifa][3].$count, number_format($t->valor,0));
			$countTarifa++;
		}

		$count++;
	}
	$objPHPExcel->getActiveSheet()->setTitle('Usuarios');
	$objPHPExcel->setActiveSheetIndex(0);
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="masiva'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

 ?>