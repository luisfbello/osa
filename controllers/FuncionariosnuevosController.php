<?php

namespace app\controllers;

use Yii;
use app\models\Funcionariosnuevos;
use app\models\FuncionariosnuevosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * FuncionariosnuevosController implements the CRUD actions for Funcionariosnuevos model.
 */
class FuncionariosnuevosController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Funcionariosnuevos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FuncionariosnuevosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Funcionariosnuevos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Funcionariosnuevos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Funcionariosnuevos();

        // echo "<pre>";
        // // print_r($usuarios['FuncionariosNuevos']);
        // print_r(Yii::$app->request->post());
        // die();


        if (Yii::$app->request->post() ) {

            // echo "<pre>";
            // print_r(Yii::$app->request->post());
            // die();



            $usuarios=Yii::$app->request->post();

            
 
            $nombre= $usuarios['FuncionariosNuevos']['funcionarios_nuevos_nombre'];
            $apellidos = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_apellidos'];
            $tipodocumento = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_tipodocumento'];
            $documento = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_documento'];
            $direccion = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_direccion'];
            $telefono = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_telefono'];
            $email = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_email'];
            $aplicacionArray = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_aplicacion'];
            $aplicacion=implode(",",$aplicacionArray);
            $tipocontrato = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_tipocontrato'];
            $fechainiciocontrato = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_fechainiciocontrato'];
            
            $fechafincontrato = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_fechafincontrato'];
            

            $cargo = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_cargo'];
            $sucursal = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_sucursal'];
            $permisorecaudoArray = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_permisosrecaudo'];
            if (!empty($permisorecaudoArray)) {
                $permisorecaudo=implode(",",$permisorecaudoArray);
            }else{
                $permisorecaudo='No maneja';
            }
            
            $permisovisitaArray = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_permisosvisita'];
            if (!empty($permisovisitaArray)) {
                $permisovisita=implode(",",$permisovisitaArray);
            }else{
                $permisovisita='No maneja';
            }
            
            $permisosconcertacionArray = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_permisosconcertacion'];
            if (!empty($permisosconcertacionArray)) {
                $permisosconcertacion=implode(",",$permisosconcertacionArray);
            }else{
                $permisosconcertacion='No maneja';
            }
            
            $permisodocumentoArray = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_permisosdocumentos'];
            if (!empty($permisodocumentoArray)) {
                $permisodocumento=implode(",", $permisodocumentoArray);
            }else{
                $permisodocumento='No maneja';
            }
            
            $permisocontabilidadArray= $usuarios['FuncionariosNuevos']['funcionarios_nuevos_permisoscontabilidad'];
            if (!empty($permisocontabilidadArray)) {
                $permisocontabilidad=implode(",",$permisocontabilidadArray);
            }else{
                $permisocontabilidad='No Maneja';
            }
            
            $permisoreportesArray = $usuarios['FuncionariosNuevos']['funcionarios_nuevos_permisosreportes'];
            if (!empty($permisoreportesArray)) {
                $permisoreportes=implode(",",$permisoreportesArray);
            }else{
                $permisoreportes='';
            }

            $quiensolicita=$usuarios['FuncionariosNuevos']['funcionarios_nuevos_quiensolicita'];
            
            if (!empty($usuarios['FuncionariosNuevos']['funcionarios_nuevos_observacion'])) {
                $observacion=$usuarios['FuncionariosNuevos']['funcionarios_nuevos_observacion'];
            }else{
                $observacion='';
            }

            
            

            // echo "<pre>";
            // print_r($permisosconcertacion);
            // die();

            
            $query = new Query();
            $connection = Yii::$app->db;

            $query=$connection->createCommand("insert into funcionarios_nuevos(
                                                `funcionarios_nuevos_nombre`, 
                                                `funcionarios_nuevos_apellidos`, 
                                                `funcionarios_nuevos_tipodocumento`, 
                                                `funcionarios_nuevos_documento`, 
                                                `funcionarios_nuevos_direccion`, 
                                                `funcionarios_nuevos_telefono`, 
                                                `funcionarios_nuevos_email`, 
                                                `funcionarios_nuevos_aplicacion`, 
                                                `funcionarios_nuevos_tipocontrato`, 
                                                `funcionarios_nuevos_fechainiciocontrato`, 
                                                `funcionarios_nuevos_fechafincontrato`, 
                                                `funcionarios_nuevos_cargo`, 
                                                `funcionarios_nuevos_sucursal`, 
                                                `funcionarios_nuevos_permisosrecaudo`, 
                                                `funcionarios_nuevos_permisosvisita`, 
                                                `funcionarios_nuevos_permisosconcertacion`, 
                                                `funcionarios_nuevos_permisosdocumentos`, 
                                                `funcionarios_nuevos_permisoscontabilidad`, 
                                                `funcionarios_nuevos_permisosreportes`,
                                                `funcionarios_nuevos_quiensolicita`,
                                                `funcionarios_nuevos_observacion`) values(
                                                    '".$nombre."',
                                                    '".$apellidos."',
                                                    ".$tipodocumento.",
                                                    '".$documento."',
                                                    '".$direccion."',
                                                    ".$telefono.",
                                                    '".$email."',
                                                    '".$aplicacion."',
                                                    ".$tipocontrato.",
                                                    '".$fechainiciocontrato."',
                                                    '".$fechafincontrato."',
                                                    ".$cargo.",
                                                    ".$sucursal.",
                                                    '".$permisorecaudo."',
                                                    '".$permisovisita."',
                                                    '".$permisosconcertacion."',
                                                    '".$permisodocumento."',
                                                    '".$permisocontabilidad."',
                                                    '".$permisoreportes."',
                                                    '".$quiensolicita."',
                                                    '".$observacion."'
                                                )");
            // echo "<pre>";
            // print_r($query);
            // die();

            $save=$query->execute();

            if ($save) {
                $model2 = new Funcionariosnuevos();

                return $this->render('create',[
                    'model'=>$model2,
                ]);

            }else{
                echo '<pre>';
                print_r($model->getErrors() );
                die();
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionAprobar($id){
        $model= $this->findModel($id);
        
        // echo "<pre>";
        // print_r($model);

        $model->funcionarios_nuevos_estado=1;
        if ($model->save()) {
            return $this->redirect(['view','id' => $model->funcionarios_nuevos_id]);
        }else{
            echo '<pre>';
            print_r($model->getErrors() );
            die();
        }

    }
    public function actionGestionar($id){
        
        $model= $this->findModel($id);
        
        // echo "<pre>";
        // print_r($model);

        $model->funcionarios_nuevos_estado=2;
        if ($model->save()) {
            return $this->redirect(['index']);
        }else{
            echo '<pre>';
            print_r($model->getErrors() );
            die();
        }

    }

    /**
     * Updates an existing Funcionariosnuevos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {   
        // $model = new Funcionariosnuevos();
        $model = $this->findModel($id);
        
        
        // print_r(Yii::$app->request->post());
        // die();


        if ($model->load(Yii::$app->request->post())) {

            $usuarioupdate=Funcionariosnuevos::findOne($id);
            // echo "<pre>";
            // print_r($model);
            // echo "=============================================================== <br>";
            // print_r($usuarioupdate);
            // die();

            if ($usuarioupdate) {
                $usuarioupdate->funcionarios_nuevos_nombre=$model->funcionarios_nuevos_nombre;
                $usuarioupdate->funcionarios_nuevos_apellidos=$model->funcionarios_nuevos_apellidos;
                $usuarioupdate->funcionarios_nuevos_tipodocumento=$model->funcionarios_nuevos_tipodocumento;
                $usuarioupdate->funcionarios_nuevos_documento=$model->funcionarios_nuevos_documento;
                $usuarioupdate->funcionarios_nuevos_direccion=$model->funcionarios_nuevos_direccion;
                $usuarioupdate->funcionarios_nuevos_telefono=$model->funcionarios_nuevos_telefono;
                $usuarioupdate->funcionarios_nuevos_email=$model->funcionarios_nuevos_email;

                $aplicacion=implode(",",$model->funcionarios_nuevos_aplicacion);
                $usuarioupdate->funcionarios_nuevos_aplicacion=$aplicacion;

                $usuarioupdate->funcionarios_nuevos_tipocontrato=$model->funcionarios_nuevos_tipocontrato;
                $usuarioupdate->funcionarios_nuevos_fechainiciocontrato=$model->funcionarios_nuevos_fechainiciocontrato;
                $usuarioupdate->funcionarios_nuevos_fechafincontrato=$model->funcionarios_nuevos_fechafincontrato;
                $usuarioupdate->funcionarios_nuevos_cargo=$model->funcionarios_nuevos_cargo;
                $usuarioupdate->funcionarios_nuevos_sucursal=$model->funcionarios_nuevos_sucursal;

                //permisos Recaudo
                if (!empty($model->funcionarios_nuevos_permisosrecaudo)) {
                    $permisoRecaudo=implode(",",$model->funcionarios_nuevos_permisosrecaudo);
                    $usuarioupdate->funcionarios_nuevos_permisosrecaudo=$permisoRecaudo;
                }else{
                    $usuarioupdate->funcionarios_nuevos_permisosrecaudo=$usuarioupdate->funcionarios_nuevos_permisosrecaudo;
                }

                //permisos visita
                if (!empty($model->funcionarios_nuevos_permisosvisita)) {
                    $permisoVisita=implode(",",$model->funcionarios_nuevos_permisosvisita);
                    $usuarioupdate->funcionarios_nuevos_permisosvisita=$permisoVisita;
                }else{
                    $usuarioupdate->funcionarios_nuevos_permisosvisita=$usuarioupdate->funcionarios_nuevos_permisosvisita;
                }

                //permisos concertacion
                if (!empty($model->funcionarios_nuevos_permisosconcertacion)) {
                    $permisoConcertacion=implode(",",$model->funcionarios_nuevos_permisosconcertacion);
                    $usuarioupdate->funcionarios_nuevos_permisosconcertacion=$permisoConcertacion;
                }else{
                    $usuarioupdate->funcionarios_nuevos_permisosconcertacion=$usuarioupdate->funcionarios_nuevos_permisosconcertacion;
                }

                //permisos documentos
                if (!empty($model->funcionarios_nuevos_permisosdocumentos)) {
                    $permisoDocumento=implode(",",$model->funcionarios_nuevos_permisosdocumentos);
                    $usuarioupdate->funcionarios_nuevos_permisosdocumentos=$permisoDocumento;
                }else{
                    $usuarioupdate->funcionarios_nuevos_permisosdocumentos=$usuarioupdate->funcionarios_nuevos_permisosdocumentos;
                }

                //permisos contabilidad
                if (!empty($model->funcionarios_nuevos_permisoscontabilidad)) {
                    $permisoContabilidad=implode(",",$model->funcionarios_nuevos_permisoscontabilidad);
                    $usuarioupdate->funcionarios_nuevos_permisoscontabilidad=$permisoContabilidad;
                }else{
                    $usuarioupdate->funcionarios_nuevos_permisoscontabilidad=$usuarioupdate->funcionarios_nuevos_permisoscontabilidad;
                }
                
                //permisos Reportes
                if (!empty($model->funcionarios_nuevos_permisosreportes)) {
                    $permisoReportes=implode(",",$model->funcionarios_nuevos_permisosreportes);
                    $usuarioupdate->funcionarios_nuevos_permisosreportes=$permisoReportes;
                }else{
                    $usuarioupdate->funcionarios_nuevos_permisosreportes=$usuarioupdate->funcionarios_nuevos_permisosreportes;
                }

                $usuarioupdate->funcionarios_nuevos_quiensolicita=$model->funcionarios_nuevos_quiensolicita;
                $usuarioupdate->funcionarios_nuevos_observacion=$model->funcionarios_nuevos_observacion;

                if($usuarioupdate->update()){
                    return $this->redirect(['view', 'id' => $model->funcionarios_nuevos_id]);
                }else{
                    echo '<pre>';
                    print_r($usuarioupdate->getErrors());
                }





            }

            
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Funcionariosnuevos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $mensaje='';
        if($this->findModel($id)->delete()){
            $mensaje="El Usuario se Elimino Correctamente ";
        }

        
    }

    public function actionFuncionarioscreados(){

        // echo "estas en el controlador";
        // die();
        $query = new Query();
        $connection = Yii::$app->db;

        $query=$connection->createCommand('SELECT * FROM funcionarios_nuevos where funcionarios_nuevos_estado=2');
        $funcionarios=$query->queryAll();

        // $funcionarios=Funcionariosnuevos::find()->where(['funcionarios_nuevos_estado'=>2])->all();
        // echo "<pre>";
        // print_r($funcionarios);
        // die();
        
        if (isset($funcionarios)) {
            return $this->render('funcionarioscreados',[
                'funcionarios'=>$funcionarios,
            ]);
        }else {
            return $this->render('funcionarioscreados',[
                'funcionarios'=>null,
            ]);
        }




    }
    /**
     * Finds the Funcionariosnuevos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Funcionariosnuevos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Funcionariosnuevos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
