<?php

namespace app\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * 
 */
class OsaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Carteleravirtual models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionSayco()
    {
        return $this->render('sayco');
    }

    public function actionAcinpro()
    {
        return $this->render('acinpro');
    }
    // /**
    //  * Displays a single  model.
    //  * @param string $id
    //  * @return mixed
    //  */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    // /**
    //  * 
    //  * If creation is successful, the browser will be redirected to the 'view' page.
    //  * @return mixed
    //  */
    // public function actionCreate()
    // {

    
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
        
    // }

    // /**
    //  * Updates an existing  model.
    //  * If update is successful, the browser will be redirected to the 'view' page.
    //  * @param string $id
    //  * @return mixed
    //  */
    // public function actionUpdate($id)
    // {
        
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
        
    // }

    // /**
    //  * Deletes an existing Carteleravirtual model.
    //  * If deletion is successful, the browser will be redirected to the 'index' page.
    //  * @param string $id
    //  * @return mixed
    //  */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    // /**
    //  * Finds the Carteleravirtual model based on its primary key value.
    //  * If the model is not found, a 404 HTTP exception will be thrown.
    //  * @param string $id
    //  * @return Carteleravirtual the loaded model
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    // protected function findModel()
    // {
    //     // if (($model = Carteleravirtual::findOne($id)) !== null) {
    //     //     return $model;
    //     // } else {
    //     //     throw new NotFoundHttpException('The requested page does not exist.');
    //     // }
    // }
}
