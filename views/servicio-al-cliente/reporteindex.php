<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Municipios;
use app\models\Departamentos;



/* @var $this yii\web\View */
/* @var $model app\models\ServicioAlCliente */
/* @var $form yii\widgets\ActiveForm */



if($idsolicitud==139){

    $this->title = 'PQR';
    $retrun="gestionadaspqr";
}else{
    $this->title = 'Solicitud Liquidaciones';
    $retrun="gestionadaslq";
}

?>

<h1></h1>

<div class="card">
    <div class="card-header bg-success" style="color:white">Reporte de <?= Html::encode($this->title) ?></div>
    <div class="card-body">
        <div class="servicio-al-cliente-form">

            <?php $form = ActiveForm::begin([
                'action' => ['excel/reporteservicioalcliente'], 
                'method' => 'post'
            ]); ?>
            

            <!-- CARD DATOS UBICACION -->

            <div class="card">
                <div class="card-header bg-success" style="color: white">Ubicacion</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                        <?= Html::label('Departamentos','select-departamentos',['class'=>'control-label']) ?>
                            <?= Html::dropdownList(
                                'select-departamentos',
                                '',
                                ArrayHelper::map(Departamentos::find()->orderBy(['nombre'=>'asc'])->all(),'idDepartamento','nombre'),
                                [
                                    'class'=>'form-control a',
                                    'prompt'=>'[-- Seleccione el Departamento --]',
                                    'value'=>'',
                                    'id'=> 'select-departamentos',
                                //     'onchange'=>'$.post("index.php?r=servicio-al-cliente/lists&id='.'"+$(this).val(), function(data){
                                //         $("#municipios").html(data);
                                // })'
                                ]
                            ) ?>
                        </div>
                        
                        <div class="col-lg-6">
                            <?= Html::label('Municipios', 'municipios', ['class'=>'control-label']) ?>
                            <?= Html::dropDownList(
                                'municipios',
                                '',
                                // ArrayHelper::map(Municipios::find()->all(), 'idMunicipio', 'nombre'),
                                [],
                                [
                                    'class'=>'form-control a',
                                    'prompt'=>'[-- Seleccione Municipios --]',
                                    'value'=>'',
                                    'id'=>'municipios',
                                ]) 
                            ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <input type="hidden" name="idsolicitud" value="<?=$idsolicitud ?>">
                <?= Html::submitButton('Generar Reporte', ['class' => 'btn btn-success btn btn-primary','id'=>'enviar','style'=>'margin-top:10px']) ?>
                <?php echo html::a('Volver',['servicio-al-cliente/'.$retrun,'idsolicitud' => $idsolicitud,'estado'=>1],['class' => ' btn btn-info','style'=>'margin-top: 10px']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    
</div>

<script>




    $('#select-departamentos').change(function(){
        var id=$(this).val();

        $.ajax({
            type: "get",
            url: "index.php?r=servicio-al-cliente/lists",
            data:{
                    'id':id
                },
            success: function(data){
                $("#municipios").html(data);
            }
        });
    });




</script>