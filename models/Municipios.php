<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "municipios".
 *
 * @property integer $idMunicipio
 * @property string $nombre
 * @property integer $esCapital
 * @property integer $codigo
 * @property string $fechaCreacion
 * @property integer $departamento_iddepartamento
 * @property integer $sucursales_idsucursal
 *
 * @property Departamentos $departamentoIddepartamento
 */
class Municipios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'municipios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'esCapital', 'departamento_iddepartamento'], 'required'],
            [['esCapital', 'codigo', 'departamento_iddepartamento', 'sucursales_idsucursal'], 'integer'],
            [['fechaCreacion'], 'safe'],
            [['nombre'], 'string', 'max' => 200],
            [['departamento_iddepartamento'], 'exist', 'skipOnError' => true, 'targetClass' => Departamentos::className(), 'targetAttribute' => ['departamento_iddepartamento' => 'idDepartamento']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMunicipio' => 'Id Municipio',
            'nombre' => 'Nombre',
            'esCapital' => 'Es Capital',
            'codigo' => 'Codigo',
            'fechaCreacion' => 'Fecha Creacion',
            'departamento_iddepartamento' => 'Departamento Iddepartamento',
            'sucursales_idsucursal' => 'Sucursales Idsucursal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamentoIddepartamento()
    {
        return $this->hasOne(Departamentos::className(), ['idDepartamento' => 'departamento_iddepartamento']);
    }
}
