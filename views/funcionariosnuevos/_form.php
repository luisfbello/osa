<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Tiposdetalles;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Funcionariosnuevos */
/* @var $form yii\widgets\ActiveForm */



?>

<div class="funcionariosnuevos-form">
    
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-12">
        <!-- begin datos Personales -->
        <div class="card">  
            <div class="cards-header alert alert-primary"><h3>Datos Personales</h3></div>
            <div class="card-body">
                <div class="row">
                <div class="col-lg-6"><?= $form->field($model, 'funcionarios_nuevos_nombre')->textInput(['maxlength' => true]) ?></div>
                <div class="col-lg-6"><?= $form->field($model, 'funcionarios_nuevos_apellidos')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'funcionarios_nuevos_tipodocumento')
                                ->dropDownList(ArrayHelper::map(TiposDetalles::find()
                                                ->where(['tipo_idtipo' => 3])
                                                ->all(), 'idTipoDetalle', 'nombre'),
                                            [
                                                'class'=>'form-control',
                                                'prompt'=>'-Seleccione el  Tipo documento-'
                                                
                                            ]) ?>
                    </div>
                    <div class="col-lg-6"><?= $form->field($model, 'funcionarios_nuevos_documento')->textInput() ?></div>
                    
                </div>
                <div class="row">
                    <div class="col-lg-6"><?= $form->field($model, 'funcionarios_nuevos_direccion')->textInput() ?></div>
                    <div class="col-lg-6"><?= $form->field($model, 'funcionarios_nuevos_telefono')->textInput(['type'=>'number']) ?></div>
                </div>
                <div class="row">
                    <div class="col-lg-6"><?= $form->field($model, 'funcionarios_nuevos_email')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-lg-6"><?= $form->field($model, 'funcionarios_nuevos_aplicacion')
                                                    ->dropDownList(ArrayHelper::map(TiposDetalles::find()
                                                    ->where(['tipo_idtipo' => 7])
                                                    ->all(), 'nombre', 'nombre'),
                                                [
                                                    'multiple'=>true,
                                                    'class'=>'form-control',
                                                    'prompt'=>'-Seleccione la sucursal-',
                                                    'required'=>'required'
                                                    
                                                ]) ?>
                </div>
            </div>

        </div>

        <div class="card">
            <div class="cards-header alert alert-primary"><h3>informacion laboral</h3></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'funcionarios_nuevos_tipocontrato')
                        ->dropDownList(ArrayHelper::map(TiposDetalles::find()
                                        ->where(['tipo_idtipo' => 4])
                                        ->all(), 'idTipoDetalle', 'nombre'),
                                    [
                                        'class'=>'form-control',
                                        'prompt'=>'-Seleccione el  Tipo de contrato-',
                                        'value'=>'',
                                        'required'=>'required'
                                    ]) ?>
                    </div>
                    <div class="col-lg-6">
                    <?php // usage without model
                        echo '<label>Fecha Inicio Contrato</label>';
                        echo DatePicker::widget([
                            'model' => $model,
                            'attribute' => 'funcionarios_nuevos_fechainiciocontrato',
                            'options' => 
                            [
                                'placeholder' => 'Digite la Fecha',
                                'required' => 'required',
                            ],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                    ]);?>
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                    <?php // usage without model
                        echo '<label>Fecha Fin Contrato</label>';
                        echo DatePicker::widget([
                            'model' => $model,
                            'attribute' => 'funcionarios_nuevos_fechafincontrato',
                            'options' => 
                            [
                                'placeholder' => 'Digite la Fecha',
                                'required' => 'required',
                            ],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                    ]);?>

                    
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'funcionarios_nuevos_cargo')
                            ->dropDownList(ArrayHelper::map(TiposDetalles::find()
                                            ->where(['tipo_idtipo' => 5])
                                            ->andWhere(['activo'=>1])
                                            ->all(), 'idTipoDetalle', 'nombre'),
                                        [
                                            'class'=>'form-control',
                                            'prompt'=>'-Seleccione Cargo-',
                                            'value'=>'',
                                            'required'=>'required'
                                        ]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'funcionarios_nuevos_sucursal')
                                        ->dropDownList(ArrayHelper::map(TiposDetalles::find()
                                        ->where(['tipo_idtipo' => 6])
                                        ->all(), 'idTipoDetalle', 'nombre'),
                                    [
                                        'class'=>'form-control',
                                        'prompt'=>'-Seleccione la sucursal-',
                                        'value'=>'',
                                        'required'=>'required'
                                    ]) ?>
                    </div>

                </div>

            </div>
        </div>

        <div class="card">
            <div class="cards-header alert alert-primary"><h3>Permisos</h3></div>
            <div class="card-body">
                <table class="table table-bordered">
                    <div class="col-lg-6">                 
                            <!-- Permisos Recaudo -->
                        <?= $form->field($model, 'funcionarios_nuevos_permisosrecaudo')->checkBoxList(ArrayHelper::map(TiposDetalles::find()
                                                                                                    ->where(['tipo_idtipo'=>8])
                                                                                                    ->all(), 'nombre','nombre'),
                                                                                                    [
                                                                                                        'class'=>'col-lg-6',
                                                                                                        'style'=>['margin:auto;']
                                                                                                        
                                                                                                    ]) ?>
                    
                        
                    </div>
                    <div class="col-lg-6">
                        
                        <!-- Permisos visita -->
                        <?= $form->field($model, 'funcionarios_nuevos_permisosvisita')->checkBoxList(ArrayHelper::map(TiposDetalles::find()
                                                                                        ->where(['tipo_idtipo'=>9])
                                                                                        ->all(), 'nombre','nombre'),
                                                                                        [
                                                                                            'class'=>'col-lg-6',
                                                                                            'style'=>['margin:auto']
                                                                                        ]) ?>
                        
                    </div>
                    <div class="col-lg-6">
                            <!-- Permisos Concertacion -->
                        <?= $form->field($model, 'funcionarios_nuevos_permisosconcertacion')->checkBoxList(ArrayHelper::map(TiposDetalles::find()
                                                                                        ->where(['tipo_idtipo'=>10])
                                                                                        ->all(), 'nombre','nombre'),
                                                                                        [
                                                                                            'class'=>'col-lg-4',
                                                                                            'style'=>['margin:auto;
                                                                                            ']
                                                                                        ]) ?> 
                    </div>
                    <div class="col-lg-6"> 
                            <!-- Permisos Documentos -->
                        <?= $form->field($model, 'funcionarios_nuevos_permisosdocumentos')->checkBoxList(ArrayHelper::map(TiposDetalles::find()
                                                                                        ->where(['tipo_idtipo'=>11])
                                                                                        ->all(), 'nombre','nombre'),
                                                                                        [
                                                                                            'class'=>'col-lg-5',
                                                                                            'style'=>['margin:auto']
                                                                                        ]) ?>
                    </div>
                    <div class="col-lg-6">
                            <!-- Permisos Contabilidad -->
                                <?= $form->field($model, 'funcionarios_nuevos_permisoscontabilidad')->checkBoxList(ArrayHelper::map(TiposDetalles::find()
                                                                                                ->where(['tipo_idtipo'=>12])
                                                                                                ->all(), 'nombre','nombre'),
                                                                                                [
                                                                                                    'class'=>'col-lg-4',
                                                                                                    'style'=>['margin:auto']
                                                                                                ]) ?>
                    </div>
                    <div class="col-lg-6">
                            <!-- Permisos Reportes -->
                        <?= $form->field($model, 'funcionarios_nuevos_permisosreportes')->checkBoxList(ArrayHelper::map(TiposDetalles::find()
                                                                                        ->where(['tipo_idtipo'=>13])
                                                                                        ->all(), 'nombre','nombre'),
                                                                                        [
                                                                                            'class'=>'col-lg-4',
                                                                                            'style'=>['margin:auto']
                                                                                        ]) ?>
                    </div>
                    
                </table>



            </div>
            <div class="cards-header alert alert-primary"><h3>Adicionales</h3></div>
            <div class="card-body">
            <div class="col-lg-6">
                    <?= $form->field($model, 'funcionarios_nuevos_quiensolicita')->textInput(['maxlength' => true]) ?>
                    </div>
                    <?php 
                if(Yii::$app->user->isGuest){
                  //not logged user
                }else{
                    
                        if(yii::$app->user->identity->id==112){
                    ?>
                            <div class='col-lg-6'>
                            <?= $form->field($model, 'funcionarios_nuevos_observacion')->textArea(['rows'=> 5,'cols'=> 60]) ?>
                            </div>
                        
                    <?php } ?>
                    <?php   }
              ?>
            </div>
        </div>    
    </div>
        

    
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(document).ready(function(
        $('').css('border','solid');
    ));

</script>