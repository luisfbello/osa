<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Registronegocios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="registronegocios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'registro_negocio_fecha')->textInput() ?>

    <?= $form->field($model, 'registro_negocio_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_apellido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_identificacion')->textInput() ?>

    <?= $form->field($model, 'registro_negocio_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_telefono1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_telefono2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_nombre_establecimiento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_zona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_nit_establecimiento')->textInput() ?>

    <?= $form->field($model, 'registro_negocio_departamento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_ciudad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_codigo_usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_liquidado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_se_agenda_visita')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_observacion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'registro_negocio_dias_sin_respuesta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_pago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registro_negocio_file1')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
