<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegistronegociosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitud de Registros Nuevos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registronegocios-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
   
    <div class="card " >
        <div class="card-header alert alert-primary"> <h4>Listado de Solicitudes Establecimientos Nuevos</h4>
        <?php 
                $form= ActiveForm::begin([
                    'action' => ['excel/registronuevos'],
                    'method' => 'post',
                    'id' => 'registronuevo',
                ]);
            ?>
            <img id="btn-registronuevo" src="images/excel.png" alt="" title="Reporte Excel" style="cursor:pointer;margin-left: 20px;">
            <input type="hidden" name="registronuevo" value="<?php echo htmlentities(serialize($dataProvider))?>">
            <?php ActiveForm::end()?> 
        </div>

        <div class="card-body">
        
            <div class="table-responsive">
                <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'  ],
                            // 'registro_negocio_id',
                            'registro_negocio_fecha',
                            'registro_negocio_nombre',
                            'registro_negocio_apellido',
                            'registro_negocio_identificacion',
                            'registro_negocio_email:email',
                            'registro_negocio_telefono1',
                            'registro_negocio_telefono2',
                            'registro_negocio_nombre_establecimiento',
                            'registro_negocio_nit_establecimiento',
                            'registro_negocio_zona',
                            // 'registro_negocio_departamento',
                            [
                                'attribute' => 'departamento_nombre',
                                'label' => 'Departamento',
                                'value' => 'departamentos.nombre'
                            ],
                            [
                                'attribute' => 'ciudad_nombre',
                                'label' => 'Municipio',
                                'value' => 'municipios.nombre'
                            ],
                            // 'registro_negocio_ciudad',
                            // 'registro_negocio_codigo_usuario',
                            // 'registro_negocio_liquidado',
                            // 'registro_negocio_se_agenda_visita',
                            // 'registro_negocio_observacion:ntext',
                            // 'registro_negocio_dias_sin_respuesta',
                            // 'registro_negocio_pago',
                            // 'registro_negocio_file1',
                            // 'file1',
                            // 'registro_negocio_file2',
                            [
                                'label' => 'ARCHIVO 1',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return "<a href='https://saycoacinpro.org/OsaIntranet/web/formularioregistro/$data->file1' target='_blanck' >Documento No. 1</a>" ;
                                    
                                },
                            ],

                            [
                                'label' => 'ARCHIVO 2',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return "<a href='https://saycoacinpro.org/OsaIntranet/web/formularioregistro/$data->registro_negocio_file2' target='_blanck' >Documento No. 2</a>" ;
                                    
                                },
                            ], [
                                'label' => 'ARCHIVO 3',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return "<a href='https://saycoacinpro.org/OsaIntranet/web/formularioregistro/$data->registro_negocio_file3' target='_blanck' >Documento No. 2</a>" ;
                                    
                                },
                            ],
                            ['class' => 'yii\grid\ActionColumn'],


                        ],
                        'pager' => [

                            'class' => 'my\custom\Pager',
                
                            //other pager config if nesessary
                
                        ],
                    ]); ?>
            </div>            
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#btn-registronuevo').click(function(){
            $('#registronuevo').submit();
        });
    });
</script>
