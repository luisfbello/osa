<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegistronegociosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Registronegocios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registronegocios-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Registronegocios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'registro_negocio_id',
            'registro_negocio_fecha',
            'registro_negocio_nombre',
            'registro_negocio_apellido',
            'registro_negocio_identificacion',
            // 'registro_negocio_email:email',
            // 'registro_negocio_telefono1',
            // 'registro_negocio_telefono2',
            // 'registro_negocio_nombre_establecimiento',
            // 'registro_negocio_zona',
            // 'registro_negocio_nit_establecimiento',
            // 'registro_negocio_departamento',
            // 'registro_negocio_ciudad',
            // 'registro_negocio_codigo_usuario',
            // 'registro_negocio_liquidado',
            // 'registro_negocio_se_agenda_visita',
            // 'registro_negocio_observacion:ntext',
            // 'registro_negocio_dias_sin_respuesta',
            // 'registro_negocio_pago',
            // 'registro_negocio_file1',
            // 'file1',
            // 'registro_negocio_file2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
