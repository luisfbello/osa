<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicioAlClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitud Liquidaciones Gestionadas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-al-cliente-index">

    <div class="card">
            <div class="card-header bg-success text-white">
                <div class="row">
                    <center><h3><?= Html::encode($this->title) ?></h3></center>
                    <?php echo html::a('<img src="images/excel.png" ></img>',['servicio-al-cliente/reporte','idsolicitud' => $idsolicitud],['style'=>'margin-left: auto;']) ?>
                </div>
            </div>
            <div class="card-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'servicio_al_cliente_id',
                        'servicio_al_cliente_nombre_usuario',
                        'servicio_al_cliente_apellidos_usuario',
                        //'servicio_al_cliente_tipoIdentificacion',
                        'servicio_al_cliente_numero_documento',
                        // 'servicio_al_cliente_razonsocial',
                        // 'servicio_al_cliente_municipio',
                        // 'servicio_al_cliente_departamento',
                        // 'servicio_al_cliente_zona',
                        // 'servicio_al_cliente_direccion',
                        // 'servicio_al_cliente_nit',
                        // 'servicio_al_cliente_telefono1',
                        // 'servicio_al_cliente_telefono2',
                        // 'servicio_al_cliente_email:email',
                        // 'servicio_al_cliente_observacion:ntext',
                        // 'servicio_al_cliente_tipo_solicitud',
                        // 'servicio_al_cliente_fecha_solicitud',
                        // 'servicio_al_cliente_estado_solicitud',
                        // 'servicio_al_cliente_operario_gestion',
                        // 'servicio_al_cliente_fecha_gestion',
                        // 'servicio_al_cliente_observacion_gestion:ntext',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'buttons' => [
                                // ====== Button aprobar comprobante
                                    'view' => function ($url, $model) {
                                        
                                        return  '<center><a href="#ModalVer" data-toggle="modal" class="fa fa-eye ver" id="'.$model->servicio_al_cliente_id.'" value="'.$model->servicio_al_cliente_id.'"></a></center>' ;
                                    },
                                    
                                
                            ],
                        ],
                    ],
                ]); ?>

            </div>
            <?php echo html::a('Volver',['servicio-al-cliente/solicitudesliquidacionindex','idsolicitud' => $idsolicitud],['class' => 'btn btn-block btn-info','style'=>'margin-top: 30px']) ?>

    </div>

</div>
<!-- MODAL DE LA VISTA SOLICITUD -->
<div class="modal fade" id="ModalVer">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
    
        <div class="modal-header text-white bg-success">
        <h4 class="modal-title" id="numticket"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        
        <div class="modal-body">
                <div class="contenido">

                </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>


</div>

<script>
   

    $('.ver').click(function(){

        var id=$(this).attr('id');
        $('#numticket').text("Nº TICKET: "+id);

        $.ajax({
                    url: '<?php echo Yii::$app->request->baseUrl ?>/?r=servicio-al-cliente/view',
                    type: 'get',
                    data: {
                        id: id
                    },
                    success: function (data) {
                        $('.contenido').html(data);
                    }
                }); // fin ajax
    
});
</script>