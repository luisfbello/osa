<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;

$url='https://saycoacinpro.org/OsaIntranet/web/formularioregistro/';
$styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '2B75CE'),
        'size'  => 11,
        'name'  => 'Calibri'
    ));

$objPHPExcel = new PHPExcel(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");
	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$objPHPExcel->getActiveSheet()->getStyle("A1:AB1")->getFont()->setBold(true);
   	foreach(range('A','Z') as $columnID) {
   	    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	        ->setAutoSize(true);
   	}
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'FECHA REGISTRO')
	            ->setCellValue('B1', 'NOMBRES')
	            ->setCellValue('C1', 'APELLIDOS')
	            ->setCellValue('D1', 'IDENTIFICACION')
	            ->setCellValue('E1', 'EMAIL')
	            ->setCellValue('F1', 'TELEFONO1')
	            ->setCellValue('G1', 'TELEFONO 2')
	            ->setCellValue('H1', 'NOMBRE DEL ESTABLECIMIENTO')
				->setCellValue('I1', 'NIT')
				->setCellValue('J1', 'ZONA')
				->setCellValue('K1', 'DEPARTAMENTO')
				->setCellValue('L1', 'MUNICIPIO')
				->setCellValue('M1', 'ARCHIVO 1')
				->setCellValue('N1', 'ARCHIVO 2')
				->setCellValue('O1', 'ARCHIVO 3');
	$count = 2;

	

	foreach ($registronuevo as $value) {

		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $value['registro_negocio_fecha'])
		            ->setCellValue('B'.$count, $value['registro_negocio_nombre'])
		            ->setCellValue('C'.$count, $value['registro_negocio_apellido'])
		            ->setCellValue('D'.$count, $value['registro_negocio_identificacion'])
		            ->setCellValue('E'.$count, $value['registro_negocio_email'])
		            ->setCellValue('F'.$count, $value['registro_negocio_telefono1'])
		            ->setCellValue('G'.$count, $value['registro_negocio_telefono2'])
					->setCellValue('H'.$count, $value['registro_negocio_nombre_establecimiento'])
					->setCellValue('I'.$count, $value['registro_negocio_nit_establecimiento'])
		            ->setCellValue('J'.$count, $value['registro_negocio_zona'])
		            ->setCellValue('K'.$count, $value['registro_negocio_departamento'])
					->setCellValue('L'.$count, $value['registro_negocio_ciudad'])
					->setCellValue('M'.$count, $url.$value['file1'])
					->setCellValue('N'.$count, $url.$value['registro_negocio_file2'])
					->setCellValue('O'.$count, $url.$value['registro_negocio_file3']);
			
			$objPHPExcel->setActiveSheetIndex(0)
						->getCell('M'.$count)
						->getHyperlink()
						->setUrl($url.$value['file1']); 
			$objPHPExcel->setActiveSheetIndex(0)
						->getCell('N'.$count)
						->getHyperlink()
						->setUrl($url.$value['registro_negocio_file2']);
			$objPHPExcel->setActiveSheetIndex(0)
						->getCell('O'.$count)
						->getHyperlink()
						->setUrl($url.$value['registro_negocio_file3']);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('M'.$count)->applyFromArray($styleArray);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('N'.$count)->applyFromArray($styleArray);

		$count++;
	}
	$objPHPExcel->getActiveSheet()->setTitle('REPORTE_REGISTROS_NUEVOS');
	$objPHPExcel->setActiveSheetIndex(0);
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="REPORTE_REGISTROS_NUEVOS'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

 ?>