<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rol;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>


    <!-- ============== informacion personal -->
    <div class="panel panel-primary">
      <div class="panel-heading">Informacion personal</div>
      <div class="panel-body">
            <div class="col-lg-12 noPadding">
              <div class="col-lg-6 form-group">
                <?= $form->field($model, "numeroIdentificacion") ?>
              </div>
              <div class="col-lg-6 form-group">
                  <?= $form->field($model, "email")->input("email") ?>   
              </div>
            </div>
            <div class="col-lg-12 noPadding">
              <div class="col-lg-6 form-group">
               <?= $form->field($model, "nombres") ?>   
              </div>
              <div class="col-lg-6 form-group">
               <?= $form->field($model, "apellidos") ?>   
              </div>
            </div>
            <div class="col-lg-12 noPadding">
                <div class="col-lg-6 form-group">
                 <?= $form->field($model, "direccion") ?>   
                </div>

                <div class="col-lg-6 form-group">
                 <?= $form->field($model, "telefono") ?>   
                </div>
            </div>
        </div>
      </div>
    </div>
    <!-- ============== informacion laboral -->
    <div class="panel panel-primary">
      <div class="panel-heading">Informacion laboral</div>
      <div class="panel-body">
            <div class="col-lg-12 noPadding">
              <div class="col-lg-6 form-group">
                <?= $form->field($model, 'username') ?>
              </div>
              <div class="col-lg-6 form-group">
                  <?= $form->field($model, "password")->input("password") ?>   
              </div>
            </div>
            <div class="col-lg-12 noPadding">
                <div class="col-lg-6 form-group">
                    <?= Html::label('Estado usuario', 'activate', ['class'=>'control-label']) ?>
                    <?= Html::activeDropDownList(
                        $model,
                        'activate',
                        [
                            1 => 'Activo',
                            0 => 'Inactivo'
                        ],
                        [
                            'class'=>'form-control',
                            'prompt'=>'[-- Seleccione estado --]',
                            'value'=> '',
                            'required' => 'required'
                        ]) 
                    ?>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- ============== ROLES ASOCIADOS AL PERFIL ============ -->
    <div class="panel panel-primary">
      <div class="panel-heading">Roles Asociados al perfil</div>
      <div class="panel-body">
        <div class="col-lg-12 noPadding">
          <?php $rolModel = explode(',', $model->role); ?>
            <?php foreach ($roles as $r) { ?>
              <?php if (in_array($r->id, $rolModel)) { ?>
                <button type="button" class="btn btn-success addRole select" id="<?php echo $r->id; ?>">
                  <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php echo $r->nombre; ?>
                </button>
              <?php }else{ ?>
                <button type="button" class="btn btn-primary addRole" id="<?php echo $r->id; ?>">
                  <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php echo $r->nombre; ?>
                </button>
              <?php } ?>
            <?php } ?>
            <?= $form->field($model, "role")->textInput(['readonly'=>'readonly']) ?>   
        </div>
      </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear Usuario' : 'Actualizar Usuario', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<!-- ================ JS =========== -->
<script>
  $(document).ready(function(){
    $(".addRole").click(function(){
      if ($(this).hasClass('select')) {
        $(this).removeClass('select');
        $(this).addClass('btn-primary');
        $(this).removeClass('btn-success');
      }else{
        $(this).addClass('btn-success');
        $(this).addClass('select')
        $(this).removeClass('btn-primary');
      }
      validateRoleActive();
    });
  })
  // =============== FUNCIONES ==================
  function validateRoleActive(){
    var concatRole = '';
    var init = false;
    $(".addRole").each(function(){
      console.log($(this).attr('class'));
      if ($(this).hasClass('select')) {
        if (init) {
          concatRole = concatRole + ',' + $(this).attr('id');
        }else{
          concatRole = $(this).attr('id');
          init = true;
        }
      }
    });
    $("#users-role").val(concatRole);
  }
</script>